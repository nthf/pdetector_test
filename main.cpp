#include "feature_analyzer.h"
#include "formatting.h"
#include "gui.h"
#include "scalespace_analyzer.h"

#include <iostream>
#include <X11/Xlib.h>

const std::string version = "0.0";

ui_selection<cv::Mat> mode_choice() {
	cv::Mat hue_strip = cv::imread("../assets/hue.png");
	cv::resize(hue_strip, hue_strip, cv::Size(110, 7));

	ui_selection uis(3, 3, "Perimeter Detector", {hue_strip});

	while (uis.mode() == M_NONE) {
		if (!uis.gui_loop()) {
			return uis;
		}
	}

	return uis;
}

enum ret_codes {
	R_UNSET = -1,
	R_GOOD = 0,
	R_UI_ERR = 1,
	R_AUTO_ERR = 2,
	R_MANUAL_ERR = 3,
	R_MISSING_FILE = 4
};

detector_status m_manual(scalespace_analyzer& pd, int argc, char** argv) {
	std::cout << colorex(45) << "[manual]\n" << reset << "\n" << std::flush;
	return pd.detector_start(argc, argv);
}

detector_status m_automatic(scalespace_analyzer& pd, ui_selection<cv::Mat>& uis, int argc, char** argv, bool ad_gen = false) {
	detector_status ret = S_UNSET;
	std::cout << colorex(45) << "[automatic" << (ad_gen ? ":adgen" : "") << "]\n"
	          << lgreen << "\tcontent directory found > attempting to read table"
	          << reset << "\n" << std::flush;
	// add picking medialog
	// add picking content folder
	// change this so it doesn't work only with scalespace detector
	// no need for automatic, really? maybe include a way to define medialog structure
	if (!std::filesystem::exists(paths::CONTENT_DIR)) {
		return S_FILE_NOT_FOUND;
	}
	std::unique_ptr<content_table> table = content_table_loader::load_xls(paths::CONTENT_DIR);
	if (table && !table->empty()) {
		std::cout << green << "table read, processing content files" << reset << std::flush;
		std::filesystem::path path_to_center;
		std::filesystem::path path_to_formatted =
		        content_formatting::format(table->unique_files(), path_to_center);

		if (ad_gen) {
			std::cout << "generating single advertisement file\n" << std::flush;
			video_glue::glue_table_videos(table.get(), 30);
			return S_SUCCESS;
		}

		std::unique_ptr<advertisement_bank> bank =
		        std::make_unique<advertisement_bank>();

		std::cout << lyellow << "added content: " << underlined << table->rows()
		          << reset << "\n\n" << std::flush;
		std::cout << colorex(202) << "preparing content for use\n" << reset << std::flush;

		advertisement_bank::generate_advertisement_bank(bank, table,
		                                                path_to_formatted);

		advertisement center;
		if (!path_to_center.empty()) {
			center = advertisement(path_to_center, path_to_center.filename());
			bank->add(std::make_unique<advertisement>(center));
			table->add_to_unique(path_to_center.string(), 0);
		}

		std::unique_ptr<content_controller> cc =
		        std::make_unique<content_controller>(
		            std::move(table), std::move(bank),
		            30, nullptr);

		pd.add_content_controller(std::move(cc));
		ret = pd.detector_start(argc, argv);
	} else {
		std::cout << lred << "table invalid, starting in manual mode\n" << reset << std::flush;
		uis.mode(M_MANUAL_FROM_FILES);
	}
	return ret;
}

//detector_status m_playground(uchar bad_hue) {
//	std::cout << colorex(45) << "[playground]\n" << reset << std::flush;
//	(void)bad_hue;
//	return detector_status::S_UNSET;
//}

detector_status m_combined(uchar bad_hue, bool prev) {
	std::cout << colorex(45) << "[combined]\n" << reset << std::flush;
	feature_analyzer fa;
	return static_cast<detector_status>(fa.analyze(bad_hue, prev));
}

detector_status m_combined(uchar bad_hue) {
	std::cout << colorex(45) << "[combined]\n" << reset << std::flush;
	feature_analyzer fa;
	return static_cast<detector_status>(fa.analyze(bad_hue));
}

detector_status m_gt_gen() {
	cv::VideoCapture cap_a;
	cv::VideoCapture cap_a_future;
	cv::VideoCapture cap_b;

	open_file_name ofn;
	if (!ofn.open_file()) return S_FILE_NOT_OPEN;
	cap_a.open(ofn.path());
	cap_a_future.open(ofn.path());
	std::cout << ofn.path();

	if (!ofn.open_file()) return S_FILE_NOT_OPEN;
	cap_b.open(ofn.path());
	std::cout << ofn.path();

	cap_a_future.set(cv::CAP_PROP_POS_FRAMES, 29);

	cv::Mat frame_a;
	cv::Mat frame_a_future;
	cv::Mat frame_b;

	std::ofstream fstr("../results/gen_gt.txt");

	int fnum = 0;
	int pos = 0;

	while (true) {
		++fnum;
		cap_a >> frame_a;
		cap_a_future >> frame_a_future;
		cap_b >> frame_b;

		if (frame_a.empty() || frame_b.empty()) {
			break;
		}

		cv::resize(frame_a, frame_a, cv::Size(0, 0), 0.5, 0.5);
		if (!frame_a_future.empty()) {
			cv::resize(frame_a_future, frame_a_future, cv::Size(0, 0), 0.5, 0.5);
		}
		cv::resize(frame_b, frame_b, cv::Size(0, 0), 0.5, 0.5);

		show_im("A", frame_a);
		if (!frame_a_future.empty()) {
			show_im("Af", frame_a_future);
		}
		show_im("B", frame_b);

		auto key = key_wait(0);
		switch (key) {
		case 'f':
			fstr << fnum << "=!1" << "\n";
			break;
		case 'g':
			fstr << fnum << "=!0" << "\n";
			break;
		case 'j':
			fstr << fnum << "=?1" << "\n";
			break;
		case 'k':
			fstr << fnum << "=?0" << "\n";
			break;
		case 'v':
			fstr << fnum << "=<1" << "\n";
			break;
		case 'b':
			fstr << fnum << "=<0" << "\n";
			break;
		case 'm':
			fstr << fnum << "=_1" << "\n";
			break;
		case ',':
			fstr << fnum << "=_0" << "\n";
			break;
		case 'x':
			fstr << "X";
			pos = static_cast<int>(cap_a.get(cv::CAP_PROP_POS_FRAMES));
			cap_a.set(cv::CAP_PROP_POS_FRAMES, pos - 31);
			cap_b.set(cv::CAP_PROP_POS_FRAMES, pos - 31);
			pos = static_cast<int>(cap_a_future.get(cv::CAP_PROP_POS_FRAMES));
			cap_a_future.set(cv::CAP_PROP_POS_FRAMES, pos - 31);
			fnum -= 31;
			break;
		}
	}
	return S_SUCCESS;
}

int run_in_mode(scalespace_analyzer& pd, modes mode, int ehue, const std::string& prev_file, bool prev, int argc, char** argv) {
	(void)pd;
	(void)argc;
	(void)argv;
	std::ofstream pf(prev_file);
	pf << mode;
	pf.close();
	detector_status ret = S_UNSET;
	if (mode == M_GEN_AD_FILE_FROM_TABLE) {
		std::cerr << "not supported" << std::endl;
		//ret = m_automatic(pd, uis, argc, argv, true);
		return static_cast<int>(ret);
	}
	cv::destroyAllWindows();
	if (mode == M_AUTOMATIC_FROM_TABLE) {
		std::cerr << "not supported" << std::endl;
		//ret = m_automatic(pd, uis, argc, argv);
	}
	if (mode == M_MANUAL_FROM_FILES) {
		ret = m_combined(static_cast<uint>(ehue * 0.5));
		//ret = m_manual(pd, argc, argv);
	}
	if (mode == M_GT_GENERATION) {
		//ret = m_playground(static_cast<uint>(uis.chosen_hue() * 0.5));
		ret = m_gt_gen();
	}
	if (mode == M_COMBINED) {
		ret = m_combined(static_cast<uint>(ehue * 0.5), prev);
	}
	return static_cast<int>(ret);
}

void read_prev(const std::string& path, modes* m) {
	std::ifstream pf(path);
	std::string temp;
	std::getline(pf, temp);
	*m = static_cast<modes>(std::stoi(temp));
}

int main(int argc, char** argv) {
	srand(4);
	// cv::setBreakOnError(true);
	std::cout << "C++:\n" << __cplusplus << std::flush;
	std::string prev_file = "../data/prev_mode.txt";
	modes prev_mode = M_NONE;

	XInitThreads();

	ui_selection uis = mode_choice();
	//cv::destroyAllWindows();
	if (uis.mode() == M_ERR) {
		return R_UI_ERR;
	}
	if (uis.mode() == M_PREVIOUS) {
		if (std::filesystem::exists(prev_file)) {
			std::ifstream pf(prev_file);
			std::string temp;
			std::getline(pf, temp);
			prev_mode = static_cast<modes>(std::stoi(temp));
			uis.mode(prev_mode);
		}
	}

	if (!uis.colors()) {
		ansi_color::disable();
	}

	std::cout << pink << "perimeter detector v\n" << version
	          << lblue << "--------------------\n\n" << reset
	          << std::flush;

	scalespace_analyzer pd;
	if (uis.exclude()) {
		pd.set_exclusion(uis.chosen_hue());
	}
	int ret = 0;

	bool cycle = true;
	while (cycle) {
		cycle = false;
		switch (uis.mode()) {
		case M_ERR:
			std::cout << " err\n" << std::flush;
			return R_UI_ERR;
		case M_PREVIOUS:
			cycle = true;
			uis.mode(prev_mode);
			break;
		case M_GEN_AD_FILE_FROM_TABLE:
			cycle = true;
		case M_COMBINED:
		case M_AUTOMATIC_FROM_TABLE:
		case M_MANUAL_FROM_FILES:
		case M_GT_GENERATION:
			std::cout << prev_mode << " " << uis.mode() << "\nMODE:" << std::flush;
			run_in_mode(pd, uis.mode(), uis.chosen_hue(), prev_file, prev_mode == uis.mode(), argc, argv);
			break;
		case M_DEL_AUTO_FORMAT_FILES:
		case M_DEL_AUTO_PRECOMP_FILES:
		case M_DEL_AD_FILE:
			uis.repeat_button_select();
		case M_NONE:
			cycle = true;
			break;
		default:
			std::cerr << red << "unknown parameter\n" << reset << std::flush;
			return R_UI_ERR;
		}
		uis.mode(M_NONE);
		uis.gui_loop();
	}

	return ret;
}
