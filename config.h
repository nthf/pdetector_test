#pragma once

/// @brief flag for whether to produce debug output
constexpr bool DEBUG_OUTPUT__ = true;
/// @brief flag for whether rectangles used in rectangular search should be drawn to an image
constexpr bool DRAW_RECTANGLES__ = true;
/// @brief flag for whether images from different steps of the algorithm should be shown
constexpr bool SHOW_IMAGES__ = true;
/// @brief waits for specified amount of ms after processing each frame - to be used when debugging and your think-sponge needs to process what your peepers see
constexpr int DEBUG_DELAY_FOR_EASIER_OBSERVATION_IN_MS__ = 20; // set to 1 when observation not needed
/// @brief set to false when debugging
constexpr bool RUN_MULTITHREAD__ = false;

/// @brief flag for whether the results should be saved to a log file
constexpr bool SAVE_OPS__ = true;
/// @brief flag for whether log output should be written to the file in each loop of detection
constexpr bool LOG_REALTIME__ = false;
/// @brief flag for whether the detector should run in ground truth generation mode
constexpr bool GT_GENERATE__ = false;
/// @brief flag for whether the program should wait for user input in each loop of detection
constexpr bool STEPPING__ = false;

constexpr bool DO_CHECK_THROUGH_DIFFERENCE__ = true;
