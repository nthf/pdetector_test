﻿#pragma once
#include "utility.h"
#include "scalespace_analyzer.h"

#include <atomic>

class feature_analyzer;

class exit_exception : public std::exception {
	virtual const char* what() const throw() {
		return "exitting";
	}
};

enum class loop_status {
	FS_BREAK,
	FS_CONTINUE,
	FS_END
};

union fs_result {
	enum class v {
		TRUE,
		MAYBE_TRUE,
		MAYBE_FALSE,
		ABSOLUTE_FALSE
	} val{v::ABSOLUTE_FALSE};

	operator bool() {
		return val == v::TRUE;
	}

	operator fs_result::v() {
		return val;
	}

	fs_result(v value) : val(value) {}
	fs_result(bool value) {
		if (value) {
			val = v::TRUE;
		} else {
			val = v::ABSOLUTE_FALSE;
		}
	}
	fs_result& operator=(bool value) {
		if (value) {
			val = v::TRUE;
		} else {
			val = v::ABSOLUTE_FALSE;
		}
		return *this;
	}
	fs_result& operator=(fs_result::v value) {
		val = value;
		return *this;
	}
	fs_result& operator=(fs_result value) {
		val = value;
		return *this;
	}
};

class export_item {
	cv::Mat data_;
	std::string name_;
public:
	bool operator<(const export_item& other) const {
		return name_.compare(other.name_) < 0;
	}
	export_item(const cv::Mat& data, const std::string& name) : data_(data), name_(name) {}
	[[nodiscard]] inline std::string get_path() const { return name_; }
	[[nodiscard]] inline cv::Mat get_data() const { return data_; }
};

typedef std::set<export_item> export_collection;

class fs_context {
	enum index {
		IDX_RECORD = 0,
		IDX_ADVERT = 1,
		FILE_COUNT_MAX // used to get number of files, should be at the end, indices start at 0 and increment by 1
	};

	std::vector<std::string> fpath_v{};
	std::vector<cv::Mat> original_img_v{};
	std::vector<cv::Mat> modified_img_v{};
	cv::Ptr<cv::FeatureDetector> fdetector{};
	cv::Ptr<cv::DescriptorMatcher> dmatcher{};
	std::vector<cv::VideoCapture> video_capture_v{};
	cv::Mat result_mat{};
	cv::Mat field_mask{};
	cv::Mat post_lsd_scene{};
	bool is_video{};
	bool histogram_change{};
	uchar excluded_hue{};
	std::ifstream input_filestream{"../results/dispersion.txt"};

	friend feature_analyzer;
	static fs_context initialize_context(bool load_files_from_config = false);

	long frame_n = 0;
	long time = 0;
	unsigned int delta_t = 0;
	unsigned int fps = 0;

	fs_result previous_result{fs_result::v::ABSOLUTE_FALSE};
	std::deque<bool> result_history{};
	double prev_histogram_comp = 0.0;
	double histogram_comp = 0.0;

	bool stop_flag = false;
	bool recalc_flag = true;

	cv::Mat previous_ad_image_for_comp_{};
	std::vector<cv::KeyPoint> scene_keypoints_{};
	std::vector<cv::KeyPoint> object_keypoints_{};
	cv::Mat scene_descriptors_{};
	cv::Mat object_descriptors_{};

public:

	inline void force_recalc() {
		recalc_flag = true;
	}

	// used to force re-calculation on next frame
	inline void undo_history() {
		result_history.pop_back();
		result_history.push_front(false);
		force_recalc();
	}

	inline void backtrack() {
		time = std::max(0l, time - 2 * delta_t);
		frame_n = std::max(0l, frame_n - 2);
		for (size_t i = 0; i < video_capture_v.size(); ++i) {
			// skipping using msec is about 2x faster than using frame pos
			video_capture_v[i].set(cv::CAP_PROP_POS_MSEC, time);
		}
		video_capture_v[IDX_ADVERT].grab();
		video_capture_v[IDX_RECORD].grab();
		undo_history();
	}

	/**
	 * @brief update gets the next frame
	 */
	inline void update() {
		++frame_n;
		time += delta_t;
		for (size_t i = 0; i < video_capture_v.size(); ++i) {
			video_capture_v[i] >> modified_img_v[i];
		}
	}

	/**
	 * @brief any_empty
	 * @return
	 */
	inline bool any_empty() {
		for (const auto& image : modified_img_v) {
			if (image.empty()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @brief fill_collections
	 */
	inline void fill_collections() {
		for (size_t i = 0; i < modified_img_v.size(); ++i) {
			if (original_img_v[i].empty()) {
				original_img_v[i] = cv::Mat::zeros(1, 1, CV_8UC1);
			}
			if (modified_img_v[i].empty()) {
				modified_img_v[i] = cv::Mat::zeros(1, 1, CV_8UC1);
			}
		}
		if (result_mat.empty()) {
			result_mat = cv::Mat::zeros(1, 1, CV_8UC1);
		}
	}

	/**
	 * @brief resize_images
	 * @param resize_factor
	 */
	inline void resize_images(double resize_factor) {
		for (size_t i = 0; i < modified_img_v.size(); ++i) {
			if (modified_img_v[i].empty()) {
				continue;
			}
			cv::resize(modified_img_v[i], modified_img_v[i], cv::Size(0, 0), resize_factor, resize_factor);
			if (i < original_img_v.size()) {
				modified_img_v[i].copyTo(original_img_v[i]);
			}
		}
		cv::resize(result_mat, result_mat, cv::Size(modified_img_v[0].cols, modified_img_v[0].rows));
	}

	/**
	 * @brief pad_images
	 * @param padding
	 */
	inline void pad_images(int padding) {
		for (size_t i = 0; i < modified_img_v.size(); ++i) {
			if (modified_img_v[i].empty()) {
				continue;
			}
			utility::pad_image(modified_img_v[i], modified_img_v[i], padding);
			//if (previous_iteration[i].empty()) {
			//	continue;
			//}
			//utility::pad_image(previous_iteration[i], previous_iteration[i], padding);
		}
	}

	[[nodiscard]] inline std::vector<cv::KeyPoint>& get_obj_kpoints_ref() {
		return object_keypoints_;
	}

	[[nodiscard]] inline cv::Mat& get_obj_desc_ref() {
		return object_descriptors_;
	}

	[[nodiscard]] inline std::vector<cv::KeyPoint>& get_scn_kpoints_ref() {
		return scene_keypoints_;
	}

	[[nodiscard]] inline cv::Mat& get_scn_desc_ref() {
		return scene_descriptors_;
	}

	[[nodiscard]] inline const cv::Mat& get_previous_ad() const {
		return previous_ad_image_for_comp_;
	}

	inline void set_previous_ad(const cv::Mat& ad) {
		ad.copyTo(previous_ad_image_for_comp_);
	}

	inline void export_data(const std::filesystem::path& path = paths::EXPORT_DIR) const {
		// sample plot generation
		// until there's an idea of what to export
		// the input images, fieldmask, ...
		//std::vector<double> x(20*1000), y1(x.size()), y2(x.size()), y3(x.size());
		//std::vector<cv::Vec2d> t(x.size());
		//for (size_t i = 0; i < x.size(); i++) {
		//	x[i] = i * CV_2PI / x.size();
		//	y1[i] = std::sin(x[i]);
		//	y2[i] = y1[i] * std::sin(x[i]*50);
		//	y3[i] = y2[i] * std::sin(x[i]*500);
		//	t[i] = cv::Vec2d(x[i], x[i] * x[i]);
		//}
		//
		//plot p(x, {y1, y2, y3}, {"-r", plot::G | plot::LINE, plot::B | plot::LINE});
		//p.add_plot(t, plot::M | plot::POINT);
		//p.show("plot test");
		// cv::imwrite("path", img);

		export_collection exports;
		// think of a way to generate unique names for the different things
		// append frame n, file format
		std::string frame_n_str = std::to_string(frame_n);
		exports.insert({original_img_v[IDX_RECORD], (path / frame_n_str) += "_input_A.png"});
		exports.insert({original_img_v[IDX_ADVERT], (path / frame_n_str) += "_input_B.png"});
		exports.insert({field_mask,                 (path / frame_n_str) += "_field_mask.png"});

		for (const auto& e : exports) {
			cv::imwrite(e.get_path(), e.get_data());
		}
	}
};

class feature_analyzer {
	friend fs_context;
	/// @brief resize_f image resize factor
	static constexpr double resize_factor = 0.5;
	/// @brief padding image border badding
	static constexpr int padding = 15;
	/// @brief results for later output to file
	static std::vector<result> full_result_history;
	// determines how many frames should pass through histogram check before the costly processing is used again
	static constexpr int reinit_wait_period = 15; // 10-60 is okay - higher is less frequent
	// used for stabilizing the results, sets the size of memory which should help in determining accidentally incorrect results among other, correct ones
	static constexpr int history_size = 15;
	// file streams for debug/logging/saving results
	//std::ofstream os_dispersion_log;
	static std::ofstream ofs_cpu_log;
	static std::ofstream output_filestream_result;
	static std::ifstream ifs_comparison;
	//std::ifstream is_groundtruth;
	static std::ifstream ifs_results_groundtruth;

	// stringbuilder for building output string
	static rope::rope_stringbuilder output_stringbuilder;

	fs_context ctx;
	std::atomic<bool> pauseflag{false};

	/**
	 * @brief class for grouping up points
	 */
	class point_clump {
		/// @brief centroid of the clump
		cv::Point centroid_{-1, -1};
		/// @brief points of the clump
		keypoint_set points_;
		std::vector<keypoint> points_v_;

		int clump_w_{0};
		int clump_h_{0};
		double angle_{};

		struct compare {
			bool operator()(const point_clump& lhs, const point_clump& rhs) const {
				return lhs.clump_area_.area() < rhs.clump_area_.area();
			}
		};

		std::multiset<point_clump, point_clump::compare> subclumps_;
		simple_parallelogram clump_area_;

		[[nodiscard]] inline int get_clump_w() const { return clump_w_; };
		[[nodiscard]] inline int get_clump_h() const { return clump_h_; };
		[[nodiscard]] inline double get_angle() const { return angle_; };

	public:
		inline std::multiset<point_clump, point_clump::compare>& get_subclumps() { return subclumps_; };
		/**
		 * @brief return the number of points in the clump
		 * @return number of points in clump
		 */
		[[nodiscard]] inline auto size() const { return points_.size(); }
		auto begin() { return points_.begin(); }
		[[nodiscard]] auto begin() const { return points_.begin(); }
		auto end() { return points_.end(); }
		[[nodiscard]] auto end() const { return points_.end(); }

		[[nodiscard]] inline bool desc_sc_area_sz() {
			long prev = -1;
			for (const point_clump& sc : subclumps_) {
				double area = sc.clump_area_.area();
				if (area < prev) {
					return false;
				}
				prev = area;
			}
			return true;
		}

	private:
		/**
		 * @brief adds a point to the clump
		 * @param kp point to add
		 */
		inline auto add_point(const keypoint& kp) { return points_.insert(kp); }

		operator cv::Point() { return centroid_; }
		operator cv::Point() const { return centroid_; }
		friend feature_analyzer;

		inline void divide_subclumps(int w, int h, double threshold) {
			std::vector<point_clump> subclumpsv = keypoint_grouping(points_, w, h, AREA_CIRCLE);
			size_t sz_thresh = threshold * std::accumulate(subclumpsv.begin(), subclumpsv.end(), 0, [](size_t a, const point_clump& b) { return std::max(a, b.size()); });
			subclumpsv.erase(std::remove_if(subclumpsv.begin(), subclumpsv.end(), [sz_thresh](const point_clump& clump){ return clump.size() <= sz_thresh; }), subclumpsv.end());
			for (auto& subclump : subclumpsv) {
				std::copy(subclump.points_.begin(), subclump.points_.end(), std::back_inserter(subclump.points_v_));
			}
			subclumps_ = std::multiset<point_clump, point_clump::compare>(subclumpsv.begin(), subclumpsv.end());
		};
	};


	/**
	 * @brief analyse_filtered_clumps
	 * @param output
	 * @param keypoint_areas
	 * @param clumps
	 * @param ret
	 * @param total
	 * @param pre_check_points
	 * @param original_clump_count
	 * @param ellipse_w
	 * @param ellipse_h
	 * @param area_shape
	 */
	void analyze_filtered_clumps(fs_context& ctx, cv::Mat& output, cv::Mat& keypoint_areas, std::vector<point_clump>& clumps, std::vector<cv::Point>& ret,
	                                     size_t total, std::vector<cv::Point>& pre_check_points, size_t original_clump_count, int ellipse_w, int ellipse_h, area_shape shape);

	/**
	 * @brief pca_analysis performs a pca analysis on a given set of points
	 * @param pts    points to analyse
	 * @param img    image to visualize results on
	 * @param center keeping a reference to the center of pca axes
	 */
	simple_mat<cv::Point> pca_analysis(const std::vector<keypoint>& pts, cv::Mat& img, cv::Point& center);

	/**
	 * @brief bad_hue_in_radius_ratio checks a circular area for pixels with bad hue
	 * @param img    image to look in
	 * @param pt     center of circle
	 * @param radius radius of circle
	 * @return ratio of pixels with bad hue / total
	 */
	double bad_hue_in_radius_ratio(cv::Mat& img, const cv::Point& pt, double radius, uchar excluded_hue);

	/**
	 * @brief find_suitable_area analyzes the given points and returns a parallelogram encompassing the area of interest
	 * @param points    points to analyze
	 * @param output    visualization image
	 * @param median_y  median y of given points
	 * @param center    centroid of subclump
	 * @param line_colo color of visualization lines
	 * @param line_thickness how thick the lines should be in visualization
	 * @return parallelogram encompassing an interesting (worth investigating further) subset of given points
	 */
	simple_parallelogram find_suitable_area(fs_context& ctx, std::vector<keypoint>& points, cv::Mat& output, int* median_y, cv::Point& center, const cv::Scalar& line_color, int line_thickness);

	/**
	 * @brief get_points_from_area extracts points that are in a circular area around kpt from pts
	 * @param area_r radius of extraction area
	 * @param kpt    center point
	 * @param pts    collection of points to extract from
	 * @return collection of points that are within area_r of kpt
	 */
	std::vector<cv::KeyPoint> get_points_from_area(int area_r, const cv::KeyPoint& kpt, const std::vector<cv::KeyPoint>& pts);

	/**
	 * @brief finds areas where points are grouped up - within a specified shape of a given size
	 * @param pts  point set
	 * @param w    width of area
	 * @param h    height of area
	 * @param area shape of area
	 * @return distinct clumps of points
	 */
	static std::vector<point_clump> keypoint_grouping(const keypoint_set& pts, double w, double h, area_shape area = AREA_CIRCLE);

	/**
	 * @brief gets three bounding boxes
	 * @todo generalize this, currently hard-coded
	 * @param image image for checking width and height
	 * @param pts   set of points to bound
	 * @return bounding boxes of given points
	 */
	std::vector<bounding_box_t> get_bounding_boxes(cv::Mat& image, std::vector<cv::Point>& pts);

	/**
	 * @brief performs pre-processing (color correction, resizing) on input images
	 * @param img_a input image a
	 * @param img_b input image b
	 * @param cc flag for color correction
	 * @param sharpen flag for sharpening
	 */
	void pre_processing(cv::Mat& img_a, cv::Mat& img_b, bool cc);
	std::vector<cv::Point> find_clumps_of_keypoints(fs_context& ctx, cv::Mat& output, cv::Mat& keypoint_areas, std::vector<point_clump>& clumps, const keypoint_set& pts);

	bool compute_keypoints();
	keypoint_set process_kpoints(int max_len, int w, int h);

	/**
	 * @brief WIP finds keypoints in input images using the provided feature detector and matches them
	 * @todo matching has poor results
	 * @param img_a input image a
	 * @param img_b input image b
	 * @param res         result image
	 * @param detector    one of opencv's feature detectors
	 * @param bad_hue     hue that should be ignored - generally this is the same as the playing field
	 * @param result      pointer to save the result of analysis
	 * @return true if both images had keypoints
	 */
	bool detect_keypoints_analyze(fs_context& ctx);

	/**
	 * @brief determine_if_points_are_good checks the properties of found points and determines if current context could contain a perimeter
	 * @param output     visualization image
	 * @param variance   point variance in x and y directions @see utility::variance
	 * @param dispersion dispersion of points @see utility::dispersion
	 * @param clumps     found clumps of points
	 * @return
	 */
	bool keypoint_validation(cv::Mat& output, const cv::Point3d& variance, const cv::Point3i& dispersion, std::vector<point_clump>& clumps);

	// replaced by fs_context::initialize_context
	//void initialize(bool prev, std::string& path_a, std::string& path_b, std::string& path_center,
	//                 cv::Mat& image_a, cv::Mat& image_b, std::vector<cv::Ptr<cv::Tracker>>& trackers,
	//                 cv::Ptr<cv::FeatureDetector>& detector, cv::Ptr<cv::DescriptorMatcher>& dm);

	/**
	 * @brief stop_at_specified_frames pauses the processes if current frame_n is in list
	 * @param list list of frames to stop at
	 */
	inline void stop_at_specified_frames(fs_context& ctx, const std::vector<int>& list) {
		if (list.empty()) {
			return;
		}
		if (ctx.stop_flag || utility::num_in_list(ctx.frame_n, list)) {
			auto k = key_wait(0);
			ctx.stop_flag = true;
			if (k == ' ') {
				ctx.stop_flag = false;
			}
			rope::rope_stringbuilder name;
			name << ctx.frame_n << "frame_a" << ".png";
			imwrite(name.str(), ctx.modified_img_v[fs_context::IDX_RECORD]);
			name.clear();
			name << ctx.frame_n << "frame_b" << ".png";
			imwrite(name.str(), ctx.modified_img_v[fs_context::IDX_ADVERT]);
		}
	}

	/**
	 * @brief show_fancy_results creates an image to view few steps of the process
	 * @param do_empty if empty images should be replaced by black
	 */
	void show_fancy_results(fs_context& ctx, bool do_empty);

	/**
	 * @brief feature_match
	 */
	size_t match_descriptors_in_subimage(fs_context& ctx, cv::Ptr<cv::FeatureDetector> fdetector, cv::Ptr<cv::DescriptorMatcher> dmatcher);

	/**
	 * @brief points_pre_checks does a few checks with the initial unfiltered collection of points
	 * @param output                    visualization image
	 * @param points                    collection of points
	 * @param median_y                  median y
	 * @param point_map                 image with marked locations of the given points
	 * @param result_points             resultant collection of points
	 * @param original_clump_count      number of clumps in the original collection of points
	 * @param clump_count               number of clumps in the new collection of points
	 * @param original_point_count      number of points in the original collection of points
	 * @param point_count_pre_reduction number of points before reducing the collection
	 * @param pre_check_points          possibly extraneous
	 */
	void secondary_keypoint_analysis(fs_context& ctx, cv::Mat& output, std::vector<keypoint>& points,
	                              std::vector<point_clump>& clumps,
	                              int median_y, cv::Mat& point_map,
	                              std::vector<cv::Point>& result_points,
	                              int original_clump_count, int clump_count,
	                              int original_point_count, int point_count_pre_reduction,
	                              std::vector<cv::Point>& pre_check_points);

	/**
	 * @brief reduce_points reduces the amount of points - currently cuts out those too far from the median y
	 * @param image_height height of image
	 * @param points   original points collection
	 * @param median_y median y
	 */
	void reduce_points(int image_height, std::vector<keypoint>& points, int median_y);

	/**
	 * @brief fm_process_context handles most of the logic, this is where the fun begins
	 * @param st flag for whether the process should be single-threaded
	 * @return code for whether to continue the loop further or not
	 */
	loop_status process_current_frame(fs_context& ctx, bool st, cv::Mat& result_crop, cv::Mat& current_b);

	void perform_lsd(fs_context& ctx);
	void create_field_mask(cv::Mat& src, cv::Mat& fieldmask);
	void connect_lines(fs_context& ctx, cv::Mat& img, std::vector<line_segment>& lines);
	void get_areas_around_lines(fs_context& ctx, cv::Mat& img, const std::vector<line_segment>& lines, double h);
	const line_segment shoot_cone_find_best_point(cv::Point& spotlight, const cv::Vec2i& direction,
	                                            double radius, double start_angle, double angle,
	                                            std::vector<line_segment>& lines, cv::Mat& img, bool visualize_cones = false);
	void paint_lines(cv::Mat& img, const std::vector<line_sequence>& lines_v);

	/**
	 * @brief multithreaded_pass performs calculation using multiple threads
	 */
	void multithreaded_pass(fs_context& ctx, scalespace_analyzer& ssd);

	/**
	 * @brief singlethreaded_pass performs calculation using a single thread [debug]
	 */
	void singlethreaded_pass(fs_context& ctx, scalespace_analyzer& ssd);

	void pause_listener();
	bool pause();

	/**
	 * @brief do_the_scalesauce_dance working title, but dance like the sauce you are, please do
	 */
	bool do_the_scalesauce_dance(scalespace_analyzer& ssd, const cv::Mat& input_a, const cv::Mat& input_b);
public:
	/**
	 * @brief finds features and then performs a scalespace search
	 * @param excluded_hue           hue value that should be ignored in calculations
	 * @param load_files_from_config flag for using previous files
	 * @return     0 on success, error code otherwise
	 */
	int analyze(uchar excluded_hue, bool load_files_from_config = false);

	/**
	 * @brief save_results_and_compare_to_gt saves the recorded results into a text file
	 */
	void save_results_and_compare_to_gt();
};
