#include "rope_stringbuilder.h"

namespace rope {
	rope_stringbuilder::rope_node::rope_node(node_p l, node_p r) : fragment_("") {
		this->left_ = std::move(l);
		this->right_ = std::move(r);
		this->weight_ = this->left_->length();
	}

	rope_stringbuilder::rope_node::rope_node(const char* str) : rope_node(std::string(str)) {}

	rope_stringbuilder::rope_node::rope_node(const rope_node& aNode)
	    : weight_(aNode.weight_), fragment_(aNode.fragment_) {
		rope_node* tmpLeft = aNode.left_.get();
		rope_node* tmpRight = aNode.right_.get();
		if (tmpLeft == nullptr) {
			this->left_ = nullptr;
		} else {
			this->left_ = std::make_unique<rope_node>(*tmpLeft);
		}
		if (tmpRight == nullptr) {
			this->right_ = nullptr;
		} else {
			this->right_ = std::make_unique<rope_node>(*tmpRight);
		}
	}

	bool rope_stringbuilder::rope_node::is_leaf() const {
		return this->left_ == nullptr && this->right_ == nullptr;
	}

	size_t rope_stringbuilder::rope_node::length() const {
		if (this->is_leaf()) {
			return this->weight_;
		}
		size_t tmp = (this->right_ == nullptr) ? 0 : this->right_->length();
		return this->weight_ + tmp;
	}

	char rope_stringbuilder::rope_node::char_by_index(size_t index) const {
		size_t w = this->weight_;
		if (this->is_leaf()) {
			if (index >= this->weight_) {
				throw std::invalid_argument("Error: string index out of bounds");
			}
			return this->fragment_[index];
		}
		if (index < w) {
			return this->left_->char_by_index(index);
		}
		return this->right_->char_by_index(index - w);
	}

	std::string rope_stringbuilder::rope_node::substring(size_t start, size_t len) const {
		size_t w = this->weight_;
		if (this->is_leaf()) {
			if (len < w) {
				return this->fragment_.substr(start, len);
			}
			return this->fragment_;
		}
		if (start < w) {
			std::string lResult =
			        (this->left_ == nullptr) ? "" : this->left_->substring(start, len);
			if ((start + len) > w) {
				size_t tmp = w - start;
				std::string rResult = (this->right_ == nullptr)
				                      ? ""
				                      : this->right_->substring(w, len - tmp);
				return lResult.append(rResult);
			}
			return lResult;
		}
		return (this->right_ == nullptr)
		        ? ""
		        : this->right_->substring(start - w, len);
	}

	std::string rope_stringbuilder::rope_node::to_string() const {
		if (this->is_leaf()) {
			return this->fragment_;
		}
		std::string lResult =
		        (this->left_ == nullptr) ? "" : this->left_->to_string();
		std::string rResult =
		        (this->right_ == nullptr) ? "" : this->right_->to_string();
		return lResult.append(rResult);
	}

	std::pair<std::unique_ptr<rope_stringbuilder::rope_node>, std::unique_ptr<rope_stringbuilder::rope_node>>
	split(std::unique_ptr<rope_stringbuilder::rope_node> node, size_t index) {
		using node_p = std::unique_ptr<rope_stringbuilder::rope_node>;
		size_t w = node->weight_;
		if (node->is_leaf()) {
			return std::pair<node_p, node_p>{
				std::make_unique<rope_stringbuilder::rope_node>(node->fragment_.substr(0, index)),
				        std::make_unique<rope_stringbuilder::rope_node>(
				            node->fragment_.substr(index, w - index))};
		}
		std::unique_ptr<rope_stringbuilder::rope_node> oldRight = move(node->right_);
		if (index < w) {
			node->right_ = nullptr;
			node->weight_ = index;
			std::pair<std::unique_ptr<rope_stringbuilder::rope_node>,
			        std::unique_ptr<rope_stringbuilder::rope_node>>
			        splitLeftResult = split(move(node->left_), index);
			node->left_ = move(splitLeftResult.first);
			return std::pair<node_p, node_p>{
				move(node), std::make_unique<rope_stringbuilder::rope_node>(
				            move(splitLeftResult.second), move(oldRight))};
		}
		if (w < index) {
			std::pair<node_p, node_p> splitRightResult =
			        split(move(oldRight), index - w);
			node->right_ = move(splitRightResult.first);
			return std::pair<node_p, node_p>{move(node), move(splitRightResult.second)};
		}
		return std::pair<node_p, node_p>{move(node->left_), move(oldRight)};
	}

	size_t rope_stringbuilder::rope_node::depth() const {
		if (this->is_leaf()) return 0;
		size_t lResult = (this->left_ == nullptr) ? 0 : this->left_->depth();
		size_t rResult = (this->right_ == nullptr) ? 0 : this->right_->depth();
		return std::max(++lResult, ++rResult);
	}

	void rope_stringbuilder::rope_node::leaves(std::vector<rope_node*>& v) {
		if (this->is_leaf()) {
			v.push_back(this);
		} else {
			rope_node* tmpLeft = this->left_.get();
			if (tmpLeft != nullptr) tmpLeft->leaves(v);
			rope_node* tmpRight = this->right_.get();
			if (tmpRight != nullptr) tmpRight->leaves(v);
		}
	}

	std::invalid_argument ERROR_OOB_ROPE =
	        std::invalid_argument("Error: string index out of bounds");

	rope_stringbuilder::rope_stringbuilder() : rope_stringbuilder("") {}

	rope_stringbuilder::rope_stringbuilder(const rope_stringbuilder& r) {
		rope_node newRoot = rope_node(*r.root_);
		this->root_ = std::make_unique<rope_node>(newRoot);

	}

	std::string rope_stringbuilder::str() const {
		if (this->root_ == nullptr) return "";
		return this->root_->to_string();
	}

	size_t rope_stringbuilder::length() const {
		if (this->root_ == nullptr) return 0;
		return this->root_->length();
	}

	char rope_stringbuilder::at(size_t index) const {
		if (this->root_ == nullptr) throw std::invalid_argument("Error: string index out of bounds");
		return this->root_->char_by_index(index);
	}

	std::string rope_stringbuilder::substring(size_t start, size_t len) const {
		size_t actualLength = this->length();
		if (start > actualLength || (start + len) > actualLength)
			throw std::invalid_argument("Error: string index out of bounds");
		return this->root_->substring(start, len);
	}

	void rope_stringbuilder::insert(size_t i, const std::string& str) {
		this->insert(i, rope_stringbuilder(str));
	}

	void rope_stringbuilder::insert(size_t i, const rope_stringbuilder& r_sb) {
		if (this->length() < i) {
			throw std::invalid_argument("Error: string index out of bounds");
		}
		rope_stringbuilder tmp = rope_stringbuilder(r_sb);
		std::pair<std::unique_ptr<rope_stringbuilder::rope_node>,
		        std::unique_ptr<rope_stringbuilder::rope_node>>
		        origRopeSplit = split(std::move(this->root_), i);
		std::unique_ptr<rope_stringbuilder::rope_node> tmpConcat =
		        std::make_unique<rope_node>(std::move(origRopeSplit.first), std::move(tmp.root_));
		this->root_ = std::make_unique<rope_node>(std::move(tmpConcat),
		                                          std::move(origRopeSplit.second));
	}

	void rope_stringbuilder::rdelete(size_t start, size_t len) {
		size_t actualLength = this->length();
		if (start > actualLength || start + len > actualLength) {
			throw std::invalid_argument("Error: string index out of bounds");
		}
		std::pair<std::unique_ptr<rope_stringbuilder::rope_node>,
		        std::unique_ptr<rope_stringbuilder::rope_node>>
		        firstSplit = split(std::move(this->root_), start);
		std::pair<std::unique_ptr<rope_stringbuilder::rope_node>,
		        std::unique_ptr<rope_stringbuilder::rope_node>>
		        secondSplit = split(std::move(firstSplit.second), len);
		secondSplit.first.reset();
		this->root_ = std::make_unique<rope_node>(std::move(firstSplit.first),
		                                          std::move(secondSplit.second));
	}

	bool rope_stringbuilder::is_balanced() const {
		if (this->root_ == nullptr) return true;
		size_t d = this->root_->depth();
		return this->length() >= fib(d + 2);
	}

	void rope_stringbuilder::balance() {
		if (!this->is_balanced()) {
			std::vector<size_t> intervals = build_fib_list(this->length());
			std::vector<std::unique_ptr<rope_stringbuilder::rope_node>> nodes(intervals.size());

			std::vector<rope_node*> leaves;
			this->root_->leaves(leaves);

			size_t i;
			size_t max_i = intervals.size() - 1;
			size_t current_max_interval = 0;
			std::unique_ptr<rope_stringbuilder::rope_node> acc = nullptr;
			std::unique_ptr<rope_stringbuilder::rope_node> tmp = nullptr;

			for (auto& leaf : leaves) {
				size_t len = leaf->length();
				bool inserted = false;

				if (len > 0) {
					acc = std::make_unique<rope_node>(*leaf);
					i = 0;

					while (!inserted) {
						while (i < max_i && len >= intervals[i + 1]) {
							if (nodes[i].get() != nullptr) {
								tmp = std::make_unique<rope_node>(*nodes[i]);
								acc = std::make_unique<rope_node>(*acc);
								acc = std::make_unique<rope_node>(std::move(tmp), std::move(acc));

								len = acc->length();

								if (len >= intervals[i + 1]) nodes[i] = nullptr;
							}
							i++;
						}

						if (nodes[i].get() == nullptr) {
							nodes[i].swap(acc);
							inserted = true;
							if (i > current_max_interval) current_max_interval = i;
						} else {
							tmp = std::make_unique<rope_node>(*nodes[i]);
							acc = std::make_unique<rope_node>(*acc);
							acc = std::make_unique<rope_node>(std::move(tmp), std::move(acc));

							len = acc->length();

							if (len >= intervals[i + 1]) nodes[i] = nullptr;
						}
					}
				}
			}

			acc = std::move(nodes[current_max_interval]);
			for (int idx = current_max_interval; idx >= 0; --idx) {
				if (nodes[idx] != nullptr) {
					tmp = std::make_unique<rope_node>(*nodes[idx].get());
					acc = std::make_unique<rope_node>(std::move(acc), std::move(tmp));
				}
			}
			this->root_ = std::move(acc);
		}
	}

	rope_stringbuilder& rope_stringbuilder::operator=(const rope_stringbuilder& rhs) {
		if (this == &rhs) return *this;
		this->root_.reset();
		this->root_ = std::make_unique<rope_node>(*(rhs.root_.get()));
		return *this;
	}

	size_t fib(size_t n) {
		int a = 0;
		int b = 1;
		int next;
		if (n == 0) return a;
		for (size_t i = 2; i <= n; i++) {
			next = a + b;
			a = b;
			b = next;
		}
		return b;
	};

	std::vector<size_t> build_fib_list(size_t len) {
		unsigned long a = 0;
		unsigned long b = 1;
		unsigned long next;
		std::vector<size_t> intervals = std::vector<size_t>();
		while (a <= len) {
			if (a > 0) {
				intervals.push_back(b);
			}
			next = a + b;
			a = b;
			b = next;
		}
		return intervals;
	}

	rope_stringbuilder& operator<<(rope_stringbuilder& out, bool v) {
		out.append(std::to_string(v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, char v) {
		out.append(std::string(1, v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, int v) {
		out.append(std::to_string(v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, long v) {
		out.append(std::to_string(v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, long long v) {
		out.append(std::to_string(v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned char v) {
		out.append(std::string(1, v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned int v) {
		out.append(std::to_string(v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned long v) {
		out.append(std::to_string(v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned long long v) {
		out.append(std::to_string(v));
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, float v) {
		char buffer[32];
		snprintf(buffer, sizeof(buffer), "%g", v);

		out.append(buffer);
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, double v) {
		char buffer[32];
		snprintf(buffer, sizeof(buffer), "%g", v);

		out.append(buffer);
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, long double v) {
		char buffer[32];
		snprintf(buffer, sizeof(buffer), "%Lg", v);

		out.append(buffer);
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, const std::string& str) {
		out.append(str);
		return out;
	}
	rope_stringbuilder& operator<<(rope_stringbuilder& out, const char* str) {
		out.append(str);
		return out;
	}
}
