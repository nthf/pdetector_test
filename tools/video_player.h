#include <opencv2/opencv.hpp>
#include <condition_variable>
#include "utility.h"

/**
 * @brief class for reading videos, mostly on a separate thread, at a controlled framerate
 */
class video_player {
private:
	std::vector<cv::VideoCapture> videos_{};
	std::vector<cv::Mat> current_frames_{};
	bool playing_{};
	bool current_fetched_{};
	int current_frame_{};
	int fps_{};
public:
	/**
	 * @brief reads frames of n videos in sequence
	 */
	void play() {
		playing_ = true;
		while (true) {
			auto ts = timer_start();
			for (size_t i = 0; i < videos_.size(); ++i) {
				videos_[i] >> current_frames_[i];
				if (current_frames_[i].empty()) {
					playing_ = false;
					break;
				}
			}
			if (!playing_) {
				break;
			}
			++current_frame_;
			current_fetched_ = false;
			//cv::imshow("test", current_frame_);
			long long t_elapsed = 0;
			timer_end(ts, nullptr, &t_elapsed);
			//cv::imshow("true time c", current_frames_[2]);
			long long diff = (1000 / fps_) - t_elapsed;
			if (diff > 0) {
				cv::waitKey(diff);
			}
		}
	}

	/**
	 * @brief reads frames of n videos in sequence, with support for multithreading
	 * @param m        lock
	 * @param cond_var conditional variable, other threads shall be notified once reading begins
	 * @param ready    flag for the condional variable
	 */
	void play(std::mutex& m, std::condition_variable& cond_var, bool* ready) {
		playing_ = true;
		{
			std::lock_guard<std::mutex> lk(m);
			*ready = true;
		}
		cond_var.notify_one();
		while (true) {
			auto ts = timer_start();
			for (size_t i = 0; i < videos_.size(); ++i) {
				videos_[i] >> current_frames_[i];
				if (current_frames_[i].empty()) {
					playing_ = false;
					break;
				}
			}
			if (!playing_) {
				break;
			}
			++current_frame_;
			current_fetched_ = false;
			//cv::imshow("true time a", current_frames_[0]);
			//cv::imshow("true time b", current_frames_[1]);
			long long t_elapsed = 0;
			timer_end(ts, nullptr, &t_elapsed);
			//cv::imshow("true time c", current_frames_[2]);
			long long diff = std::min(65ll, (1000 / fps_) - t_elapsed);
			if (diff > 0) {
				cv::waitKey(diff);
			}
		}
	}

	/**
	 * @brief check whether video is being read
	 * @return true if video is being read
	 */
	[[nodiscard]] bool playing() const {
		return playing_;
	}

	/**
	 * @brief gets the number of the currently loaded frame
	 * @return number of frames read after calling play()
	 */
	[[nodiscard]] int frame_n() const {
		return current_frame_;
	}

	/**
	 * @brief check for whether the currently loaded frame has been fetched before
	 * @return true if no more frames have been read in the read() method since last fetch_frame() call
	 */
	[[nodiscard]] bool fetched() const {
		return current_fetched_;
	}

	/**
	 * @brief gets the currently loaded frame from the video specified by index
	 * @param index specifies which video should the frame come from
	 * @return the currently loaded frame of the video of given index
	 */
	[[nodiscard]] cv::Mat fetch_frame(int index = 0) {
		current_fetched_ = true;
		return current_frames_[index];
	}

	/**
	 * @brief creates a video player prepared for reading one video file. framerate is set to that of the first video
	 * @param video an instance of cv::VideoCapture
	 */
	video_player(const cv::VideoCapture& video) : videos_({video}), playing_(false) {
		fps_ = static_cast<int>(videos_.front().get(cv::CAP_PROP_FPS));
		videos_.front() >> current_frames_.front();
	}

	/**
	 * @brief creates a video player prepared for reading multiple video files at the same time. framerate is set to that of the first video
	 * @param videos a collection of instances of cv::VideoCapture
	 */
	video_player(const std::vector<cv::VideoCapture>& videos) : videos_(videos), playing_(false) {
		fps_ = static_cast<int>(videos_.front().get(cv::CAP_PROP_FPS));
		current_frames_.resize(videos_.size());
		for (size_t i = 0; i < videos.size(); ++i) {
			videos_[i] >> current_frames_[i];
		}
	}
};
