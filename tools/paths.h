#pragma once

#ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL

#if defined(__cpp_lib_filesystem)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0

#elif defined(__cpp_lib_experimental_filesystem)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1

#elif !defined(__has_include)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1

#elif __has_include(<filesystem>)

#ifdef _MSC_VER

#if __has_include(<yvals_core.h>)
#include <yvals_core.h>

#if defined(_HAS_CXX17) && _HAS_CXX17
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#endif
#endif

#ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1
#endif

#else  // #ifdef _MSC_VER
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#endif

#elif __has_include(<experimental/filesystem>)
#define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1

#else
#error Could not find system header "<filesystem>" or "<experimental/filesystem>"
#endif

#if INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
#include <experimental/filesystem>

namespace std {
	namespace filesystem = experimental::filesystem;
}

#else
#include <filesystem>
#endif

#endif  // #ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL

#include <algorithm>

/// @brief common video formats
const std::vector<std::string> video_extensions = {
    ".avi",  ".flv", ".h264", ".m4v", ".mkv", ".mov", ".mp4",
    ".mpeg", ".mpg", ".rm",   ".swf", ".vob", ".wmv"};
/// @brief image formats supported by OpenCV
const std::vector<std::string> image_extensions = {
    ".bmp", ".dib", ".jpeg", ".jpg", ".jpe", ".jp2",  ".png",
    ".pbm", ".pgm", ".ppm",  ".sr",  ".ras", ".tiff", ".tif"};

/**
 * @brief namespace for constant path formats
 */
namespace paths {
	/// @brief path to content directory
	static const std::filesystem::path CONTENT_DIR = "../content";
	/// @brief path to formatted files directory
	static const std::filesystem::path FORMAT_DIR = CONTENT_DIR / "formatted";
	/// @brief path to backup directory
	static const std::filesystem::path BACKUP_DIR = CONTENT_DIR / "backup_files";

	static const std::filesystem::path EXPORT_DIR = "../data/export";
	static const std::filesystem::path PREV_LOAD_TXT = "../data/prev.txt";
	/// @brief path to random samples directory
	static const std::filesystem::path RAND_SAMPLES_DIR = CONTENT_DIR / "rand_samples";
	/// @brief suffix to add to the name of files that have been formatted
	static const std::string format_suffix = "_f";
	/// @brief preferred video format
	static const std::string preferred_video = ".avi";
	/// @brief preferred image format
	static const std::string preferred_image = ".png";
	/// @brief preferred text format
	static const std::string prefd_processed = ".txt";
	/// @brief substring of centerboard advertisement file name for identification
	static const std::string centerboard = "center";
	/// @brief name of video created by gluing together advertisements from table
	static const std::string combined_video = "table_combined";
	/// @brief path to log directory
	static const std::filesystem::path LOG_DIR = "../logs";
	/// @brief path to ground truth for its generation
	static const std::string ground_truth_path = LOG_DIR / "ground_truth.txt";
	/// @brief path to results for saving
	static const std::string result_path = LOG_DIR / "detector_results.txt";

	/// @brief enum to classify type of file between image, video or unknown
	enum class file_type { IMAGE = 0, VIDEO = 1, UNKNOWN = 2 };
}  // namespace paths

/**
 * @brief determines the type of extension
 * @param extension extension to check
 * @return          type from extension (image, video or unknown)
 */
inline paths::file_type type_from_extension(
        const std::filesystem::path& extension) {
	auto search = std::find(video_extensions.begin(), video_extensions.end(),
	                        extension.string());
	if (search != video_extensions.end()) {
		return paths::file_type::VIDEO;
	}
	search = std::find(image_extensions.begin(), image_extensions.end(),
	                   extension.string());
	if (search != image_extensions.end()) {
		return paths::file_type::IMAGE;
	}
	return paths::file_type::UNKNOWN;
}

/**
 * @brief removes all files in a given directory that satisfy predicate
 * @tparam predicate function that returns bool
 * @param pt         path to directory
 * @param pred       predicate to select files
 */
template <typename predicate>
inline void delete_files(const std::filesystem::path& pt, predicate pred) {
	std::filesystem::create_directory(paths::BACKUP_DIR);
	for (auto& p : std::filesystem::directory_iterator(pt)) {
		if (pred(p.path())) {
			std::filesystem::copy_file(
			            p.path(), (paths::BACKUP_DIR / p.path().filename()).concat(".backup"),
			            std::filesystem::copy_options::overwrite_existing);
			std::filesystem::remove(p.path());
		}
	}
}
