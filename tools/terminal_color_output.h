#pragma once

#include "rope_stringbuilder.h"

/**
 * @brief namespace for internal use
 */
namespace color_output__ {
	/// @brief internal flag for whether colorful output is enabled
	extern bool enabled_ansi_escape_sequences;
}

/// @brief code for foreground colors used for extended color palette
inline constexpr int EXT_FOREGROUND = 38;
/// @brief code for background colors used for extended color palette
inline constexpr int EXT_BACKGROUND = 48;

/**
 * @brief struct for convenient usage of color output codes based on ansi escape sequences
 * @see   https://en.wikipedia.org/wiki/ANSI_escape_code
 */
struct ansi_color {
private:
	/// @brief string for color output including ansi escape sequence
	rope::rope_stringbuilder str_;
	/// @brief code of modifier
	rope::rope_stringbuilder code_;

	/// @brief checks whether ansi escape sequences are allowed, disables them if not
	void check_enable();

	/**
	 * @brief contstructor for creating a combination modifier from two codes
	 * @param string combined string of two modifiers
	 * @param code   combined code of two modifiers
	 */
	ansi_color(const rope::rope_stringbuilder& string, const rope::rope_stringbuilder& code);

public:
	/// @brief sets internal flag for colorful outputs to false (use in case of incompatible terminals)
	static void disable() {
		color_output__::enabled_ansi_escape_sequences = false;
	}
	/**
	 * @brief constructor to create a modifier from its string
	 * @param string string of modifier
	 */
	ansi_color(const std::string& string, int code);

	/**
	 * @brief constructor for creating custom colors (extension may not be
	 *        supported by all terminals)
	 * @param r      red component
	 * @param g      green component
	 * @param b      blue component
	 * @param ground foreground or background code
	 */
	ansi_color(int r, int g, int b, int ground);

	/**
	 * @brief constructor for creating colors from
	 *        extended palette (extension may not be supported by all terminals)
	 * @param code   code of color from extended palette
	 * @param ground foreground or background code
	 */
	ansi_color(int code, int ground);

	/**
	 * @brief explicit conversion of modifier to string
	 */
	operator std::string() { return str_.str(); }

	/**
	 * @brief explicit conversion of modifier to string
	 */
	operator std::string() const { return str_.str(); }

	/**
	 * @brief overload for output to stream
	 * @param stream stream to output to
	 * @return       stream with added data
	 */
	template<typename S>
	S& operator<<(S& stream) const {
		stream << (color_output__::enabled_ansi_escape_sequences ? str_.str() : "");
		return stream;
	}

	/**
	 * @brief overload for output to stream
	 * @param stream stream to output to
	 * @param color  modifier to output
	 * @return       stream with added data
	 */
	template<typename S>
	friend S& operator<<(S& stream,
	                                const ansi_color& color) {
		stream << (color_output__::enabled_ansi_escape_sequences ? color.str_.str() : "");
		return stream;
	}

	/**
	 * @brief overload for combining multiple modifiers
	 * @param lhs first modifier
	 * @param rhs second modifier
	 * @return    combined modifiers
	 */
	friend ansi_color operator+(const ansi_color& lhs,
	                            const ansi_color& rhs);

	/**
	 * @brief combines foreground and background colors, this makes it possible
	 *        to modify the foreground and background colors at the same time
	 * @param lhs first modifier
	 * @param rhs second modifier
	 * @return    combined modifiers
	 */
	static ansi_color combine(const ansi_color& lhs, const ansi_color& rhs);

	/**
	 * @brief combines foreground and background colors, this makes it possible
	 *        to modify the foreground and background colors at the same time
	 * @param other other ansi color code
	 * @return      combined modifiers
	 */
	ansi_color combine(const ansi_color& other) const;

	/**
	 * @brief gets the modifier string
	 * @return modifier string
	 */
	std::string str() const;
};

/**
 * constants for colorful terminal output, make sure to use reset afterwards
 * eg.: std::cout << red << "hello world" << reset << std::"
";
 * l - light
 * b - bold
 */
const ansi_color black("\033[0;30m", 30);
const ansi_color red("\033[0;31m", 31);
const ansi_color green("\033[0;32m", 32);
const ansi_color yellow("\033[0;33m", 33);
const ansi_color blue("\033[0;34m", 34);
const ansi_color magenta("\033[0;35m", 35);
const ansi_color cyan("\033[0;36m", 36);
const ansi_color lgray("\033[0;37m", 37);
const ansi_color dgray("\033[0;90m", 90);
const ansi_color lred("\033[0;91m", 91);
const ansi_color lgreen("\033[0;92m", 92);
const ansi_color lyellow("\033[0;93m", 93);
const ansi_color lblue("\033[0;94m", 94);
const ansi_color lmagenta("\033[0;95m", 95);
const ansi_color lcyan("\033[0;96m", 96);
const ansi_color lwhite("\033[0;97m", 97);

const ansi_color blackbg("\033[0;40m", 40);
const ansi_color redbg("\033[0;41m", 41);
const ansi_color greenbg("\033[0;42m", 42);
const ansi_color yellowbg("\033[0;43m", 43);
const ansi_color bluebg("\033[0;44m", 44);
const ansi_color magentabg("\033[0;45m", 45);
const ansi_color cyanbg("\033[0;46m", 46);
const ansi_color lgraybg("\033[0;47m", 47);
const ansi_color dgraybg("\033[0;100m", 100);
const ansi_color lredbg("\033[0;101m", 101);
const ansi_color lgreenbg("\033[0;102m", 102);
const ansi_color lyellowbg("\033[0;103m", 103);
const ansi_color lbluebg("\033[0;104m", 104);
const ansi_color lmagentabg("\033[0;105m", 105);
const ansi_color lcyanbg("\033[0;106m", 106);
const ansi_color lwhitebg("\033[0;107m", 107);

/**
 * @brief creates a custom foreground color from specified red, green and blue components
 * @see   https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit
 * @param r red component
 * @param g green component
 * @param b blue component
 * @return  struct to use to get colorful terminal output
 */
inline ansi_color rgb(int r, int g, int b) { return ansi_color(r, g, b, EXT_FOREGROUND); }
/**
 * @brief creates a foreground color from the extended color palette
 * @see   https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
 * @param code code of color from the extended color palette
 * @return     struct to use to get colorful terminal output
 */
inline ansi_color colorex(int code) { return ansi_color(code, EXT_FOREGROUND); }
const ansi_color pink = colorex(213);

/**
 * @brief creates a custom background color from specified red, green and blue components
 * @see   https://en.wikipedia.org/wiki/ANSI_escape_code#24-bit
 * @param r red component
 * @param g green component
 * @param b blue component
 * @return  struct to use to get colorful terminal output
 */
inline ansi_color rgbbg(int r, int g, int b) { return ansi_color(r, g, b, EXT_BACKGROUND); }
/**
 * @brief creates a background color from the extended color palette
 * @see   https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
 * @param code code of color from the extended color palette
 * @return     struct to use to get colorful terminal output
 */
inline ansi_color colorexbg(int code) { return ansi_color(code, EXT_BACKGROUND); }
const ansi_color pinkbg = colorexbg(213);

/**
 * modifiers
 */
const ansi_color reset("\033[0m", 0);
const ansi_color bold("\033[1m", 1);
const ansi_color dim("\033[2m", 2);
const ansi_color underlined("\033[4m", 4);
const ansi_color blink("\033[5m", 5);
const ansi_color inversed("\033[7m", 7);
