#pragma once

#include <opencv2/imgproc.hpp>

/**
 * @brief Adaptation from i3d library implementation of infinite impulse response
 *        algorithm for computing gaussian filter, library is available from
 *        https://cbia.fi.muni.cz/software/i3d-library.html
 * @see   [https://cbia.fi.muni.cz/software/i3d-library.html]
 */
namespace fast_gaussian_blur {
	/**
	 * @brief gets the pointer to the last element of mat
	 * @param mat single channel matrix of doubles
	 * @return    pointer to the last element of mat
	 */
	double *back_ptr(cv::Mat1d &mat);
	template <typename T>
	/**
	 * @brief gets the pointer to the last element of mat
	 * @param mat single channel matrix
	 * @return    pointer to the last element of mat
	 */
	inline double *back_ptr(cv::Mat &mat) {
		return mat.ptr<T>(mat.rows - 1) + mat.cols - 1;
	}

	/**
	 * @brief gets magic parameters b through arcane math,
	 *        needed for further com
	 * @param sigma determines shape of the gaussian kernel
	 * @param B     parameter B
	 * @param b1    parameter b1
	 * @param b2    parameter b2
	 * @param b3    parameter b3
	 */
	void compute_b_from_sigma(double sigma, double *B, double *b1, double *b2,
	                          double *b3);
	/**
	 * @brief another slightly magical function,
	 *        needed to compute the gaussian
	 * @param b1 parameter b1
	 * @param b2 parameter b2
	 * @param b3 parameter b3
	 * @param M  vector of doubles to save results in
	 */
	void suggest_iir_boundaries(double b1, double b2, double b3,
	                            std::vector<double> &M);
}

/**
 * @brief performs an IIR gaussian blur on img image with strength sigma
 * @param img   img image
 * @param out   output image, result is saved here
 * @param sigma strength of blur
 */
void gauss_iir(cv::Mat& img, cv::Mat& out, int sigma);
