#pragma once

#include "advertisement.h"
#include "xls_loader.h"

/**
 * @brief The content_controller class for loading the advertisements in
 *        sequence as given by the content table
 */
class content_controller {
	/**
	 * @brief content table, holds data about the advertisements and their
	 *        order of appearance
	 */
	std::unique_ptr<content_table> table_;
	/**
	 * @brief data structure to hold all unique appearances variations of
	 *        the advertisements found in table_
	 */
	std::unique_ptr<advertisement_bank> bank_;

	/**
	 * @brief index of the row of the table that the controller is
	 *        reading from
	 */
	int table_row_index_{};

	/**
	 * @brief flag for when it is a transition period - content change
	 *        on the perimeter
	 */
	bool transition_{false};
	/**
	 * @brief time it takes for one advertisement to fully
	 *        replace another on the perimeter in frames
	 */
	int transition_period_{};
	/**
	 * @brief how far along the advertisement is in the
	 *        transition
	 */
	int current_transition_time_{};

	/// @brief pointer to the current content
	std::unique_ptr<advertisement> current_ = nullptr;
	/// @brief pointer to the previous content
	std::unique_ptr<advertisement> previous_ = nullptr;
	/// @brief pointer to the constant center content
	std::unique_ptr<advertisement> constant_center_ = nullptr;
	/// @brief duration of the current content
	long curr_duration_{};

	/// @brief serial number of the current frame
	int ad_frame_{};
	/// @brief framerate of the current advertisement
	int fps_{0};

public:
	/**
	 * @brief constructor for content controller
	 * @param table             unique pointer to a content table, this controller
	 *                          will take ownership
	 * @param bank              unique pointer to an advertisement bank, this
	 *                          controller will take ownership
	 * @param pv_from           extension of unmodified advertisement video files
	 * @param pv_to             extension of modified advertisement video files
	 * @param pi_from           extension of unmodified advertisement image files
	 * @param pi_to             extension of modified advertisement image files
	 * @param transition_period the time in frames of advertisement change period
	 * @param constant_center   set to true if perimeter contains an advertisement
	 *                          that is always present
	 */
	inline content_controller(std::unique_ptr<content_table> table,
	                          std::unique_ptr<advertisement_bank> bank,
	                          int transition_period,
	                          std::unique_ptr<advertisement> constant_center)
	    : table_(std::move(table)),
	      bank_(std::move(bank)),
	      table_row_index_(404
	                       // 586
	                       ),
	      transition_period_(transition_period),
	      constant_center_(std::move(constant_center)) {}

	/**
	 * @brief gets the next advertisement from the advertisement bank as
	 *        determined by the table
	 */
	inline void grab_ad() {
		std::string tname =
		        (*table_)[table_row_index_][table_indices__::FILE_NAME_INDEX]
		        .get_string();
		int duration =
		        (*table_)[table_row_index_][table_indices__::DURATION_INDEX].get_int();
		std::filesystem::path table_path(tname);
		std::filesystem::path ext = table_path.extension();

		paths::file_type ft = type_from_extension(ext);
		if (ft == paths::file_type::IMAGE) {
			table_path = table_path.stem().concat(paths::format_suffix +
			                                      paths::preferred_image);
		} else if (ft == paths::file_type::VIDEO) {
			table_path = table_path.stem().concat(paths::format_suffix +
			                                      paths::preferred_video);
		} else {
			table_path = table_path.stem().concat(ext.string()).string();
		}

		current_ = std::make_unique<advertisement>((*bank_)(table_path, duration));
		curr_duration_ = current_->duration() * fps_;
		ad_frame_ = 0;
	}

	/**
	 * @brief removes an advertisement from the advertisement bank
	 * @param ad advertisement that is to be removed
	 * @return   unique pointer to the removed advertisement
	 */
	inline std::unique_ptr<advertisement> remove(std::string& ad) {
		return bank_->remove(ad);
	}

	/// @brief moves advertisement forward by a frame and updates all relevant variables
	inline void tick() {
		if (fps_ == 0) {
			std::cerr << "CONTROLLER FPS SET TO 0" << rope::REN;
		}
		--curr_duration_;
		--current_transition_time_;
		if (curr_duration_ <= 0) {
			if (current_) current_->reset();
			if (previous_) previous_->reset();
			previous_ = std::move(current_);
			current_transition_time_ = transition_period_;
			++table_row_index_;
			grab_ad();
			transition_ = true;
		} else if (current_) {
			current_->advance();
			++ad_frame_;
			if (current_transition_time_ <= 0) {
				transition_ = false;
			}
		}
	}

	/**
	 * @brief sets the framerate of the advertisement
	 * @param fps new framerate of the advertisement
	 */
	inline void set_fps(int fps) { fps_ = fps; }

	/**
	 * @brief returns a reference to the currently loaded advertisement
	 * @return reference to the currently loaded advertisement
	 */
	[[nodiscard]] inline const advertisement& ad() const { return *current_; }

	/**
	 * @brief returns a reference to the previously loaded advertisement
	 * @return reference to the previously loaded advertisement
	 */
	[[nodiscard]] inline const advertisement& prev() const { return *previous_; }

	/**
	 * @brief returns true if more than one advertisements have been
	 * loaded
	 * @return true if current advertisement is not the first that has been loaded
	 */
	[[nodiscard]] inline bool has_prev() const { return bool(previous_); }

	/**
	 * @brief returns true if an advertisement has been loaded
	 * @return true if an advertisement has been loaded
	 */
	[[nodiscard]] inline bool has_curr() const { return bool(current_); }

	/**
	 * @brief sets the centerboard advertisement
	 * @param center centerboard advertisement
	 */
	inline void set_center(std::unique_ptr<advertisement> center) {
		constant_center_ = std::move(center);
	}

	/**
	 * @brief check if loader has center ready
	 * @return true if loader owns center
	 */
	[[nodiscard]] inline bool has_center() const { return bool(constant_center_); }

	/**
	 * @brief returns a reference to the advertisement from the
	 * constant part of the perimeter
	 * @return reference to the advertisement from the constant part of the
	 * perimeter
	 */
	[[nodiscard]] inline const advertisement& center() const { return *constant_center_; }

	/**
	 * @brief returns a reference to the advertisement bank
	 * @return reference to the advertisement bank
	 */
	[[nodiscard]] inline const advertisement_bank& bank() const { return *bank_; }

	/**
	 * @brief returns the duration of the currently loaded
	 * advertisement
	 * @return duration of the currently loaded advertisement
	 */
	[[nodiscard]] inline long duration() const { return curr_duration_; }

	/**
	 * @brief returns the current position in the advertisement
	 * @return current position in the advertisement
	 */
	[[nodiscard]] inline int ad_time() const {
		switch (current_->type()) {
		case advertisement::type::image:
			return 0;
		case advertisement::type::video:
			return ad_frame_;
		}
	}

	/**
	 * @brief returns true if transition is currently underway
	 * @return true if transition is currently underway
	 */
	[[nodiscard]] inline bool transition() const { return transition_; }

	/**
	 * @brief returns the remaining time that it takes for
	 *        advertisements to replace the previous one on the perimeter
	 * @return remaining time that advertisements need to replace the
	 *         previous advertisement on the perimeter
	 */
	[[nodiscard]] inline int transition_time() const {
		return transition_period_ - std::max(0, current_transition_time_);
	}

	/**
	 * @brief returns the total time that it takes for
	 *        advertisements to replace the previous one on the perimeter
	 * @return total time that advertisements need to replace the
	 *         previous advertisement on the perimeter
	 */
	[[nodiscard]] inline int total_trans_time() const { return transition_period_; }
};
