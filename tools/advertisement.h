#pragma once

/**
  Header for advertisement container class
*/

#include "xls_loader.h"
#include "utility.h"

/**
 * @brief struct to hold various data about a given advertisement
 */
struct advertisement_context {
private:
	/**
	 * @brief counter of most common hues or saturations from the
	 *        central line of the advertisement's scalespace
	 */
	vpair_vuc_i v_counts_;
	/**
	 * @brief vector that specifies whether the hue (false) or saturation
	 *        (true) channel is used for each frame of the advertisement
	 */
	bool sat_;
	/**
	 * @brief vector with an assigned most common hue for each
	 *        of the frames of the advertisement
	 */
	uchar most_common_hues_;
	/**
	 * @brief vector with the count of unique colors for each of
	 *        the frames of the advertisement
	 */
	size_t unique_colors_;
public:
	/// @brief v_counts getter
	[[nodiscard]] inline vpair_vuc_i v_counts() const { return v_counts_; }
	/**
	 * @brief v_counts setter
	 * @param value new value for v_counts
	 */
	inline void v_counts(const vpair_vuc_i& value) { v_counts_ = value; }
	/// @brief sat getter
	[[nodiscard]] inline bool sat() const { return sat_; }
	/**
	 * @brief sat setter
	 * @param value new value for sat
	 */
	inline void sat(bool value) { sat_ = value; }
	/// @brief most_common_hues getter
	[[nodiscard]] inline uchar most_common_hues() const { return most_common_hues_; }
	/**
	 * @brief most_common_hues setter
	 * @param value new value for most_common_hues
	 */
	inline void most_common_hues(const uchar& value) { most_common_hues_ = value; }
	/// @brief unique_colors getter
	[[nodiscard]] inline size_t unique_colors() const { return unique_colors_; }
	/**
	 * @brief unique_colors setter
	 * @param value new value for unique_colors
	 */
	inline void unique_colors(const size_t& value) { unique_colors_ = value; }

	/**
	 * @brief constructor for advertisement context
	 * @param v_counts         counter of most common hues or saturations from the
	 *                         advertisement's scalespace
	 * @param sat vector       that specifies whether the hue (false) or saturation
	 *                         (true) channel is used for each frame of the advertisement
	 * @param most_common_hues vector with an assigned most common hue for each
	 *                         of the frames of the advertisement
	 * @param unique_colors    vector with the count of unique colors for each of
	 *                         the frames of the advertisement
	 */
	advertisement_context(const vpair_vuc_i& v_counts, bool sat,
	                      uchar most_common_hues, size_t unique_colors)
	    : v_counts_(v_counts),
	      sat_(sat),
	      most_common_hues_(most_common_hues),
	      unique_colors_(unique_colors) {}

	/**
	 * @brief copy constructor
	 * @param other instance of advertisement_context
	 */
	advertisement_context(const advertisement_context& other)
	    : advertisement_context(other.v_counts_, other.sat_,
	                            other.most_common_hues_, other.unique_colors_){}

	~advertisement_context() = default;

	advertisement_context& operator=(const advertisement_context& other) =
	        default;

	advertisement_context(advertisement_context&& other) = default;
	advertisement_context& operator=(advertisement_context&& other) = default;
};

class advertisement;

/**
 * @brief Helper struct name_duration to hold the name and duration of an
 *        advertisement in one place
 */
struct name_duration {
private:
	/// @brief advertisement name
	std::string name_{};
	/// @brief advertisement duration
	int duration_{};
public:

	/**
	 * @brief overload for comparing advertisements based on their
	 *        duration first, name second
	 * @param other name_duration of an advertisement to compare this against
	 * @return      true if this has lower duration than other, or if their durations
	 *              are equal, this.name < other.name evaluates to true
	 */
	inline bool operator<(const name_duration& other) const {
		return name_ == other.name_ ? duration_ < other.duration_ : name_ < other.name_;
	}

	/**
	 * @brief constructor, sets the name and duration
	 * @param name     name of an advertisement
	 * @param duration duration of an advertisement in seconds
	 */
	name_duration(const std::string& name, int duration)
	    : name_(name), duration_(duration) {}

	/**
	 * @brief name getter
	 * @return name
	 */
	[[nodiscard]] std::string name() const { return name_; }
	/**
	 * @brief name setter
	 * @param value new value for name
	 */
	void name(const std::string& value) { name_ = value; }
	/**
	 * @brief duration getter
	 * @return duration
	 */
	[[nodiscard]] int duration() const { return duration_; }
	/**
	 * @brief duration setter
	 * @param value new value for duration
	 */
	void duration(int value) { duration_ = value; }
};

using ad_bank_t = std::map<name_duration, std::unique_ptr<advertisement>>;

/**
 * @brief The advertisement_bank class to hold all unique advertisements for
 *        ease of access
 */
class advertisement_bank {
	/**
	 * @brief bank to hold all advertisements that appear in the content table, in
	 *        the correct format
	 */
	ad_bank_t bank_;
	/**
	 * @brief vector for accessing bank_ through int indexes rather than the names
	 *        of the advertisements
	 */
	std::vector<name_duration> indexer_;
	/**
	 * @brief NOT IMPLEMENTED used to hold all combinations of
	 *        advertisements (a, b) where a appears immediately before b in the content
	 *        table
	 * @todo implement it
	 */
	std::set<std::pair<const std::string, const std::string>> pairings_;

public:
	using iterator = ad_bank_t::iterator;
	using const_iterator = ad_bank_t::const_iterator;

	/**
	 * @brief returns begin iterator of advertisement bank
	 * @return begin iterator of advertisement bank
	 */
	[[nodiscard]] inline iterator begin() {
		return bank_.begin();
	}

	/**
	 * @brief returns const begin iterator of advertisement bank
	 * @return const begin iterator of advertisement bank
	 */
	[[nodiscard]] inline const_iterator	begin() const {
		return bank_.begin();
	}

	/**
	 * @brief returns end iterator of advertisement bank
	 * @return end iterator of advertisement bank
	 */
	[[nodiscard]] inline iterator end() {
		return bank_.end();
	}

	/**
	 * @brief returns const end iterator of advertisement bank
	 * @return const end iterator of advertisement bank
	 */
	[[nodiscard]] inline const_iterator	end() const {
		return bank_.end();
	}

	/**
	 * @brief overload for ease of accessing underlying data structure
	 * @param index index of advertisement to find in underlying data structure
	 * @return      advertisement at index
	 */
	inline advertisement& operator()(int index) {
		return *(bank_.at(indexer_[index]).get());
	}

	/**
	 * @brief overload for ease of accessing underlying data structure
	 *        through a key-value pair of name and duration
	 * @param name     name of requested advertisement
	 * @param duration duration of requested advertisement
	 * @return         advertisement with the same name and duration as was given
	 */
	inline advertisement& operator()(const std::string& name, int duration) {
		return *(bank_.at({name, duration}).get());
	}

	/**
	 * @brief const overload for ease of accessing underlying data
	 *        structure
	 * @param index index of advertisement to find in underlying data structure
	 * @return      advertisement at index
	 */
	const advertisement& operator()(int index) const {
		return *(bank_.at(indexer_[index]).get());
	}

	/**
	 * @brief const overload for ease of accessing underlying data
	 *        structure through a key-value pair of name and duration
	 * @param name     name of requested advertisement
	 * @param duration duration of requested advertisement
	 * @return         advertisement with the same name and duration as was given
	 */
	inline const advertisement& operator()(const std::string& name,
	                                       int duration) const {
		return *(bank_.at({name, duration}).get());
	}

	/**
	 * @brief searches the underlying data structure for any key-value
	 *        pair that has name as its key
	 * @param name key part of searched key-value pair
	 * @return     iterator to the first pair that contains name as its key
	 */
	[[nodiscard]] inline iterator find_any(const std::string& name) {
		for (auto it = bank_.begin(); it != bank_.end(); ++it) {
			if (it->first.name() == name) {
				return it;
			}
		}
		return bank_.end();
	}

	/**
	 * @brief const searches the underlying data structure for any
	 *        key-value pair that has name as its key
	 * @param name key part of searched key-value pair
	 * @return     iterator to the first pair that contains name as its key
	 */
	[[nodiscard]] inline const_iterator find_any(const std::string& name) const {
		for (auto it = bank_.begin(); it != bank_.end(); ++it) {
			if (it->first.name() == name) {
				return it;
			}
		}
		return bank_.end();
	}

	/**
	 * @brief adds an advertisement to the underlying data structures
	 * @param ad advertisement to add to the bank
	 */
	void add(std::unique_ptr<advertisement> ad);
	/**
	 * @brief removes an advertisement from the underlying data structures
	 * @param ad advertisement to remove from the bank
	 * @return   unique pointer to the removed advertisement
	 */
	std::unique_ptr<advertisement> remove(std::string& ad);

	/**
	 * @brief gives the size of the advertisement bank
	 * @return size of the advertisement bank
	 */
	[[nodiscard]] inline size_t size() const { return bank_.size(); }

	/**
	 * @brief populates the bank automatically, taking
	 *        data from given content table
	 * @param bank              unique pointer to an advertisement bank
	 * @param table             unique pointer to a table of advertisements
	 * @param path_to_formatted path to folder where advertisements are stored in
	 * the correct format
	 */
	static void generate_advertisement_bank(
	        const std::unique_ptr<advertisement_bank>& bank,
	        const std::unique_ptr<content_table>& table,
	        std::filesystem::path& path_to_formatted) {
		for (const auto& s : table->unique_files()) {
			std::filesystem::path p = path_to_formatted / s.first;
			bank->add(std::make_unique<advertisement>(p, s.second, s.first));
		}
	}
};

/**
 * @brief exception class thrown when there is an issue with
 *        an advertisement during its initialization
 */
class advertisement_exception : public std::exception {
	/**
	 * @brief error message
	 */
	std::string msg;

public:
	/**
	 * @brief some common states for failure
	 */
	enum class ad_except {
		BAD_INIT,
		NO_IMAGE,
		NO_VIDEO,
	};

	/**
	 * @brief constructor that takes one of the error
	 *        states as enum and sets the appropriate message
	 * @param cause reason for exception
	 */
	advertisement_exception(const ad_except& cause) {
		switch (cause) {
		case ad_except::BAD_INIT:
			msg = "failed to initialize advertisement";
			break;
		case ad_except::NO_IMAGE:
			msg = "no image loaded";
			break;
		case ad_except::NO_VIDEO:
			msg = "no video loaded";
			break;
		}
	}

	/**
	 * @brief constructor that takes any string and makes
	 *        it into the exception message
	 * @param cause string to use as exception message
	 */
	advertisement_exception(const std::string& cause) : msg(cause) {}

	/**
	 * @brief override of std::exception::what() to return message specific
	 *        to advertisements and their error states
	 * @return error message
	 */
	[[nodiscard]] const char* what() const noexcept override { return msg.c_str(); }
};

/**
 * @brief The advertisement class to hold information about each unique
 *        advertisement
 */
class advertisement {
public:
	/**
	 * @brief enum for specifying whether an advertisement is a video or a
	 *        still image
	 */
	enum class type { video, image };

	/**
	 * @brief returns true if advertisement is a video
	 * @return true if advertisement is a video
	 */
	bool is_video() { return t_ == type::video; }

	friend class advertisement_bank;

private:
	/**
	 * @brief id of the advertisement, given based on the order in which
	 *        advertisements are in the content table
	 */
	uint id_{};
	/// @brief name of the advertisement, part of path_
	std::string name_;
	/// @brief path to the file system location of the advertisement file
	std::string path_;
	/// @brief video capture for working with a video, if the advertisement is one
	cv::VideoCapture video_;
	/// @brief image that the advertisement consists of, if it is not a video
	cv::Mat image_;

	/**
	 * @brief length of time that the advertisement is expected to run
	 *        for in seconds, as given by the content table
	 */
	int duration_{-1};

	/// @brief total duration in frames
	long long total_frames_{-1};

	/**
	 * @brief current position in the advertisement video, stays at 0
	 *        if advertisement is an image
	 */
	int current_pos_{0};

	/// @brief width of the advertisement
	int w_{};
	/// @brief height of the advertisement
	int h_{};

	/**
	 * @brief fourcc code of the advertisement used when the advertisement
	 *        is a video
	 */
	int fourcc_{-1};
	/// @brief framerate of the advertisement used when the advertisement is a video
	int fps_{-1};

	/// @brief type of the advertisement: video or image
	type t_{};

	/**
	 * @brief sets the id if the advertisement
	 * @param id new id of advertisement
	 */
	inline void id(int id) { id_ = id; }

	/**
	 * @brief sets up all the necessary parameters for a given
	 *advertisement, that is:
	 *		* image_ if the advertisement is an image
	 *		* video_, fourcc_ and fps_ if the advertisement is a video
	 *		* t_, w_, h_, path_, valid_ for all advertisements
	 * @param path path to the advertisement file
	 * @throws     advertisement_exception if there is no video (.avi) or image (.png)
	 *             at path
	 */
	void init_(std::filesystem::path& path);

	/**
	 * @brief validity of the advertisement - set to true after successful
	 *        initialization
	 */
	bool valid_ = false;

	/// @brief used in case both hue and saturation is saved in a file
	int sat_offset_ = 0;

public:
	/// @brief separator to use in file between hue and saturation section
	static constexpr std::string_view hue_sat_sep = "^^^";

private:
	/**
	 * @brief counter of most common hues or saturations from the
	 *        central line of the advertisement's scalespace
	 */
	std::vector<vpair_vuc_i> vector_counts_;
	/**
	 * @brief vector that specifies whether the hue (false) or saturation
	 *        (true) channel is used for each frame of the advertisement
	 */
	std::vector<bool> sat_;
	/**
	 * @brief vector with an assigned most common hue for each
	 *        of the frames of the advertisement
	 */
	std::vector<uchar> most_common_hues_;
	/**
	 * @brief vector with the count of unique colors for each of
	 *        the frames of the advertisement
	 */
	std::vector<size_t> unique_colors_;

public:
	/**
	 * @brief gets the total number of all vector counts
	 * @param vcounts collection of vector counts
	 * @return number of all vector counts
	 */
	static long total_vcounts(std::vector<vpair_vuc_i>& vcounts) {
		long t = 0;
		for (const auto& v : vcounts) {
			t += v.size();
		}
		return t;
	}

	/**
	 * @brief returns a vector with the most common hues or
	 *        saturations from the central line of the advertisement's scalespace
	 * @return vector_counts of advertisement
	 */
	inline std::vector<vpair_vuc_i>& vector_counts() { return vector_counts_; }

	/**
	 * @brief returns a vector that specifies which channel should be used for
	 *        each frame of the advertisement
	 * @return vector with boolean value for each frame of advertisement
	 */
	inline std::vector<bool>& sat() { return sat_; }

	/**
	 * @brief returns a vector with most common hues for each
	 *        frame of advertisement
	 * @return vector with most common hues for each frame of advertisement
	 */
	inline std::vector<uchar>& most_common_hues() { return most_common_hues_; }

	/**
	 * @brief returns a vector with a count of unique colors for
	 *        each frame of advertisement
	 * @return vector with a count of unique colors for each frame of
	 * advertisement
	 */
	inline std::vector<size_t>& unique_colors() { return unique_colors_; }

	/**
	 * @brief returns this advertisement's context for a specific frame
	 * @param frame frame number of the desired context
	 * @return      information about a specific frame of the advertisement
	 */
	inline advertisement_context get_context(int frame) {
		return advertisement_context(vector_counts_[frame], sat_[frame],
		                             most_common_hues_[frame],
		                             unique_colors_[frame]);
	}

	/**
	 * @brief adds an existing context to the end of this advertisement's context
	 * @param actx context to append
	 */
	inline void append_context(advertisement_context& actx) {
		vector_counts_.push_back(actx.v_counts());
		sat_.push_back(actx.sat());
		most_common_hues_.push_back(actx.most_common_hues());
		unique_colors_.push_back(actx.unique_colors());
	}

	/**
	 * @brief vector counts getter
	 * @return vector counts
	 */
	[[nodiscard]] inline const std::vector<std::vector<std::pair<std::vector<uchar>, int>>>&
	vector_counts() const {
		return vector_counts_;
	}

	/**
	 * @brief getter for information about which frame shall be processed using its
	 *        saturation channel
	 * @return information about saturation channel usage
	 */
	[[nodiscard]] inline const std::vector<bool>& sat() const { return sat_; }
	/**
	 * @brief getter for information about which hue was the most common
	 *        for each of this advertisement's frames
	 * @return information about most common hues in the advertisement
	 */
	[[nodiscard]] inline const std::vector<uchar>& most_common_hues() const { return most_common_hues_; }
	/**
	 * @brief getter for information about how many unique colors were found
	 *        in the advertisement in each of its frames
	 * @return information about unique color counts in the advertisement
	 */
	[[nodiscard]] inline const std::vector<size_t>& unique_colors() const { return unique_colors_; }

	/**
	 * @brief returns name of advertisement
	 * @return name of advertisement
	 */
	[[nodiscard]] inline const std::string& get_name() const { return name_; }

	/**
	 * @brief returns true if advertisement is ready to be used
	 * @return true if advertisement has its parameters ready for use
	 */
	[[nodiscard]] inline bool valid() const { return valid_; }

	/**
	 * @brief returns id of advertisement
	 * @return id of advertisement
	 */
	[[nodiscard]] inline int id() const { return id_; }

	/**
	 * @brief returns width of the advertisement video or image
	 * @return width of the advertisement video or image
	 */
	[[nodiscard]] inline int w() const { return w_; }

	/**
	 * @brief returns height of the advertisement video or image
	 * @return height of the advertisement video or image
	 */
	[[nodiscard]] inline int h() const { return h_; }

	/**
	 * @brief returns the fourcc code of the advertisement if it is a video
	 * @return fourcc code of advertisement video or -1 if advertisement is an
	 * image
	 */
	[[nodiscard]] inline int fourcc() const { return fourcc_; }

	/**
	 * @brief returns the framerate of the advertisement if it is a video
	 * @return framerate of advertisement video or -1 if advertisement is an image
	 */
	[[nodiscard]] inline int fps() const { return fps_; }

	/**
	 * @brief returns the duration (seconds) of the advertisement
	 * @return duration (seconds) of the advertisement
	 */
	[[nodiscard]] inline int duration() const { return duration_; }

	/**
	 * @brief returns the current position in the advertisement video
	 * @return current position in the advertisement video
	 */
	[[nodiscard]] inline int current_pos() const { return current_pos_; }

	/**
	 * @brief returns the file system path to the advertisement file
	 * @return file system path to the advertisement file
	 */
	[[nodiscard]] inline const std::string& path() const { return path_; }

	/**
	 * @brief returns advertisement::type::image if advertisement is
	 *        an image or advertisement::type::video if advertisement is a video
	 * @return enum type based on whether the advertisement is an image or a video
	 */
	[[nodiscard]] inline type type() const { return t_; }

	/**
	 * @brief image check whether the advertisement has an image loaded
	 * @return true if this advertisement has an image loaded
	 */
	bool has_image() { return !image_.empty(); }

	/**
	 * @brief returns a void pointer to the content of the
	 *        advertisement, based on its type
	 * @return void pointer to the content of the advertisement
	 */
	inline void* get_content() {
		switch (t_) {
		case type::video:
			return (void*)&video();
			break;
		case type::image:
			return (void*)&image();
			break;
		}
	}

	/**
	 * @brief returns the image that the advertisement consists of
	 * @return advertisement image if the advertisement is an image, empty matrix
	 *         otherwise
	 */
	[[nodiscard]] inline cv::Mat& image() {
		if (image_.empty()) {
			throw advertisement_exception(
			            advertisement_exception::ad_except::NO_IMAGE);
		}
		return image_;
	}

	/**
	 * @brief const returns the image that the advertisement consists of
	 * @return const advertisement image if the advertisement is an image, empty
	 *         matrix otherwise
	 */
	[[nodiscard]] inline const cv::Mat& image() const {
		if (image_.empty()) {
			throw advertisement_exception(
			            advertisement_exception::ad_except::NO_IMAGE);
		}
		return image_;
	}

	/**
	 * @brief returns the video capture that the advertisement consists of
	 * @return advertisement video capture if the advertisement is a video, empty
	 *         video capture otherwise
	 */
	[[nodiscard]] inline cv::VideoCapture& video() {
		if (!video_.isOpened()) {
			throw advertisement_exception(
			            advertisement_exception::ad_except::NO_VIDEO);
		}
		return video_;
	}

	/**
	 * @brief const returns the video capture that the advertisement
	 *        consists of
	 * @return const advertisement video capture if the advertisement is a video,
	 *         empty video capture otherwise
	 */
	[[nodiscard]] inline const cv::VideoCapture& video() const {
		if (!video_.isOpened()) {
			throw advertisement_exception(
			            advertisement_exception::ad_except::NO_VIDEO);
		}
		return video_;
	}

	/**
	 * @brief returns saturation offset as was used in file to
	 *        separate the saturation section from the hue section
	 * @return saturation offset
	 */
	[[nodiscard]] inline int get_satoffset() const { return sat_offset_; }

	/**
	 * @brief sets saturation offset as it is used in file to
	 *        separate the saturation section from the hue section
	 * @param offset new offset
	 */
	inline void set_satoffset(int offset) { sat_offset_ = offset; }

	/**
	 * @brief constructor to initialize an advertisement based on
	 *        table data
	 * @param path     path to the advertisement file
	 * @param duration duration of the advertisement as given by the table
	 * @param name     name of the advertisement, usually part of the advertisement's path
	 */
	inline advertisement(std::filesystem::path& path, int duration,
	                     const std::string& name)
	    : name_(name), duration_(duration) {
		init_(path);
	}

	/**
	 * @brief constructor to initialize an advertisement that has
	 * unknown duration
	 * @param path to the advertisement file
	 * @param name of the advertisement, usually part of the advertisement's path
	 */
	inline advertisement(std::filesystem::path& path, const std::string& name)
	    : name_(name), duration_(0) {
		init_(path);
	}

	/**
	 * @brief constructor to create a blank advertisement,
	 *        advertisements created in this way are invalid
	 */
	inline advertisement() : name_("") {}

	/**
	 * @brief check whether the vector cluster has been
	 *        calculated for this advertisement
	 * @return true if advertisement has had its vector counts
	 *         calculated and assigned
	 */
	[[nodiscard]] inline bool has_vector_counts() const { return !vector_counts_.empty(); }

	/**
	 * @brief moves the current position of the advertisement video
	 *        forward by one frame, loops back to the first frame after reaching end
	 * @return the advanced advertisement
	 */
	inline advertisement& advance() {
		if (t_ == type::image) {
			return *this;
		}
		if (t_ == type::video) {
			video_ >> image_;
			if (image_.empty() && current_pos_ < total_frames_) {
				reset();
				video_ >> image_;
			}
		}
		++current_pos_;
		return *this;
	}

	/**
	 * @brief sets the current frame position in the
	 *        advertisement video back to 0
	 */
	inline void reset() {
		if (t_ == type::video) {
			video_.set(cv::CAP_PROP_POS_FRAMES, 0);
		}
		current_pos_ = 0;
	}
};

/**
 * @brief centerboard is a special type of advertisement - this appears in
 *        the middle of the advertisement panel
 */
class centerboard : public advertisement {
public:
	/**
	 * @brief constructor to prepare the centerboard advertisement for use
	 * @param path path to the advertisement file
	 */
	inline centerboard(std::filesystem::path& path)
	    : advertisement(path, INT_MAX, "centerboard") {};
	inline centerboard() = default;
};
