#pragma once

#include <xls_loader.h>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include "utility.h"

/// @brief expected height of the advertisement in pixels
const int CONTENT_HEIGHT = 90;
/// @brief desired number of widths of the advertisement in pixels
const int CONTENT_WIDTH_N = 4;

/**
 * @brief namespace of functions for formatting images
 */
namespace image_formatting {
	/**
	 * @brief formats an image to make it easier to work with at later steps
	 * @param width_original original image width
	 * @param n_widths       used to determine width of new image,
	 *                       width_original * n_widths
	 * @param new_height     new image height
	 * @param original       image to format
	 * @param result         formatted image
	 * @param allow_empty    flag to determine if images with fully transparent areas
	 *                       of size width_original x new_height should be formatted as well, using a
	 *                       special method to remove the transparent areas
	 */
	void format(int width_original, int n_widths, int new_height, cv::Mat& original,
	            cv::Mat& result, bool allow_empty = true);
}  // namespace image_formatting

/**
 * @brief namespace of functions for formatting videos
 */
namespace video_formatting {
	/**
	 * @brief formats a video to make it easier to work with at later steps
	 * @param width_original original video width
	 * @param n_widths       used to determine width of new video,
	 *                       width_original * n_widths
	 * @param new_height     new video height
	 * @param reader         video capture for reading existing video files
	 * @param writer         video writer for writing new video files
	 */
	void format(int width_original, int n_widths, int new_height,
	            cv::VideoCapture& reader, cv::VideoWriter& writer);
}  // namespace video_formatting

/**
 * @brief namespace of functions for formatting content
 */
namespace content_formatting {
	/**
	 * @brief formats the given files to make them easier to work with
	 * @param unique_files   files to format
	 * @param path_to_center path to center, which has special rules
	 * @return               path to formatted files
	 */
	std::filesystem::path format(const unique_content& unique_files,
	                             std::filesystem::path& path_to_center);

}  // namespace content_formatting

/**
 * @brief namespace of functions for combining videos
 */
namespace video_glue {
	/**
	 * @brief combines videos in the order of appearance in
	 *        the given table into one
	 * @param table table of videos
	 * @param       transition time to keep videos blended together on transition
	 */
	void glue_table_videos(content_table* table, int transition);
} // namespace video_glue
