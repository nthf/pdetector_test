#include "terminal_color_output.h"

using namespace rope;

namespace color_output__ {
	bool enabled_ansi_escape_sequences = true;
}

void ansi_color::check_enable() {
	if (!color_output__::enabled_ansi_escape_sequences) {
		str_.clear();
		code_.clear();
	}
}

ansi_color::ansi_color(const rope_stringbuilder& string, const rope_stringbuilder& code) : str_(string), code_(code) {
	check_enable();
}

ansi_color::ansi_color(const std::string& string, int code) {
	str_.append(string);
	code_.append(std::to_string(code));
	check_enable();
}

ansi_color::ansi_color(int r, int g, int b, int ground) {
	str_ << "\033[" << ground << ";2;" << r << ";" << g << ";" << b << "m";
	code_ << ground << ";" << 2;
	check_enable();
}

ansi_color::ansi_color(int code, int ground) {
	str_ << "\033[" << ground << ";5;" << code << "m";
	code_ << ground << ";5;" << code;
	check_enable();
}

ansi_color operator+(const ansi_color& lhs, const ansi_color& rhs) {
	rope_stringbuilder rope_sb1;
	rope_stringbuilder rope_sb2;

	rope_stringbuilder temp_sb(lhs.str_.str());
	temp_sb.rdelete(lhs.str_.str().length() - 1, 1);
	temp_sb << ";";
	rope_sb1 << temp_sb.str() << rhs.code_.str() << "m";
	rope_sb2 << lhs.code_.str() << ";" << rhs.code_.str();
	return ansi_color(rope_sb1, rope_sb2);
}

ansi_color ansi_color::combine(const ansi_color& lhs, const ansi_color& rhs) {
	return lhs + rhs;
}

ansi_color ansi_color::combine(const ansi_color& other) const {
	return *this + other;
}

std::string ansi_color::str() const { return str_.str(); }
