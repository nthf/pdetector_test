#include "gaussian_iir.h"

/**
 * Adaptation from i3d library implementation of infinite impulse response
 * algorithm for computing gaussian filter, library is available from
 * [https://cbia.fi.muni.cz/software/i3d-library.html]
 */
namespace fast_gaussian_blur {
    double *back_ptr(cv::Mat1d &mat) {
        return mat.ptr<double>(mat.rows - 1) + mat.cols - 1;
    }



    void compute_b_from_sigma(const double sigma, double *B, double *b1, double *b2,
                              double *b3) {
        double q;

        const double m0 = 1.16680;
        const double m1 = 1.10783;
        const double m2 = 1.40586;
        const double m1_sq = m1 * m1;
        const double m2_sq = m2 * m2;

        if (sigma < 3.556) {
            q = -0.2568 + 0.5784 * sigma + 0.0561 * sigma * sigma;
        } else {
            q = 2.5091 + 0.9804 * (sigma - 3.556);
        }

        const double qsq = q * q;

        const double scale = (m0 + q) * (m1_sq + m2_sq + 2 * m1 * q + qsq);
        *b1 = -q * (2 * m0 * m1 + m1_sq + m2_sq + (2 * m0 + 4 * m1) * q + 3 * qsq) /
                scale;
        *b2 = qsq * (m0 + 2 * m1 + 3 * q) / scale;
        *b3 = -qsq * q / scale;

        *B = (m0 * (m1_sq + m2_sq)) / scale;
        *B *= *B;
    }

    void suggest_iir_boundaries(const double b1, const double b2, const double b3,
                                std::vector<double> &M) {
        const double scale =
                1 / ((1 + b1 - b2 + b3) * (1 - b1 - b2 - b3) * (1 + b2 + (b1 - b3) * b3));

        M[0] = scale * (-b3 * b1 + 1 - b3 * b3 - b2);
        M[1] = scale * (b3 + b1) * (b2 + b3 * b1);
        M[2] = scale * b3 * (b1 + b3 * b2);
        M[3] = scale * (b1 + b3 * b2);
        M[4] = -scale * (b2 - 1) * (b2 + b3 * b1);
        M[5] = -scale * b3 * (b3 * b1 + b3 * b3 + b2 - 1);
        M[6] = scale * (b3 * b1 + b2 + b1 * b1 - b2 * b2);
        M[7] = scale *
                (b1 * b2 + b3 * b2 * b2 - b1 * b3 * b3 - b3 * b3 * b3 - b3 * b2 + b3);
        M[8] = scale * b3 * (b1 + b3 * b2);
    }
}  // namespace fast_gauss_functions

void gauss_iir(cv::Mat &img, cv::Mat &output, const double sigma) {
    img.copyTo(output);
    cv::Mat out;
    output.convertTo(out, CV_64FC1);
    const signed long x_size = out.cols;
    const signed long y_size = out.rows;
    //	const signed long z_size = 1;
    //	const signed long s_size = x_size * y_size;
    const signed long x_2_size = 2 * x_size;
    const signed long x_3_size = 3 * x_size;

    double b1;
    double b2;
    double b3;
    double B;
    double S = sigma;
    fast_gaussian_blur::compute_b_from_sigma(S, &B, &b1, &b2, &b3);

    std::vector<double> M = std::vector<double>(9);
	fast_gaussian_blur::suggest_iir_boundaries(-b1, -b2, -b3, M);
    const double denominator = 1 + b1 + b2 + b3;

    signed long i = 0;
    signed long c = 0;

    auto im_ptr = out.ptr<double>();

    if (out.cols > 1) {
        for (signed long rows = 0; rows < y_size; rows++) {
            const double bnd = *im_ptr / denominator;
            *im_ptr = *im_ptr - bnd * (b1 + b2 + b3);
            ++im_ptr;

            *im_ptr = *im_ptr - *(im_ptr - 1) * b1 - bnd * (b2 + b3);
            ++im_ptr;

            *im_ptr = *im_ptr - *(im_ptr - 1) * b1 - *(im_ptr - 2) * b2 - bnd * b3;
            ++im_ptr;

            for (i = 3; i < (x_size - 1); i++) {
                *im_ptr = *im_ptr - *(im_ptr - 1) * b1 - *(im_ptr - 2) * b2 -
                        *(im_ptr - 3) * b3;
                ++im_ptr;
            }

            ++im_ptr;
        }

        im_ptr = fast_gaussian_blur::back_ptr<double>(out);

        for (signed long rows = 0; rows < y_size; rows++) {
            double u_plus = *im_ptr / denominator;

            *im_ptr = *im_ptr - *(im_ptr - 1) * b1 - *(im_ptr - 2) * b2 -
                    *(im_ptr - 3) * b3;
            const double bnd1_ = *im_ptr - u_plus;
            const double bnd2_ = *(im_ptr - 1) - u_plus;
            const double bnd3_ = *(im_ptr - 2) - u_plus;
            u_plus /= denominator;

            *im_ptr = (M[0] * bnd1_ + M[1] * bnd2_ + M[2] * bnd3_ + u_plus) * B;
            const double bnd2 =
                    (M[3] * bnd1_ + M[4] * bnd2_ + M[5] * bnd3_ + u_plus) * B;
            const double bnd3 =
                    (M[6] * bnd1_ + M[7] * bnd2_ + M[8] * bnd3_ + u_plus) * B;

            --im_ptr;

            *im_ptr = (*im_ptr) * B - *(im_ptr + 1) * b1 - bnd2 * b2 - bnd3 * b3;
            --im_ptr;

            *im_ptr =
                    (*im_ptr) * B - *(im_ptr + 1) * b1 - *(im_ptr + 2) * b2 - bnd2 * b3;
            --im_ptr;

            for (i = 3; i < x_size; i++) {
                *im_ptr = (*im_ptr) * B - *(im_ptr + 1) * b1 - *(im_ptr + 2) * b2 -
                        *(im_ptr + 3) * b3;
                --im_ptr;
            }
        }

        im_ptr = out.ptr<double>();
    }

    if (out.cols > 1) {
        double *im_ptr2 = im_ptr;

        auto bnd = new double[x_size];

        im_ptr = im_ptr2;

        for (c = 0; c < x_size; c++) {
            *(bnd + c) = *(im_ptr + c) / denominator;

            *(im_ptr + c) = *(im_ptr + c) - *(bnd + c) * (b1 + b2 + b3);
        }
        im_ptr += x_size;

        for (c = 0; c < x_size; c++) {
            *(im_ptr + c) =
                    *(im_ptr + c) - *(im_ptr + c - x_size) * b1 - *(bnd + c) * (b2 + b3);
        }
        im_ptr += x_size;

        for (c = 0; c < x_size; c++) {
            *(im_ptr + c) = *(im_ptr + c) - *(im_ptr + c - x_size) * b1 -
                    *(im_ptr + c - x_2_size) * b2 - *(bnd + c) * b3;
        }
        im_ptr += x_size;

        for (i = 3; i < (y_size - 1); i++) {
            for (c = 0; c < x_size; c++) {
                *(im_ptr + c) = *(im_ptr + c) - *(im_ptr + c - x_size) * b1 -
                        *(im_ptr + c - x_2_size) * b2 -
                        *(im_ptr + c - x_3_size) * b3;
            }
            im_ptr += x_size;
        }

        im_ptr += x_size;

        im_ptr2 = out.ptr<double>(out.rows - 1);

        auto bnd2 = new double[x_size];
        auto bnd3 = new double[x_size];
        auto v_plus = new double[x_size];

        auto Bnd2 = new double[x_size];
        auto Bnd3 = new double[x_size];

        im_ptr = im_ptr2;

        for (c = 0; c < x_size; c++) {
            *(bnd + c) = *(im_ptr + c) / denominator;

            *(im_ptr + c) = *(im_ptr + c) - *(im_ptr + c - x_size) * b1 -
                    *(im_ptr + c - x_2_size) * b2 -
                    *(im_ptr + c - x_3_size) * b3;
        }

        for (c = 0; c < x_size; c++) *(v_plus + c) = *(bnd + c) / denominator;
        for (c = 0; c < x_size; c++)
            *(bnd3 + c) = *(im_ptr + c - x_2_size) - *(bnd + c);
        for (c = 0; c < x_size; c++)
            *(bnd2 + c) = *(im_ptr + c - x_size) - *(bnd + c);
        for (c = 0; c < x_size; c++) *(bnd + c) = *(im_ptr + c) - *(bnd + c);

        for (c = 0; c < x_size; c++)
            *(Bnd2 + c) = (*(bnd + c) * M[3] + *(bnd2 + c) * M[4] +
                *(bnd3 + c) * M[5] + *(v_plus + c)) *
                B;
        for (c = 0; c < x_size; c++)
            *(Bnd3 + c) = (*(bnd + c) * M[6] + *(bnd2 + c) * M[7] +
                *(bnd3 + c) * M[8] + *(v_plus + c)) *
                B;

        for (c = 0; c < x_size; c++) {
            *(im_ptr + c) = (*(bnd + c) * M[0] + *(bnd2 + c) * M[1] +
                    *(bnd3 + c) * M[2] + *(v_plus + c)) *
                    B;
        }
        im_ptr -= x_size;

        for (c = 0; c < x_size; c++) {
            *(im_ptr + c) = *(im_ptr + c) * B - *(im_ptr + c + x_size) * b1 -
                    *(Bnd2 + c) * b2 - *(Bnd3 + c) * b3;
        }
        im_ptr -= x_size;

        for (c = 0; c < x_size; c++) {
            *(im_ptr + c) = *(im_ptr + c) * B - *(im_ptr + c + x_size) * b1 -
                    *(im_ptr + c + x_2_size) * b2 - *(Bnd2 + c) * b3;
        }
        im_ptr -= x_size;

        for (i = 3; i < y_size; i++) {
            for (c = 0; c < x_size; c++) {
                *(im_ptr + c) = *(im_ptr + c) * B - *(im_ptr + c + x_size) * b1 -
                        *(im_ptr + c + x_2_size) * b2 -
                        *(im_ptr + c + x_3_size) * b3;
            }
            im_ptr -= x_size;
        }

        delete[] bnd;
        delete[] bnd2;
        delete[] bnd3;
        delete[] v_plus;
        delete[] Bnd2;
        delete[] Bnd3;

        im_ptr = out.ptr<double>();
    }
    out.convertTo(output, CV_8UC1);
}

void gauss_iir(cv::Mat &img, cv::Mat &out, const int sigma) {
    if (sigma > 0) {
        gauss_iir(img, out, static_cast<double>(sigma));
    } else {
        img.copyTo(out);
    }
}

//***/
