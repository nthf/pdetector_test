#include "xls_loader.h"

using namespace xls;

void content_table::add(content_table_row& row) {
	std::filesystem::path filepath(row[table_indices__::FILE_NAME_INDEX].get_string());
	std::filesystem::path ext = filepath.extension();

	paths::file_type ft = type_from_extension(ext);
	if (ft == paths::file_type::VIDEO) {
		filepath.replace_filename(filepath.stem() += (paths::format_suffix + paths::preferred_video));
	} else if (ft == paths::file_type::IMAGE) {
		filepath.replace_filename(filepath.stem() += (paths::format_suffix + paths::preferred_image));
	}
	table_.push_back(row);
	unique_files_.emplace(filepath.string(), row[table_indices__::DURATION_INDEX].get_int());
}

int content_table_loader::get_cols_num(WorkBook& wb, uint sheetnum) {
	int cols = 0;
	int gap = 0;
	for (int i = 0; i < wb.GetCols(); ++i) {
		cellContent c = wb.GetCell(sheetnum, 1, i + 1);
		if (c.type == cellUnknown || c.type == cellBlank) {
			++gap;
			continue;
		}
		++cols;
		cols += gap;
		gap = 0;
	}
	return cols;
}

void content_table_loader::read_xls() {
	WorkBook wb(path_.string());

	for (uint sheetNum = 0; sheetNum < wb.GetSheetCount(); ++sheetNum) {
		wb.InitIterator(sheetNum);
		content_table_row row;

		wb.SetCols(wb.GetActiveSheetCols());
		wb.SetRows(wb.GetActiveSheetRows());
		int rown = -1;
		int cols = get_cols_num(wb, sheetNum);
		wb.SetCols(cols);
		wb.InitIterator(sheetNum);
		while (true) {
			cellContent c = wb.GetNextCell();
			unsigned int current_col = c.col / 256 - 1;

			if (current_col > (0u - 1000)) {
				break;
			}

			if (current_col == 0) {
				++rown;
				row.clear();
			}

			if (rown == 0 || current_col == static_cast<unsigned int>(cols)) {
				continue;
			}

			switch (c.type) {
			case cellInteger:
				row.push_back(std::stoi(c.str));
				break;
			case cellFloat:
				row.push_back(std::stof(c.str));
				break;
			case cellBool:
				row.push_back(c.str == "true");
				break;
			case cellString:
				row.push_back(c.str.c_str());
				break;
			case cellBlank:
				row.push_back("");
				break;
			case cellUnknown:
			case cellError:
				continue;
			}

			if (static_cast<int>(current_col) == cols - 1) {
				// if (prev_) {
				// 	pairings_.insert({(*prev_)[table_indices__::FILE_NAME_INDEX].get_string(), row[table_indices__::FILE_NAME_INDEX].get_string()});
				// }
				// prev_ = &row;
				table_.add(row);

			}
		}
	}
}
