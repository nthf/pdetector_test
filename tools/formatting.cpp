#include "formatting.h"
#include "terminal_color_output.h"

using namespace std::filesystem;
using namespace paths;

namespace image_formatting {
	namespace {
		/**
		  * @brief format_with_empty format an image with expected transparent pixels
		  * that shall be removed
		  * @param width_original original image width
		  * @param n_widths used to determine width of new image, width_original *
		  * n_widths
		  * @param new_height new image height
		  * @param original image to format
		  * @param result formatted image
		  */
		void format_with_empty(int width_original, int n_widths, int new_height,
		                       cv::Mat& original, cv::Mat& result) {
			cv::Mat channels[4];
			cv::split(original, static_cast<cv::Mat *>(channels));

			std::vector<cv::Point> non_zero;
			cv::findNonZero(channels[3], non_zero);

			int x = non_zero.front().x;
			int y = non_zero.front().y;
			cv::Rect roi_from_original(x, y, width_original - x, new_height);
			cv::Mat roi_img = original(roi_from_original);
			roi_img.copyTo(result);

			if (x != 0 || result.cols < width_original * n_widths) {
				while (y + new_height < non_zero.back().y) {
					non_zero.erase(std::remove_if(
					            non_zero.begin(), non_zero.end(),
					            [y, new_height](const cv::Point& p) { return p.y < y + new_height; }),
					           non_zero.end());
					y += new_height;
					x = non_zero.front().x;
					roi_from_original =
					        cv::Rect(x, y,
					                 y + new_height > non_zero.back().y ? non_zero.back().x + 1
					                                                    : width_original - x,
					                 new_height);
					roi_img = original(roi_from_original);
					if (result.cols + roi_img.cols <= width_original * n_widths) {
						cv::hconcat(result, roi_img, result);
					} else {
						break;
					}
				}
			}
		}
	}  // namespace
	void format(int width_original, int n_widths, int new_height, cv::Mat& original,
	            cv::Mat& result, bool allow_empty /* = true*/) {
		if (original.empty()) {
			return;
		}
		int x = 0;
		int y = 0;
		int w = width_original;
		int h = new_height;

		for (int i = 0; i < n_widths; ++i) {
			cv::Rect roi_from_original(x, y + i * h, w - x, h);
			cv::Rect roi_to_new(x + i * w, 0, w, h);

			cv::Mat roi_img = original(roi_from_original);
			if (allow_empty) {
				cv::Mat channels[4];
				cv::split(roi_img, static_cast<cv::Mat *>(channels));
				if (cv::countNonZero(channels[3]) == 0) {
					format_with_empty(width_original, n_widths, new_height, original,
					                  result);
					break;
				}
			}
			if (result.empty()) {
				roi_img.copyTo(result);
			} else {
				cv::hconcat(result, roi_img, result);
			}
		}
	}
}  // namespace image_formatting

namespace video_formatting {
	void format(int width_original, int n_widths, int new_height,
	            cv::VideoCapture& reader, cv::VideoWriter& writer) {
		if (!reader.isOpened()) {
			return;
		}

		cv::Mat frame;

		int w = width_original > 0 ? width_original
		                           : static_cast<int>(reader.get(cv::CAP_PROP_FRAME_WIDTH));
		int h = new_height > 0 ? new_height : CONTENT_HEIGHT;

		int count = static_cast<int>(reader.get(cv::CAP_PROP_FRAME_COUNT));
		int position = 0;
		float progress = 0.0;

		while (true) {
			reader >> frame;
			if (frame.empty()) {
				break;
			}

			cv::Mat res;
			image_formatting::format(w, n_widths, h, frame, res, false);
			writer.write(res);

			if (position < count) {
				std::cout << "------ " << std::flush;
				utility::progress_bar(30, count, &position, &progress);
			}
		}
	}
}  // namespace video_formatting

namespace content_formatting {
	path format(const unique_content& unique_files,
	            std::filesystem::path& path_to_center) {
		path content_dir(paths::CONTENT_DIR);
		path path_to_formatted = paths::FORMAT_DIR;

		if (!std::filesystem::exists(path_to_formatted)) {
			std::filesystem::create_directory(path_to_formatted);
		}

		std::vector<std::filesystem::path> paths;
		for (auto& p : std::filesystem::directory_iterator(content_dir)) {
			paths.push_back(p.path());
		}

		for (auto& i : paths) {
			if (std::filesystem::is_directory(i) ||
			        i.filename().string().front() == '.' ||
			        !content_table::uc_contains(
			            i.stem().concat(paths::format_suffix).string(),
			            unique_files)) {
				std::string str = i.stem().string();
				if (starts_with(to_lower(str), paths::centerboard)) {
					path_to_center = path_to_formatted / (i.stem());
					paths::file_type ft = type_from_extension(i.extension());
					if (ft == paths::file_type::VIDEO) {
						path_to_center += (paths::format_suffix + paths::preferred_video);
					} else if (ft == paths::file_type::IMAGE) {
						path_to_center += (paths::format_suffix + paths::preferred_image);
					} else {
						path_to_center = std::filesystem::path();
					}
				} else {
					continue;
				}
			}

			std::cout << lcyan << "found: " << lyellow << i << reset << "\n" << std::flush;
			const path& extension = i.extension();

			paths::file_type ft = type_from_extension(extension);
			if (ft == paths::file_type::VIDEO &&
			        !std::filesystem::exists((path_to_formatted / i.stem()) +=
			                                 (paths::format_suffix + paths::preferred_video))) {
				std::cout << "> formatting video\n" << std::flush;
				cv::VideoCapture capture(i.string());
				cv::Size size(capture.get(cv::CAP_PROP_FRAME_WIDTH) * CONTENT_WIDTH_N,
				              CONTENT_HEIGHT);

				cv::VideoWriter writer(
				            ((path_to_formatted / i.stem()) += (paths::format_suffix + paths::preferred_video))
				            .string(),
				            cv::VideoWriter::fourcc('M', 'J', 'P', 'G'),
				            capture.get(cv::CAP_PROP_FPS), size);

				video_formatting::format(0, CONTENT_WIDTH_N, CONTENT_HEIGHT, capture,
				                         writer);
				std::cout << "\n" << std::flush;
			} else if (ft == paths::file_type::IMAGE &&
			           !std::filesystem::exists((path_to_formatted / i.stem()) +=
			                                    (paths::format_suffix + paths::preferred_image))) {
				std::cout << "> formatting image " << std::flush;
				cv::Mat image = cv::imread(i.string(), cv::IMREAD_UNCHANGED);
				cv::Size size(image.cols * CONTENT_WIDTH_N, CONTENT_HEIGHT);
				cv::Mat result;
				image_formatting::format(image.cols, CONTENT_WIDTH_N, CONTENT_HEIGHT,
				                         image, result);

				cv::imwrite(
				            ((path_to_formatted / i.stem()) += (paths::format_suffix + paths::preferred_image))
				            .string(),
				            result);
				std::cout << "\n" << "------ " << lgreen << "done\n" << reset << std::flush;
			} else {
				if (ft == paths::file_type::IMAGE ||
				        ft == paths::file_type::VIDEO) {
					std::cout << lgreen << "------ file already formatted\n" << reset << std::flush;
				} else {
					std::cout << lblue << "------ file not part of content\n" << reset << std::flush;
				}
			}
		}
		return path_to_formatted;
	}
}  // namespace content_formatting

namespace video_glue {
	void glue_table_videos(content_table* table, int transition) {
		content_table& t = *table;
		cv::VideoCapture capture(paths::FORMAT_DIR / std::filesystem::path(t[404][table_indices__::FILE_NAME_INDEX].get_string()).stem().concat(paths::format_suffix).concat(paths::preferred_video));
		cv::Size size(capture.get(cv::CAP_PROP_FRAME_WIDTH), capture.get(cv::CAP_PROP_FRAME_HEIGHT));
		auto fps = capture.get(cv::CAP_PROP_FPS);
		cv::VideoWriter writer(
		            (paths::FORMAT_DIR / (paths::combined_video + paths::preferred_video)).string(),
		            cv::VideoWriter::fourcc('M', 'J', 'P', 'G'),
		            fps, size);
		int i = 405;
		int curr_trans = transition;
		cv::Mat last;
		int count = 0;
		int k = i;
		while (strcmp(t[k][table_indices__::ROT_PHASE_INDEX].get_string(), "1st Half") == 0) {
			++count;
			++k;
		}
		int prog = 1;
		while (strcmp(t[i][table_indices__::ROT_PHASE_INDEX].get_string(), "1st Half") == 0) {
			bool video = std::filesystem::exists(paths::FORMAT_DIR / std::filesystem::path(t[i][table_indices__::FILE_NAME_INDEX].get_string()).stem().concat(paths::format_suffix).concat(paths::preferred_video));
			float progress = static_cast<float>(prog) / static_cast<float>(count);

			std::cout << "------ " << lblue << prog << " / " << count << " " << reset << std::flush;
			utility::progress_bar(30, count + 1, &prog, &progress);

			if (video) {
				cv::VideoCapture capture(paths::FORMAT_DIR / std::filesystem::path(t[i][table_indices__::FILE_NAME_INDEX].get_string()).stem().concat(paths::format_suffix).concat(paths::preferred_video));
				cv::Mat frame;
				capture >> frame;

				int j = 0;
				curr_trans = transition;
				while (!frame.empty()) {
					if (!last.empty() && j <= transition) {
						if (curr_trans > 0) {
							double alpha = curr_trans / static_cast<double>(transition);
							cv::addWeighted(last, alpha, frame, 1 - alpha, 0, frame);
							--curr_trans;
						} else {
							curr_trans = transition;
						}
					}
					cv::imshow("frame", frame);
					key_wait(1);
					writer.write(frame);
					capture >> frame;
					++j;
					if (j == 600 - 1) {
						frame.copyTo(last);
					}
				}
			} else {
				cv::Mat frameOr = cv::imread(paths::FORMAT_DIR / std::filesystem::path(t[i][table_indices__::FILE_NAME_INDEX].get_string()).stem().concat(paths::format_suffix).concat(paths::preferred_image));
				cv::Mat frame;
				int duration = static_cast<int>(20 * fps);
				for (int j = 0; j < duration; ++j) {
					frameOr.copyTo(frame);
					if (!last.empty() && j <= transition) {
						if (curr_trans > 0) {
							double alpha = curr_trans / static_cast<double>(transition);
							cv::addWeighted(last, alpha, frame, 1 - alpha, 0, frame);
							--curr_trans;
						} else {
							curr_trans = transition;
						}
					}
					cv::imshow("frame", frame);
					key_wait(1);
					writer.write(frame);
				}
				frame.copyTo(last);
			}
			++i;
		}
		std::cout << "\n" << std::flush;
	}
} // namespace video_glue
