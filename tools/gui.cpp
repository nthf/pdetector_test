#include "gui.h"
#include "opencv2/highgui.hpp"
#include "terminal_color_output.h"
#include <iostream>

using namespace cv;
using namespace std;

void event_handler::mouse_event_callback(int event, int x, int y, int flags, void* data)
{
	(void)flags;
	auto mouse = static_cast<mouse_s *>(data);
	mouse->x = x - 1;
	mouse->y = y - 3;
	mouse->event = event;
}

template<class T>
bool ui_selection<T>::repeat_button_select() {
	const auto& it = std::next(buttons_.begin(), selected_);
	const std::string& label = it->label();
	switch(mode_) {
	case M_DEL_AD_FILE:
		std::cout << (lred + dgraybg) << ">> deleting singular ad file at " << (yellow + dgraybg) << (paths::FORMAT_DIR / paths::combined_video) << reset << "\n" << std::flush;
		delete_files(paths::FORMAT_DIR, [](const std::filesystem::path& p) {
			return p.filename() == paths::combined_video + paths::preferred_video;
		});
		break;
	case M_DEL_AUTO_FORMAT_FILES:
		std::cout << (lred + dgraybg) << ">> deleting all formatted files in " << (yellow + dgraybg) << paths::FORMAT_DIR << reset << "\n" << std::flush;
		delete_files(paths::FORMAT_DIR, [](const std::filesystem::path& p) {
			std::string pstr = p.string();
			return pstr.substr(pstr.size() - 6) == paths::format_suffix + paths::preferred_video ||
			        pstr.substr(pstr.size() - 6) == paths::format_suffix + paths::preferred_image;
		});
		break;
	case M_DEL_AUTO_PRECOMP_FILES:
		std::cout << (lred + dgraybg) << ">> deleting all pre-calculated files in " << (yellow + dgraybg) << paths::FORMAT_DIR << reset << "\n" << std::flush;
		delete_files(paths::FORMAT_DIR, [](const std::filesystem::path& p) {
			return p.extension() == paths::prefd_processed;
		});
		break;
	default:
		return false;
	}

	std::cout << colorex(51) << ">> done, deleted files can be found at " << reset << "\n" << std::flush;
	std::cout << std::filesystem::canonical(paths::BACKUP_DIR) << "\n" << std::flush;
	return true;
}

template<>
void ui_selection<cv::Mat>::draw_window() {
	cvui::window(window_, 0, 0, WINDOW_W, WINDOW_H, "Choose mode");
	for (const auto& b : buttons_) {
		*this |= button(b);
	}

	cvui::checkbox(window_, H_MARGIN, 160, "Exclude a hue", &exclude_);
	if (!exclude_) {
		rectangle(cv::Point(0, 180), cv::Point(300, 230), cv::Scalar::all(49), true);
	} else {
		cvui::image(window_, 20, 200, graphics_[0]);
		cvui::trackbar(window_, 5, 180, 138, &chosen_hue_, 0, 360, 1, "%.0Lf", cvui::TRACKBAR_DISCRETE, 2);
		auto hue = static_cast<float>(chosen_hue_ % 360);

		rectangle(cv::Point(2 * PADDING + COL_W, 180), cv::Point(2 * PADDING + COL_W + B_WIDTH, 180 + B_HEIGHT), utility::hsv2bgr(hue, 1, 1), true);
	}

	cvui::checkbox(window_, H_MARGIN + 2 * COL_W, 160, "Enable term", &colors_);
	cvui::text(window_, H_MARGIN + 2 * COL_W + 20, 175, "colors");
	cvui::update();
	if (selected_ >= 0) {
		cv::rectangle(window_, selection_, cv::Scalar::all(255));
	}
	cv::imshow(window_name_, window_);
}

template<>
bool ui_selection<cv::Mat>::gui_loop() {
	if (cv::getWindowProperty(window_name_, cv::WND_PROP_AUTOSIZE) == -1) {
		mode_ = M_ERR;
		return false;
	}
	draw_window();

	int key = cv::waitKey(1);

	if (key != -1) {
		if (key == 27 || ((key == 227) && (cv::waitKey() == 99))) {
			cv::destroyAllWindows();
			mode_ = M_ERR;
			return false;
		}
		if (selected_ != -1 && (key == 13 || key == 141 || key == 32)) {
			if (repeat_button_select()) {
				return true;
			}
			const auto& it = std::next(buttons_.begin(), selected_);
			mode_ = it->mode();
			return false;
		}
		if (key == 81) { --*this; }
		if (key == 82) { *this -= rows_; }
		if (key == 83) { ++*this; }
		if (key == 84) { *this += rows_; }
	}
	return true;
}
