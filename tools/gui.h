#pragma once
#include "cvui.h"
#include "utility.h"

/**
 * @brief class to keep callbacks
 */
class event_handler
{
public:
	/**
	 * @brief callback for mouse events to use with
	 *        cv::setMouseCallback(...);
	 * @see cv::setMouseCallback
	 * @param event what happened
	 * @param x     x coordinate of where it happened
	 * @param y     y coordinate of where it happened
	 * @param flags flags for the callback
	 * @param data  data to pass to the callback
	 */
	static void mouse_event_callback(int event, int x, int y, int flags, void* data);
};

/**
 * @brief creation of std::array without needing to specify its size except by
 *        passing the entire array, must include empty spaces as well
 * @tparam T    any type to fill the array with
 * @tparam tail collection of items of type T
 * @param h     head of the array
 * @param t     tail of the array
 * @return      the newly created array
 */
template<typename T, typename... tail>
auto make_array(T h, tail... t) -> std::array<T, 1 + sizeof...(tail)> {
	std::array<T, 1 + sizeof...(tail)> arr = { h, t... };
	return arr;
}

/// @brief labels to use in the ui, in order of appearance
static const auto labels = make_array( " ", "filepick", "gt_gen", "fm + ss",
                                       "ad_gen", "prev", "del ad file", "del formatted",
                                       "del pre-comp");

/// @brief lists out the different modes that can be chosen
enum modes {
	M_ERR                    = -1,
	M_NONE                   = 0,
	M_AUTOMATIC_FROM_TABLE   = 1,
	M_MANUAL_FROM_FILES      = 1 << 1,
	M_GT_GENERATION          = 1 << 2,
	M_COMBINED               = 1 << 3,
	M_GEN_AD_FILE_FROM_TABLE = 1 << 4,
	M_PREVIOUS               = 1 << 5,
	M_DEL_AD_FILE            = 1 << 6,
	M_DEL_AUTO_FORMAT_FILES  = 1 << 7,
	M_DEL_AUTO_PRECOMP_FILES = 1 << 8
}; // numbers chosen for combinations

/**
 * @brief class for picking the mode of operation
 * @tparam T type of extra data
 */
template<typename T>
class ui_selection {
private:
	/// @brief standard height of each row of elements
	static const int ROW_H = 70;
	/// @brief standard width of each column of elements
	static const int COL_W = 130;
	/// @brief horizontal margin
	static const int H_MARGIN = 20;
	/// @brief vertical margin
	static const int V_MARGIN = 30;
	/// @brief standard button width
	static const int B_WIDTH = 110;
	/// @brief standard button height
	static const int B_HEIGHT = 30;
	/// @brief standard padding between elements
	static const int PADDING = 10;
	/// @brief window width
	const int WINDOW_W;
	/// @brief window height
	const int WINDOW_H;

	friend struct button_t;

	/// @brief struct for streamlined button use
	struct button_t {
	private:
		/// @brief which column the button is located in
		int col_;
		/// @brief which row the button is located in
		int row_;
		/// @brief button label
		std::string label_;
		/// @brief pointer to owner for access to standard parameters
		const ui_selection* uis_;

		/// @brief x coordinate of top-left corner
		int x_;
		/// @brief y coordinate of top-left corner
		int y_;
		/// @brief width of button
		int w_;
		/// @brief height of button
		int h_;

		/// @brief button id for bit shifting
		uint id_;

		/// @brief mode sets the mode the button represents
		modes mode_ = M_NONE;

	public:
		/**
		 * @brief constructor to create a button and determine necessary parameters
		 * @param col   column location
		 * @param row   row location
		 * @param label button label
		 * @param uis   pointer to owner
		 */
		button_t(int col, int row, const std::string& label, const ui_selection* uis, uint* id, modes mode = modes::M_NONE)
		    : col_(col), row_(row), label_(label), uis_(uis), x_(H_MARGIN + col * COL_W),
		      y_(V_MARGIN + row * (B_HEIGHT + PADDING)), w_(B_WIDTH), h_(B_HEIGHT), id_(*id), mode_(mode) {
			if (!label.empty()) ++*id;
		}
		/**
		 * @brief overload for use in std::set
		 * @param other different button
		 * @return      true if this button appears in the ui menu sooner
		 */
		bool operator<(const button_t& other) const {
			return (row_ * uis_->rows_ + col_) < (other.row_ * uis_->rows_ + other.col_);
		}

		/// @brief conversion to std::string
		inline operator std::string() const {
			return label_;
		}

		/**
		 * @brief comparison, done on labels
		 * @param str string to compare against
		 * @return    true if labels are the same
		 */
		bool operator==(const std::string& str) const {
			return label_ == str;
		}

		/**
		 * @brief column number getter
		 * @return column number
		 */
		[[nodiscard]] inline int col() const { return col_; }
		/**
		 * @brief column setter
		 * @param value new value for col
		 */
		inline void col(int value) { col_ = value; }
		/**
		 * @brief row number getter
		 * @return row number
		 */
		[[nodiscard]] inline int row() const { return row_; }
		/**
		 * @brief row setter
		 * @param value new value for row
		 */
		inline void row(int value) { row_ = value; }
		/**
		 * @brief label getter
		 * @return label
		 */
		[[nodiscard]] inline std::string label() const { return label_; }
		/**
		 * @brief label setter
		 * @param value new value for label
		 */
		inline void label(const std::string& value) { label_ = value; }
		/**
		 * @brief ui selection pointer getter
		 * @return ui selection pointer
		 */
		[[nodiscard]] inline const ui_selection* uis() const { return uis_; }
		/**
		 * @brief ui selection pointer setter
		 * @param value new value for uis
		 */
		inline void uis(const ui_selection* value) { uis_ = value; }
		/**
		 * @brief x getter
		 * @return x
		 */
		[[nodiscard]] inline int x() const { return x_; }
		/**
		 * @brief x setter
		 * @param value new value for x
		 */
		inline void x(int value) { x_ = value; }
		/**
		 * @brief y getter
		 * @return y
		 */
		[[nodiscard]] inline int y() const { return y_; }
		/**
		 * @brief y setter
		 * @param value new value for y
		 */
		inline void y(int value) { y_ = value; }
		/**
		 * @brief width getter
		 * @return width
		 */
		[[nodiscard]] inline int w() const { return w_; }
		/**
		 * @brief width setter
		 * @param value new value for w
		 */
		inline void w(int value) { w_ = value; }
		/**
		 * @brief height getter
		 * @return height
		 */
		[[nodiscard]] inline int h() const { return h_; }
		/**
		 * @brief height setter
		 * @param value new value for h
		 */
		inline void h(int value) { h_ = value; }
		/**
		 * @brief id getter
		 * @return id
		 */
		[[nodiscard]] inline uint id() const { return id_; }
		/**
		 * @brief id setter
		 * @param value new value for id
		 */
		inline void id(const uint& value) { id_ = value; }
		/**
		 * @brief mode getter
		 * @return mode
		 */
		[[nodiscard]] inline modes mode() const { return mode_; }
		/**
		 * @brief mode setter
		 * @param value new value for mode
		 */
		inline void mode(const modes& value) { mode_ = value; }
	};

	/// @brief chosen mode
	modes mode_ = modes::M_NONE;
	/// @brief visualization tool for picking using the keyboard
	cv::Rect selection_ = cv::Rect(21, 31, 108, 28);
	/// @brief flag for whether a specific hue should be excluded from further calculations
	bool exclude_ = true;
	/// @brief flag for whether ansi escape sequences should be used in standard output
	bool colors_ = true;
	/// @brief picked hue for exclusion
	int chosen_hue_ = 90;
	/// @brief number of columns of the ui menu
	int cols_ = 1;
	/// @brief number of rows of the ui menu
	int rows_ = 3;
	/// @brief number of buttons in the ui menu
	int n_ = 3;
	/// @brief name of window
	const std::string window_name_;
	/// @brief window render
	cv::Mat window_;
	/**
	 * @brief optional graphics to include in the ui.
	 *        Each has to have separate logic coded for it,
	 *        there is no standard way to render them
	 */
	std::vector<T> graphics_;

	/// @brief collection of buttons
	std::set<button_t> buttons_;
	/// @brief currently chosen button
	int selected_ = -1;

	/**
	 * @brief calls the cvui function to draw a button and handle mouse events for that button
	 * @param col   column location
	 * @param row   row location
	 * @param shift bit shift for mode choice
	 * @param label button label
	 * @return      mode, if a button was pressed or 0 otherwise
	 */
	modes button(int col, int row, int shift, const std::string& label) {
		if (label.empty()) {
			return modes::M_NONE;
		}
		return static_cast<modes>(cvui::button(window_, H_MARGIN + col * COL_W, V_MARGIN + row * (B_HEIGHT + PADDING), B_WIDTH, B_HEIGHT, label) << shift);
	}

	/**
	 * @brief convenient handling of buttons for button_t
	 * @param butt button but shortened i promise
	 * @return     mode, if a button was pressed or 0 otherwise
	 */
	modes button(const button_t& butt) {
		return button(butt.col(), butt.row(), butt.id(), butt.label());
	}

	/**
	 * @brief draws a rectangle this is mostly for convenience
	 * @param l    location
	 * @param s    size
	 * @param c    color
	 * @param fill fill or no fill
	 */
	void rectangle(const cv::Point& l, const cv::Size& s, const cv::Scalar& c, bool fill) {
		cv::rectangle(window_, l, s, c, fill ? -1 : 1);
	}

	/// @brief initializes all buttons and places them into the button collection
	void init_buttons() {
		uint id = 0;
		uint c = 0;
		for (int col = 0; col < cols_; ++col) {
			for (int row = 0; row < rows_; ++row) {
				if (c >= labels.size()) {
					c = 0;
				}
				buttons_.insert(button_t(col, row, labels.at(c), this, &id, static_cast<modes>(std::pow(2, id))));
				++c;
			}
		}
		n_ = buttons_.size();
	}

public:
	/**
	 * @brief constructor, initializes the ui
	 * @param cols        number of columns of buttons
	 * @param rows        number of rows of buttons
	 * @param window_name name of window
	 * @param gfx_list    list of optional graphics
	 */
	inline ui_selection(int cols, int rows, const std::string& window_name, std::initializer_list<T> gfx_list)
	    : WINDOW_W(COL_W * cols + H_MARGIN), WINDOW_H(ROW_H * rows + V_MARGIN), cols_(cols), rows_(rows), window_name_(window_name) {
		cv::namedWindow(window_name);
		cvui::init(window_name);
		window_ = cv::Mat::zeros(WINDOW_H, WINDOW_W, CV_8UC3);
		cv::imshow(window_name, window_);
		cv::waitKey(1);
		graphics_.insert(graphics_.end(), gfx_list.begin(), gfx_list.end());

		init_buttons();
	}

	/**
	 * @brief returns the selected mode
	 * @return     selected mode
	 */
	[[nodiscard]] inline modes mode() const {
		return mode_;
	}

	/**
	 * @brief sets a mode, if necessary
	 * @param i index of mode to set, for specific values see operator=
	 * @return  the newly set mode
	 */
	inline modes mode(int i) {
		return mode_ = static_cast<modes>(std::pow(2, i));
	}

	/**
	 * @brief mode sets a mode, if necessary
	 * @param m mode to set
	 * @return  the newly set mode
	 */
	inline modes mode(modes m) {
		return mode_ = m;
	}

	/**
	 * @brief returns true if a hue is to be excluded
	 * @return true if hue is to be excluded
	 */
	[[nodiscard]] inline bool exclude() const {
		return exclude_;
	}

	/**
	 * @brief returns which hue was chosen for exclusion
	 * @return hue chosen for exclusion
	 */
	[[nodiscard]] inline int chosen_hue() const {
		return chosen_hue_;
	}

	/**
	 * @brief returns true if standard output should have colors
	 * @return true if standard output should have colors
	 */
	[[nodiscard]] inline bool colors() const {
		return colors_;
	}

	/**
	 * @brief convenience operator for moving around the menu using arrow keys
	 * @return this ui selection
	 */
	inline const ui_selection& operator++() {
		if (selected_ < 0) {
			selected_ = 0;
			return *this;
		}

		++selected_;
		if (selected_ >= n_) {
			selected_ = 0;
		}

		const auto& it = std::next(buttons_.begin(), selected_);
		if (it->label().empty()) {
			++*this;
		} else {
			selection_.y = it->y();
			selection_.x = it->x();
		}
		return *this;
	}

	/**
	 * @brief convenience operator for moving around the menu using arrow keys
	 * @return this ui selection
	 */
	inline ui_selection operator++(int) {
		ui_selection copy(*this);
		++*this;
		return copy;
	}

	/**
	 * @brief convenience operator for moving around the menu using arrow keys
	 * @return this ui selection
	 */
	inline const ui_selection& operator--() {
		if (selected_ < 0) {
			selected_ = n_ - 1;
			return *this;
		}

		--selected_;
		if (selected_ < 0) {
			selected_ = n_ - 1;
		}

		const auto& it = std::next(buttons_.begin(), selected_);
		if (it->label().empty()) {
			--*this;
		} else {
			selection_.y = it->y();
			selection_.x = it->x();
		}
		return *this;
	}

	/**
	 * @brief convenience operator for moving around the menu using arrow keys
	 * @return this ui selection
	 */
	inline ui_selection operator--(int) {
		ui_selection copy(*this);
		--*this;
		return copy;
	}

	/**
	 * @brief convenience operator for moving around the menu using arrow keys
	 * @param uis ui selection
	 * @param val this has to be here, doesn't do anything
	 * @return    this ui selection
	 */
	inline friend ui_selection& operator+=(ui_selection<T>& uis, int val) {
		(void)val;
		if (uis.selected_ < 0) {
			uis.selected_ = 0;
			return uis;
		}
		if (uis.selected_ == uis.n_ - 1) {
			uis.selected_ = 0;
		} else {
			uis.selected_ += uis.cols_;
			if (uis.selected_ >= uis.n_) {
				uis.selected_ %= uis.n_;
				++uis;
			}
		}
		const auto& it = std::next(uis.buttons_.begin(), uis.selected_);
		if (it->label().empty()) {
			uis += 1;
		} else {
			uis.selection_.y = it->y();
			uis.selection_.x = it->x();
		}
		return uis;
	}

	/**
	 * @brief convenience operator for moving around the menu using arrow keys
	 * @param uis ui selection
	 * @param val this has to be here, doesn't do anything
	 * @return    this ui selection
	 */
	inline friend ui_selection& operator-=(ui_selection<T>& uis, int val) {
		(void)val;
		if (uis.selected_ < 0) {
			uis.selected_ = uis.n_ - 1;
			return uis;
		}
		if (uis.selected_ == 0) {
			uis.selected_ = uis.n_ - 1;
		} else {
			uis.selected_ -= uis.cols_;
			if (uis.selected_ < 0) {
				uis.selected_ += 2 * uis.n_;
				uis.selected_ %= uis.n_;
				--uis;
			}
		}
		const auto& it = std::next(uis.buttons_.begin(), uis.selected_);
		if (it->label().empty()) {
			uis -= 1;
		} else {
			uis.selection_.y = it->y();
			uis.selection_.x = it->x();
		}
		return uis;
	}

	/**
	 * @brief used for choosing the mode, only one button can be pressed at any time (i hope)
	 * @param uis ui selection
	 * @param val value to bitwise or with currently chosen mode (usually 0)
	 * @return    this ui selection
	 */
	inline friend ui_selection& operator|=(ui_selection& uis, modes val) {
		auto tmp = static_cast<int>(uis.mode_) | static_cast<int>(val);
		uis.mode_ = static_cast<modes>(tmp);
		return uis;
	}

	/**
	 * @brief operator for setting the chosen mode to a specific value (can be out of range), if necessary
	 * @param val value to set mode to
	 * @return    this
	 */
	inline ui_selection& operator=(modes val) {
		mode_ = val;
		return *this;
	}

	/// @brief conversion to mode
	inline operator modes() const {
		return mode_;
	}

	/// @brief renders the ui elements
	void draw_window();

	/**
	 * @brief handles the functionality of 'utility' buttons
	 * @return true if either button was pressed
	 */
	bool repeat_button_select();

	/**
	 * @brief one iteration of the gui render cycle
	 * @return true if no mode was chosen
	 */
	bool gui_loop();
};
