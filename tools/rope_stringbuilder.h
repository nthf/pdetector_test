#pragma once

#include <memory>
#include <vector>
#include <iostream>

namespace rope {
	static constexpr auto REN = "\n";
	// rope implementation from https://github.com/wroever/rope
	class rope_stringbuilder : public std::ostream {
	private:
		class rope_node {
		public:
			using node_p = std::unique_ptr<rope_node>;
			rope_node(node_p l, node_p r);

			rope_node(const char* str);
			template<typename T> rope_node(const T& str)
			    : weight_(0), left_(nullptr), right_(nullptr), fragment_(std::to_string(str)) {
				weight_ = fragment_.length();
			}
			rope_node(const std::string& str) : weight_(str.length()), left_(nullptr), right_(nullptr), fragment_(str) {}
			rope_node(const rope_node&);

			[[nodiscard]] size_t length() const;
			[[nodiscard]] char char_by_index(size_t) const;
			[[nodiscard]] std::string substring(size_t start, size_t len) const;
			[[nodiscard]] std::string to_string() const;

			friend std::pair<node_p, node_p> split(node_p, size_t);

			[[nodiscard]] size_t depth() const;
			void leaves(std::vector<rope_node *>& v);
		private:
			[[nodiscard]] bool is_leaf() const;

			size_t weight_{};
			node_p left_{};
			node_p right_{};
			std::string fragment_{};
		};
		using node_p = std::unique_ptr<rope_node>;
		friend std::pair<node_p, node_p> split(node_p, size_t);
	public:
		rope_stringbuilder();
		template<typename T>
		rope_stringbuilder(const T& str) {
			this->root_ = std::make_unique<rope_node>(str);
		}

		rope_stringbuilder(const rope_stringbuilder& r);
		[[nodiscard]] std::string str() const;
		[[nodiscard]] size_t length() const;
		char at(size_t index) const;
		[[nodiscard]] std::string substring(size_t start, size_t len) const;
		[[nodiscard]] bool is_balanced() const;
		void balance();

		void insert(size_t i, const std::string& str);
		void insert(size_t i, const rope_stringbuilder& r_sb);
		template<typename T>
		void append(const T& str) {
			rope_stringbuilder tmp = rope_stringbuilder(str);
			this->root_ = std::make_unique<rope_node>(std::move(this->root_), std::move(tmp.root_));
		}
		void rdelete(size_t start, size_t len);

		rope_stringbuilder& operator=(const rope_stringbuilder& rhs);
		inline bool operator==(const rope_stringbuilder& rhs) const {
			return this->str() == rhs.str();
		}
		inline bool operator!=(const rope_stringbuilder& rhs) const {
			return !(*this == rhs);
		}
		friend std::ostream& operator<<(std::ostream& out, const rope_stringbuilder& r) {
			return out << r.str();
		}

		inline void dump() {
			std::cout << *this << std::flush;
			clear();
		}

		inline void clear() {
			root_.reset();
			root_ = std::make_unique<rope_node>("");
		}

	private:
		node_p root_;

	public:
		inline bool empty() const {
			return bool(root_);
		}
	};

	size_t fib(size_t n);
	std::vector<size_t> build_fib_list(size_t len);

	rope_stringbuilder& operator<<(rope_stringbuilder& out, bool v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, char v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, int v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, long v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, long long v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned char v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned int v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned long v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, unsigned long long v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, float v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, double v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, long double v);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, const std::string& str);
	rope_stringbuilder& operator<<(rope_stringbuilder& out, const char* str);
}

//template<> inline rope& operator<<(rope& out, const std::string str) {
//	out.append(str);
//	return out;
//}
