#include "advertisement.h"

void advertisement_bank::add(std::unique_ptr<advertisement> ad) {
	auto it = bank_.find({ad->get_name(), ad->duration()});
	if (it != bank_.end() && it->second->duration_ == ad->duration_) {
		return;
	}
	std::filesystem::path name(ad->get_name());
	name = name.stem();
	int duration = ad->duration_;
	// bank_[ad.get_name()] = ad;
	ad->id(indexer_.size());
	bank_.insert({{ad->get_name(), duration}, std::move(ad)});
	indexer_.emplace_back(name, duration);
}

std::unique_ptr<advertisement> advertisement_bank::remove(std::string& ad) {
	auto it = find_any(ad);
	if (it == bank_.end()) {
		return nullptr;
	}
	std::filesystem::path name(ad);
	name = name.stem();
	// bank_[ad.get_name()] = ad;
	(void)std::remove_if(indexer_.begin(), indexer_.end(), [&](const name_duration& n) { return n.name() == name;} );

	std::unique_ptr<advertisement> ret = std::move(it->second);
	bank_.erase(it);
	return ret;
}

void advertisement::init_(std::filesystem::path& path) {
	std::filesystem::path extension = path.extension();
	if (extension == ".png") {
		image_ = cv::imread(path.string());
		if (image_.empty()) {
			throw advertisement_exception(advertisement_exception::ad_except::BAD_INIT);
		}

		total_frames_ = 1;

		t_ = type::image;
	} else if (extension == ".avi") {
		video_ = cv::VideoCapture(path.string());
		if (!video_.isOpened()) {
			throw advertisement_exception(advertisement_exception::ad_except::BAD_INIT);
		}
		video_ >> image_; // loading the first frame and setting video back to frame 0
		video_.set(cv::CAP_PROP_POS_FRAMES, 0);

		fourcc_ = static_cast<int>(video_.get(cv::CAP_PROP_FOURCC));
		fps_ = static_cast<int>(video_.get(cv::CAP_PROP_FPS));

		total_frames_ = duration_ * fps_;

		t_ = type::video;
	} else {
		throw advertisement_exception(advertisement_exception::ad_except::BAD_INIT);
	}

	w_ = image_.cols;
	h_ = image_.rows;
	path_ = path;
	valid_ = true;
}
