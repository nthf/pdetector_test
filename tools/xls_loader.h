#pragma once

#include <iostream>
#include <unordered_set>
#include "utility.h"
#pragma pack(push, 1)
#include "xlsreader/XlsReader.h"
#pragma pack(pop)

/**
 * @brief lists the indices of advertisement properties
 *        as they appear in the content table
 */
enum class table_indices__ {
	MATCH_INDEX = 0,
	ROT_PHASE_INDEX = 1,
	START_TIME_INDEX = 2,
	SLOT_POS_INDEX = 3,
	DURATION_INDEX = 4,
	FILE_NAME_INDEX = 5,
	// BLANK_INDEX = 6, // column empty
	SPONSOR_INDEX = 7,
	CAMPAIGN_INDEX = 8
};

/**
 * @brief currently unused, lists the different phases of a match
 */
enum class Phase {
	PREMATCH,
	WALKON,
	LINEUP,
	HANDSHAKE,
	FIRST_HALF,
	HALF_TIME,
	SECOND_HALF,
	BREAK,
	FIRST_HALF_EXTRA,
	HALF_TIME_EXTRA,
	SECOND_HALF_EXTRA,
	POST_MATCH,
	OTHER
};

/**
 * @brief struct to hold an integer, float or a string for easier
 *        manipulation with a content table
 */
struct any {
	/**
	 * @brief enum to determine what this struct should act as
	 */
	enum type { Int, Float, String };
	/**
	 * @brief constructor to make a new integer any
	 * @param e value of any
	 */
	inline any(int e) {
		m_data.INT = e;
		m_type = Int;
	}
	/**
	 * @brief constructor to make a new float any
	 * @param e value of any
	 */
	inline any(float e) {
		m_data.FLOAT = e;
		m_type = Float;
	}

	// inline any(char* e) { m_data.STRING = e; m_type = String;}

	/**
	 * @brief constructor to make a new char* any
	 * @param e value of any
	 */
	inline any(const char* e) {
		STRING = std::string(e);
		m_type = String;
	}
	/**
	 * @brief returns type of any
	 * @return type of any
	 */
	[[nodiscard]] inline type get_type() const { return m_type; }
	/**
	 * @brief returns data of any as integer
	 * @return data of any as integer
	 */
	[[nodiscard]] inline int get_int() const { return m_data.INT; }
	/**
	 * @brief returns data of any as float
	 * @return data of any as float
	 */
	[[nodiscard]] inline float get_float() const { return m_data.FLOAT; }
	/**
	 * @brief returns data of any as char*
	 * @return data of any as char*
	 */
	[[nodiscard]] const char* get_string() const { return STRING.c_str(); }

	/**
	 * @brief overload for easier printing of any to ostream
	 * @param str destination output stream
	 * @param obj any to send to output stream
	 * @return    stream str
	 */
	inline friend std::ostream& operator<<(std::ostream& str, const any& obj) {
		switch (obj.m_type) {
		case any::type::Int:
			str << obj.m_data.INT;
			break;
		case any::type::Float:
			str << obj.m_data.FLOAT;
			break;
		case any::type::String:
			str << obj.STRING;
			break;
		}
		return str;
	}

private:
	/// @brief type of any
	type m_type;
	/// @brief union to hold int and float
	union {
		int INT;
		float FLOAT;
	} m_data{};
	/// @brief member string to use if any is string
	std::string STRING;
};

class content_table_row {
	/// @brief row from a content table
	std::vector<any> row_;
public:
	/**
	 * @brief row getter
	 * @return row
	 */
	[[nodiscard]] inline std::vector<any> row() const { return row_; }

	/**
	 * @brief copy constructor
	 * @param other a different row
	 */
	inline content_table_row(const content_table_row& other)
	    : content_table_row(other.row()) {}

	content_table_row(content_table_row&& other) = default;
	content_table_row& operator=(content_table_row&& other) = default;
	content_table_row& operator=(const content_table_row& other) = default;

	/**
	 * @brief constructor that takes a row in the form of vector<any>
	 * @param row row to turn into an instance of this
	 */
	inline content_table_row(std::vector<any> row) : row_(std::move(row)) {}

	content_table_row() = default;

	/**
	 * @brief add any item to the end of the row
	 * @param item item to add to the row
	 */
	inline void push_back(const any& item) { row_.push_back(item); }

	/// @brief clear removes all items from the row
	inline void clear() { row_.clear(); }

	/**
	 * @brief overload for ease of access
	 * @param index index to desired value from the row
	 * @return      any which was found at index
	 */
	inline any& operator[](int index) { return row_[index]; }

	/**
	 * @brief overload for ease of access, taking enum class instead of an integer
	 * @param index pre-defined index from table_indices__ enum class
	 * @return      any which was found at index
	 */
	inline any& operator[](table_indices__ index) {
		return row_[static_cast<int>(index)];
	}

	/**
	 * @brief const overload for ease of access
	 * @param index index to desired value from the row
	 * @return      any which was found at index
	 */
	const any& operator[](int index) const { return row().at(index); }

	/**
	 * @brief const overload for ease of access, taking enum class instead of integer
	 * @param index pre-defined index from table_indices__ enum class
	 * @return      any which was found at index
	 */
	const any& operator[](table_indices__ index) const {
		return row().at(static_cast<int>(index));
	}

	using iterator = std::vector<any>::iterator;
	using const_iterator = std::vector<any>::const_iterator;

	/**
	 * @brief returns begin iterator of row
	 * @return begin iterator of row
	 */
	inline iterator begin() { return row_.begin(); }

	/**
	 * @brief returns const begin iterator of row
	 * @return const begin iterator of row
	 */
	[[nodiscard]] const_iterator begin() const { return row_.begin(); }

	/**
	 * @brief returns end iterator of row
	 * @return end iterator of row
	 */
	inline iterator end() { return row_.end(); }

	/**
	 * @brief returns const end iterator of row
	 * @return const end iterator of row
	 */
	[[nodiscard]] const_iterator end() const { return row_.end(); }

	virtual ~content_table_row() = default;
};

/**
 * @brief content_exception to handle invalid requests on table
 */
class content_exception : public std::exception {
	/// @brief error message
	std::string msg;

public:
	/**
	 * @brief constructor that takes any string and makes it
	 *        into the exception message
	 * @param cause string to use as exception message
	 */
	content_exception(std::string cause) : msg(std::move(cause)) {}

	/**
	 * @brief override of std::exception::what() to return message specific
	 *        to content table requests
	 * @return error message
	 */
	[[nodiscard]] const char* what() const noexcept override { return msg.c_str(); }
};

/**
 * @brief struct to make unordered sets of std::pair<std::string, int>> possible
 */
struct pair_hash {
	/**
	 * @brief overload required to create
	 *        std::unordered_set<std::pair<std::string, int>>,
	 *        calculates the hash of this pair
	 * @param p pair to get the hash of
	 * @return  simple hash of std::pair<std::string, int>
	 */
	std::size_t operator()(const std::pair<std::string, int>& p) const {
		return std::hash<std::string>{}(p.first) + std::hash<int>{}(p.second);
}
};

using unique_content = std::unordered_set<std::pair<std::string, int>, pair_hash>;

/**
 * @brief class to hold the data from an .xls file
 */
class content_table {
	/**
	 * @brief content table, holds data about the advertisements and their
	 *        order of appearance
	 */
	std::vector<content_table_row> table_;
	/**
	 * @brief set to keep track of which advertisements appeared in
	 *        the content table
	 */
	unique_content unique_files_;

public:
	/**
	 * @brief checks if an advertisement already appeared in the
	 *        content table and is therefore present among the unique_content
	 * @param key key to look for in the unique_content, this is the name of the
	 *            advertisement without the extension
	 * @param uc unique_content to search through
	 * @return   true if uc contains a std::pair<std::string, int> where string is key
	 */
	static bool uc_contains(const std::string& key, const unique_content& uc) {
		for (const auto& kp : uc) {
			if (std::filesystem::path(kp.first).stem() == key) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @brief returns the size of the content table
	 * @return size of the content table
	 */
	size_t size() const { return table_.size(); }

	/**
	 * @brief returns begin iterator of content table
	 * @return begin iterator of content table
	 */
	inline std::vector<content_table_row>::iterator begin() {
		return table_.begin();
	}

	/**
	 * @brief returns const begin iterator of content table
	 * @return const begin iterator of content table
	 */
	std::vector<content_table_row>::const_iterator begin() const {
		return table_.begin();
	}

	/**
	 * @brief returns end iterator of content table
	 * @return const end iterator of content table
	 */
	inline std::vector<content_table_row>::iterator end() { return table_.end(); }

	/**
	 * @brief returns const begin iterator of content table
	 * @return const end iterator of row
	 */
	std::vector<content_table_row>::const_iterator end() const {
		return table_.end();
	}

	inline content_table() = default;

	/**
	 * @brief overload for copy assignment
	 * @param other another table to copy from
	 * @return      this table
	 */
	inline content_table& operator=(const content_table& other) = default;

	content_table(const content_table& other) = default;
	content_table(content_table&& other) = default;
	content_table& operator=(content_table&& other) = default;
	~content_table() = default;

	/**
	 * @brief adds a row to the table
	 * @param row row that should be added
	 */
	void add(content_table_row& row);

	/**
	 * @brief attempts to add a path-duration pair to unique content
	 * @param path     path to an advertisement
	 * @param duration duration of the advertisement
	 */
	inline void add_to_unique(const std::string& path, int duration) {
		unique_files_.emplace(path, duration);
	}

	/**
	 * @brief checks whether the table is empty
	 * @return true if table is empty
	 */
	bool empty() const { return table_.empty(); }

	/**
	 * @brief returns the number of rows in the table
	 * @return number of rows in the table
	 */
	int rows() const { return static_cast<int>(table_.size()); }

	/**
	 * @brief overload for ease of access to the content table
	 * @param index index for accessing a table row
	 * @return      row from content table found at index
	 */
	inline content_table_row& operator[](int index) { return table_[index]; }

	/**
	 * @brief const overload for ease of access to the content table
	 * @param index index for accessing a table row
	 * @return      row from content table found at index
	 */
	const content_table_row& operator[](int index) const {
		return table_.at(index);
	}

	/**
	 * @brief returns the first row of the content table
	 * @return first row of the content table
	 */
	const content_table_row& first_row() const {
		if (table_.empty()) {
			throw content_exception("table is empty, could not get first row");
		}
		return table_.front();
	}

	/**
	 * @brief returns the unique content collection
	 * @return unique content collection
	 */
	const unique_content& unique_files() const { return unique_files_; }
};

/**
 * @brief class for loading the content table
 */
class content_table_loader {
	/// @brief path to the file system location of the content table xls
	std::filesystem::path path_;

	/// @brief collection to hold content table data
	content_table table_ = content_table();

	/**
	 * @brief cols_num return the number of colums in the content table
	 * @param wb       workbook, structure used by xlsreader which holds the table data
	 * @param sheetnum sheet number to work with, usually 0
	 * @return         number of colums of the content workbook's sheetnum sheet
	 */
	int get_cols_num(xls::WorkBook& wb, uint sheetnum);

	/// @brief read_xls controls parsing of the content table
	void read_xls();

	/**
	 * @brief constructor, automatically starts reading the
	 *        xls file provided in parameter path
	 * @param path    path to xls file
	 * @param pv_from extension of unmodified advertisement video files
	 * @param pv_to   extension of modified advertisement video files
	 * @param pi_from extension of unmodified advertisement image files
	 * @param pi_to   extension of modified advertisement image files
	 */
	inline content_table_loader(std::filesystem::path& path) : path_(path) {
		read_xls();
	}

	/**
	 * @brief returns the table
	 * @return table
	 */
	content_table table() const { return table_; }

public:
	/**
	 * @brief preferred way of initializing the loader, performs checks
	 *        and creates a unique pointer, will automatically read provided xls file if
	 *        it is valid as part of constructor
	 * @return unique pointer to a content table
	 */
	static std::unique_ptr<content_table> load_xls(
	        const std::filesystem::path& path) {
		std::filesystem::path xls_path(path);
		for (auto& p : std::filesystem::directory_iterator(path)) {
			if (p.path().extension() == ".xls") {
				xls_path = p.path();
				break;
			}
		}

		if (xls_path.extension() != ".xls") {
			return nullptr;
		}

		try {
			content_table_loader cl(xls_path);
			return std::make_unique<content_table>(cl.table());
		} catch (std::string& err) {
			return nullptr;
		}
		return nullptr;
	}
};
