#pragma once
#include <iostream>
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

class motion_deblurrer {
	static void calculate_psf_(cv::Mat& img_out, cv::Size filter_size, int len, double theta) {
		cv::Mat h(filter_size, CV_32F, cv::Scalar(0));
		cv::Point point(filter_size.width / 2, filter_size.height / 2);
		ellipse(h, point, cv::Size(0, cvRound(float(len) / 2.0)), 90.0 - theta, 0, 360, cv::Scalar(255), cv::FILLED);
		cv::Scalar summa = sum(h);
		img_out = h / summa[0];
	}

	static void fftshift_(const cv::Mat& img_in, cv::Mat& img_out) {
		img_out = img_in.clone();
		int cx = img_out.cols / 2;
		int cy = img_out.rows / 2;
		cv::Mat q0(img_out, cv::Rect(0, 0, cx, cy));
		cv::Mat q1(img_out, cv::Rect(cx, 0, cx, cy));
		cv::Mat q2(img_out, cv::Rect(0, cy, cx, cy));
		cv::Mat q3(img_out, cv::Rect(cx, cy, cx, cy));
		cv::Mat tmp;
		q0.copyTo(tmp);
		q3.copyTo(q0);
		tmp.copyTo(q3);
		q1.copyTo(tmp);
		q2.copyTo(q1);
		tmp.copyTo(q2);
	}

	static void filter_2d_freq_(const cv::Mat& img_in, cv::Mat& img_out, const cv::Mat& h) {
		cv::Mat planes[2] = { cv::Mat_<float>(img_in.clone()), cv::Mat::zeros(img_in.size(), CV_32F) };
		cv::Mat complexI;
		merge(planes, 2, complexI);
		dft(complexI, complexI, cv::DFT_SCALE);
		cv::Mat planesH[2] = { cv::Mat_<float>(h.clone()), cv::Mat::zeros(h.size(), CV_32F) };
		cv::Mat complexH;
		merge(planesH, 2, complexH);
		cv::Mat complexIH;
		mulSpectrums(complexI, complexH, complexIH, 0);
		idft(complexIH, complexIH);
		split(complexIH, planes);
		img_out = planes[0];
	}

	static void calculate_wnr_filter_(const cv::Mat& input_h_psf, cv::Mat& output_g, double nsr) {
		cv::Mat h_psf_shifted;
		fftshift_(input_h_psf, h_psf_shifted);
		cv::Mat planes[2] = { cv::Mat_<float>(h_psf_shifted.clone()), cv::Mat::zeros(h_psf_shifted.size(), CV_32F) };
		cv::Mat complexI;
		merge(planes, 2, complexI);
		dft(complexI, complexI);
		split(complexI, planes);
		cv::Mat denom;
		pow(abs(planes[0]), 2, denom);
		denom += nsr;
		divide(planes[0], denom, output_g);
	}

	static void edgetaper_(const cv::Mat& img_in, cv::Mat& img_out, double gamma = 5.0, double beta = 0.2) {
		int nx = img_in.cols;
		int ny = img_in.rows;
		cv::Mat w1(1, nx, CV_32F, cv::Scalar(0));
		cv::Mat w2(ny, 1, CV_32F, cv::Scalar(0));
		float* p1 = w1.ptr<float>(0);
		float* p2 = w2.ptr<float>(0);
		float dx = float(2.0 * CV_PI / nx);
		float x = float(-CV_PI);
		for (int i = 0; i < nx; ++i) {
			p1[i] = float(0.5 * (tanh((x + gamma / 2) / beta) - tanh((x - gamma / 2) / beta)));
			x += dx;
		}
		float dy = float(2.0 * CV_PI / ny);
		float y = float(-CV_PI);
		for (int i = 0; i < ny; ++i) {
			p2[i] = float(0.5 * (tanh((y + gamma / 2) / beta) - tanh((y - gamma / 2) / beta)));
			y += dy;
		}
		cv::Mat w = w2 * w1;
		multiply(img_in, w, img_out);
	}

public:
	static int deblur(int len, double theta, int snr, cv::Mat& img_in, cv::Mat& img_out) {
		cv::Mat img_in_cpy;
		img_in.copyTo(img_in_cpy);
		if (img_in_cpy.empty()) {
			std::cout << "empty image given to motion deblurrer" << std::endl;
			return -1;
		}
		cv::Mat img_res;
		cv::Rect roi = cv::Rect(0, 0, img_in_cpy.cols & -2, img_in_cpy.rows & -2);
		cv::Mat Hw, h;
		calculate_psf_(h, roi.size(), len, theta);
		calculate_wnr_filter_(h, Hw, 1.0 / double(snr));
		img_in_cpy.convertTo(img_in_cpy, CV_32F);
		edgetaper_(img_in_cpy, img_in_cpy);
		filter_2d_freq_(img_in_cpy(roi), img_res, Hw);
		img_res.convertTo(img_res, CV_8U);
		normalize(img_res, img_res, 0, 255, cv::NORM_MINMAX);
		img_res.copyTo(img_out);
		return 0;
	}
};





