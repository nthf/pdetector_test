﻿#include "clipper/clipper.hpp"
#include "feature_analyzer.h"
#include "lsd.h"
#include "video_player.h"

#include <condition_variable>
#include <omp.h>

using namespace cv;
using namespace std;
using namespace rope;
using namespace quick_access_utility;
using namespace ClipperLib;

typedef fs_context CTX;

ofstream feature_analyzer::ofs_cpu_log("../results/performance.txt");

ifstream feature_analyzer::ifs_comparison("../results/comp.txt");
ifstream feature_analyzer::ifs_results_groundtruth("../results/res_gt_cmp.txt");
rope_stringbuilder feature_analyzer::output_stringbuilder;
std::vector<result> feature_analyzer::full_result_history;

void feature_analyzer::pause_listener() {
	while (true) {
		if (auto key = waitKey() >= 0) {
			pauseflag = true;
			if (auto key = waitKey() == ' ') {
				pauseflag = false;
			}
		}
	}
}

bool feature_analyzer::pause() {
	auto key = cv::waitKey(0);
	switch (key) {
	case 'e':
		// handle export logic here?
		ctx.export_data();
		output_stringbuilder << lmagenta << "files exported\n" << reset;
		return false;
	case ',':
		ctx.backtrack();
		full_result_history.erase(full_result_history.end() - min(2ul, full_result_history.size()), full_result_history.end());
		return true;
	case '.':
		ctx.force_recalc();
		return true;
	case 'q':
		throw exit_exception();
		break;
	default:
		break;
	}
	return false;
}

int feature_analyzer::analyze(uchar excluded_hue, bool load_files_from_config/* = false*/) {
	ctx = CTX::initialize_context(load_files_from_config);

	scalespace_analyzer ssd;
	ssd.set_exclusion(excluded_hue);
	ctx.excluded_hue = excluded_hue;

	if (ctx.is_video) {
		int frame_skip = 0; // used when debugging to skip n frames at the beginning

		for (int i = 0; i < frame_skip; ++i) {
			string s;
			getline(ifs_comparison, s);
		}

		ctx.video_capture_v[CTX::IDX_RECORD].set(CAP_PROP_POS_FRAMES, frame_skip);
		ctx.video_capture_v[CTX::IDX_ADVERT].set(CAP_PROP_POS_FRAMES, frame_skip);
		ctx.frame_n = frame_skip;

		// advance by one frame so the previous one can be used for comparison
		// with video feed - wait one frame
		ctx.video_capture_v[CTX::IDX_RECORD] >> ctx.original_img_v[CTX::IDX_RECORD];
		ctx.video_capture_v[CTX::IDX_ADVERT] >> ctx.original_img_v[CTX::IDX_ADVERT];

		full_result_history.push_back(result()); // needed for correct synchronization with ground truth
		// it is modified to match the first real result before the end

		std::thread pause_thread(&feature_analyzer::pause_listener, this);
		pause_thread.detach();
	}

	if (RUN_MULTITHREAD__) {
		multithreaded_pass(ctx, ssd);
	} else {
		try {
			singlethreaded_pass(ctx, ssd);
		} catch (const exit_exception& ex) {
			return 0;
		}
	}

	return 0;
}

CTX CTX::initialize_context(bool load_files_from_config/* = false*/) {
	//string load_file_list = "../data/prev.txt";

	CTX new_ctx;

	new_ctx.fpath_v.resize(CTX::FILE_COUNT_MAX);
	if (load_files_from_config && std::filesystem::exists(paths::PREV_LOAD_TXT)) {
		std::ifstream fileload(paths::PREV_LOAD_TXT);
		std::getline(fileload, new_ctx.fpath_v[CTX::IDX_RECORD]);
		std::getline(fileload, new_ctx.fpath_v[CTX::IDX_ADVERT]);
	}
	if (new_ctx.fpath_v[CTX::IDX_RECORD].empty() || new_ctx.fpath_v[CTX::IDX_ADVERT].empty()) {
		utility::open_file(new_ctx.fpath_v[CTX::IDX_RECORD]);
		utility::open_file(new_ctx.fpath_v[CTX::IDX_ADVERT]);
		std::ofstream os_load_file_list(paths::PREV_LOAD_TXT);
		os_load_file_list << new_ctx.fpath_v[CTX::IDX_RECORD] << REN;
		os_load_file_list << new_ctx.fpath_v[CTX::IDX_ADVERT] << REN;
	}

	new_ctx.original_img_v.resize(CTX::FILE_COUNT_MAX);
	new_ctx.original_img_v[CTX::IDX_RECORD] = imread(new_ctx.fpath_v[CTX::IDX_RECORD]);
	new_ctx.original_img_v[CTX::IDX_ADVERT] = imread(new_ctx.fpath_v[CTX::IDX_ADVERT]);

	new_ctx.is_video = new_ctx.original_img_v[CTX::IDX_RECORD].empty(); // image won't be read if path leads to a video

	new_ctx.fdetector = ORB::create(4000, 1.5, 8, 4, 0, 2, ORB::HARRIS_SCORE, 15, 15);
	new_ctx.dmatcher = DescriptorMatcher::create("BruteForce-Hamming");

	if (new_ctx.is_video) {
		new_ctx.video_capture_v = { VideoCapture(new_ctx.fpath_v[CTX::IDX_RECORD]),
		                            VideoCapture(new_ctx.fpath_v[CTX::IDX_ADVERT]),
		                            VideoCapture(new_ctx.fpath_v[CTX::IDX_RECORD]) };
		new_ctx.modified_img_v.resize(new_ctx.video_capture_v.size());
	} else {
		new_ctx.modified_img_v.resize(new_ctx.fpath_v.size());
	}

	new_ctx.result_history.resize(feature_analyzer::history_size);

	if (!new_ctx.is_video) {
		new_ctx.result_mat = give_mat<pixel>(new_ctx.original_img_v[CTX::IDX_RECORD]);
	} else {
		new_ctx.result_mat = give_mat<pixel>(
		                          new_ctx.video_capture_v[CTX::IDX_RECORD].get(cv::CAP_PROP_FRAME_HEIGHT),
		                          new_ctx.video_capture_v[CTX::IDX_RECORD].get(cv::CAP_PROP_FRAME_WIDTH));
		new_ctx.fps = new_ctx.video_capture_v.front().get(CAP_PROP_FPS);
		new_ctx.delta_t = 1000.0 / new_ctx.fps;
	}

	return new_ctx;
}

void feature_analyzer::singlethreaded_pass(CTX& ctx, scalespace_analyzer& ssd) {
	while (true) {
		Mat cropped, current_b;

		if (ctx.frame_n == 2130) {
			pauseflag = true;
		}

		while (pauseflag) {
			if (pause()) {
				break;
			}
		}

		if (process_current_frame(ctx, true, cropped, current_b) == loop_status::FS_BREAK) {
			break;
		}

		output_stringbuilder << "result: " << (ctx.previous_result ? lgreen : lred) <<
		               ctx.previous_result << reset << "\n";

		if (ctx.previous_result == fs_result::v::MAYBE_FALSE) {
			utility::depad_image(cropped, cropped, padding);
			if (!cropped.empty()) {
				if (do_the_scalesauce_dance(ssd, cropped, current_b)) {
					ctx.previous_result = fs_result::v::TRUE;
					ctx.result_history.back() = true;
					output_stringbuilder << lgreen << "ss successful" << reset << " // new result: " << lgreen << ctx.previous_result << reset << "\n";
				} else {
					output_stringbuilder << lred << "ss unsuccessful\n" << reset;
				}
			}
		}

		output_stringbuilder.dump();

		//string s;
		//is_comparison.getline(&s[0], 6);

		//if (s[0] == '0' && s[2] == '?' && s[4] == '1') {
		//	show_im("input a dbg", cropped);
		//	key_wait();
		//}

		//if (range<int>::test(num, 5548, 5766)) {
		//	auto key = key_wait();
		//	new_test_gt << num << "=" << swkey(key) << endl;
		//} else {
		//	new_test_gt << "\\\\" << endl;
		//}
	}
	full_result_history.front().good() = full_result_history[1].good();
	save_results_and_compare_to_gt();
}

template<typename func>
vector<Point> reduce_contours(const vector<Point>& contours, const func& op, int min_distance) {
	vector<Point> new_contours;
	new_contours.push_back(contours.front());
	for (size_t i = 1; i < contours.size() - 1; ++i) {
		Point p1 = op(contours[i - 1]);
		Point p2 = op(contours[i]);
		Point p3 = op(contours[i + 1]);
		int dx1 = abs(p2.x - p1.x);
		int dy1 = abs(p2.y - p1.y);

		int dx2 = abs(p3.x - p1.x);
		int dy2 = abs(p3.y - p1.y);
		if ((p1 == p2 && p1 != p3) || (p2 == p3 && p1 != p3) ||
		     ((dx2 * dy2 == 1) ||
		     ((dx1 * dy1 == 1) && utility::distance(p2, new_contours.back()) > min_distance))
		        ) {
			new_contours.push_back(p2);
		}
	}
	//new_contours.push_back(contours.back());
	return new_contours;
}

vector<Point> reduce_contours(const vector<Point>& contours, int min_distance) {
	return reduce_contours(contours, [](const Point& p){ return p; }, min_distance);
}

void feature_analyzer::create_field_mask(Mat& src, Mat& fieldmask) {
	Mat redmask;
	auto splt = utility::mat_split(src);
	cv::threshold(splt[2], redmask, 50, 255, THRESH_BINARY);
	Mat gminbminr = ((2 * splt[1] - splt[0]) - splt[2]) & redmask;
	threshold(gminbminr, fieldmask, 15, 255, THRESH_BINARY);
	morphologyEx(fieldmask, fieldmask, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(13, 13)));
	morphologyEx(fieldmask, fieldmask, MORPH_CLOSE, getStructuringElement(MORPH_ELLIPSE, Size(29, 29)));
	erode(fieldmask, fieldmask, getStructuringElement(MORPH_ELLIPSE, Size(9, 9)));

	int n = fieldmask.cols * fieldmask.rows;
	double fillratio = static_cast<double>(countNonZero(fieldmask)) / n;

	vector<vector<Point>> contours;
	Mat fieldmask_original;
	fieldmask.copyTo(fieldmask_original);
	gauss_iir(fieldmask, fieldmask, 11);
	morphologyEx(fieldmask, fieldmask, MORPH_CLOSE, getStructuringElement(MORPH_ELLIPSE, Size(255, 15)), Point(-1, -1), 1, BORDER_REPLICATE);
	morphologyEx(fieldmask, fieldmask, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(81, 9)), Point(-1, -1), 1, BORDER_REPLICATE);

	threshold(fieldmask, fieldmask, 127, 255, THRESH_BINARY);
	Mat fieldmask_difference = fieldmask_original ^ fieldmask;
	int holes_area = countNonZero(fieldmask_difference);
	if (holes_area > 6000) {
		return;
	}
	Mat invfield = give_mat<uchar>(fieldmask, 255) - fieldmask;
	cv::findContours(invfield, contours, RETR_LIST, CHAIN_APPROX_NONE);
	Mat complement = give_mat<uchar>(fieldmask);
	int largest_contour_area = 0;
	const vector<Point>* largest_contour = nullptr;
	int carea = std::accumulate(contours.begin(), contours.end(), 0, [&largest_contour_area, &largest_contour](int total, const vector<Point>& c){
		int contour_area = contourArea(c);
		if (contour_area > largest_contour_area) {
			largest_contour_area = contour_area;
			largest_contour = &c;
		}
		return total + contour_area;
	});
	carea -= largest_contour_area;


	double gap_size = max(carea > (0.0175 * n) ? 0.75 * fieldmask.rows : 0.05 * fieldmask.rows, 75 * (1 - (fillratio * fillratio)));
	//Point inside_complement{};
	Point inside_point{};
	if (largest_contour != nullptr) {
		vector<Point> reduced_contours = reduce_contours(*largest_contour, [gap_size](const Point& p){ return Point(p.x, max(0, static_cast<int>(p.y - gap_size))); }, 16);
		for (size_t i = 1; i < reduced_contours.size(); ++i) {
			line(complement, reduced_contours[i - 1], reduced_contours[i], Scalar::all(255));
		}

		inside_point = utility::triangulation<pair<Point, double>>(reduced_contours, complement, [&reduced_contours](const vector<Point>& triangle, pair<Point, double>& largest_triangle_center){
			vector<pair<line_segment, Point>> edges{{{triangle[0], triangle[1]}, triangle[2]},
				                                    {{triangle[0], triangle[2]}, triangle[1]},
				                                    {{triangle[1], triangle[2]}, triangle[0]}}; // save edges and the opposite point for getting base and height of triangle
			sort(edges.begin(), edges.end(), [](const pair<line_segment, Point>& a, const pair<line_segment, Point>& b){ return a.first.magnitude() > b.first.magnitude(); });
			line_segment& base = edges.front().first;
			double h = base.shortest_distance_to_point(edges.front().second);
			Point2d triangle_center = utility::sum_pts<Point2d>(triangle) / 3.0;
			double triangle_size = (base.magnitude() * h) / 2.0;
			if (triangle_size > largest_triangle_center.second && polygon::point_in_polygon(triangle_center, reduced_contours)) {
				largest_triangle_center.second = triangle_size;
				largest_triangle_center.first = triangle_center;
			}
		}).first;
	}

	if (largest_contour_area < 0.9 * n && countNonZero(complement) > 0) {
		floodFill(complement, inside_point, Scalar(255));
	} else {
		complement *= 0;
	}
	Mat fullmask = give_mat<uchar>(fieldmask);
	if (countNonZero(complement) < countNonZero(fieldmask)) {
		fieldmask = fieldmask + complement;
	}
}

void feature_analyzer::perform_lsd(CTX& ctx) {
	Mat src;
	ctx.modified_img_v.front().copyTo(src);
	utility::depad_image(src, src, padding);

	create_field_mask(src, ctx.field_mask);
	int fmask_count = countNonZero(ctx.field_mask);
	if (fmask_count < 0.4 * ctx.field_mask.cols * ctx.field_mask.rows) {
		throw "close-up shot"; // possibly do something else in this case
		// might also catch other things accidentally needs testing
	}
	cerr << cyan << fmask_count << reset << endl;

	//show_im("fieldmask", ctx.field_mask);

	Mat inv_fieldmask = give_mat<uchar>(ctx.field_mask, 255) - ctx.field_mask;
	Mat test;
	src.copyTo(test, inv_fieldmask);

	cvtColor(test, src, COLOR_BGR2Lab);
	vector<Mat> splet;
	split(src, splet);

	splet[0].convertTo(src, -1, 1.9); // helps a lot

	vector<double> w, p, n;
	/**
	* width     Vector of widths of the regions, where the lines are found. E.g. Width of line.
	* prec      Vector of precisions with which the lines are found.
	* nfa       Vector containing number of false alarms in the line region, with precision of 10%.
	*               The bigger the value, logarithmically better the detection.
	*                   * -1 corresponds to 10 mean false alarms
	*                   * 0 corresponds to 1 mean false alarm
	*                   * 1 corresponds to 0.1 mean false alarms
	*               This vector will be calculated _only_ when the objects type is REFINE_ADV
	**/
	Ptr<lsd::LineSegmentDetector> lsd = lsd::createLineSegmentDetectorPtr(LSD_REFINE_ADV);
	vector<Vec4i> lines_for_detector;
	lsd->detect(src, lines_for_detector, w, p, n);
	vector<line_segment> lines;

	// lines are filtered away when too many vertical
	// maybe don't filter based on wpn

	double avg_angle = 0, avg_magni = 0;
	double multiplier = 1.75;
	for (size_t i = 0; i < lines_for_detector.size(); ++i) {
		if (/*p[i] < 0.1 || w[i] < 2 || n[i] < 10 ||*/
		        ctx.field_mask.at<uchar>(lines_for_detector[i](1), lines_for_detector[i](0)) > 0 ||
		        ctx.field_mask.at<uchar>(lines_for_detector[i](3), lines_for_detector[i](2)) > 0) {
			continue;
		}
		line_segment new_line(lines_for_detector[i](0), lines_for_detector[i](1),
		                      lines_for_detector[i](2), lines_for_detector[i](3),
		                      multiplier);
		if (abs(abs(new_line.angle()) - 90) < 6) {
			continue;
		}
		lines.push_back(new_line),
		avg_angle += new_line.angle();
		avg_magni += new_line.magnitude();
	}
	avg_angle /= lines.size();
	avg_magni /= lines.size();
	lines.erase(remove_if(lines.begin(), lines.end(), [&](const line_segment& line){
		return (abs(line.angle() - avg_angle) > 20 ||
		        line.magnitude() < 0.66 * avg_magni);
	}), lines.end());

	vector<Vec4i> lines_to_draw;
	std::transform(lines.begin(), lines.end(), std::back_inserter(lines_to_draw), [](const line_segment& v){ return v.to_cv_vec4(); });

	src.copyTo(ctx.post_lsd_scene);

	lsd->drawSegments(src, lines_to_draw);
	if (src.channels() == 1) {
		cvtColor(src, src, COLOR_GRAY2BGR);
	}
	connect_lines(ctx, src, lines);

	show_im("lsd", src);
	if (key_wait(1) == ' ') {
		key_wait(0);
	}
}

void feature_analyzer::connect_lines(CTX& ctx, Mat& img, vector<line_segment>& lines) {
	sort(lines.begin(), lines.end(), [](const line_segment& a, const line_segment& b){ return a.p().x < b.p().x; });
	deque<line_segment> start_candidates(lines.begin(), lines.end());
	int half_angle = 3;
	line_sequence::append_mode mode = line_sequence::append_mode::CLEAR_AND_CONNECT_MIDDLES;

	vector<line_sequence> line_sequences;
	while(!start_candidates.empty()) {
		line_sequence new_line_sequence{start_candidates.front()};
		line_segment original_line = start_candidates.front();
		start_candidates.pop_front();

		Point spotlight(new_line_sequence.back().q().x, new_line_sequence.back().q().y);
		Point_<double> tail_tip = mode == line_sequence::append_mode::CLEAR_AND_CONNECT_MIDDLES ?
		                            new_line_sequence.back().get_middle() : static_cast<Point_<double>>(spotlight);
		while (true) {
			line_segment next_line = shoot_cone_find_best_point(spotlight, new_line_sequence.back().v(), 125,
			                                                    -half_angle, 2 * half_angle, lines, img, false);
			if (!next_line.is_valid() ||
			        (mode == line_sequence::append_mode::CLEAR_AND_CONNECT_MIDDLES &&
			         abs(next_line.angle() - original_line.angle()) > 10)) {
				if (new_line_sequence.count() > 1) {
					line_sequences.push_back(new_line_sequence);
				}
				new_line_sequence.clear();
				break;
			} else {
				new_line_sequence.append_line(next_line, tail_tip, mode);
			}
			spotlight.x = next_line.q().x;
			spotlight.y = next_line.q().y;
		}
		new_line_sequence.clear();
	}
	sort(line_sequences.begin(), line_sequences.end(), [](const line_sequence& a, const line_sequence& b){ return a.length() > b.length(); });
	auto longest = line_sequences.empty() ? 0 : line_sequences.front().length();
	line_sequences.erase(remove_if(line_sequences.begin(), line_sequences.end(), [longest](const line_sequence& a){ return a.length() < 0.3 * longest; }), line_sequences.end());
	paint_lines(img, line_sequences);
	if (mode == line_sequence::append_mode::CLEAR_AND_CONNECT_MIDDLES) {
		vector<line_segment> line_segments;
		transform(line_sequences.begin(), line_sequences.end(), back_inserter(line_segments), [](const line_sequence& ls){ return ls.back(); });
		get_areas_around_lines(ctx, img, line_segments, 100);
	}
}

// would like to account for zoomed in not picking up top of perimeter
// kind of a problem because can't tell it's zoomed in from here
void feature_analyzer::get_areas_around_lines(CTX& ctx, cv::Mat &img, const std::vector<line_segment> &lines, double h) {
	if (lines.empty()) {
		return;
	}
	Mat mask = give_mat<uchar>(img);
	double half_h = h / 2.0;
	linked_list<pair<vector<polygon>, line_segment>> islands;
	for (auto l = lines.begin(); l < lines.end(); ++l) {
		Vec2d n = l->get_normal();
		polygon poly = {l->p() +  n * half_h,
		                l->p() + -n * half_h,
		                l->q() + -n * half_h,
		                l->q() +  n * half_h};
		Vec2d AD(poly[3].x - poly[0].x, poly[3].y - poly[0].y);
		double b = sqrt(AD(0) * AD(0) + AD(1) * AD(1));
		double area = h * b;
		if (area < 40000) {
			continue;
		}
		vector<vector<Point>> polyv{poly.points()};
		cv::fillPoly(mask, polyv, Scalar::all(255));

		// place this poly into one of possibly existing islands, or make a new island
		auto current_island = islands.first();
		pair<vector<polygon>, line_segment> new_island{{poly}, *l};
		while (current_island) {
			for (const auto& p : current_island->data().first) {
				if (poly.intersects(p)) {
					new_island.first = poly.bool_op(p, ClipperLib::ctUnion);
					new_island.second = new_island.second.average_with(current_island->data().second);
					current_island = islands.erase(current_island);
					break;
				}
			}
			if (!current_island) {
				break;
			}
			current_island = current_island->next();
		}
		islands.push_back(new_island);
	}

	line_segment avg_v(0, 0, 0, 0);
	int count{0};
	auto is = islands.first();
	while (is) {
		avg_v.set_pq(Point(avg_v.p().x + is->data().second.p().x, avg_v.p().y + is->data().second.p().y),
		             Point(avg_v.q().x + is->data().second.q().x, avg_v.q().y + is->data().second.q().y));
		++count;
		is = is->next();
	}
	avg_v /= max(static_cast<double>(count), 1.0);
	avg_v.shrink_expand(h * -0.33);

	if (avg_v.magnitude() < 20 || countNonZero(mask) == 0) { // or if it's too small anyway
		//ctx.modified_img_v[CTX::IDX_RECORD] *= 0;
		return;
	}
	cv::line(mask, avg_v.p(), avg_v.q(), Scalar::all(255), h * 0.66);
	cv::morphologyEx(mask, mask, MORPH_CLOSE, getStructuringElement(MORPH_ELLIPSE, Size(h * 0.25, h * 0.25)));
	Mat masked;
	ctx.original_img_v[CTX::IDX_RECORD].copyTo(masked, mask & (255 - ctx.field_mask));
	//masked.copyTo(ctx.modified_img_v[CTX::IDX_RECORD]);
	//utility::trim(masked, masked); // don't do
	show_im("line areas", masked);
}

bool is_counterclockwise(const Vec2d& v1, const Vec2d& v2) {
	return -v1(0) * v2(1) + v1(1) * v2(0) > 0;
}

bool is_within_radius(const Vec2d& v, double radius_sq) {
	return v(0) * v(0) + v(1) * v(1) <= radius_sq;
}

bool is_in_circle_part(const Vec2d& start, const Vec2d& end, const Vec2d& v, double radius) {
	return (is_counterclockwise(start, v) &&
	        !is_counterclockwise(end, v) &&
	        is_within_radius(v, radius * radius));
}

const line_segment feature_analyzer::shoot_cone_find_best_point(Point& spotlight, const Vec2i& direction,
                                                 double radius, double start_angle, double angle,
                                                 vector<line_segment>& lines, Mat& img, bool visualize_cones /*= false*/) {
	double dir_angle = atan2(direction(1), direction(0));
	start_angle += dir_angle;
	double end_angle = start_angle + angle;
	double srad = utility::deg_to_rad(start_angle);
	double erad = utility::deg_to_rad(end_angle);

	Vec2d start((cos(-srad) * direction(0) - sin(-srad) * direction(1)),
	            (sin(-srad) * direction(0) + cos(-srad) * direction(1)));
	Vec2d end((cos(-erad) * direction(0) - sin(-erad) * direction(1)),
	          (sin(-erad) * direction(0) + cos(-erad) * direction(1)));
	Vec4i tunnel(spotlight.x, spotlight.y, spotlight.x + direction(0) * img.rows * img.cols, spotlight.y + direction(1) * img.cols * img.rows);

	vector<vector<line_segment>::iterator> candidates;

	/// visualisation only
	if (visualize_cones && lines.size() < 30) {
		for (int row = 0; row < img.rows; ++row) {
			for (int col = 0; col < img.cols; ++col) {
				Vec2d v(col - spotlight.x, row - spotlight.y);
				if ((is_in_circle_part(start, end, v, radius) ||
				     is_within_radius(v, radius / 2))) {
					circle(img, Point(col, row), 1, Scalar(255, 255, 255), 1);
				}
			}
		}
		show_im("cone search", img);
	}

	for (auto line = lines.begin(); line < lines.end(); ++line) {
		if ((*line).q().x == spotlight.x && (*line).q().y == spotlight.y &&
		        (*line).p().x == (spotlight.x - direction(0)) && (*line).p().y == (spotlight.y - direction(1))) {
			continue;
		}
		circle(img, Point((*line).p().x, (*line).p().y), 1, Scalar(0, 255, 255), 2);
		Vec2i v((*line).p().x - spotlight.x, (*line).p().y - spotlight.y);
		if ((is_in_circle_part(start, end, v, radius) ||
		     is_within_radius(v, radius / 2)) ||
		        utility::lines_intersect(spotlight, Point(start(0) + spotlight.x, start(1) + spotlight.y), Point((*line).p().x, (*line).p().y), Point((*line).q().x, (*line).q().y)) ||
		        utility::lines_intersect(spotlight, Point(end(0) + spotlight.x, end(1) + spotlight.y), Point((*line).p().x, (*line).p().y), Point((*line).q().x, (*line).q().y)))
		{
			circle(img, Point((*line).p().x, (*line).p().y), 1, Scalar(0, 255, 0), 2);
			candidates.push_back(line);
		}
	}
	double min = INT_MAX;
	line_segment chosen;
	vector<line_segment>::iterator to_delete;
	for (const auto& c : candidates) {
		double dir_candidangle = atan2(c->p().y, c->p().x);
		double anglediff = abs(dir_angle - dir_candidangle);
		if (abs(anglediff - min) < 0.001) { // if the angles are same, compare distance
			Vec2i v_to_old(chosen.p().x - spotlight.x, chosen.p().y - spotlight.y);
			Vec2i v_to_new(c->p().x - spotlight.x, c->p().y - spotlight.y);
			if ((v_to_new(0) * v_to_new(0) + v_to_new(1) * v_to_new(1)) <
			        (v_to_old(0) * v_to_old(0) + v_to_old(1) * v_to_old(1))) {
				    min = anglediff;
					chosen = *c;
					to_delete = c;
			}
		} else if (anglediff < min) {
			min = anglediff;
			chosen = *c;
			to_delete = c;
		}
	}
	if (chosen.is_valid()) {
		lines.erase(to_delete);
	}
	return chosen;
}

void feature_analyzer::paint_lines(cv::Mat& img, const std::vector<line_sequence>& lines_v) {
	int color_step = 360 / max(1ul, lines_v.size() + 1);
	int color = color_step;
	for (const auto& lines : lines_v) {
		for (const auto& line : lines) {
			cv::line(img, Point(line.p().x, line.p().y), Point(line.q().x, line.q().y), utility::hsv2bgr(color, 1, 1), 1, LINE_AA);
		}
		color += color_step;
	}
}

// might require updating
void feature_analyzer::multithreaded_pass(CTX& ctx, scalespace_analyzer& ssd) {
	video_player viper(ctx.video_capture_v); // VIdeo PlayER very clever right
	bool ready = false;
	std::mutex m;
	std::condition_variable cond_var;

#pragma omp parallel sections
	{
#pragma omp section
		{
			viper.play(m, cond_var, &ready);
			cerr << "thread " << omp_get_thread_num() << " finished" << endl;
		}
#pragma omp section
		{
			std::unique_lock<std::mutex> lk(m);
			cond_var.wait(lk, [&]{ return ready; });
			unsigned int loops_before_change = 0;
			while (viper.playing()) {
#pragma omp flush(viper)
				if (viper.fetched()) {
					continue;
				}
#pragma omp critical
				{
					ctx.original_img_v[CTX::IDX_RECORD] = viper.fetch_frame(CTX::IDX_RECORD);
					ctx.original_img_v[CTX::IDX_ADVERT] = viper.fetch_frame(CTX::IDX_ADVERT);
					ctx.frame_n = viper.frame_n();
				}
				ctx.histogram_change = true;

				Mat cropped, current_b;
				loop_status ls = process_current_frame(ctx, false, cropped, current_b);

				ctx.frame_n = viper.frame_n();

				++loops_before_change;
				output_stringbuilder.dump();
				//show_im("grabbed frame", ctx.original_imgs.front());
				if (ls == loop_status::FS_BREAK) {
					break;
				}
				if (!ctx.previous_result || ctx.histogram_change) {
					loops_before_change = 1;
				}

				if (ctx.previous_result == fs_result::v::MAYBE_FALSE) {
					utility::depad_image(cropped, cropped, padding);
					if (!cropped.empty() && do_the_scalesauce_dance(ssd, cropped, current_b)) {
						ctx.previous_result = fs_result::v::TRUE;
					}
				}
				//show_fancy_results(!(ctx.previous_result && result::past_n_majority(ctx.result_history, 11, loops_before_change, !histogram_change)));
			}
			cerr << "thread " << omp_get_thread_num() << " finished" << endl;

		}
	}
}

loop_status feature_analyzer::process_current_frame(CTX& ctx, bool single_thread, Mat& result_crop, Mat& current_b) {
	output_stringbuilder << "-------------------------------\n";
	auto ts = timer_start();
	if (single_thread) {
		if (!ctx.is_video) {
			ctx.original_img_v[CTX::IDX_RECORD].copyTo(ctx.modified_img_v[CTX::IDX_RECORD]);
			ctx.original_img_v[CTX::IDX_ADVERT].copyTo(ctx.modified_img_v[CTX::IDX_ADVERT]);
		} else {
			ctx.update();
		}
	} else {
		ctx.original_img_v[CTX::IDX_RECORD].copyTo(ctx.modified_img_v[CTX::IDX_RECORD]);
		ctx.original_img_v[CTX::IDX_ADVERT].copyTo(ctx.modified_img_v[CTX::IDX_ADVERT]);
	}
	stop_at_specified_frames(ctx, {}); // used for debug, modify list to have the program pause at specified frames

	if (single_thread) {
		if (ctx.any_empty()) {
			output_stringbuilder << "end of feed" << "\n";
			return loop_status::FS_BREAK;
		}
	}

	///// this is used only for debugging
	//string gt_line;
	//getline(is_results_groundtruth, gt_line);
	//auto sp_gtl = utility::split(gt_line, '=');
	////bool false_neg = sp_gtl[0][0] == 'n';
	////bool false_pos = sp_gtl[0][0] == 'p';
	////bool p_t = sp_gtl[0].back() == '!';
	output_stringbuilder << lblue << "current frame: " << cyan << ctx.frame_n << reset << "\n";
	/////

	result res;
	res.cpu_use() = get_cpu_used_by_current();
	ofs_cpu_log << ctx.frame_n << " cpu usage: " << res.cpu_use() << "% ";

	ctx.resize_images(resize_factor);
	ctx.pad_images(padding);

	full_result_history.push_back(res);


	if (ctx.is_video) {
		ctx.prev_histogram_comp = ctx.histogram_comp;
		ctx.histogram_comp = utility::compare_histograms(ctx.modified_img_v[CTX::IDX_RECORD], ctx.modified_img_v[2]);
		ctx.histogram_change = ctx.prev_histogram_comp < 0.99;

		//double psnr = utility::get_psnr(ctx.modified_img_v[CTX::IDX_RECORD], ctx.modified_img_v[2]);

		Mat absdiff_curr_prev_scene;
		cv::absdiff(ctx.modified_img_v[CTX::IDX_RECORD], ctx.modified_img_v[2], absdiff_curr_prev_scene);
		cv::threshold(absdiff_curr_prev_scene, absdiff_curr_prev_scene, 25, 255, THRESH_BINARY);
		cvtColor(absdiff_curr_prev_scene, absdiff_curr_prev_scene, COLOR_BGR2GRAY);
		int absdiff_count = countNonZero(absdiff_curr_prev_scene);
		bool cut = absdiff_count > (absdiff_curr_prev_scene.cols * absdiff_curr_prev_scene.rows * 0.66);

		if (cut) {
			ctx.recalc_flag = true;
		}

		show_im("absdiff", absdiff_curr_prev_scene);

		output_stringbuilder << "previous result: " << lyellow <<
		               ctx.previous_result << reset <<
		               ", \nhistogram comparison: " <<
		               (ctx.histogram_change ? lred : lgreen) <<
		               ctx.prev_histogram_comp << reset <<
		               " - absd: " <<
		               (absdiff_count <= (absdiff_curr_prev_scene.cols * absdiff_curr_prev_scene.rows * 0.66) ? lgreen : lred) << absdiff_count << reset << "\n";

		if (single_thread) {
			key_wait(DEBUG_DELAY_FOR_EASIER_OBSERVATION_IN_MS__);
		}

		if (ctx.frame_n % reinit_wait_period != 0 && !ctx.recalc_flag) {
			//if (!ctx.histogram_change && utility::all_same(ctx.result_history)) {
			if (!cut && utility::all_same(ctx.result_history)) {
				// the frame looks similar to the one before, assumed true result
				output_stringbuilder.dump();
				timer_end(ts, &ofs_cpu_log, &full_result_history.back().time());
				full_result_history.back().good() = ctx.previous_result;
				return loop_status::FS_CONTINUE;
			}
		}

		if (!ctx.result_history.empty()) {
			ctx.result_history.pop_front();
		}
	}
	ctx.recalc_flag = false;
	pre_processing(ctx.modified_img_v[CTX::IDX_RECORD], ctx.modified_img_v[CTX::IDX_ADVERT], false);

	// the following does most of the logic behind the per-frame decision
	bool detect_result = detect_keypoints_analyze(ctx);

	if (!detect_result) {
		timer_end(ts, &ofs_cpu_log, &full_result_history.back().time());
		full_result_history.back().good() = ctx.previous_result;
		ctx.result_history.push_back(false);

		return loop_status::FS_CONTINUE;
	}

	ctx.result_history.push_back(ctx.previous_result);

	result_crop = ctx.result_mat;
	current_b = ctx.original_img_v[CTX::IDX_ADVERT];

	string line;
	getline(ctx.input_filestream, line);
	timer_end(ts, &ofs_cpu_log, &full_result_history.back().time());
	full_result_history.back().good() = ctx.previous_result;

	if (!ctx.is_video) {
		pause();
		return loop_status::FS_BREAK;
	}
	return loop_status::FS_CONTINUE;
}

void feature_analyzer::pre_processing(Mat& img_a, Mat& img_b, bool cc) {
	Mat ressz_a;
	Mat ressz_b;
	resize(img_a, ressz_a, Size(0, 0), 1, 1);
	resize(img_b, ressz_b, Size(0, 0), 1, 1);


	// color correction
	if (cc) {
		Mat hsva = utility::bgr_to_hsv(ressz_a, 1.0);
		image_modification::color_correction(hsva, 100, 160, true, false, 0, 0);
		img_a = utility::hsv_to_bgr(hsva);

		Mat hsvb = utility::bgr_to_hsv(ressz_b, 1.0);
		image_modification::color_correction(hsvb, 100, 160, true, false, 0, 0);
		img_b = utility::hsv_to_bgr(hsvb);
	} else {
		img_a = ressz_a;
		img_b = ressz_b;
	}
}

bool feature_analyzer::compute_keypoints() {
	// detector is used to find interesting points (keypoints)
	ctx.fdetector->detect(ctx.modified_img_v[CTX::IDX_RECORD], ctx.get_scn_kpoints_ref());
	ctx.fdetector->compute(ctx.modified_img_v[CTX::IDX_RECORD], ctx.get_scn_kpoints_ref(), ctx.get_scn_desc_ref());

	if (ctx.get_scn_kpoints_ref().empty()) {
		//ctx.previous_result = fs_result::v::ABSOLUTE_FALSE;
		return false;
	}

	if (ctx.get_previous_ad().empty()) {
		ctx.set_previous_ad(ctx.modified_img_v[CTX::IDX_ADVERT]);
	} else {
		Mat bdiff;
		cv::absdiff(ctx.modified_img_v[CTX::IDX_ADVERT], ctx.get_previous_ad(), bdiff);
		cv::cvtColor(bdiff, bdiff, COLOR_BGR2GRAY);
		if (cv::countNonZero(bdiff) > 0) {
			ctx.set_previous_ad(ctx.modified_img_v[CTX::IDX_ADVERT]);
			ctx.fdetector->detect(ctx.modified_img_v[CTX::IDX_ADVERT], ctx.get_obj_kpoints_ref());
			ctx.fdetector->compute(ctx.modified_img_v[CTX::IDX_ADVERT], ctx.get_obj_kpoints_ref(), ctx.get_obj_desc_ref());
		}
	}
	return true;
}

keypoint_set feature_analyzer::process_kpoints(int max_len, int w, int h) {
	keypoint_set processed_pts;

	// filter out points that are found inside areas where they shouldn't be - field
	for (const auto& t : ctx.get_scn_kpoints_ref()) {
		int len = static_cast<int>(std::abs((max_len + 1) - t.size) * 0.5);
		if (len == 0) {
			continue;
		}
		int x = std::max(0, static_cast<int>(t.pt.x - static_cast<float>(len) * 2));
		int y = std::max(0, static_cast<int>(t.pt.y - static_cast<float>(len) * 0.5));
		int min_w = min_1(static_cast<int>((x + 5 * len) >= w ? (w - x) : (5 * len)));
		int min_h = min_1(static_cast<int>((y + len) >= h ? (h - y) : len));
		Rect r = Rect(x, y, static_cast<int>(min_w * resize_factor), static_cast<int>(min_h * resize_factor));
		Mat roi = ctx.modified_img_v[CTX::IDX_RECORD](r);
		double ratio = 0.0;
		//utility::contains_hue(roi, ctx.excluded_hue, &ratio);
		if (ratio > 0.75) {
			continue;
		}
		processed_pts.emplace(t.pt, len, t.angle, r);
	}

	if (processed_pts.empty()) {
		ctx.previous_result = fs_result::v::ABSOLUTE_FALSE;
		return {};
	}

	return processed_pts;
}

bool feature_analyzer::detect_keypoints_analyze(CTX& ctx) {
	try {
		perform_lsd(ctx);
	} catch (const char* ex) {
		ctx.previous_result = fs_result::v::ABSOLUTE_FALSE;
		return false;
	}

	if (!compute_keypoints()) {
		return false;
	}

	// Matrices initialized, primarily for visualization
	Mat keypoint_areas = give_mat<uchar>(ctx.modified_img_v[CTX::IDX_RECORD]);
	Mat draw_image = give_mat<uchar, 3>(ctx.modified_img_v[CTX::IDX_RECORD]);
	ctx.result_mat *= 0;

	//Mat obj_cpy;
	//ctx.modified_img_v[CTX::IDX_RECORD].copyTo(obj_cpy);
	//Mat scn_cpy = give_mat<uchar>(ctx.modified_img_v[CTX::IDX_RECORD]);

	//Mat all_points = give_mat<uchar>(ctx.modified_img_v[CTX::IDX_RECORD]);
	float max_len = 0;
	for (const auto& kp : ctx.get_scn_kpoints_ref()) {
		//all_points.at<uchar>(kp.pt) = 255;
		if (kp.angle >= 20.0f && kp.angle <= 320.0f) {
			if (kp.size > max_len) { max_len = kp.size; }
		}
	}

	// helps clear them out and speed up the process
	ctx.get_scn_kpoints_ref().erase(remove_if(ctx.get_scn_kpoints_ref().begin(), ctx.get_scn_kpoints_ref().end(),
	                            [&](const KeyPoint& kp) {
		//return kp.response < -0.0002 || kp.size < 50;
		// maybe even lower response ratio needed, otherwise it throws out most cases
		return kp.size < 50 || (kp.angle < 35.0f || kp.angle > 325.0f);
		// kp.size < 50, angle <20 and >320 seems best
	}), ctx.get_scn_kpoints_ref().end());

	keypoint_set processed_pts = process_kpoints(max_len, keypoint_areas.cols, keypoint_areas.rows);
	if (processed_pts.empty()) {
		return false;
	}

	vector<point_clump> clumps_v;
	// works with found points to find interesting areas that could contain desired object
	vector<Point> pts = find_clumps_of_keypoints(ctx, draw_image, keypoint_areas, clumps_v, processed_pts);

	Point3d var;
	Point3i disp = utility::dispersion(pts, var);

	ctx.previous_result = keypoint_validation(draw_image, var, disp, clumps_v);

	if (!RUN_MULTITHREAD__) {
		if (ctx.previous_result) {
			utility::pad_image(draw_image, draw_image, 1, Scalar(0, 255, 0));
		} else {
			utility::pad_image(draw_image, draw_image, 1, Scalar(0, 0, 255));
			ctx.previous_result = fs_result::v::MAYBE_FALSE;
		}
	} else {
		if (!ctx.previous_result) {
			ctx.previous_result = fs_result::v::MAYBE_FALSE;
		}
	}

	//Mat overlayed, depadded_visuals;
	//utility::depad_image(draw_image, depadded_visuals, 1);
	//utility::overlay_image_3c(ctx.modified_img_v.front(), depadded_visuals, overlayed);
	utility::depad_image(keypoint_areas, keypoint_areas, padding);
	int non_zero_count = cv::countNonZero(keypoint_areas);
	return non_zero_count > 0;
}

vector<Point> feature_analyzer::find_clumps_of_keypoints(CTX& ctx, Mat& output, Mat& keypoint_areas, vector<point_clump>& clumps, const keypoint_set& pts) {
	int ellipse_w = static_cast<int>(0.14 * output.cols);
	int ellipse_h = static_cast<int>(0.04 * output.rows);
	vector<Point> ret;

	area_shape shape = AREA_ELLIPSE;

	clumps = keypoint_grouping(pts, ellipse_w, ellipse_h, shape);
	sort(clumps.begin(), clumps.end(),
	     [](const point_clump& a, const point_clump& b) {
		return a.size() > b.size();
	});
	size_t original_clump_count = clumps.size();

	size_t original_point_count = accumulate(clumps.begin(), clumps.end(), 0,
	                          [](int acc, const point_clump& ps) {
		return acc += ps.size();
	});

	vector<Point> pre_check_points;
	pre_check_points.reserve(original_point_count);

	for (auto clump : clumps) {
		for (auto kp : clump.points_) {
			pre_check_points.emplace_back(kp);
		}
	}

	clumps.erase(
	            remove_if(clumps.begin(), clumps.end(),
	                      [&](const point_clump& pr) {
		return pr.size() < 0.5 * original_point_count;
	}), clumps.end());

	Point primary_centroid = clumps.front().centroid_;
	vector<point_clump> filtered_clumps;
	copy_if(clumps.begin(), clumps.end(), std::back_inserter(filtered_clumps),
	        [&](point_clump& clump) {
		cv::Point centroid = accumulate(clump.begin(), clump.end(), Point(0, 0),
		                                [&](Point& p, const keypoint& kp) {
		                     drawing::draw_cross(output, p, 5, Scalar(0, 0, 255));
		                     return Point(p.x + kp.x(), p.y + kp.y());
	});
		centroid.x /= clump.size();
		centroid.y /= clump.size();
		clump.centroid_ = centroid;
		if (primary_centroid.x == -1) {
			primary_centroid = centroid;
		}
		return std::abs(centroid.y - primary_centroid.y) < 0.25 * output.rows;
	});
	clumps = filtered_clumps;

	analyze_filtered_clumps(ctx, output, keypoint_areas, clumps, ret, original_point_count, pre_check_points, original_clump_count, ellipse_w, ellipse_h, shape);

	return ret;
}

vector<feature_analyzer::point_clump> feature_analyzer::keypoint_grouping(const keypoint_set& pts, double w,
                                                                      double h, area_shape area/* = AREA_CIRCLE*/) {
	vector<point_clump> groups;
	keypoint_set unassigned_points(pts);
	while (!unassigned_points.empty()) {
		auto p_unass_pts_iter = unassigned_points.begin();
		std::advance(p_unass_pts_iter, std::rand() % unassigned_points.size());

		point_clump group;
		group.clump_w_ = w;
		group.clump_h_ = h;
		group.add_point(*p_unass_pts_iter);
		stack<keypoint> point_stack;
		point_stack.push(*p_unass_pts_iter);
		unassigned_points.erase(p_unass_pts_iter);

		while (!point_stack.empty() && !unassigned_points.empty()) {
			keypoint top_point = point_stack.top();
			point_stack.pop();

			vector<keypoint_set::iterator> points_to_delete;
			points_to_delete.reserve(unassigned_points.size());

			size_t stack_size = point_stack.size();
			bool added_to_stack = false;

			for (auto q_unass_pts_iter = unassigned_points.begin(); q_unass_pts_iter != unassigned_points.end(); ++q_unass_pts_iter) {
				keypoint current_point = *q_unass_pts_iter;

				bool in_area = false;
				switch (area) {
				case AREA_CIRCLE:
					in_area = utility::point_in_circle(top_point, current_point, static_cast<int>(w));
					break;
				case AREA_ELLIPSE:
					in_area = utility::point_in_ellipse(top_point, current_point, static_cast<int>(h), static_cast<int>(w), 0.0);// ||
					break;
				case AREA_RECTANGLE:
					in_area = utility::point_in_rectangle(top_point, current_point, static_cast<int>(h), static_cast<int>(w));
					break;
				}

				if (in_area) {
					group.add_point(current_point);
					point_stack.push(current_point);
					points_to_delete.push_back(q_unass_pts_iter);
					added_to_stack = true;
				} else if (point_stack.size() == stack_size && added_to_stack) {
					break;
				}
				stack_size = point_stack.size();
			}
			for (const auto& it : points_to_delete) {
				unassigned_points.erase(it);
			}
		}

		groups.push_back(group);
	}
	return groups;
}

void feature_analyzer::analyze_filtered_clumps(CTX& ctx, Mat& output, Mat& keypoint_areas, vector<point_clump>& clumps, vector<Point>& ret,
                                               size_t total, vector<Point>& pre_check_points, size_t original_clump_count,
                                               int ellipse_w, int ellipse_h, area_shape shape) {
	size_t filtered_total = accumulate(clumps.begin(), clumps.end(), 0,
	                                   [](int acc, const point_clump& ps) {
		return acc += ps.size();
	});
	ret.reserve(filtered_total);
	size_t sum = 0;

	vector<int> ys{};
	int median_y = 0;
	vector<keypoint> filtered_points{};
	filtered_points.reserve(filtered_total);

	Mat point_map = give_mat<uchar>(output);

	Mat cover = give_mat<uchar>(output);

	int clump_count = 0;
	int el_hw = ellipse_w / 2;
	int el_hh = ellipse_h / 2;

	// here divide clumps into subclumps
	for (auto& clump : clumps) {
		if (clump.size() < 0.33 * filtered_total) {
			break;
		}

		Scalar color = rand_colormap[clump_count++];
		std::transform(clump.points_.begin(), clump.points_.end(), std::back_inserter(ys),
		               [](const keypoint& kp) { return kp.y(); });

		sum += clump.size();
		for (const auto& point : clump) {
			keypoint_areas.at<uchar>(point.area().y + el_hh, point.area().x + el_hw) = 255;
			drawing::draw_cross(output, point, 3, color);
			point_map.at<uchar>(point) = 255;

			switch (shape) {
			case AREA_CIRCLE:
				circle(cover, point, el_hw, 255, -1);
				break;
			case AREA_RECTANGLE:
				rectangle(cover, Point(point.x() - el_hw, point.y() - el_hh),
				          Point(point.x() + el_hw, point.y() + el_hh), 255, -1);
				break;
			case AREA_ELLIPSE:
				ellipse(cover, point, Size(el_hw, el_hh), 0.0, 0, 360, 255, -1);
				break;
			}

			filtered_points.emplace_back(point);
		}
		clump.divide_subclumps(ellipse_h * 2, ellipse_h * 2, 0.2);
		drawing::draw_cross(output, clumps[clump_count - 1], 10, Scalar(0, 0, 255));
		if (sum > 0.8 * total) {
			break;
		}
	}

	morphologyEx(cover, cover, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(2 * el_hh, el_hw)));
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	findContours(cover, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

	vector<vector<Point>> bad_contours;
	for (const auto cnt : contours) {
		auto rect = minAreaRect(cnt);
		float rangle = abs(rect.angle);
		if ((rect.size.aspectRatio() < 1.10 && rangle < 85 && rangle > 5) || contourArea(cnt) < 7500) {
			bad_contours.push_back(cnt);
		}
	}
	if (contours.size() - bad_contours.size() <= 2 && contours.size() >= 5) {
		filtered_points.clear();
	}

	std::sort(ys.begin(), ys.end());
	if (!ys.empty()) {
		median_y = ys[ys.size() / 2];
		drawing::draw_cross(output, Point(output.cols / 2, median_y), 10, Scalar(0, 255, 255), 3);
	}

	reduce_points(ctx.modified_img_v[CTX::IDX_RECORD].rows, filtered_points, median_y);

	secondary_keypoint_analysis(ctx, output, filtered_points, clumps, median_y, point_map, ret, original_clump_count, clump_count, total, sum, pre_check_points);
}

void feature_analyzer::reduce_points(int image_height, vector<keypoint>& points, int median_y) {
	vector<keypoint> new_filtered_points{};
	for (const auto& pt : points) {
		if (abs(median_y - pt.y()) < 0.2 * image_height) {
			new_filtered_points.emplace_back(pt);
		}
	}

	points = new_filtered_points;
}

void feature_analyzer::secondary_keypoint_analysis(CTX& ctx, Mat& output, vector<keypoint>& points, std::vector<point_clump>& clumps, int median_y,
                                       Mat& point_map, vector<Point>& result_points,
                                       int original_clump_count, int clump_count,
                                       int original_point_count, int point_count_pre_reduction,
                                       vector<Point>& pre_check_points) {
	(void)clump_count;
	Point center;
	simple_parallelogram chosen_area = find_suitable_area(ctx, points, output, &median_y, center, rand_colormap[5], 3);

	if (!chosen_area.valid()) { // check the angles next
		result_points.clear();
		return;
	}

	cv::Ptr<cv::FeatureDetector> fdetector = ORB::create(1800);
	cv::Ptr<cv::DescriptorMatcher> dmatcher = DescriptorMatcher::create(DescriptorMatcher::MatcherType::BRUTEFORCE_HAMMING);

	for (auto clump : clumps) {
		size_t matches = 0;
		for (auto subclump : clump.get_subclumps()) {
			subclump.clump_area_ = find_suitable_area(ctx, subclump.points_v_, output, nullptr, subclump.centroid_, rand_colormap[7], 1);
			if (range<double>::test(subclump.get_angle(), 70, 110) || range<double>::test(subclump.get_angle(), 250, 290)) {
				continue;
			}
			double sc_h = subclump.clump_area_.get_h();
			Mat rotated_output;
			utility::rotate(ctx.modified_img_v[CTX::IDX_RECORD], rotated_output, -subclump.get_angle());
			int begin = subclump.centroid_.y - sc_h;
			int end = subclump.centroid_.y + sc_h;

			Mat mask = give_mat<uchar>(ctx.modified_img_v[CTX::IDX_RECORD]);
			mask.forEach<uchar>(
			            [begin, end](uchar& p, const int* pos){
				if (pos[0] > begin && pos[0] < end) {
					p = 255;
				}
			});
			utility::rotate(mask, mask, -subclump.get_angle());

			Mat masked_rotated_img;
			utility::apply_binary_mask(mask, rotated_output, masked_rotated_img);
			utility::trim(masked_rotated_img, masked_rotated_img);

			size_t new_matches_count = 0;
			size_t matches_found = match_descriptors_in_subimage(ctx, fdetector, dmatcher);
			//if (matches_found == 0) { // causes many false negatives
			//	result_points.clear();
			//	return;
			//}
			if (ctx.result_mat.empty() || matches_found > matches) {
				utility::rotate(masked_rotated_img, masked_rotated_img, subclump.get_angle());
				utility::trim(masked_rotated_img, masked_rotated_img);
				masked_rotated_img.copyTo(ctx.result_mat);

				new_matches_count = matches_found;
				matches = new_matches_count;
				// currently doesn't lead to anything useful
			}
		}
	}

	size_t cut_off_count = 0;

	for (const auto pt : points) {
		if (chosen_area.test_point_inside(pt) && utility::count_in_area(point_map, pt, AREA_CIRCLE, 45) >= 3) {
			++cut_off_count;
			result_points.emplace_back(pt);
		}
	}

	if ((static_cast<double>(cut_off_count) / original_point_count < (0.20 / resize_factor) && original_clump_count <= 6) ||
	        ((result_points.size() < 0.5 * point_count_pre_reduction || point_count_pre_reduction < 50) &&
	         original_clump_count > 6/*clumps.size() > 1*/) ||
	        (original_point_count < 135 && cut_off_count < 90)) {
		result_points.clear();
	}

	Point3d pre_processing_variance;
	Point3i pre_processing_dispersion = utility::dispersion(pre_check_points, pre_processing_variance);
	if ((pre_processing_variance.y > 30000 && original_clump_count >= 12) ||
	        (pre_processing_variance.x > 60000 && pre_processing_variance.z > 120000 && original_clump_count > 8) ||
	        (std::abs(pre_processing_dispersion.x + pre_processing_dispersion.y - pre_processing_dispersion.z) < pre_processing_dispersion.z * 0.025 &&
	         pre_processing_dispersion.z > pre_processing_dispersion.x + 0.5 * pre_processing_dispersion.y)) {
		result_points.clear();
	}
}

simple_parallelogram feature_analyzer::find_suitable_area(CTX& ctx, std::vector<keypoint>& points, Mat& output, int* median_y, Point& center, const Scalar& line_color, int line_thickness) {
	simple_mat p_comps = pca_analysis(points, output, center);
	Vec2d direction_x = cv::normalize(Vec2d(p_comps(1, 0).x - p_comps(0, 0).x, p_comps(1, 0).y - p_comps(0, 0).y));
	Vec2d direction_y = Vec2d(p_comps(1, 1).x - p_comps(0, 1).x, p_comps(1, 1).y - p_comps(0, 1).y);
	double y_mag = sqrt(direction_y[0] * direction_y[0] + direction_y[1] * direction_y[1]);
	double y_mag_mult = max(0.5, pow(10, -(y_mag / 70.0) + 1));
	double push_down_bias = 0.00;
	// best results when left like this
	double radius = y_mag * y_mag_mult;

	Point2d SAB = p_comps(0, 0) + Vec2d(0, p_comps(0, 0).y * push_down_bias - radius);
	Point2d SCD = p_comps(0, 0) + Vec2d(0, p_comps(0, 0).y * push_down_bias + radius);

	//if (bad_hue_in_radius_ratio(ctx.modified_img_v[CTX::IDX_RECORD], center, radius, ctx.excluded_hue) > 0.8) {
	//	return simple_parallelogram();
	//}

	Point2d A = SAB + (-2 * output.cols) * direction_x;
	Point2d B = SAB + (2 * output.cols) * direction_x;
	Point2d C = SCD + (2 * output.cols) * direction_x/* + Vec2d(y_mag * y_mag_mult, 0)*/;
	Point2d D = SCD + (-2 * output.cols) * direction_x/* + Vec2d(y_mag * y_mag_mult, 0)*/;

	drawing::draw_line(output, A.x, A.y, B.x, B.y, line_color, line_thickness);
	drawing::draw_line(output, C.x, C.y, D.x, D.y, line_color, line_thickness);
	drawing::draw_cross(output, SAB, 5, Scalar(255, 0, 255), 2);
	drawing::draw_cross(output, SCD, 5, Scalar(255, 0, 255), 2);

	if (median_y) {
		Point2d MX = Point2d(output.cols / 2, *median_y);
		Point2d MA = MX + (-2 * output.cols) * direction_x;
		Point2d MB = MX + (2 * output.cols) * direction_x;
		drawing::draw_line(output, MA.x, MA.y, MB.x, MB.y, rand_colormap[6]);
	}

	if (A != B) {
		ctx.result_mat *= 0;
	}

	//double x_mag = sqrt(direction_x[0] * direction_x[0] + direction_x[1] * direction_x[1]);

	if (y_mag > 150) {
		return simple_parallelogram();
	}

	show_im("keypoint analysis visualization", output);

	Vec2d SabScd = SCD - SAB;
	double h = sqrt(SabScd[0] * SabScd[0] + SabScd[1] * SabScd[1]);

	return simple_parallelogram(A, B, C, D, h);
}

simple_mat<Point> feature_analyzer::pca_analysis(const std::vector<keypoint>& pts, Mat& img, Point& center) {
	simple_mat<Point> principal_components(2, 2);

	if (pts.empty()) {
		return principal_components;
	}

	int sz = static_cast<int>(pts.size());
	Mat data_pts = Mat(sz, 2, CV_64F);
	for (int i = 0; i < data_pts.rows; i++) {
		data_pts.at<double>(i, 0) = pts[i].x();
		data_pts.at<double>(i, 1) = pts[i].y();
	}
	PCA pca_analysis(data_pts, Mat(), PCA::DATA_AS_ROW);
	center = Point(static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
	                   static_cast<int>(pca_analysis.mean.at<double>(0, 1)));
	vector<Point2d> eigen_vecs(2);
	vector<double> eigen_val(2);
	for (int i = 0; i < 2; i++) {
		eigen_vecs[i] = Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
		                        pca_analysis.eigenvectors.at<double>(i, 1));
		eigen_val[i] = pca_analysis.eigenvalues.at<double>(i);
	}
	circle(img, center, 3, Scalar(255, 0, 255), 2);


	Point p1 =
	        center + 0.02 * Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]),
	        static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
	Point p2 =
	        center - 0.02 * Point(static_cast<int>(eigen_vecs[1].x * eigen_val[1]),
	        static_cast<int>(eigen_vecs[1].y * eigen_val[1]));
	drawing::draw_axis(img, center, p1, Scalar(0, 255, 0), 1);
	drawing::draw_axis(img, center, p2, Scalar(255, 255, 0), 3);
	Vec2f v1(p1.x - center.x, p1.y - center.y);
	principal_components(0, 0) = center;
	principal_components(1, 0) = p1;
	principal_components(0, 1) = center;
	principal_components(1, 1) = p2;
	//*angle = atan2(eigen_vecs[0].y, eigen_vecs[0].x);  // orientation in radians
	//*angle = atan2(v1[1], v1[0]);
	return principal_components;
}

bool feature_analyzer::keypoint_validation(cv::Mat& output, const cv::Point3d& variance, const cv::Point3i& dispersion,
                                         std::vector<point_clump>& clumps) {
	full_result_history.back().extra_str() << clumps.size();
	if (clumps.size() > 8) {
		return false;
	}
	double sum = utility::sum(dispersion);

	if (sum <= 0) {
		return false;
	}

	double nx = dispersion.x / sum;
	double ny = dispersion.y / sum;
	double nz = dispersion.z / sum;
	double min_norm = 1.0 / utility::min3(nx, ny, nz);
	// values chosen based on observed data to provide good results
	int ax = std::abs(dispersion.x - 230);
	int ay = std::abs(dispersion.y - 30);
	int az = std::abs(dispersion.z - 245);
	int asum = ax + ay + az;
	nx *= min_norm;
	ny *= min_norm;
	nz *= min_norm;

	bounding_box_t bounding_box_0 = bounding_box_t::create(clumps[0].points_);
	double rect_dist = -1.0;

	std::vector<point_clump> good_clumps{clumps[0]};

	size_t i = 1;
	for (; i < clumps.size(); ++i) {
		rect_dist = utility::rect_distance(bounding_box_t::create(clumps[i].points_).r(), bounding_box_0.r());
		cv::line(output, clumps[0], clumps[i], cv::Scalar(255, 0, 255), 2);
		if (rect_dist < 300) {
			good_clumps.push_back(clumps[i]);
		} else {
			break;
		}
	}

	clumps = good_clumps;

	bool acceptable_near_top = i <= 2 && clumps.front().centroid_.y < output.rows * 0.1 && variance.y < 500 && variance.y > 80;
	bool near_bottom = i <= 2 && clumps.front().centroid_.y > output.rows * 0.9 && variance.y < 500;
	bool good_parameters = /*asum < 300 &&
												  (variance.x > 7000 && variance.z > 7000) &&*/
	                       ((variance.y < 1000 && asum < (700 * resize_factor)) ||
	                        (variance.y < 4500 && asum < (300 * resize_factor)) ||
	                        (variance.y < 6500 && asum < (150 * resize_factor)));
	bool result_is_good = !near_bottom &&
	                  (acceptable_near_top ||
	                   (good_parameters && i <= 1 &&
	                    ((dispersion.y < 40 && dispersion.x > 220 && dispersion.z > 220) ||
	                     (dispersion.y < 80 && asum < 200) ||
	                     (dispersion.y < 30 && asum < 400 && nx <= 12.0))
	                    )
	                   );
	full_result_history.back().extra_str() << "-[" << variance.x << ";" << variance.y << ";" << variance.z << "]-" << i << "-{" << ax << ";" << ay << ";" << az << "}-" <<
	                            asum << "-[" << nx << ";" << ny << ";" << nz << "]-" << result_is_good;

	return result_is_good;
}

double feature_analyzer::bad_hue_in_radius_ratio(Mat& img, const Point& pt, double radius, uchar excluded_hue) {
	if (img.empty() || radius == 0) {
		return 0;
	}
	Mat center_circle_mask = give_mat<uchar>(img);
	circle(center_circle_mask, pt, radius, Scalar(255), -1);
	Mat center_circle_roi;
	utility::apply_binary_mask(center_circle_mask, img, center_circle_roi, Scalar(255, 0, 255));
	utility::trim(center_circle_roi, center_circle_roi);
	if (center_circle_roi.empty()) {
		return 0;
	}
	cvtColor(center_circle_roi, center_circle_roi, COLOR_BGR2HSV);
	Point norm_center(radius, radius);
	int count_total = 0, count_bad = 0;
	for (int row = 0; row < center_circle_roi.rows; ++row) {
		auto img_ptr = center_circle_roi.ptr<Point3_<uint8_t>>(row);
		for (int col = 0; col < center_circle_roi.cols; ++col) {
			Point AB(norm_center.x - col, norm_center.y - row);
			if (((AB.x * AB.x) + (AB.y * AB.y)) > (radius * radius)) {
				continue;
			}
			if (abs(img_ptr->x - excluded_hue) < 5) {
				++count_bad;
			}
			++count_total;
			++img_ptr;
		}
	}
	return static_cast<double>(count_bad) / count_total;
}

struct dmatch_comp {
	bool operator()(const DMatch& a, const DMatch& b) const {
		if (a.queryIdx == b.queryIdx && a.trainIdx == b.trainIdx) { // same keypoints = same match
			return false;
		}
		return a.distance < b.distance;
	}
};

// perform on smaller area, compare relative positions of matched points
size_t feature_analyzer::match_descriptors_in_subimage(CTX& ctx, cv::Ptr<cv::FeatureDetector> fdetector, cv::Ptr<cv::DescriptorMatcher> dmatcher) {
	Mat obj_lab, scn_lab;
	cvtColor(ctx.modified_img_v[CTX::IDX_ADVERT](
	            Range(0, ctx.modified_img_v[CTX::IDX_ADVERT].rows),
	            Range(0, ctx.modified_img_v[CTX::IDX_ADVERT].cols / 5)
	        ), obj_lab, COLOR_BGR2Lab);
	cvtColor(ctx.modified_img_v[CTX::IDX_RECORD], scn_lab, COLOR_BGR2Lab);

	vector<Mat> obj_split_lab, scn_split_lab;
	split(obj_lab, obj_split_lab);
	split(scn_lab, scn_split_lab);

	Mat object, scene, padded_mask;
	utility::pad_image(255 - ctx.field_mask, padded_mask, padding);

	obj_split_lab[0].copyTo(object);
	scn_split_lab[0].copyTo(scene, padded_mask);

	utility::pad_image(object, object, padding);

	std::vector<KeyPoint> obj_kpts, scn_kpts;
	Mat obj_descriptors, scn_descriptors;
	fdetector->detect(object, obj_kpts);
	fdetector->detect(scene, scn_kpts);

	fdetector->compute(object, obj_kpts, obj_descriptors);
	fdetector->compute(scene, scn_kpts, scn_descriptors);

	if (scn_kpts.empty() || obj_kpts.empty()) {
		return 0;
	}

	vector<DMatch> filtered_knn_matches{};
	map<DMatch, int, dmatch_comp> knn_match_counter{};

	vector<vector<DMatch>> knn_matches, inv_knn_matches;

	dmatcher->knnMatch(obj_descriptors, scn_descriptors, knn_matches, 2);
	dmatcher->knnMatch(scn_descriptors, obj_descriptors, inv_knn_matches, 2);

	const float ratio_thresh = 0.9f; // higher lets more through
	for (size_t i = 0; i < knn_matches.size(); ++i) {
		if (knn_matches[i][0].distance < ratio_thresh * knn_matches[i][1].distance) {
			knn_match_counter[knn_matches[i][0]]++;
		}
	}

	for (size_t i = 0; i < inv_knn_matches.size(); ++i) {
		if (inv_knn_matches[i][0].distance < ratio_thresh * inv_knn_matches[i][1].distance) {
			DMatch inverted = inv_knn_matches[i][0];
			inverted.queryIdx = inv_knn_matches[i][0].trainIdx;
			inverted.trainIdx = inv_knn_matches[i][0].queryIdx;
			knn_match_counter[inverted]++;
		}
	}

	for (const auto& mc : knn_match_counter) {
		if (mc.second > 1) {
			const auto& kp_o = obj_kpts[mc.first.queryIdx];
			Size o_diameter(kp_o.size, kp_o.size);
			int o_radius = o_diameter.width / 2;
			const auto& kp_s = scn_kpts[mc.first.trainIdx];
			Size s_diameter(kp_s.size, kp_s.size);
			int s_radius = s_diameter.width / 2;
			vector<int> directions{0, 0, 0, 0};
			Mat roi_o = ctx.modified_img_v[CTX::IDX_ADVERT](
			                Rect(kp_o.pt.x - o_radius, kp_o.pt.y - o_radius,
			                     clamper<int>::directional_clamp(0, static_cast<int>(ctx.modified_img_v[CTX::IDX_ADVERT].cols - (kp_o.pt.x - o_radius)), o_diameter.width, directions[0]),
			                     clamper<int>::directional_clamp(0, static_cast<int>(ctx.modified_img_v[CTX::IDX_ADVERT].rows - (kp_o.pt.y - o_radius)), o_diameter.height, directions[1]))
			        );
			Mat roi_s = ctx.modified_img_v[CTX::IDX_RECORD](
			                Rect(kp_s.pt.x - s_radius, kp_s.pt.y - s_radius,
			                     clamper<int>::directional_clamp(0, static_cast<int>(ctx.modified_img_v[CTX::IDX_RECORD].cols - (kp_s.pt.x - s_radius)), s_diameter.width, directions[2]),
			                     clamper<int>::directional_clamp(0, static_cast<int>(ctx.modified_img_v[CTX::IDX_RECORD].rows - (kp_s.pt.y - s_radius)), s_diameter.height, directions[3]))
			        );

			//Mat obj_hsv, scn_hsv;
			//cvtColor(roi_o, obj_hsv, COLOR_BGR2HSV);
			//cvtColor(roi_s, scn_hsv, COLOR_BGR2HSV);

			vector<cv::Mat> bgr_planes_o;
			split(roi_o, bgr_planes_o);
			vector<cv::Mat> bgr_planes_s;
			split(roi_s, bgr_planes_s);
			vector<Mat> hist_o(3), hist_s(3);

			int bins = 256;
			int hist_size[] = {bins, bins, bins};
			float vranges[]{0, 255};
			const float* ranges[] = { vranges, vranges, vranges };
			int channels[] = { 0, 1, 2 };

			for (size_t i = 0; i < hist_o.size(); ++i) {
				cv::calcHist(&bgr_planes_o[i], 1, 0, cv::Mat(), hist_o[i], 1, hist_size,
				             ranges, true, false);
				cv::calcHist(&bgr_planes_s[i], 1, 0, cv::Mat(), hist_s[i], 1, hist_size,
				             ranges, true, false);
			}

			Mat hist_vis_o = drawing::draw_histogram(hist_o, bins, 1);
			Mat hist_vis_s = drawing::draw_histogram(hist_s, bins, 1);

			show_im("hist o", hist_vis_o);
			show_im("hist s", hist_vis_s);

			Mat comp_hist_o, comp_hist_s;
			calcHist(&roi_o, 1, channels, cv::Mat(), comp_hist_o, 2, hist_size, ranges, true, false );
			cv::normalize(comp_hist_o, comp_hist_o, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());
			calcHist(&roi_s, 1, channels, cv::Mat(), comp_hist_s, 2, hist_size, ranges, true, false );
			cv::normalize(comp_hist_s, comp_hist_s, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

			double hist_comp1 = compareHist(comp_hist_o, comp_hist_s, 1);
			double hist_comp2 = compareHist(comp_hist_o, comp_hist_s, 2);
			double hist_comp3 = compareHist(comp_hist_o, comp_hist_s, 3);
			cerr << hist_comp1 << " | " << hist_comp2 << " | " << hist_comp3 << endl;
			//if (hist_comp1 < 2 || (hist_comp2 > 0.02 || hist_comp3 < 0.98)) {
			if (hist_comp1 < 5 || (hist_comp2 > 0.1 || hist_comp3 < 0.99)) {
				filtered_knn_matches.push_back(mc.first);
			}
		}
	}

	Mat img_knn_matches;
	custom_cv::draw_matches(object, obj_kpts, scene, scn_kpts, filtered_knn_matches, img_knn_matches, Scalar::all(-1),
	                        Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	if (filtered_knn_matches.size() > 0) {
		drawing::draw_square(img_knn_matches, img_knn_matches.cols - 5, 5, 5, Scalar(0, 255, 0));
	 } else {

		drawing::draw_square(img_knn_matches, img_knn_matches.cols - 5, 5, 5, Scalar(0, 0, 255));
	}

	show_im("keypoint matches knn filtered", img_knn_matches);
	return filtered_knn_matches.size();
}

vector<KeyPoint> feature_analyzer::get_points_from_area(int area_r, const KeyPoint& kpt, const vector<KeyPoint>& pts) {
	vector<KeyPoint> area_pts;
	int r_sq = area_r * area_r;
	for (size_t i = 0; i < pts.size(); ++i) {
		int dx = pts[i].pt.x - kpt.pt.x;
		int dy = pts[i].pt.y - kpt.pt.y;
		if ((dx * dx + dy * dy) < r_sq) {
			area_pts.push_back(pts[i]);
		}
	}
	return area_pts;
}

bool feature_analyzer::do_the_scalesauce_dance(scalespace_analyzer& ssd, const Mat& input_a, const Mat& input_b) {
	Mat squished_b;
	cv::resize(input_b, squished_b, Size(0, 0), 0.5, 1);

	Mat a_ref, rotated_a;
	input_a.copyTo(a_ref);

	return ssd.run_on_input(input_a, squished_b, a_ref);
}

void feature_analyzer::show_fancy_results(CTX& ctx, bool do_empty) {
	Mat original_img;
	Mat original_adv;

	Mat conv_3;

	Mat concat_half_left;
	Mat concat_half_right;

	utility::depad_image(conv_3, conv_3, padding);
	utility::pad_image(conv_3, conv_3, 1, Scalar(255, 255, 255));
	utility::pad_image(conv_3, conv_3, padding - 1);

	Mat resized_advert_image;
	double factor = static_cast<double>(2 * padding + 2 * original_img.cols) / static_cast<double>(original_img.cols);
	double divisor = static_cast<double>(original_adv.cols) / static_cast<double>(original_img.cols);
	factor /= divisor;
	resize(original_adv, resized_advert_image, Size(0, 0), factor, factor);
	utility::pad_image(resized_advert_image, resized_advert_image, padding, 0, padding, padding);

	Mat full_img;

	if (!do_empty) {

		Mat mask_w_alpha;
		Mat alphamasked_img;
		utility::alpha_mask(mask_w_alpha, original_img, alphamasked_img);

		utility::pad_image(alphamasked_img, alphamasked_img, 1, Scalar(0, 255, 0));
		utility::pad_image(alphamasked_img, alphamasked_img, padding - 1);

		vconcat(conv_3, alphamasked_img, concat_half_right);

		Mat specialized_visuals;
		alphamasked_img.copyTo(specialized_visuals);

	} else {
		Mat masked_img = Mat::zeros(ctx.modified_img_v[CTX::IDX_RECORD].rows, ctx.modified_img_v[CTX::IDX_RECORD].cols, CV_8UC3);
		utility::depad_image(masked_img, masked_img, padding);
		utility::pad_image(masked_img, masked_img, 1, Scalar(0, 0, 255));
		utility::pad_image(masked_img, masked_img, padding - 1);
		vconcat(conv_3, masked_img, concat_half_right);
	}

	hconcat(concat_half_left, concat_half_right, full_img);

	vconcat(full_img, resized_advert_image, full_img);
	resize(full_img, full_img, Size(0, 0), 0.8, 0.8);
}

void feature_analyzer::save_results_and_compare_to_gt() {
	ofstream of_res("../results/reses.txt");

	result::median_filter(full_result_history, 15);
	for (size_t i = 0; i < full_result_history.size(); ++i) {
		of_res << (i + 1) << "=" << full_result_history[i] << "," << full_result_history[i].time() <<
		          "," << full_result_history[i].cpu_use() << "," << full_result_history[i].extra_str() << endl;
	}

	ofs_cpu_log.close();
	of_res.close();
	ifs_comparison.close();

	std::ofstream res_gt_cmp("../results/res_gt_cmp.txt");
	std::ifstream gt("../results/gen_gtn.txt");
	std::ifstream rs("../results/reses.txt");

	std::string line_gt;
	std::string line_rs;

	while (true) {
		std::getline(gt, line_gt);
		std::getline(rs, line_rs);

		if (line_gt.empty() || line_rs.empty()) {
			break;
		}

		auto gtl = utility::split(line_gt, '=');
		auto rsl = utility::split(line_rs, '=');

		uchar comp = 0b00;
		if (gtl[1][1] == '1') {
			comp |= 0b10;
		}
		if (rsl[1][0] == '1') {
			comp |= 0b01;
		}

		switch (comp) {
		case 0b00:
			res_gt_cmp << "N";
			break;
		case 0b01:
			res_gt_cmp << "p";
			break;
		case 0b10:
			res_gt_cmp << "n";
			break;
		case 0b11:
			res_gt_cmp << "P";
			break;
		}

		res_gt_cmp << gtl[0] << gtl[1][0] << "=" << ((comp & 0b10) >> 1) << "," << (comp & 0b01) << "\n";
	}
	res_gt_cmp.flush();
}
