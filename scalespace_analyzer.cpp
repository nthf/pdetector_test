﻿#include "scalespace_analyzer.h"

int const scalespace_analyzer::GT_GEN_FRAME_LAST = 91000;

bool const scalespace_analyzer::CORRECT_COLORS_PRE_SSGEN = true;
bool const scalespace_analyzer::USE_OUT_OF_RANGE_HUE_VALUES = false;
int const scalespace_analyzer::OUT_OF_RANGE_WHITE = 230;
int const scalespace_analyzer::OUT_OF_RANGE_BLACK = 210;
uchar const scalespace_analyzer::RESERVED_UNUSED_HUE = 215;

using namespace cv;
using namespace std;
using namespace rope;
using namespace quick_access_utility;

void scalespace_analyzer::parallel_gen_scalespace_exp(
        Mat& img, vmat& scalespace) {
	const int levels = static_cast<int>(log2(img.rows / 2));
	if (levels < 0) {
		return;
	}
	scalespace.resize(static_cast<size_t>(levels));
	uint n_threads = thread::hardware_concurrency();

#pragma omp parallel for num_threads(n_threads) schedule(static)
	for (uint i = 0; i < static_cast<uint>(levels); ++i) {
		const int sigma = static_cast<int>(pow(2, i));

		Mat h = Mat::zeros(img.rows, img.cols, img.type());
		gauss_iir(img, h, sigma / 2);
		scalespace[i] = h;
	}
}

void scalespace_analyzer::parallel_gen_scalespace_halving(
        Mat& img, vmat& scalespace) {
	scalespace.resize(ss_generation_levels_);
	uint n_threads = min(static_cast<uint>(ss_generation_levels_),
	                     thread::hardware_concurrency());

#pragma omp parallel for num_threads(n_threads) schedule(static)
	for (uint i = 0; i < static_cast<uint>(ss_generation_levels_); ++i) {
		const int sigma =
		        img.rows / static_cast<int>(pow(2, ss_generation_levels_ - i));
		Mat h = Mat::zeros(img.size(), img.type());
		gauss_iir(img, h, sigma / 2);
		scalespace[i] = h;
	}
}

void scalespace_analyzer::generate_scalespace(Mat& frame_a, Mat& frame_b,
                                              vmat& scalespace_a,
                                              vmat& scalespace_b) {
	parallel_gen_scalespace_exp(frame_a, scalespace_a);
	parallel_gen_scalespace_halving(frame_b, scalespace_b);
}

void scalespace_analyzer::generate_scalespace(Mat& img,
                                              vmat& scalespace,
                                              char mode) {
	switch (mode) {
	case 'a':
		parallel_gen_scalespace_exp(img, scalespace);
		return;
	case 'b':
		parallel_gen_scalespace_halving(img, scalespace);
		return;
	}
}

bool scalespace_analyzer::seek_similarities(const vector<uchar>& values_a,
                                            const vector<uchar>& values_b,
                                            int* offset,
                                            bool comparing_saturation) {
	if (values_b.empty() || values_a.empty()) {
		return false;
	}

	const auto res =
	        comparing_saturation
	        ? find_end(begin(values_a), end(values_a), begin(values_b),
	                   end(values_b), std::bind(&scalespace_analyzer::saturation_compare, this, std::placeholders::_1, std::placeholders::_2))
	        : find_end(begin(values_a), end(values_a), begin(values_b),
	                   end(values_b), std::bind(&scalespace_analyzer::hue_compare, this, std::placeholders::_1, std::placeholders::_2));
	*offset = static_cast<int>(res - values_a.begin());
	return res != values_a.end();
}

double scalespace_analyzer::get_weight(const vector<uchar>& values) {
	double sum = accumulate(values.begin(), values.end(), 0.0);
	double mean = sum / values.size();

	vector<double> diff(values.size());
	transform(values.begin(), values.end(), diff.begin(),
	          [mean](double x) { return x - mean; });
	double sq_sum = inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	double stdev = sqrt(sq_sum / values.size());
	double d = stdev / 127.5;
	return (floor(d) + floor((d - floor(d)) * 10.0 + 0.5) / 10.0 + 0.1);
}

bool scalespace_analyzer::seek_similarities(
        const vector<uchar>& values_a, const line_ss_value& values_b, int x, int y,
        vector<pair<uint, uint>>& scale_sequence, map<int, int>& scale_count,
        map<double, double>& weight_count, bool comparing_saturation,
        bool differential, bool bg) {
	if (values_b.empty() || values_a.empty() || values_a.size() < values_b.size()) {
		return false;
	}

	size_t size_a = values_a.size();
	size_t size_b = values_b.size();
	bool anyfound = false;

	if (differential) {
		bool good_matchup = false;
		if (abs(im_secondary_.at(0)->at<uchar>(y, x) - values_b.get_val()) > 200) {
			for (size_t i = 0; i <= size_a - size_b; ++i) {
				uint count_similiarity = 0;
				for (size_t j = 0; j < size_b; ++j) {
					if (hue_compare_w_tolerance(values_a[i + j], values_b[j], hue_tolerance_ * 3)) {
						++count_similiarity;
					} else {
						count_similiarity = 0;
					}
				}
				if (count_similiarity == size_b || (!bg && count_similiarity >= 1)) {
					good_matchup = true;
					break;
				}
			}
		}
		// the values themselves should be at least somewhat similar first of all
		if (good_matchup) {
			nullable_int prev_a = values_a.front();
			nullable_int prev_b = values_b.front();
			for (size_t k = 0; k <= values_a.size() - values_b.size(); ++k) {
				uint count = 0;
				for (size_t l = 0; l < values_b.size(); ++l) {
					if (std::abs(values_a[k + l] - excluded_hue_) > hue_tolerance_ &&
					        hue_compare_relative_progress(values_a[k + l],
					                                      values_b[l],
					                                      DO_CHECK_THROUGH_DIFFERENCE__ ?
					                                      (bg ? /*3 */ hue_tolerance_ / 2 :
					                                       3 * hue_tolerance_) :
					                                      hue_tolerance_, prev_a, prev_b)) {
						++count;
					}
				}
				if (count >= 1) {
					scale_sequence.emplace_back(static_cast<uint>(k), bg ? 127 : 255);
					anyfound = true;
					++scale_count[static_cast<int>(k)];
				}
			}
		}
		return anyfound;
	}

	// currently outdated section, results in zero detections
	for (size_t i = 0; i <= size_a - size_b; ++i) {
		uint count = 0;
		for (size_t j = 0; j < size_b; ++j) {
			if ((!comparing_saturation &&
			     hue_compare(values_a[i + j], values_b[j])) ||
			        (comparing_saturation &&
			         saturation_compare(values_a[i + j], values_b[j]))) {
				++count;
			}
		}
		if (count >= 1) {
			double weight = get_weight(values_a);
			scale_sequence.emplace_back(static_cast<uint>(i), weight);
			anyfound = true;
			//@ #pragma omp critical
			{
				++scale_count[static_cast<int>(i)];
				weight_count[weight] += get_weight_curve(weight);
			}
		}
	}
	return anyfound;
}

void scalespace_analyzer::populate_point_vector(
        vector<WPoint3>& points, vector<pair<uint, uint>>& matches, int x,
        int y) {
//@ #pragma omp critical
	for (auto& p : matches) {
		points.emplace_back(x, y, static_cast<int>(p.first), p.second);
	}
}

std::vector<line_ss_value> scalespace_analyzer::get_values_on_line(bool round_down,
                                                         const Mat& base_img, const vmat& ss, int offset, int length,
                                                         map<uchar, int>& unique_colors, int* background) {
	map_vuc_i line_values;
	map<vector<uchar>, map<uchar, uint>> val_assigner;
	map<int, int> background_counter;
	uchar max_difference = 12;
	int hits = 0;
	int bg_hits = 0;
	int step = 0.003 * base_img.cols;
	Mat visualization;
	cvtColor(base_img, visualization, cv::COLOR_GRAY2BGR);
	for (int j = 0; j < length; ++j/* += step*/) {
		vector<uchar> scale_space_values_in_pixel;
		uchar sat = im_secondary_.at(1)->at<uchar>(offset, offset + j);
		set<uchar> value_set;
		for (auto& img : ss) {
			uchar p = img.at<uchar>(offset, offset + j);
			if (round_down) {
				p = static_cast<uchar>(
				        (p / max(1, value_round_down_))
				        * max(1, value_round_down_));
			}
			if (std::abs(p - excluded_hue_) <= 1.5 * hue_tolerance_) {
				scale_space_values_in_pixel.clear();
				break;
			}
			scale_space_values_in_pixel.push_back(p);
			value_set.insert(p / (max_difference * 2));
		}
		if (value_set.size() == 1) {
			++background_counter[scale_space_values_in_pixel.front()];
			vector<uchar> vectorized_val{scale_space_values_in_pixel.front()};
			++line_values[vectorized_val];
			val_assigner[vectorized_val][sat] += 1;

			drawing::draw_cross(visualization, Point(offset + j, offset), 3, Scalar(0, 255, 0));
			scale_space_values_in_pixel.clear();
			//j += step;
			++bg_hits;
			continue;
		}
		//if (value_set.size() != scale_space_values_in_pixel.size()) {
		if (scale_space_values_in_pixel.empty() || utility::all_same(scale_space_values_in_pixel, max_difference) /*|| value_set.size() == 1*/) {
			continue;
		}
		drawing::draw_cross(visualization, Point(offset + j, offset), 3, Scalar(0, 0, 255));
		uchar c = static_cast<uchar>((base_img.at<uchar>(offset, offset + j) /
		                            (max(1, value_round_down_))) *
		                            (max(1, value_round_down_)));
		++hits;
		++unique_colors[c];
		++line_values[scale_space_values_in_pixel];
		val_assigner[scale_space_values_in_pixel][sat] += 1;
		scale_space_values_in_pixel.clear();
		j += step;
	}

	auto bg_max = utility::get_max(background_counter);
	if (!background_counter.empty()) {
		if (background != nullptr) {
			*background = bg_max.first;
		}
	}

	//show_im("picked points", visualization);
	std::vector<line_ss_value> ret_values;
	for (auto& v : line_values) {
		int count = v.second;
		auto top_sat = max_element(val_assigner[v.first].begin(), val_assigner[v.first].end());
		if (v.first.size() > 1 || v.first.front() != bg_max.first) {
			ret_values.push_back(line_ss_value(v.first, top_sat->second, count, false));
		} else if (v.first.size() == 1) {
			ret_values.push_back(line_ss_value(v.first, top_sat->second, count, true, ss.size()));
		}
	}
	return ret_values;
}

void scalespace_analyzer::rect_se_morphology(Mat& img, uint* count, morph_ops op, int se_width, int se_height) {
	se_width = min_1(se_width);
	se_height = min_1(se_height);
	if (se_width == 1 && se_height == 1) {
		return;
	}
	switch(op) {
	case morph_ops::CLOSE_OPEN:
		morphologyEx(img, img, MORPH_CLOSE, getStructuringElement(
		                 MORPH_RECT, Size(min_1(static_cast<int>(se_width - 0.2 * se_width)), se_height)));
		morphologyEx(img, img, MORPH_OPEN, getStructuringElement(
		                 MORPH_RECT, Size(se_width, se_height)));
		break;
	case morph_ops::OPEN_CLOSE:
		morphologyEx(img, img, MORPH_OPEN, getStructuringElement(
		                 MORPH_RECT, Size(min_1(static_cast<int>(se_width - 0.2 * se_width)), se_height)));
		morphologyEx(img, img, MORPH_CLOSE, getStructuringElement(
		                 MORPH_RECT, Size(se_width, se_height)));
		break;
	case morph_ops::CLOSE:
		morphologyEx(img, img, MORPH_CLOSE, getStructuringElement(
		                 MORPH_RECT, Size(static_cast<int>(se_width), se_height)));
		break;
	case morph_ops::OPEN:
		morphologyEx(img, img, MORPH_OPEN, getStructuringElement(
		                 MORPH_RECT, Size(static_cast<int>(se_width), se_height)));
		break;
	case morph_ops::ERODE:
		morphologyEx(img, img, MORPH_ERODE, getStructuringElement(
		                 MORPH_RECT, Size(static_cast<int>(se_width), se_height)));
		break;
	case morph_ops::DILATE:
		morphologyEx(img, img, MORPH_DILATE, getStructuringElement(
		                 MORPH_RECT, Size(static_cast<int>(se_width), se_height)));
		break;
	}
	*count = static_cast<uint>(countNonZero(img));
}

Mat scalespace_analyzer::points_to_image(const vector<WPoint3>& match_points,
                                         const uint match_count, const int scale,
                                         const int h, const int w, Mat& mask) {
	Mat img = Mat::zeros(h, w, CV_8UC1);
	bool few_matches = match_count < (0.025 * img.rows * img.cols);
	set<tuple<int, int, double>> coords;
	for (const auto& p : match_points) {
		if ((((p.z() == 1 && scale == 0) || (scale == 1 && p.z() == 0)) ||
		     //(scale > 2 && p.z == scale) || (scale <= 2 && abs(p.z - scale) <= 1)
		     p.z() == scale || (few_matches && abs(p.z() - scale) <= 2))) {
			coords.insert(make_tuple(p.y(), p.x(), p.w()));
			// img.at<uchar>(p.y, p.x) = 255;
		}
	}
	for (const auto& c : coords) {
		int y = get<0>(c);
		int x = get<1>(c);
		auto* imptr = img.ptr<uchar>(y);
		*(imptr + x) =
		        static_cast<uchar>(min(255.0, *(imptr + x) + get<2>(c) * 255));
		// img.at<uchar>(y, x) = static_cast<uchar>(min(255.0, img.at<uchar>(y,
		// x) + get<2>(c) * 255));

		auto* mkptr = mask.ptr<uchar>(y);

		int plop = get<2>(c);
		//if (plop == 255) {
			*(mkptr + x) = plop;
		//}
		// mask.at<uchar>(y, x) = 255;
	}
	return img;
}

double scalespace_analyzer::nonzero_image(const Mat& img, const rect& r,
                                          int overlap /* = 2*/) {
	if (overlap > 0) {
		rect new_r = rect(min(img.cols - r.r().width - 1, max(0, r.r().x)),
		               min(img.rows - (r.r().height + overlap) - 1,
		                   max(0, r.r().y - overlap / 2)),
		               r.r().width, r.r().height + overlap);
		Mat roi = img(new_r.r());
		return static_cast<double>(countNonZero(roi)) / static_cast<double>(r.area());
	}
	Mat roi = img(r.r());
	return static_cast<double>(countNonZero(roi)) / static_cast<double>(r.area());
}

pair<bool, Rect> scalespace_analyzer::gap_check(Mat& img, Rect& rec, Mat& out) {
	if (rec.x + rec.width >= img.cols) {
		Rect new_rec(rec.x + rec.width, rec.y, rec.width, rec.height);
		drawing::draw_rectangle(out, new_rec, Scalar(120, 255, 0));
		return make_pair(true, new_rec);
	}

	const int offset = rec.width * 2;
	int x = rec.x + offset - 4;

	if (x >= img.cols || x + rec.width >= img.cols) {
		x -= rec.width;
		x += 2;
		if (x >= img.cols || x + rec.width >= img.cols) {
			Rect new_rec(x, rec.y, img.cols - x, rec.height);
			drawing::draw_rectangle(out, new_rec, Scalar(255, 0, 120));
			return make_pair(nonzero_image(img, new_rec) > 0.0, new_rec);
		}
	}
	Rect r(x, rec.y, rec.width, rec.height);
	bool res = nonzero_image(img, r) > 0.0;

	if (!res) {
		r = Rect(x - rec.width + 2, rec.y, rec.width, rec.height);
		res = nonzero_image(img, r, 0.0) > 0.0;
	}

	drawing::draw_rectangle(out, r, Scalar(0, 255, 120));
	return make_pair(res, r);
}

bool scalespace_analyzer::one_significant_line(vector<int>& lengths,
                                               int expected) {
	int count = 0;
	const int significant = expected / 3;
	for (int len : lengths) {
		if (len >= significant) {
			++count;
		}
		if (count > 1) {
			return false;
		}
	}
	return count == 1;
}

void scalespace_analyzer::add_new_rectangles(stack<rect>& s,
                                             set<rect, rcomp>& searched,
                                             const Mat& img, Mat& out,
                                             bool binary, rect& r, int w,
                                             int h) {
	const int new_x = r.r().x + w - 2;
	const int new_w = min(img.cols - new_x, w);
	if (binary) {
		const int new_y_up = r.r().y - (h / 2);
		int new_y_dn = r.r().y + (h / 2);

		if (r.r().height == h / 2 && r.r().y == 0) {
			new_y_dn = r.r().y;
		} else {
			rect upr(new_x, max(0, new_y_up), new_w, new_y_up < 0 ? h / 2 : h, -1);
			if (searched.find(upr) == searched.end() && (new_y_up >= h * r_search_padding_f_)) {
				drawing::draw_rectangle(out, upr.r(), Scalar(0, 120, 255), 2);
				s.push(upr);
			}
		}

		if (new_y_dn + h / 2 < img.rows) {
			rect dnr(new_x, min(img.rows - 1, new_y_dn), new_w,
			         new_y_dn >= img.rows ? 0 : min(h, img.rows - 1 - new_y_dn), 1);
			if (searched.find(dnr) == searched.end() && ((new_y_dn + h - 1) <= img.rows - h * r_search_padding_f_)) {
				drawing::draw_rectangle(out, dnr.r(), Scalar(255, 255, 255), 2);
				s.push(dnr);
			}
		}
	} else {
		rect next(new_x, r.r().y, new_w, h);
		if (searched.find(next) == searched.end()) {
			s.push(next);
		}
	}
}

bool scalespace_analyzer::check_max_count(const uint match_count,
                                          const uint color_count,
                                          const uint scale, const Mat& img) {
	uint max_count_for_given_scale =
	        max_for_scale(img.cols, scale, color_count, color_samples_until_exclusion_);

	cout << "current/max: " << match_count << " / " << magenta << bold
	     << max_count_for_given_scale << reset << " @ " << scale << "\n";
	return match_count <= max_count_for_given_scale;
}

void scalespace_analyzer::init_rectangle_search(
        int* h, int* w, int* gaps_allowed, bool* binary, int* max_slope,
        const uint match_count, const int scale, const int max_scale,
        const Mat& img) {
	*h = a_ref_.rows / (2 * (rectangle_size_factor_ + 4 * resize_));
	*w = a_ref_.cols / (2 * (rectangle_size_factor_ + 4 * resize_));
	//*h = static_cast<int>(*h * (1 - (0.15 + 0.05625 * (scale - 1))));
	*h = max(2, static_cast<int>(*h * (1 - 0.275 * (static_cast<double>(scale) /
	                                         static_cast<double>(max_scale)))));
	*w = max(2, static_cast<int>(*w * (1 - 0.275 * (static_cast<double>(scale) /
	                                         static_cast<double>(max_scale)))));
	// ensure the rectangles are not a 1px line

	//*w = static_cast<int>(*w * (1 - (0.15 + 0.05625 * (scale - 1))));

	*h += *h & 0x01;
	*w += *w & 0x01;

	*binary = scale <= (max_scale - max_scale / 3) ||
	          match_count <= static_cast<uint>((img.rows * img.cols) / 3);

	int count_nz = countNonZero(img);
	*gaps_allowed = max_gaps_allowed_ - min(max_gaps_allowed_, scale - 1) + (static_cast<double>(count_nz) / (img.cols * img.rows) < 0.15 ? 1 : 0);

	int inv_h = img.rows / rectangle_size_factor_;
	inv_h = static_cast<int>(
	            (inv_h * (1 - 0.50 * (static_cast<double>(max_scale - scale) /
	                                  static_cast<double>(max_scale)))));
	*max_slope = static_cast<int>((static_cast<double>(img.rows) / static_cast<double>(inv_h)) * 0.7);
}

bool scalespace_analyzer::test_combined_between_rect(const Mat& img, rect& curr_r, rect& next_r, double min_ratio) {
	if (curr_r.x != next_r.x || curr_r.dir() != -1 * next_r.dir()) {
		return false;
	}
	rect combined_r(curr_r.x, curr_r.dir() == -1 ? curr_r.y : next_r.y,
	                curr_r.width, curr_r.height);
	if (nonzero_image(img, combined_r) > min_ratio) {
		curr_r = combined_r;
		return true;
	}
	return false;
}

bool scalespace_analyzer::accept_rectangle(
        int* j, int* length, const bool* binary, const int* w, const int* h,
        int* slope, Mat& out, rect& r, stack<rect>& s, set<rect, rcomp>& searched,
        const Mat& img, vector<int>& lengths, int max_slope, int* max_up,
        int* max_dn, double* ratio /* = nullptr */, double min_ratio /* = 0.0*/) {
	if (ratio != nullptr && *ratio <= min_ratio) {
		// the combining has... weird results (more false positives and negatives)
		//if (!s.empty() && !test_combined_between_rect(img, r, s.top(), min_ratio)) {
			drawing::draw_rectangle(out, r.r(), Scalar(0, 0, 255), 3);
			return false;
		//}
	}
	if (abs(*slope + r.dir()) > max_slope) {
		if (r.r().x + r.r().width + 1 >= img.cols) {
			*j = r.r().x;
			drawing::draw_rectangle(out, r.r(), Scalar(100, 100, 100));
			return true;
		}
		return false;
	}

	++(*length);
	drawing::draw_rectangle(out, r.r(), Scalar(255, 255, 120));
	*slope += r.dir();

	if (*slope > *max_dn) {
		*max_dn = *slope;
	}
	if (*slope < *max_up) {
		*max_up = *slope;
	}
	add_new_rectangles(s, searched, img, out, *binary, r, *w, *h);

	++lengths.back();
	*j = r.r().x;
	return true;
}

bool scalespace_analyzer::good_verticality(Mat& img, int w, int h) {
	int w_cutoff = img.cols - 1;
	int h_cutoff = img.rows - 1;
	int count_nonempty = 0;
	int count_total = 0;
	for (int i = 0; i < img.rows; i += h) {
		rect rec(0, i, min(w_cutoff, 3 * w), min(h_cutoff - i, h));
		if (nonzero_image(img, rec) > 0.22) {
			++count_nonempty;
		}
		++count_total;
	}

	return count_nonempty < 0.5 * count_total;
}

bool scalespace_analyzer::rectangle_structure_search(Mat& img, uint match_count,
                                                     const uint color_count,
                                                     const int scale,
                                                     int max_scale) {
	(void)color_count;
	int h;
	int w;
	int gaps_allowed;
	int max_slope;
	bool binary = false;

	init_rectangle_search(&h, &w, &gaps_allowed, &binary, &max_slope, match_count,
	                      scale, max_scale, img);

	//if (!check_max_count(match_count, color_count, scale, img)) {
	//	Mat out = give_mat(img);
	//	show_im("Rectangle search result", out);
	//	return false;
	//}

	Mat out;

	if (SHOW_IMAGES__) {
		Mat tmp[] = {img, img, img};
		merge(static_cast<Mat *>(tmp), 3, out);
	}

	vector<int> lengths;
	set<rect, rcomp> searched;

	int i = scale == 1 ? 0 : h * r_search_padding_f_;
	if (h == 0) {
		return false;
	}
	for (; i < img.rows - (h * 2); i += h) {
		int x = 0;
		rect rec(x, i, min(img.cols - x - 1, w), min(img.rows - i - 1, h));

		stack<rect> s;
		s.push(rec);

		int gaps = 0;
		int j = 0;
		int length = 0;
		int slope = 0;
		int max_up = 0;
		int max_dn = 0;
		lengths.push_back(length);
		bool good_rectangle = false;
		double rectangle_fill_ratio = 0.0;
		while (!s.empty() && j + w < img.cols && i + h < img.rows) {
			// show_im("progress", out);
			// key_wait(0);
			rect r = s.top();
			s.pop();
			searched.insert(r);
			// draw_rectangle(out, r.r_, Scalar(160, 160, 160), 2);
			Mat roi_s = img(r.r());

			rectangle_fill_ratio = nonzero_image(img, r);
			good_rectangle = rectangle_fill_ratio > 0.45;
			if (!good_rectangle) {
				drawing::draw_rectangle(out, r.r(), Scalar(255, 0, 0));
				// too many gaps, do possibly final checks
				if (gaps >= gaps_allowed && gaps > 0) {
					drawing::draw_rectangle(out, r.r(), Scalar(255, 255, 0), 2);
					// either it's the very end or there's a very good reason to continue on this line?
					if (r.r().width < w/* || rectangle_fill_ratio > 0.5*/) {
						good_rectangle = accept_rectangle(
						                     &j, &length, &binary, &w, &h, &slope, out, r, s, searched, img,
						                     lengths, max_slope, &max_up, &max_dn, &rectangle_fill_ratio, rect_accept_ratio_);
					}
					continue;
				}
				if (!s.empty() && s.top().r().x == r.r().x) {
					rect backup_r = s.top();
					s.pop();
					if (nonzero_image(img, backup_r) > 0.0) {
						r = backup_r;
						// roi_s = img(backup_r.r_);
						good_rectangle = accept_rectangle(
						                     &j, &length, &binary, &w, &h, &slope, out, r, s, searched, img,
						                     lengths, max_slope, &max_up, &max_dn, &rectangle_fill_ratio, rect_accept_ratio_);
						drawing::draw_rectangle(out, r.r(), Scalar(0, 255, 0));
						continue;
					}
				}
				//if (gaps < gaps_allowed) {
				    pair<bool, Rect> gap = gap_check(img, r.r(), out);
					if (!gap.first) {
						drawing::draw_rectangle(out, gap.second, Scalar(0, 255, 255));
						if (gap.second.x > img.cols / 2) {
							drawing::draw_rectangle(out, gap.second, Scalar(0, 0, 255));
						}
						// if (s.empty()) {
						continue;
						//}
					}
					r = gap.second;
					++gaps;
					drawing::draw_rectangle(out, gap.second, Scalar(255, 0, 255));
					j = gap.second.x;
				//}

				if (j + w >= img.cols) {
					drawing::draw_rectangle(out, r.r(), Scalar(255, 120, 0));
					show_im("Rectangle search result", out);
					return true;
				}
			}
			rectangle_fill_ratio = nonzero_image(img, r);
			good_rectangle =
			        accept_rectangle(&j, &length, &binary, &w, &h, &slope, out, r, s,
			                         searched, img, lengths, max_slope, &max_up, &max_dn, &rectangle_fill_ratio, rect_accept_ratio_);
			drawing::draw_rectangle(out, r.r(), Scalar(120, 0, 120));
		}
		if (j + w >= img.cols && good_rectangle) {
			show_im("Rectangle search result", out);
			return binary ? good_verticality(img, w, h) : one_significant_line(lengths, img.cols / w);
		}
	}

	show_im("Rectangle search result", out);
	return binary ? false : good_verticality(img, w, h) && one_significant_line(lengths, img.cols / w);
}

bool scalespace_analyzer::init_scalespace_matching(int* n, uint* match_count, int* bg_color, vector<line_ss_value>& line_values,
                                                   vector<WPoint3>& match_points, const Mat& base_image_main_primary,
                                                   const Mat& base_image_alt_primary, const vmat& ss_to_detect) {
	const int offset = (base_image_alt_primary.rows) / 2;
	const int inside_line_length = (base_image_alt_primary.cols) - 2 * offset;

	*match_count = 0;
	*n = base_image_main_primary.cols * base_image_main_primary.rows;

	map<uchar, int> unique_colors;

	const std::vector<line_ss_value> ss_vector_counter = get_values_on_line(
	                                                     bool(cc_),
	                                                     *im_secondary_.at(1),
	                                                     ss_to_detect,
	                                                     offset,
	                                                     inside_line_length,
	                                                     unique_colors, bg_color);

	line_values = utility::copy_most_common_from_cont(
	                    inside_line_length, ss_vector_counter, color_count_threshold_,
	                    range<uchar>(excluded_hue_ < 0 ? -1 : std::max(0, excluded_hue_ - static_cast<int>(1.5 * hue_tolerance_)),
	                                 std::min(255/* / VALUE_ROUND_DOWN*/, excluded_hue_ + static_cast<int>(1.5 * hue_tolerance_))),
	                    [](const line_ss_value& a, const line_ss_value& b){ return a.get_count() > b.get_count(); },
	                    at_least_vector_samples_, USE_OUT_OF_RANGE_HUE_VALUES, OUT_OF_RANGE_WHITE, OUT_OF_RANGE_BLACK);
	//vector_counts.insert(vector_counts.end(), centerboard_.vector_counts()[centerboard_.current_pos()].begin(), centerboard_.vector_counts()[centerboard_.current_pos()].end());
	//std_out(cout, true, line_ss_value::vector_as_string(line_values, false));

	match_points.reserve(static_cast<size_t>(*n));
	return true;
}

bool scalespace_analyzer::init_scalespace_matching(bool sat_comp,
                                                   int* n, uint* match_count, vpair_vuc_i& vector_counts,
                                                   vector<WPoint3>& match_points, const Mat& base_image_main_primary) {
	*match_count = 0;
	*n = base_image_main_primary.cols * base_image_main_primary.rows;

	//size_t unique_colors = cc_->ad().unique_colors()[cc_->ad_time()];

	vector_counts = cc_->ad().vector_counts()[cc_->ad_time()];

	if (cc_->has_center()) {
		int index = cc_->center().type() == advertisement::type::image
		            ? 0
		            : cc_->ad_time();
		if (!sat_comp) {
			vector_counts.insert(vector_counts.end(),
			                     cc_->center().vector_counts()[index].begin(),
			                     cc_->center().vector_counts()[index].begin() + cc_->center().get_satoffset());
		} else {
			vector_counts.insert(vector_counts.end(),
			                     cc_->center().vector_counts()[index].begin() + cc_->center().get_satoffset(),
			                     cc_->center().vector_counts()[index].end());
		}
	}
	std_out(cout, true, utility::vector_as_string(vector_counts));

	match_points.reserve(static_cast<size_t>(*n));
	return true;
}

bool scalespace_analyzer::weights_have_similar_peaks(map<double, double>& m) {
	vector<double> peaks[2];
	vector<pair<double, double>> values{{-1.0, 0.0}};
	std::transform(m.begin(), m.end(), back_inserter(values),
	               [](const pair<double, double>& p) {
		// rsb p.first << " -> " << p.second << std::endl;
		return p;
	});
	values.emplace_back(-1.0, 0.0);
	vector<pair<double, double>> max(2);
	for (ulong i = 1; i < values.size() - 1; ++i) {
		// rsb lgray << values[i].first << " ~ " << lyellow << values[i].second
		//      << reset << endl;
		if ((values[i].second > values[i - 1].second) &&
		        (values[i].second > values[i + 1].second)) {
			peaks[0].push_back(values[i].second);
			peaks[1].push_back(values[i].first);
			if (!peaks[0].empty() && peaks[1].front() < 0.15 &&
			        peaks[0].back() < 0.4 * peaks[0].front()) {
				uint j = i + 1;
				double current = values[i].second;
				while (j < values.size() &&
				       utility::similar(values[j].second, static_cast<int>(current),
				                        0.25)) {
					peaks[0].back() += values[j].second;
					current = values[j].second;
					++j;
				}
			}
		}
	}

	if (peaks[0].size() <= 2 && values.back().first > 0.95) {
		return false;
	}

	partial_sort_copy(
	            begin(values), end(values), begin(max), end(max),
	            [](const pair<double, double>& l, const pair<double, double>& r) {
		return l.second > r.second;
	});
	// rsb "m1[" << max.front().first << ", " << max.front().second << "]" <<
	// endl; rsb "m2[" << max.back().first << ", " << max.back().second << "]"
	// << endl;
	bool good_two_peaks = max.front().first > 0.25 &&
	                      max.front().second > max.back().second &&
	                      max.front().first != max.back().first;
	// rsb good_two_peaks << " " << utility::is_near_constant<double>(peaks,
	// 0.2, max.front().first > 0.15) << endl;
	return good_two_peaks || utility::is_near_constant<double>(
	            peaks[0], 0.2, max.front().first > 0.15);
}

bool scalespace_analyzer::post_scalespace_search_processing(
        const Mat& im_base_a, Mat& out, map<int, int>& scale_count,
        map<double, double>& weight_count, vector<WPoint3>& match_points,
        uint match_count, uint bg_count, const uint color_count, Mat& hue_finds_in_sat, int n,
        int ss_to_search_sz, int ss_to_detect_sz, bool only_bg) {
	(void)weight_count;
	(void)bg_count;
	// if (color_count == 1) {
	// 	return false;
	// }
	int count_at_scale = 0;
	int scale = get_weighted_max(im_base_a.cols, scale_count, &count_at_scale);
	Mat mask = Mat::zeros(im_base_a.rows, im_base_a.cols, CV_8UC1);
	Mat points = points_to_image(match_points, match_count, scale, im_base_a.rows,
	                             im_base_a.cols, mask);
	// maybe split mask up into bg and non-bg
	// bg at 127, non-bg at 255
	// then non-bg can have smaller morphology, remove big blobs? white top hat retains small blobs
	// open to get rid off small blobs?
	// bg can have big morphology to shave a bunch of stuff away, remove small blobs

	Mat bg, fg, big_bg;
	threshold(mask, bg, 200, 255, THRESH_BINARY_INV); // leaves everything that isn't above 200
	bg = bg & mask;
	threshold(mask, fg, 200, 255, THRESH_BINARY);

	//show_im("bg", bg);
	//show_im("fg", fg);

	int bg_count_before = countNonZero(bg);
	int fg_count_before = countNonZero(fg);
	//int count_before_tophat = countNonZero(bg);
	// when the advertisement is red/white or black/white the foreground is largely empty
	// current values provide good results
	//morphologyEx(fg, fg, MORPH_TOPHAT, getStructuringElement(MORPH_ELLIPSE, Size(51, 51)));
	morphologyEx(fg, fg, MORPH_DILATE, getStructuringElement(MORPH_ELLIPSE, Size(23, 15)));
	//morphologyEx(bg, bg, MORPH_OPEN, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
	if (bg_count_before + fg_count_before > 0.05 * bg.cols * bg.rows) {
		morphologyEx(bg, big_bg, MORPH_TOPHAT, getStructuringElement(MORPH_ELLIPSE, Size(29, 15)));
		morphologyEx(bg, bg, MORPH_CLOSE, getStructuringElement(MORPH_ELLIPSE, Size(9, 7)));
		big_bg = 255 - big_bg;
		bg = bg & big_bg;
	} else {
		morphologyEx(bg, bg, MORPH_DILATE, getStructuringElement(MORPH_ELLIPSE, Size(27, 15)));
	}


	//morphologyEx(bg, bg, MORPH_OPEN, getStructuringElement(MORPH_RECT, Size(41, 11)));

	//// remove this if not good
	//if ((static_cast<double>(countNonZero(bg)) / bg.cols * bg.rows > 0.6) &&
	//        countNonZero(fg) < 10) {
	//	bg *= 0;
	//}

	threshold(bg, bg, 10, 255, THRESH_BINARY);
	mask *= 0;
	mask = bg | fg;
	morphologyEx(mask, mask, MORPH_CLOSE, getStructuringElement(MORPH_RECT, Size(45, 23)));
	int new_count = countNonZero(mask);

	// ends up with worse results
	Mat canny_output, blurred_mask;
	GaussianBlur(mask, blurred_mask, Size(151, 1), 50, 1);
	//gauss_iir(mask, blurred_mask, 15);
	threshold(blurred_mask, blurred_mask, 200, 255, THRESH_BINARY);
	vector<vector<Point>> contours;
	vector<Vec4i> hierarchy;
	//Canny(mask, canny_output, 50, 150, 3);
	findContours(blurred_mask, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));
	if (!contours.empty()) {
		std::sort(contours.begin(), contours.end(), [](const vector<Point>& a, const vector<Point>& b) {
			return a.size() > b.size();
		});
		Mat test_m;
		cvtColor(mask, test_m, COLOR_GRAY2BGR);
		bounding_box_t bbox = bounding_box_t::create(contours.front());
		rectangle(test_m, bbox.r(), Scalar(255, 0, 255), 2);
		//show_im("bounding box show", test_m);
		//int bbox_nz_count = countNonZero(mask(bbox.r()));
		if (bbox.r().height > 0.95 * mask.rows &&
		        (bbox.r().width < 0.1 * mask.cols || bbox.r().width > 0.7 * mask.cols)) {
			mask *= 0;
		}
	}

	// not good when ad bg is red, with white text it's all the same
	//if (static_cast<double>(countNonZero(bg)) / new_count > 5 * static_cast<double>(countNonZero(fg)) / new_count &&
	//        !(match_count < static_cast<uint>(0.005) * bg_count)) {
	//	utility::pad_image(out, out, 1, Scalar(255, 0, 0));
	//	show_im("Scale match finding result", out);
	//	return false;
	//}

	if (new_count < 0.75 * match_count || new_count > 0.6 * mask.rows * mask.cols) {
		utility::pad_image(out, out, 1, Scalar(255, 0, 255));
		show_im("Scale match finding result", out);
		return false;
	}

	drawing::draw_text(out, to_string(scale), Point(out.cols - 20, out.rows - 20), FONT_HERSHEY_SIMPLEX, 1, Scalar(255, 255, 255));
	Mat masked_pts = mask;

	bool color = true;
	Scalar background_color;

	Mat cpy;
	ressz_a_.copyTo(cpy, mask);
	uint added_pts = countNonZero(hue_finds_in_sat);
	if (added_pts < 0.035 * n && scale <= 2) {
		mask |= hue_finds_in_sat;
		match_count += added_pts;

		if (only_bg) {
			if (match_count > 0.66 * n) {
				background_color = Scalar(120, 120, 0);
				utility::pad_image(out, out, 1, background_color);
				show_im("Scale match finding result", out);
				return false;
			}
			rect_se_morphology(mask, &match_count, morph_ops::OPEN_CLOSE,
			                   min_1(match_count / 8000 * 0.05 * mask.cols),
			                   min_1(match_count / 8000 * 0.01 * mask.rows));
		} else {
			if (match_count > 0.5 * n) {
				background_color = Scalar(0, 120, 120);
				utility::pad_image(out, out, 1, background_color);
				show_im("Scale match finding result", out);
				return false;
			}
			rect_se_morphology(mask, &match_count, morph_ops::CLOSE_OPEN,
			                   min_1(match_count / 10000 * 0.02 * mask.cols),
			                   min_1(match_count / 10000 * 0.01 * mask.cols));
		}
		if (scale == 0 && (match_count < std::sqrt(n) * 0.2)) {
			return false;
		}

	} else {
		if (only_bg) {
			if (match_count > 0.66 * n) {
				background_color = Scalar(120, 120, 0);
				utility::pad_image(out, out, 1, background_color);
				show_im("Scale match finding result", out);
				return false;
			}
			uint prev_match_count = match_count;
			rect_se_morphology(mask, &match_count, morph_ops::OPEN_CLOSE,
			                   min_1(match_count / 8000 * 0.05 * mask.cols),
			                   min_1(match_count / 8000 * 0.01 * mask.rows));
			if (match_count < 0.25 * prev_match_count) {
				background_color = Scalar(255, 0, 0);
				utility::pad_image(out, out, 1, background_color);
				show_im("Scale match finding result", out);
				return false;
			}
		} else {
			if (match_count > 0.5 * n) {
				background_color = Scalar(0, 120, 120);
				utility::pad_image(out, out, 1, background_color);
				show_im("Scale match finding result", out);
				return false;
			}
			rect_se_morphology(mask, &match_count, morph_ops::CLOSE_OPEN,
			                   min_1(match_count / 8000 * 0.05 * mask.cols),
			                   min_1(match_count / 8000 * 0.01 * mask.cols));
		}
		//if (scale >= 3 && match_count > 0.1175 * n) {
		//	// if (match_count >= 0.2 * n) {
		//	//points.copyTo(masked_pts, mask);
		//	//mask = masked_pts;
		//	bool detailed_perimeter = weights_have_similar_peaks(weight_count);
		//	if (detailed_perimeter) {
		//		background_color = Scalar(255, 0, 0);
		//	} else {
		//		background_color = Scalar(255, 0, 255);
		//		utility::pad_image(out, out, 1, background_color);
		//		show_im("Scale match finding result", out);
		//		//show_im("Mask", mask);
//		//		return false;
		//	}
		//	color = false;
		//} else if (match_count > 0.08 * n) {
		//	background_color = Scalar(0, 120, 255);
		//	utility::pad_image(out, out, 1, background_color);
		//	show_im("Scale match finding result", out);
		//	//show_im("Mask", mask);
		//	color = false;
//		//	return false;
		//}
	}

	bool line_through_image = false;

	//if (!bg_mask.empty()) {
	//	Mat combined_points_and_bg = mask | bg_mask;
	//	show_im("combined masks", combined_points_and_bg);
	//	line_through_image = rectangle_structure_search(
	//	                         combined_points_and_bg, match_count, color_count, scale,
	//	                         static_cast<int>(ss_to_search_sz - ss_to_detect_sz));
	//} else {
		line_through_image = rectangle_structure_search(
		                         mask, match_count, color_count, scale,
		                         static_cast<int>(ss_to_search_sz - ss_to_detect_sz));
	//}

	if (DEBUG_OUTPUT__) {
		if (line_through_image && color) {
			background_color = Scalar(0, 255, 0);
		} else if (color) {
			background_color = Scalar(255, 0, 255);
		}
		utility::pad_image(out, out, 1, background_color);
		show_im("Scale match finding result", out);
	}

	return line_through_image;
}

bool scalespace_analyzer::compare_scalespaces(
        const Mat& im_base_a, const Mat& im_base_b, const Mat& sc_im_base_a,
        const Mat& sc_im_base_b, const vmat& ss_to_search,
        const vmat& ss_to_detect,
        bool compare_on_saturation /* = false */, int most_common_hue /* = -1*/) {
	(void)sc_im_base_b;
	if (ss_to_search.empty() || ss_to_detect.empty()) {
		return false;
	}

	Mat out;
	if (DEBUG_OUTPUT__) {
		cvtColor(im_base_a, out, COLOR_GRAY2BGR);
	}

	int n;
	uint match_count = 0;
	uint bg_count = 0;
	int bg_color = -1;
	vector<Point> bg_points;
	vector<WPoint3> match_points;
	map<int, int> scale_count;
	map<double, double> weight_count;
	vector<line_ss_value> line_values;
	if (!init_scalespace_matching(&n, &match_count, &bg_color, line_values, match_points,
	                              im_base_a, im_base_b, ss_to_detect)) {
		return false;
	}

	bool using_only_background_color = bg_color >= 0 && line_values.size() == 1 && utility::all_same(line_values.front().get_values());

	Mat hue_finds_in_sat = Mat::zeros(im_base_a.rows, im_base_a.cols, CV_8UC1);
	const uint color_count = line_values.size();

	////@ #pragma omp parallel for
	for (int i = 0; i < n; ++i) {
		bool skip = false;
		vector<uchar> values_from_main_im;
		values_from_main_im.reserve(ss_to_search.size());
		for (auto& im_main : ss_to_search) {
			uchar val = im_main.at<uchar>(i);
			//skip = (abs(val - excluded_hue_) < 2) || (compare_on_saturation && val == 50);
			if (skip) {
				break;
			}
			values_from_main_im.push_back(val);
		}
		if (skip) {
			continue;
		}

		uchar sc_im_at_i = sc_im_base_a.at<uchar>(i);
		if (compare_on_saturation &&
		        hue_compare_w_tolerance(most_common_hue, sc_im_at_i, hue_tolerance_)) {
			hue_finds_in_sat.at<uchar>(i) = 255;
		}

		uchar im_at_i = im_base_a.at<uchar>(i);
		if (compare_on_saturation && im_at_i >= 15 &&
		        (im_at_i != 255 && sc_im_at_i != 210)) {
			continue;
		}

		uchar val_a = im_tertiary_.at(0)->at<uchar>(i);
		//uchar val_b = 0;
		if (compare_on_saturation && (val_a > 50 && val_a < 205)) {
			continue;
		}

		int x = i % im_base_a.cols;
		int y = i / im_base_a.cols;

		vector<pair<uint, uint>> matches;
		for (auto& line_ss_point : line_values) {
			if (seek_similarities(values_from_main_im, line_ss_point, x, y,
			                      matches, scale_count, weight_count,
			                      compare_on_saturation,
			                      DO_CHECK_THROUGH_DIFFERENCE__,
			                      line_ss_point.is_bg())) {
				////@ #pragma omp atomic

				if (!line_ss_point.is_bg()) {
					++match_count;
				} else {
					++bg_count;
				}

				populate_point_vector(match_points, matches, x, y);

				if (DEBUG_OUTPUT__) {
					utility::paint_point(x, y, matches.back().first, out);
				}
				break;
			}
		}
	}

	if (((bg_count >= 0.75 * (bg_count + match_count)) || (match_count > 2 * bg_count)) &&
	        !(match_count < 0.005 * bg_count && bg_count < n * 0.5)) {
		return false;
	}

	if (match_count > n * 0.75) {
		cerr << "many matches" << endl;
		// return false;
	}

	return post_scalespace_search_processing(
	            im_base_a, out, scale_count, weight_count, match_points, match_count,
	            bg_count, color_count, hue_finds_in_sat, n, ss_to_search.size(),
	            ss_to_detect.size(), using_only_background_color);
}

// automatic mode
bool scalespace_analyzer::compare_scalespaces(
        const Mat& im_base_a, const Mat& sc_im_base_a,
        const vmat& ss_to_search,
        bool compare_on_saturation /* = false */, int most_common_hue /* = -1*/) {
	throw std::runtime_error("currently not supported");
	if (ss_to_search.empty()) {
		cout << red << "scalespace is empty" << "\n";
		return false;
	}

	Mat out;
	if (DEBUG_OUTPUT__) {
		cvtColor(im_base_a, out, COLOR_GRAY2BGR);
	}

	int n;
	uint match_count;
	vector<WPoint3> match_points;
	map<int, int> scale_count;
	map<double, double> weight_count;
	vpair_vuc_i vector_counts;
	if (!init_scalespace_matching(compare_on_saturation, &n, &match_count, vector_counts,
	                              match_points, im_base_a)) {
		return false;
	}

	Mat hue_finds_in_sat = Mat::zeros(im_base_a.rows, im_base_a.cols, CV_8UC1);
	const uint color_count = vector_counts.size();

	////@ #pragma omp parallel for
	for (int i = 0; i < n; ++i) {
		vector<uchar> values_from_main_im;
		values_from_main_im.reserve(ss_to_search.size());
		for (auto& im_main : ss_to_search) {
			uchar val = im_main.at<uchar>(i);
			values_from_main_im.push_back(val);
		}

		uchar sc_im_at_i = sc_im_base_a.at<uchar>(i);
		if (compare_on_saturation &&
		        hue_compare_w_tolerance(most_common_hue, sc_im_at_i, DO_CHECK_THROUGH_DIFFERENCE__ ? 2.5 * hue_tolerance_ : hue_tolerance_)) {
			hue_finds_in_sat.at<uchar>(i) = 255;
		}

		uchar im_at_i = im_base_a.at<uchar>(i);
		if (compare_on_saturation && im_at_i >= 15 &&
		        (im_at_i != 255 && sc_im_at_i != 210)) {
			continue;
		}

		//vector<pair<uint, double>> matches;
		//for (auto& line_ss_point : vector_counts) {
		//	if (line_ss_point.first == std::vector<uchar>{ 10, 10, 10 }) {
		//		continue;
		//	}
		//	// broken due to expansion for using line_ss_value class
		//	//if (seek_similarities(values_from_main_im, p.first, matches, scale_count,
		//	//                      weight_count, compare_on_saturation, DO_CHECK_THROUGH_DIFFERENCE__)) {
		//	//	////@ #pragma omp atomic
		//	//	++match_count;
		//	//
		//	//	int x = i % im_base_a.cols;
		//	//	int y = i / im_base_a.cols;
		//	//	populate_point_vector(match_points, matches, x, y);
		//	//
		//	//	if (DEBUG_OUTPUT__) {
		//	//		utility::paint_point(x, y, matches.back().first, out);
		//	//	}
		//	//	break;
		//	//}
		//}
	}

	if (match_count > n * 0.75) {
		cerr << "many matches" << endl;
		// return false;
	}
	return post_scalespace_search_processing(
	            im_base_a, out, scale_count, weight_count, match_points, match_count,
	            0, color_count, hue_finds_in_sat, n, ss_to_search.size(),
	            ss_generation_levels_, false);
}

bool scalespace_analyzer::gray_image(const matarr_3& img, double* rat,
                                     int* most_common_hue,
                                     bool msg /* = true */) {
	int high_sat_count = 0;
	int low_sat_count = 0;
	map<int, int> high_sat_counter;

//@ #pragma omp parallel for
	for (int row = 0; row < img[1].rows; ++row) {
		const auto* hue = img[0].ptr<const uchar>(row);
		const auto* sat = img[1].ptr<const uchar>(row);
		const auto* val = img[2].ptr<const uchar>(row);
		for (int col = 0; col < img[1].cols; ++col) {
			if (*sat < gray_threshold_sat_ ||
			        (*val < 100 /*&& *sat < GRAY_THRESHOLD * 3*/)) {
//@ #pragma omp atomic
				++low_sat_count;
			} else {
//@ #pragma omp critical
				{
					++high_sat_counter[*hue];
					++high_sat_count;
				}
			}
			++sat;
			++val;
			++hue;
		}
	}
	const double n = img[1].rows * img[1].cols;
	const double ratio = low_sat_count / n;
	*rat = ratio;
	*most_common_hue =
	        high_sat_counter.empty()
	        ? -1
	        : utility::copy_n_most_common_from_map<int, int>(1, high_sat_counter)
	        .front()
	        .first;
	if (msg)
		cout << ">> gray: " << (ratio * 100) << "%" << " ~ most common hue: <"
		     << static_cast<int>(*most_common_hue) << ">\n";
	return ratio > 0.8;
}

bool scalespace_analyzer::gray_image(const Mat& img, double* rat,
                                     int* most_common_hue,
                                     bool msg /* = true */) {
	int high_sat_count = 0;
	int low_sat_count = 0;
	map<int, int> high_sat_counter;
	Mat hsv;
	cvtColor(img, hsv, COLOR_BGR2HSV);

	auto splt = utility::mat_split(hsv);

//@ #pragma omp parallel for
	for (int row = 0; row < img.rows; ++row) {
		const auto* hue = splt[0].ptr<const uchar>(row);
		const auto* sat = splt[1].ptr<const uchar>(row);
		const auto* val = splt[2].ptr<const uchar>(row);
		for (int col = 0; col < splt[1].cols; ++col) {
			if (*sat < gray_threshold_sat_ ||
			        (*val < 100 /*&& *sat < GRAY_THRESHOLD * 3*/)) {
//@ #pragma omp atomic
				++low_sat_count;
			} else {
//@ #pragma omp critical
				{
					++high_sat_counter[*hue];
					++high_sat_count;
				}
			}
			++sat;
			++val;
			++hue;
		}
	}
	const double n = splt[1].rows * splt[1].cols;
	const double ratio = low_sat_count / n;
	*rat = ratio;
	*most_common_hue =
	        high_sat_counter.empty()
	        ? -1
	        : utility::copy_n_most_common_from_map<int, int>(1, high_sat_counter)
	        .front()
	        .first;
	if (msg)
		cout << ">> gray: " << (ratio * 100) << "% ~ most common hue: <"
		     << static_cast<int>(*most_common_hue) << ">\n";
	hsv = utility::mat_merge(splt);
	cvtColor(hsv, img, COLOR_HSV2BGR);
	return ratio > 0.8;
}

void scalespace_analyzer::get_one_channel_scalespace(vector<matarr_3>& ss,
                                                     vmat& dst,
                                                     int channel) {
	dst.reserve(ss.size());
	for (auto& img : ss) {
		dst.push_back(img[channel]);
	}
}

void scalespace_analyzer::populate_scalespace(vector<vmat>& bgr_ss_a,
                                              vector<vmat>& bgr_ss_b,
                                              matarr_3& a_small,
                                              matarr_3& b_small) {
	for (int i = 0; i < 3; ++i) {
		vmat ss_a;
		vmat ss_b;
		generate_scalespace(a_small[i], b_small[i], ss_a, ss_b);
		bgr_ss_a[i] = ss_a;
		bgr_ss_b[i] = ss_b;
	}
}

void scalespace_analyzer::populate_scalespace(vector<vmat>& bgr_ss,
                                              matarr_3& small,
                                              char mode) {
	for (int i = 0; i < 3; ++i) {
		vmat ss;
		generate_scalespace(small[i], ss, mode);
		bgr_ss[i] = ss;
	}
}

int scalespace_analyzer::prepare_channels_for_processing(bool compare_on_saturation, Mat& im_a,
                                                         Mat& im_b, Mat& sc_im_a, Mat& sc_im_b,
                                                         vector<matarr_3>& hsv_ss_a,
                                                         vector<matarr_3>& hsv_ss_b) {
	(void)compare_on_saturation;
	int channel = 0;
	//if (compare_on_saturation) {
	//	im_a = hsv_ss_a[0][1];
	//	im_b = hsv_ss_b[0][1];
	//	sc_im_a = hsv_ss_a[0][0];
	//	sc_im_b = hsv_ss_b[0][0];
	//	channel = 1;
	//} else {
	    // 0 - hue, 1 - saturation, 2 - value
		im_a = hsv_ss_a[0][0];
		im_b = hsv_ss_b[0][0];
		sc_im_a = hsv_ss_a[0][2];
		sc_im_b = hsv_ss_b[0][2];
	//}

	im_primary_.at(0) = &im_a;
	im_primary_.at(1) = &im_b;
	im_secondary_.at(0) = &sc_im_a;
	im_secondary_.at(1) = &sc_im_b;

	im_tertiary_.at(0) = &hsv_ss_a[0][2];
	im_tertiary_.at(1) = &hsv_ss_b[0][2];
	return channel;
}

int scalespace_analyzer::prepare_channels_for_processing(bool compare_on_saturation, Mat& im_a,
                                                         Mat& sc_im, vector<matarr_3>& hsv_ss,
                                                         Mat* prim, Mat* sec) {
	int channel = 0;
	if (compare_on_saturation) {
		im_a = hsv_ss[0][1];
		sc_im = hsv_ss[0][0];
		channel = 1;
	} else {
		im_a = hsv_ss[0][0];
		sc_im = hsv_ss[0][1];
	}

	prim = &im_a;
	sec = &sc_im;
	// im_primary_[index] = &im_a;
	// im_secondary_[index] = &sc_im;
	return channel;
}

bool scalespace_analyzer::run_detector_scalespace_hs_auto() {
	int most_common_hue = cc_->ad().most_common_hues()[cc_->ad_time()];
	bool sat_comp = cc_->ad().sat()[cc_->ad_time()];
	sat_comp = false;
	sat_comp_ = sat_comp;
	Mat hsv = utility::bgr_to_hsv(frame_a_, 1.0);
	image_modification::squish_value_channel(sat_comp, hsv, 70, 55, 220,
	                     gray_threshold_sat_,
	                     value_round_down_,
	                     USE_OUT_OF_RANGE_HUE_VALUES,
	                     OUT_OF_RANGE_BLACK,
	                     OUT_OF_RANGE_WHITE);
	image_modification::color_correction(hsv, sat_comp ? 100 : 70, 160, false, true, OUT_OF_RANGE_BLACK, OUT_OF_RANGE_WHITE);
	frame_a_ = utility::hsv_to_bgr(hsv);

	matarr_3 bgr_a;
	split(frame_a_, bgr_a);
	matarr_3 a_small;

	vector<vmat> bgr_ss_a(3);

	utility::resize_im(bgr_a, a_small, resize_);
	ressz_a_ = utility::mat_merge(a_small);

	show_im("resa", ressz_a_);

	populate_scalespace(bgr_ss_a, a_small, 'a');

	vector<matarr_3> hsv_ss_a = utility::bgr_ss_to_hsv(bgr_ss_a);

	Mat im_a;
	Mat sc_im_a;

	int channel = prepare_channels_for_processing(sat_comp, im_a, sc_im_a, hsv_ss_a, im_primary_[0], im_secondary_[0]);

	vmat ss_a;
	get_one_channel_scalespace(hsv_ss_a, ss_a, channel);

	return compare_scalespaces(im_a, sc_im_a, ss_a, sat_comp, most_common_hue);
}

bool scalespace_analyzer::run_detector_scalespace_hs_manual() {
	matarr_3 bgr_a;
	matarr_3 bgr_b;
	matarr_3 bgr_center;
	split(frame_a_, bgr_a);
	split(frame_b_, bgr_b);
	matarr_3 a_small;
	matarr_3 b_small;
	//cc_->get_center();

	//double low_saturation_ratio{};
	int most_common_hue{};
	bool sat_comp = false;
	        //gray_image(frame_b_, &low_saturation_ratio, &most_common_hue);
	sat_comp_ = sat_comp;
	vector<vmat> bgr_ss_a(3);
	vector<vmat> bgr_ss_b(3);

	if (bgr_a.front().cols * resize_ < 2 || bgr_a.front().rows * resize_ < 2 ||
	        bgr_b.front().cols * resize_ < 2 || bgr_b.front().rows * resize_ < 2) {
		return false;
	}

	utility::resize_im(bgr_a, bgr_b, a_small, b_small, resize_);

	merge(a_small.data(), 3, ressz_a_);
	merge(b_small.data(), 3, ressz_b_);

	populate_scalespace(bgr_ss_a, bgr_ss_b, a_small, b_small);

	vector<matarr_3> hsv_ss_a = utility::bgr_ss_to_hsv(bgr_ss_a);
	if (hsv_ss_a.empty()) {
		return false;
	}
	vector<matarr_3> hsv_ss_b = utility::bgr_ss_to_hsv(bgr_ss_b);

	cc_images_a_b(sat_comp, hsv_ss_a, hsv_ss_b, gray_threshold_sat_, gray_threshold_val_, true);
	// use_out_of_range_values should probably be true - otherwise red == white
	Mat cca = utility::hsv_to_bgr(utility::mat_merge(hsv_ss_a.front()));

	//if (!a_ref_.empty()) {
	//	if (a_ref_.size() != cca.size()) {
	//		resize(a_ref_, a_ref_, Size(0, 0), resize_, resize_);
	//		Rect roi((cca.cols - a_ref_.cols) / 2,
	//		         (cca.rows - a_ref_.rows) / 2,
	//		         a_ref_.cols, a_ref_.rows);
	//		cca(roi).copyTo(ressz_a_);
	//	}
	//	Scalar color(RESERVED_UNUSED_HUE);
	//	if (sat_comp) {
	//		color = Scalar(50);
	//	}
	//	//utility::mask_mat_array(a_ref_, hsv_ss_a, color);
	//}

	Mat im_a;
	Mat im_b;
	Mat sc_im_a;
	Mat sc_im_b;

	int channel =
	        prepare_channels_for_processing(sat_comp, im_a, im_b, sc_im_a, sc_im_b, hsv_ss_a, hsv_ss_b);

	vmat ss_a;
	vmat ss_b;
	get_one_channel_scalespace(hsv_ss_a, ss_a, channel);
	get_one_channel_scalespace(hsv_ss_b, ss_b, channel);

	//Mat canvas_b = utility::images_to_grid(ss_b, 200, 3);
	//Mat canvas_a = utility::images_to_grid(ss_a, 500, 3);
	//show_im("Scale-space of img B", canvas_b);
	//show_im("Scale-space of img A", canvas_a);
	//show_im("b thing", ss_b.front());
	//show_im("a thing", ss_a.front());

	return compare_scalespaces(im_a, im_b, sc_im_a, sc_im_b, ss_a, ss_b,
	                           sat_comp, most_common_hue);
}


bool scalespace_analyzer::run_detector_scalespace_hs() {
	if (!cc_) {
		return run_detector_scalespace_hs_manual();
	}
	return run_detector_scalespace_hs_auto();
}

bool scalespace_analyzer::check_videocapture(VideoCapture& cap,
                                             const string& path,
                                             string& message) {
	if (!cap.isOpened()) {
		rope_stringbuilder rope_sb;
		rope_sb << "failed to open video - bad directory or file at \"" << path << "\""
		    << "\n";
		message = rope_sb.str();
		return false;
	}
	return true;
}

void scalespace_analyzer::sane_result(bool* res) {
	if (result_history_.size() == 4) {
		if (*res == result_history_.front() && *res == result_history_.back()) {
			result_history_.at(1) = *res;
			result_history_.at(2) = *res;

			for (auto i = results_.end() - 3; i != results_.end(); ++i) {
				i->first = *res;
			}
		}
		result_history_.pop_front();
		result_history_.push_back(*res);
	} else {
		result_history_.push_back(*res);
	}
}

bool scalespace_analyzer::run_detector(bool message /*= true*/) {
	(void)message;
	if (LOG_REALTIME__) {
		output_ << frame_num_;
	}

	const auto start = chrono::high_resolution_clock::now();
	bool res = run_detector_scalespace_hs();
	const auto end = chrono::high_resolution_clock::now();
	const chrono::duration<double> elapsed_seconds = end - start;
	const long long ms =
	        chrono::duration_cast<chrono::milliseconds>(elapsed_seconds).count();

	// sane_result(&res);

	if (LOG_REALTIME__) {
		output_ << "=" << res << "=" << ms << "\n";
	}
	results_.emplace_back(res, ms);
	return res;
}

void scalespace_analyzer::log() {
	if (SAVE_OPS__) {
		cout << colorex(210) << "saving results to " << output_file_ << reset << "\n";
		for (unsigned long i = 0; i < results_.size(); ++i) {
			output_ << "$" << (i + 1) << "=" << results_[i].first << "="
			        << results_[i].second << "\n";
		}
		cout << lgreen << "done" << reset << "\n";
	}
}

void scalespace_analyzer::get_video_info(VideoCapture& cap, int index) {
	width_[index] = static_cast<int>(cap.get(CAP_PROP_FRAME_WIDTH));
	height_[index] = static_cast<int>(cap.get(CAP_PROP_FRAME_HEIGHT));
	fps_[index] = static_cast<int>(cap.get(CAP_PROP_FPS));
	fourcc_[index] = static_cast<int>(cap.get(CAP_PROP_FOURCC));
	frame_count_[index] = static_cast<int>(cap.get(CAP_PROP_FRAME_COUNT));
	cout << "video: " << width_[index] << " x " << height_[index] << " at "
	     << fps_[index] << "fps, " << fourcc_[index] << "\n";
}

bool scalespace_analyzer::run_on_input(const Mat& frame_a, const Mat& frame_b, Mat& a_ref) {
	if (GT_GENERATE__) {
		stepping_mode_ = true;
	}
	if (SAVE_OPS__) {
		results_.reserve(frame_count_[0]);
	}

	if (frame_a.empty() || frame_b.empty()) {
		return false;
	}

	++frame_num_;
	frame_a.copyTo(frame_a_);
	frames_[0] = &frame_a_;
	frame_b.copyTo(frame_b_);
	frames_[1] = &frame_b_;
	a_ref.copyTo(a_ref_);
	if (has_center_) {
		centerboard_.advance();
	}
	bool result = run_detector();

	const char key = static_cast<char>(key_wait(1));
	if (key == 'q') {
		return false;
	}

	(void)key;
	//pause();
	//key_wait();
	// destroyAllWindows();
	// if (SAVE_OPS__) {
	// 	median_results();
	// 	log();
	// }
	return result;
}

bool scalespace_analyzer::open_video(const string& path_a,
                                     const string& path_b) {
	string message_a;
	string message_b;
	VideoCapture cap_a(path_a);
	VideoCapture cap_b(path_b);
	if (!check_videocapture(cap_a, path_a, message_a) ||
	        !check_videocapture(cap_b, path_b, message_b)) {
		cerr << message_a << message_b;
		return false;
	}
	get_video_info(cap_a, 0);
	get_video_info(cap_b, 1);
	if (GT_GENERATE__) {
		stepping_mode_ = true;
	}
	if (SAVE_OPS__) {
		results_.reserve(frame_count_[0]);
	}

	while (true) {
		++frame_num_;
		advance(cap_a, frame_a_, 0);
		advance(cap_b, frame_b_, 1);
		if (has_center_) {
			centerboard_.advance();
		}
		if (GT_GENERATE__) {
			while (frame_num_ <= GT_GEN_FRAME_LAST - 4) {
				++frame_num_;
				advance(cap_a, frame_a_, 0);
				advance(cap_b, frame_b_, 1);
			}
		}
		if (frame_a_.empty() || frame_b_.empty()) {
			break;
		}  // end of feed
		if (STEPPING__) {
			stepping_mode_ = stepping_mode_ || utility::num_in_list(frame_num_, step_list_);
			if (stepping_mode_) {
				run_detector();
			} else {
				cout << colorex(242) << frame_num_ << reset << "\n";
			}
		} else {
			run_detector();
		}

		const char key = static_cast<char>(key_wait(1));
		if (key == 'q') {
			break;
		}
		if (key == 's' && STEPPING__) {
			stepping_mode_ = !stepping_mode_;
		} else if (key == ' ' || ((STEPPING__ || GT_GENERATE__) && stepping_mode_)) {
			pause();
		}
	}
	key_wait();
	destroyAllWindows();
	if (SAVE_OPS__) {
		//utility::median_filter(results_, 15);
		log();
	}
	return true;
}

void goback(VideoCapture& cap, int* fnum) {
	int pos = static_cast<int>(cap.get(cv::CAP_PROP_POS_FRAMES));
	cap.set(cv::CAP_PROP_POS_FRAMES, pos - 11);
	if (fnum) *fnum -= 11;
}

bool scalespace_analyzer::open_video(const string& path_a) {
	string message_a;
	VideoCapture cap_a(path_a);
	if (!check_videocapture(cap_a, path_a, message_a)) {
		cerr << message_a;
		return false;
	}
	get_video_info(cap_a, 0);
	cc_->set_fps(fps_[0]);

	VideoCapture future_copy(path_a);
	Mat future;
	Mat old;
	if (GT_GENERATE__) {
		stepping_mode_ = true;
		for (int i = 0; i < 30; ++i) {
			future_copy >> future;
		}
	}
	if (SAVE_OPS__) {
		results_.reserve(frame_count_[0]);
	}

	while (true) {
		++frame_num_;
		advance(cap_a, frame_a_, 0);
		cc_->tick();
		if (GT_GENERATE__) {
			future.copyTo(old);
			advance(future_copy, future, 0);
			if (future.empty()) {
				old.copyTo(future);
			}
			while (frame_num_ <= GT_GEN_FRAME_LAST - 4) {
				++frame_num_;
				advance(cap_a, frame_a_, 0);
				cc_->tick();
				future.copyTo(old);
				advance(future_copy, future, 0);
				if (future.empty()) {
					old.copyTo(future);
				}
			}
		}
		if (frame_a_.empty()) {
			cout << "end of feed\n";
			break;
		}  // end of feed
		if (STEPPING__) {
			stepping_mode_ = stepping_mode_ || utility::num_in_list(frame_num_, step_list_);
		}
		if (stepping_mode_) {
			Mat small;
			resize(frame_a_, small, Size(0, 0), 0.5, 0.5);
			show_im("a", small);
			show_im("b", cc_->ad().image());

			if (GT_GENERATE__) {
				Mat smallf;
				resize(future, smallf, Size(0, 0), 0.5, 0.5);
				show_im("af", smallf);
			}
			run_detector();
		} else if (STEPPING__) {
			cout << colorex(242) << frame_num_ << reset << "\n";
		} else {
			run_detector();
		}

		const char key = static_cast<char>(key_wait(1));
		if (key == 'q') {
			break;
		}
		if (key == 's') {
			if (STEPPING__) {
				stepping_mode_ = !stepping_mode_;
			}
		} else if ((STEPPING__ || (GT_GENERATE__ && stepping_mode_)) || key == ' ') {
			try {
				pause();
			} catch (...) {
				if (GT_GENERATE__) {
					goback(cap_a, nullptr);
					goback(future_copy, &frame_num_);
				}
			}
		}
	}
	cout << "press any key to continue\n";
	key_wait();
	destroyAllWindows();
	if (SAVE_OPS__) {
		//median_results();
		log();
	}
	return true;
}

void scalespace_analyzer::pause() {
	char key = static_cast<char>(key_wait(0));
	if (key == 'c') {  // image gets saved if user presses 'c'
		char id = 'A';
		for (const Mat* img : frames_) {
			rope_stringbuilder rope_sb;
			rope_sb << "./cap" << id << frame_num_ << ".png";
			cerr << "saved cap" << id << frame_num_ << "\n";
			if (img) {
				imwrite(rope_sb.str(), *img);
				++id;
			} else {
				VideoCapture cap = cc_->ad().video();
				cap.set(CAP_PROP_POS_FRAMES, cc_->ad_time());
				Mat frame;
				cap.read(frame);
				imwrite(rope_sb.str(), frame);
				++id;
				cap.set(CAP_PROP_POS_FRAMES, 0);
			}
		}
	} else if (key == 's') {
		if (STEPPING__) {
			stepping_mode_ = !stepping_mode_;
		}
		cout << "continue\n";
		return;
	} else if (key == ' ') {
		return;
	}
	if (GT_GENERATE__) {
		if (key == 'x' || key == -80) {
			output_gt_ << "X";
			throw std::runtime_error("step-back command encountered");
		}

		output_gt_ << '$' << frame_num_ << '=' << flush;
		switch (key) {
		case 'f':
		case -79: // numpad 1
			output_gt_ << '1' << endl;
			break;
		case 'k':
		case -77: // numpad 3
			output_gt_ << '0' << endl;
			break;
		case 'g':
			output_gt_ << "<<1." << '1' << endl;
			break;
		case 'h':
			output_gt_ << "<<1." << '0' << endl;
			break;
		case 'l':
			output_gt_ << "<<0." << '1' << endl;
			break;
		case ';':
			output_gt_ << "<<0." << '0' << endl;
			break;
		case -78: // numpad 2
			output_gt_ << 'F' << endl;
			break;
		default:
			cout << static_cast<int>(key);
			output_gt_ << '_' << endl;
			break;
		}

		cout << key << "\n";
	}
}

bool scalespace_analyzer::check_image(const Mat& img, const string& path,
                                      string& message) {
	if (img.empty()) {
		rope_stringbuilder rope_sb;
		rope_sb << "failed to load image - bad directory or file at \"" << path << "\""
		    << "\n";
		message = rope_sb.str();
		return false;
	}
	return true;
}

bool scalespace_analyzer::open_image(const string& path_a,
                                     const string& path_b) {
	string message_a;
	string message_b;
	frame_a_ = imread(path_a);
	frame_b_ = imread(path_b);

	if (!check_image(frame_a_, path_a, message_a) ||
	        !check_image(frame_b_, path_b, message_b)) {
		cerr << message_a << message_b;
		return false;
	}

	width_[0] = frame_a_.cols;
	width_[1] = frame_b_.cols;
	height_[0] = frame_a_.rows;
	height_[1] = frame_b_.rows;

	if (SAVE_OPS__) {
		const string search_str = "cap";
		const size_t found = path_a.find(search_str);
		//rope ss;

		if (found != string::npos) {
			size_t index = found + 4;
			while (path_a[index] != '.') {
				//ss << path_a[index];
				++index;
			}
		}
		//ss << ".png";
	}
	++frame_num_;

	Mat output = frame_a_.clone();

	run_detector();

	char key = static_cast<char>(key_wait(0));
	destroyAllWindows();
	if (SAVE_OPS__) {
		log();
	}
	return key != ' ';
}

void scalespace_analyzer::parseline(const string& line, advertisement* ad) {
	if (line == advertisement::hue_sat_sep) {
		ad->set_satoffset(static_cast<int>(advertisement::total_vcounts(ad->vector_counts())));
		return;
	}
	std::vector<std::string> sat_mhue_vec = utility::split(line, '/');
	std::vector<std::string> sat_mhue = utility::split(sat_mhue_vec[0], '~');

	bool sat = static_cast<bool>(stoi(sat_mhue[0]));
	int m_common_hue = stoi(sat_mhue[1]);
	size_t unique_color_count = stoi(sat_mhue_vec[1]);
	ad->sat().push_back(sat);
	ad->most_common_hues().push_back(m_common_hue);
	ad->unique_colors().push_back(unique_color_count);

	std::vector<std::string> vectors = utility::split(sat_mhue_vec[2], '|');
	vpair_vuc_i cluster;
	for (auto& v : vectors) {
		std::pair<std::vector<uchar>, int> vcount;
		std::vector<std::string> lr = utility::split(v, ':');
		vcount.second = stoi(lr[1]);
		std::vector<std::string> l = utility::split(lr[0], ',');
		for (auto& p : l) {
			vcount.first.push_back(static_cast<uchar>(stoi(p)));
		}
		cluster.push_back(vcount);
	}
	ad->vector_counts().push_back(cluster);
}

bool scalespace_analyzer::check_first_line(fstream* file, scalespace_analyzer* ssd, advertisement* ad, bool correct_colors_pre_ssgen, bool center, Mat* prim, Mat* sec, int excluded_hue) {
	string firstline;
	getline(*file, firstline);
	rope_stringbuilder rope_sb;
	ssd->prepare_ad_image_one(ad, ssd, correct_colors_pre_ssgen, prim, sec, excluded_hue, &rope_sb, center, false);
	auto end = rope_sb.str().find_first_of('\n');
	string str = rope_sb.str().substr(0, end);
	ad->vector_counts().clear();
	ad->sat().clear();
	ad->most_common_hues().clear();
	ad->unique_colors().clear();
	ad->reset();
	file->seekg(0, std::ios_base::beg);
	file->seekp(0, std::ios_base::beg);

	return firstline == str;
}

void scalespace_analyzer::prepare_ad(bool video, bool file_good, filesystem::path& path, fstream* file, advertisement* ad, bool center, bool correct_colors_pre_ssgen) {
	if (file_good) {
		cout << "------ " << lgreen << "reading existing file" << reset << "\n";
		string line;
		while (getline(*file, line)) {
			parseline(line, ad);
		}
	} else {
		file->open(path, ios::out | ios::trunc);
		cout << colorex(202) << "generating new vector cluster file" << reset << "\n";

		int count = video ? static_cast<int>(ad->video().get(cv::CAP_PROP_FRAME_COUNT)) : 1;
		int position = 0;
		float progress = 0.0;

		map<int, int> hash_table;
		int frame = 0;

		do {
			ad->advance();
			if (!ad->has_image()) {
				break;
			}

			Rect rect(ad->h() / 2, ad->h() / 2, ad->w() - ad->h(), 1);
			Mat roi = ad->image();
			roi = roi(rect);
			int hash = dhash(roi);
			bool need_to_compute = true;

			auto search = hash_table.find(hash);
			if (search == hash_table.end()) {
				hash_table[hash] = frame;
			} else {
				advertisement_context ctx = ad->get_context((*search).second);
				ad->append_context(ctx);
				need_to_compute = false;
				*file << ctx.sat() << '~' << static_cast<int>(ctx.most_common_hues()) << "/" << ctx.unique_colors()
				      << "/" << utility::vector_as_string(ctx.v_counts(), true) << "\n";
			}

			if (need_to_compute) {
				if (center) {
					prepare_ad_image_one(ad, this, correct_colors_pre_ssgen, im_primary_[1], im_secondary_[1], excluded_hue_, file, true);
				} else {
					prepare_ad_image_one(ad, this, correct_colors_pre_ssgen, im_primary_[1], im_secondary_[1], excluded_hue_, file);
				}
			}

			cout << "------ " << flush;
			utility::progress_bar(30, count, &position, &progress);
			++frame;
		} while (video);

		if (center) {
			*file << advertisement::hue_sat_sep << "\n";
			ad->set_satoffset(static_cast<int>(advertisement::total_vcounts(ad->vector_counts())));
			ad->reset();
			do {
				ad->advance();
				if (!ad->has_image()) {
					break;
				}
				prepare_ad_image_one(ad, this, correct_colors_pre_ssgen, im_primary_[1], im_secondary_[1], excluded_hue_, file, true, true);
			} while (video);
		}

		cout << lgreen << " done\n" << reset << flush;
	}

	file->close();
	file->open(path, ios::in);
	if (file->peek() == std::ifstream::traits_type::eof()) {
		std::cerr << "empty file: " << path << std::endl;
		file->close();
		remove(path);
	}
	file->close();
}

void scalespace_analyzer::prepare_advertisements_in_bank() {
	std::string center;
	size_t count = cc_->bank().size();
	int i = 1;

	for (auto& ad : cc_->bank()) {
		auto* ad_p = ad.second.get();
		cout << colorex(177) << i << "/" << count << " - " << reset;
		cout << colorex(111) << underlined << "content: " << ad_p->path() << bold << colorex(242) << " :" << ad_p->duration() << reset << endl;
		std::filesystem::path writepath(ad_p->path());
		writepath = writepath.parent_path() / writepath.filename().stem();

		bool centerboard = starts_with(to_lower(writepath.filename()), "center");

		if (CORRECT_COLORS_PRE_SSGEN) {
			writepath.concat("_cc");
		}
		writepath.concat(paths::prefd_processed);
		cout << "looking for: " << writepath.string() << endl;
		fstream file;
		bool file_good = check_file_exist(writepath, &file, check_first_line, this, ad_p, CORRECT_COLORS_PRE_SSGEN, centerboard, im_primary_[1], im_secondary_[1], excluded_hue_);

		switch (ad.second->type()) {
		case advertisement::type::image:
			prepare_ad(ad.second->is_video(), file_good, writepath, &file, ad_p, centerboard, CORRECT_COLORS_PRE_SSGEN);
			break;
		case advertisement::type::video:
			prepare_ad(ad.second->is_video(), file_good, writepath, &file, ad_p, centerboard, CORRECT_COLORS_PRE_SSGEN);
			break;
		}
		if (starts_with(to_lower(ad.first.name()), paths::centerboard)) {
			center = ad.first.name();
		}
		++i;
	}
	if (!center.empty()) {
		cc_->set_center(cc_->remove(center));
	}
}

detector_status scalespace_analyzer::detector_start(int argc, char** argv) {
	setBreakOnError(true);

	argc_ = argc;
	argv_ = argv;

	cout << "\n"
	     << lmagenta << "*** starting detector ***\n"
	     << lblue << "-------------------------\n\n" << reset
	     << flush;

	if (GT_GENERATE__) {
		output_gt_ = ofstream(gt_path_, ios_base::app);
	}
	output_ = ofstream(output_file_, ios_base::app);
	output_ << "## perimeter detection results" << endl;

	if (!cc_) {
		do {
			open_file_name ofn_a;

			if (!ofn_a.open_file()) return S_FILE_NOT_OPEN;

			auto& path_a = ofn_a.path();

			if (path_a.empty()) return S_FILE_NOT_FOUND;
			if (is_image(ofn_a)) {
				const auto& path_b = get_pathb_automatic(path_a);

				if (path_b.empty()) return S_FILE_NOT_FOUND;
				if (!open_image(path_a, path_b)) return S_IMAGE_OPEN_UNSUCCESSFUL;
			} else {
				open_file_name ofn_b;
				if (!ofn_b.open_file()) return S_FILE_NOT_OPEN;
				auto& path_b = ofn_b.path();
				if (path_b.empty()) return S_FILE_NOT_FOUND;
				if (is_image(ofn_b)) return S_GOT_IMAGE_WHEN_EXPECTED_VIDEO;

				//open_file_name ofn_center;
				has_center_ = false; //ofn_center.open_file();
				if (has_center_) {
					//auto& path_center = ofn_center.path();
					//if (path_center.empty()) return S_FILE_NOT_FOUND;
					//centerboard_ = centerboard(path_center);
					//fstream file;
					//bool center_video = !is_image(ofn_center);
					//filesystem::path centerboard_formatted_file = path_center.replace_extension(paths::prefd_processed);
					//bool file_good = check_file_exist(centerboard_formatted_file, &file, check_first_line, this, &centerboard_, CORRECT_COLORS_PRE_SSGEN, true, nullptr, nullptr, excluded_hue_);
					//prepare_ad(center_video, file_good, centerboard_formatted_file, &file, &centerboard_, true, true);
				}

				if (!open_video(path_a, path_b)) return S_VIDEO_OPEN_UNSUCCESSFUL;
			}
		} while (true);
	} else {
		open_file_name ofn_a;
		if (!ofn_a.open_file()) return S_FILE_NOT_OPEN;

		const string& path_a = ofn_a.path();

		if (path_a.empty()) return S_FILE_NOT_FOUND;

		if (!open_video(path_a)) return S_VIDEO_OPEN_UNSUCCESSFUL;
	}

	return S_SUCCESS;
}

detector_status scalespace_analyzer::set_centerboard(const std::filesystem::path& path_center) {
	if (path_center.empty()) return S_FILE_NOT_FOUND;
	std::filesystem::path cpy = path_center;
	centerboard_ = centerboard(cpy);
	has_center_ = true;
	std::fstream file;
	bool center_video = type_from_extension(path_center) == paths::file_type::IMAGE;

	std::filesystem::path centerboard_formatted_file = cpy.replace_extension(paths::prefd_processed);
	bool file_good = check_file_exist(centerboard_formatted_file, &file, check_first_line, this, &centerboard_, CORRECT_COLORS_PRE_SSGEN, true, nullptr, nullptr, excluded_hue_);

	prepare_ad(center_video, file_good, centerboard_formatted_file, &file, &centerboard_, true, true);
	return S_SUCCESS;
}


void scalespace_analyzer::cc_images_a_b(bool compare_on_saturation,
                                        std::vector<matarr_3> &hsv_ss_a,
                                        std::vector<matarr_3> &hsv_ss_b,
                                        int gray_threshold_sat,
                                        int gray_threshold_val,
                                        bool use_out_of_range_values) {
	(void)compare_on_saturation;
	image_modification::squish_low_value_many_im(hsv_ss_a, 45, gray_threshold_sat, gray_threshold_val, use_out_of_range_values, OUT_OF_RANGE_BLACK);
	image_modification::squish_low_value_many_im(hsv_ss_b, 45, gray_threshold_sat, gray_threshold_val, use_out_of_range_values, OUT_OF_RANGE_BLACK);
	image_modification::squish_high_value_many_im(hsv_ss_a, 210, gray_threshold_sat, gray_threshold_val, use_out_of_range_values, OUT_OF_RANGE_WHITE);
	image_modification::squish_high_value_many_im(hsv_ss_b, 210, gray_threshold_sat, gray_threshold_val, use_out_of_range_values, OUT_OF_RANGE_WHITE);
	image_modification::shift_hue_vsplit(hsv_ss_a, true, OUT_OF_RANGE_BLACK);
	image_modification::shift_hue_vsplit(hsv_ss_b, true, OUT_OF_RANGE_BLACK);
}

void scalespace_analyzer::cc_for_auto_b_feed(bool compare_on_saturation,
                                             std::vector<matarr_3> &hsv_ss,
                                             int gray_threshold_sat,
                                             int gray_threshold_val,
                                             bool use_out_of_range_values,
                                             bool display) {
	(void)compare_on_saturation;
	image_modification::squish_low_value_many_im(hsv_ss, 45, gray_threshold_sat, gray_threshold_val, use_out_of_range_values, OUT_OF_RANGE_BLACK);
	image_modification::squish_high_value_many_im(hsv_ss, 160, gray_threshold_sat, gray_threshold_val, use_out_of_range_values, OUT_OF_RANGE_WHITE);
	image_modification::shift_hue_vsplit(hsv_ss, true, OUT_OF_RANGE_BLACK);

	if (display) {
		cv::Mat merg = utility::mat_merge(hsv_ss[0]);
		cvtColor(merg, merg, cv::COLOR_HSV2BGR);
		show_im("color corrected", merg);
	}
}

void scalespace_analyzer::fix_scale(int* scale, int total, int sz, int desired,
                      int count_at_scale, int count_at_desired) {
	if (count_at_scale * 0.95 > count_at_desired) {
		return;
	}
	*scale = (total >= 1.0 / (sz + 1) * total) &&
	         (count_at_desired >= 0.55 * count_at_scale)
	         ? desired
	         : *scale;
}

int scalespace_analyzer::get_weighted_max(int im_width, std::map<int, int>& m, int* count_at_scale) {
	//		int max = -1;
	std::pair<int, int> true_max{-1, -1};
	int scale = 0;
	// int s = -1;
	int total = 0;
	int sz = static_cast<int>(m.size());

	std::vector<int> considered;

	//double d_part = 0.6;
	if (sz > 2) {
		for (int i = 0; i < sz; ++i) {
			if (DO_CHECK_THROUGH_DIFFERENCE__) {
				m[i] = m[i] * (0.5032143 + 0.1533214 * i - (0.01089286 * i) * (0.01089286 * i)); // better results?
			}
			//m[i] *= ((1 + d_part) - i * (d_part / sz)); // play around with this
			//m[i] = m[i] * (0.7525 + 0.09110714 * i - (0.008392857 * i) * (0.008392857 * i));
			total += m[i];
			//	int sum = m[i - 1] + m[i] + m[i + 1];
			//	int diff = m[i] - s;
			std::cout << ";" << lcyan << "[" << i << "]" << colorex(208) << " <"
			          << m[i] << ">" << reset;
			if (m[i] > true_max.second) {
				true_max.second = m[i];
				true_max.first = i;
			}
		}
		std::cout << ";" << lcyan << "[" << sz - 1 << "]" << colorex(208) << " <"
		          << m[sz - 1] << ">\n" << reset << std::flush;
		total += m[0];
		total += m[sz - 1];
	} else {
		std::cout << "\n" << std::flush;
		if (sz == 1) {
			total = m[0];
			*count_at_scale = m[0];
			return 0;
		}
		total = m[0] + m[1];
		scale = m[0] >= m[1] ? 0 : 1;
		*count_at_scale = m[scale];

		*count_at_scale = true_max.second;
		return 0;
		// return scale;
	}
	std::cout << dgray << scale << reset << "\n" << std::flush;
	if (scale == static_cast<int>(m.size()) - 2 && m[sz - 1] > m[scale]) {
		if (utility::similar(m[scale], m[scale + 1])) {
			++scale;
		}
	} else if (scale == 1 &&
	           !(static_cast<uint>(m[0]) * 2 >= max_for_scale(im_width, 0))) {
		if (m[scale] < 0.7 * m[0]) {
			std::cout << lred << "changing 1 -> 0\n" << reset << std::flush;
			scale = 0;
		}
	}

	//bool _near_const = false;
	//if (considered.size() == m.size() - 2) {
	////	bool nc = utility::is_near_constant(considered);
	//	//_near_const = nc;
	//}

	*count_at_scale = m[scale];
	*count_at_scale = true_max.second;
	return true_max.first;
}
