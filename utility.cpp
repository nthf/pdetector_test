#include "osdialog/osdialog.h"
#include "sys/times.h"
#include "terminal_color_output.h"
#include "utility.h"

namespace utility {
	int mod(int a, int b) {
		if (b < 0) return mod(a, -b);
		int ret = a % b;
		if (ret < 0) ret += b;
		return ret;
	}

	double rad_to_deg(double angle) {
		return angle * DEG_MUL;
	}

	double deg_to_rad(double angle) {
		return angle * RAD_MUL;
	}


	double vector_angle(double x, double y) {
		if (std::abs(x) < 0.00001) {
			return (y > 0) ? 90 : (std::abs(y) < 0.00001) ? 0 : 270;
		} else if (std::abs(y) < 0.00001) { // special cases
			return (x >= 0) ? 0 : 180;
		}
		double ret = rad_to_deg(std::atan2(y, x));
		if (x < 0 && y < 0) { // quadrant Ⅲ
			ret = 180 + ret;
		} else if (x < 0) { // quadrant Ⅱ
			ret = 180 + ret; // it actually substracts
		} else if (y < 0) { // quadrant Ⅳ
			ret = 270 + (90 + ret); // it actually substracts
		}
		return ret;
	}

	double vector_magnitude(double x, double y) {
		return sqrt(x * x + y * y);
	}

	int distance(const cv::Point& A, const cv::Point& B) {
		double xx = A.x - B.x;
		xx *= xx;
		double yy = A.y - B.y;
		yy *= yy;
		return static_cast<int>(sqrt(xx + yy));
	}

	std::array<int, 3> cross(const std::array<int, 3>& a, const std::array<int, 3>& b) {
		std::array<int, 3> result;
		result[0] = a[1] * b[2] - a[2] * b[1];
		result[1] = a[2] * b[0] - a[0] * b[2];
		result[2] = a[0] * b[1] - a[1] * b[0];
		return result;
	}

	double point_to_line_distance(const cv::Point& p, const cv::Vec4i& line) {
		cv::Vec2d d(line(2) - line(0), line(3) - line(1));
		return  abs((d(0) * (line(1) - p.y) - (line(0) - p.x) * d(1))) / sqrt((d(0) * d(0)) + (d(1) * d(1)));
	}

	double point_to_ls_distance(const cv::Point&p, const cv::Vec4i& line) {
		cv::Vec2d ab(line(2) - line(0), line(3) - line(1));
		cv::Vec2d bp(p.x - line(2), p.y - line(3));
		cv::Vec2d ap(p.x - line(0), p.y - line(1));

		double ab_bp = (ab(0) * bp(0) + ab(1) * bp(1));
		double ab_ap = (ab(0) * ap(0) + ab(1) * ap(1));

		if (ab_bp > 0) {
			double y = p.y - line(3);
			double x = p.x - line(2);
			return sqrt(x * x + y * y);
		} else if (ab_ap < 0) {
			double y = p.y - line(1);
			double x = p.x - line(0);
			return sqrt(x * x + y * y);
		} else {
			return abs(ab(0) * ap(1) - ab(1) * ap(0)) / sqrt(ab(0) * ab(0) + ab(1) * ab(1));
		}
	}

	double rect_distance(const cv::Rect& r0, const cv::Rect& r1) {
		//int xproj = r1.x + r1.width + r0.x + r0.width;
		//int yproj = r1.y + r1.height + r0.y + r0.height;

		int left = std::min(r0.x, r1.x);
		int top = std::min(r0.y, r1.y);
		int right = std::max(r0.x + r0.width, r1.x + r1.width);
		int bottom = std::max(r0.y + r0.height, r1.y + r1.height);
		int width = right - left;
		int height = bottom - top;

		double w = std::max(0.0, static_cast<double>(width - r0.width - r1.width));
		double h = std::max(0.0, static_cast<double>(height - r0.height - r1.height));

		return sqrt(w * w + h * h);
	}

	bool on_segment(cv::Point p, cv::Point q, cv::Point r) {
		return (q.x <= std::max(p.x, r.x) && q.x >= std::min(p.x, r.x) &&
		        q.y <= std::max(p.y, r.y) && q.y >= std::min(p.y, r.y));
	}

	int direction(cv::Point p, cv::Point q, cv::Point r) {
		int val = (q.y - p.y) * (r.x - q.x) -
		          (q.x - p.x) * (r.y - q.y);

		if (val == 0) {
			return 0;
		}
		return (val > 0) ? 2 : 1;
	}

	bool lines_intersect(const cv::Point& p1, const cv::Point& q1, const cv::Point& p2, const cv::Point& q2) {
		    int d1 = direction(p1, q1, p2);
			int d2 = direction(p1, q1, q2);
			int d3 = direction(p2, q2, p1);
			int d4 = direction(p2, q2, q1);
			if (d1 != d2 && d3 != d4) {
				return true;
			}

			if ((d1 == 0 && on_segment(p1, p2, q1)) ||
			        (d2 == 0 && on_segment(p1, q2, q1)) ||
			        (d3 == 0 && on_segment(p2, p1, q2)) ||
			        (d4 == 0 && on_segment(p2, q1, q2))) {
				return true;
			}

			return false;
	}


	std::vector<cv::Point> densify(std::vector<cv::Point>& pts, double dx, double dy, int w, double from, double to) {
		std::vector<cv::Point> new_reg;
		cv::Point centroid;
		int min_x = static_cast<int>(w * from);
		int max_x = static_cast<int>(w * to);
		int count = 0;
		centroid = std::accumulate(pts.begin(), pts.end(), centroid, [&](cv::Point& acc, const cv::Point& p) {
		               if (p.x >= min_x && p.x < max_x) {
		                   acc.x += p.x;
		                   acc.y += p.y;

		                   ++count;
	                   }
		                return acc;
	               });
		centroid.x /= min_1(count);
		centroid.y /= min_1(count);
		for (auto& p : pts) {
			if (p.x >= min_x && p.x < max_x) {
				auto v = centroid - p;
				v.x *= dx;
				v.y *= dy;
				auto new_p = (p + v);
				new_p.y += 20;
				new_reg.push_back(new_p);
			}
		}
		return new_reg;
	}

	void progress_bar(int barwidth, int count, int* position, float* progress) {
		++(*position);
		*progress = static_cast<float>(*position) / static_cast<float>(count);
		bool done = *position == count;

		std::cout << (lwhite + lwhitebg) << "[" << reset
		          << (done ? (lgreen + lgreenbg) : (lred + lredbg));
		int pos = static_cast<int>(static_cast<float>(barwidth) * (*progress));
		for (int i = 0; i < barwidth; ++i) {
			if (i < pos) {
				std::cout << "#";
			} else {
				std::cout << reset << " ";
			}
		}
		std::cout << reset << (lwhite + lwhitebg) << "]" << reset << " "
		          << static_cast<int>((*progress) * 100.0);
		std::cout << " %";
		if (!done) {
			std::cout << "\r";
		}
		std::cout << std::flush;
	}

	int open_file(std::string& path) {
		open_file_name ofn;
		if (!ofn.open_file()) return 1;
		path = ofn.path();
		if (path.empty()) return 2;
		return 0;
	}

	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding) {
		pad_image(img, out, padding, cv::Scalar(0, 0, 0));
	}

	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding,
	               const cv::Scalar& color) {
		pad_image(img, out, padding, padding, padding, padding, color);
	}

	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding0, const int padding1, const int padding2, const int padding3) {
		pad_image(img, out, padding0, padding1, padding2, padding3, cv::Scalar(0, 0, 0));
	}

	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding0, const int padding1, const int padding2, const int padding3,
	               const cv::Scalar& color) {
		const int rows = img.rows;
		const int cols = img.cols;
		const cv::Mat res(rows + padding1 + padding3, cols + padding0 + padding2, img.type(), color);
		img.copyTo(res(cv::Rect(padding0, padding1, cols, rows)));
		res.copyTo(out);
	}

	void depad_image(const cv::Mat& img, cv::Mat& out, const int padding, const cv::Mat& mask /* = cv::Mat() */) {
		if (img.rows < padding * 2 || img.cols < padding * 2) {
			out = cv::Mat();
			return;
		}
		const int rows = img.rows;
		const int cols = img.cols;
		const cv::Rect rect(padding, padding, cols - 2 * padding, rows - 2 * padding);
		const cv::Mat subarea = img(rect);
		subarea.copyTo(out, mask);
	}

	int count_in_area(const cv::Mat& img, const cv::Point& point, area_shape shape, int dx, int dy /*= 0*/) {
		if (shape != AREA_ELLIPSE) {
			dy = dx;
		}
		cv::Rect roi(std::max(0, point.x - dx / 2), std::max(0, point.y - dy / 2), point.x + dx >= img.cols ? img.cols - point.x - 1 : dx, point.y + dy >= img.rows ? img.rows - point.y - 1 : dy);
		cv::Mat roi_img = img(roi);
		int count = 0;
		for (int row = 0; row < roi_img.rows; ++row) {
			auto imptr = roi_img.ptr<uchar>(row);
			for (int col = 0; col < roi_img.cols; ++col) {
				//int val = *imptr;
				if (*imptr > 0) {
					if (shape == AREA_CIRCLE) {
						int xs = (point.x - roi.x) - col;
						int ys = (point.y - roi.y) - row;
						int radius = dx / 2;
						xs *= xs; ys *= ys;
						if (xs + ys < (radius * radius)) {
							++count;
						}
					}
					if (shape == AREA_ELLIPSE) {
						break;
					}
					if (shape == AREA_RECTANGLE) {
						++count;
					}
				}
				++imptr;
			}
		}
		return count;
	}

	bool contains_hue(const cv::Mat& img, uchar hue, double* ratio) {
		cv::Mat sm;
		if (!(img.cols > 1 && img.rows > 1 && img.cols * img.rows > 50)) {
			return false;
		}
		cv::resize(img, sm, cv::Size(0, 0), 0.5, 0.5);
		cv::Mat hsv = bgr_to_hsv(sm);
		matarr_3 splt = mat_split(hsv);
		int counter = 0;
		for (int row = 0; row < sm.rows; ++row) {
			for (int col = 0; col < sm.cols; ++col) {
				auto sample = splt[0].at<uchar>(row, col);
				if (((hue - 8) < sample && (hue + 8) > sample)) {
					++counter;
				}
			}
		}
		if (ratio) {
			*ratio =
			        static_cast<double>(counter) / static_cast<double>(sm.cols * sm.rows);
		}
		return counter > 0;
	}

	void rotate(const cv::Mat& input, cv::Mat& output, double angle) {
		if (input.empty()) {
			return;
		}
		cv::Point2f image_center(input.cols / 2.0f, input.rows / 2.0f);

		cv::Mat rotation_mat = cv::getRotationMatrix2D(image_center, angle, 1.0);

		double abs_cos = abs(rotation_mat.at<double>(0,0));
		double abs_sin = abs(rotation_mat.at<double>(0,1));

		int bound_w = int(input.rows * abs_sin + input.cols * abs_cos);
		int bound_h = int(input.rows * abs_cos + input.cols * abs_sin);

		rotation_mat.at<double>(0, 2) += bound_w / 2.0 - image_center.x;
		rotation_mat.at<double>(1, 2) += bound_h / 2.0 - image_center.y;

		cv::Mat dst;
		cv::warpAffine(input, dst, rotation_mat, cv::Size(bound_w, bound_h));
		dst.copyTo(output);
	}

	void trim(const cv::Mat& input, cv::Mat& output) {
		if (input.empty()) {
			return;
		}
		int extremes[] = { input.cols, input.rows, 0, 0 };
		for (int row = 0; row < input.rows; ++row) {
			auto ptr = input.ptr<pixel>(row);
			for (int col = 0; col < input.cols; ++col) {
				if (ptr->x > 0 && ptr->y > 0 && ptr->z > 0) {
					extremes[0] = std::min(col, extremes[0]);
					extremes[1] = std::min(row, extremes[1]);
					extremes[2] = std::max(col, extremes[2]);
					extremes[3] = std::max(row, extremes[3]);
				}
				++ptr;
			}
		}
		cv::Rect crop_box(cv::Point(extremes[0], extremes[1]), cv::Point(extremes[2], extremes[3]));
		cv::Mat crop = input(crop_box);
		crop.copyTo(output);
	}

	cv::Mat rotate_and_trim(const cv::Mat& input, double angle) {
		cv::Mat ret;
		rotate(input, ret, angle);
		trim(ret, ret);
		return ret;
	}

	bool img_is_blurry(const cv::Mat& input, int threshold) {
		cv::Mat lap;
		cv::Laplacian(input, lap, CV_64F);
		cv::Scalar mean, variance;
		cv::meanStdDev(lap, mean, variance);
		variance *= variance;

		return variance(0) < threshold;
	}

	void mask_mat_array(const cv::Mat& mask, std::vector<cv::Mat>& array,
	                    const cv::Scalar& default_color /*= cv::Scalar(0, 0, 0)*/) {
		for (auto& mat : array) {
			apply_binary_mask(mask, mat, mat, default_color);
			// cv::Mat tmp(mask.rows, mask.cols, mat.type(), default_color);
			// mat.copyTo(tmp, mask);
			// tmp.copyTo(mat);
		}
	}

	void mask_mat_array(const cv::Mat& mask, std::vector<matarr_3>& array,
	                    const cv::Scalar& default_color /*= cv::Scalar(0, 0, 0)*/) {
		for (auto& mat : array) {
			for (size_t i = 0; i < array.data()->size(); ++i) {
				apply_binary_mask(mask, mat.at(i), mat.at(i), default_color);
				// cv::Mat tmp(mask.rows, mask.cols, mat[i].type(), default_color);
				// mat[i].copyTo(tmp, mask);
				// tmp.copyTo(mat[i]);
			}
		}
	}

	void apply_binary_mask(
	        const cv::Mat& mask, const cv::Mat& img, cv::Mat& output,
	        const cv::Scalar& default_color /*= cv::Scalar(0, 0, 0)*/) {
		cv::Mat in_cpy;
		if (mask.rows < img.rows || mask.cols < img.cols) {
			int x_offset = (img.cols - mask.cols) / 2;
			int y_offset = (img.rows - mask.rows) / 2;
			cv::Rect roi(x_offset, y_offset, mask.cols, mask.rows);
			cv::Mat rimg = img(roi);
			rimg.copyTo(in_cpy);
		} else {
			img.copyTo(in_cpy);
		}
		cv::Mat masked_output(mask.rows, mask.cols, img.type(), default_color);
		in_cpy.copyTo(masked_output, mask);
		masked_output.copyTo(output);
	}

	void alpha_mask(const cv::Mat& mask, const cv::Mat& img, cv::Mat& output) {
		img.copyTo(output);
		for (int row = 0; row < output.rows; ++row) {
			auto iprow = output.ptr<pixel>(row);
			auto mprow = mask.ptr<uchar>(row);
			for (int col = 0; col < output.cols; ++col) {
				*iprow *= (*mprow / 255.0);
				++iprow;
				++mprow;
			}
		}
	}

	std::string vector_as_string(const std::vector<uchar>& vec) {
		if (vec.empty()) {
			return std::string();
		}
		rope::rope_stringbuilder rope_sb;
		const char* separator = "";
		for (auto thing : vec) {
			rope_sb << separator << static_cast<int>(thing);
			separator = ",";
		}
		return rope_sb.str();
	}

	std::string vas(const std::vector<uchar>& vec) {
		return vector_as_string(vec);
	}

	std::string vector_as_string(const vpair_vuc_i& vec,
	                             bool simple /* = false */) {
		rope::rope_stringbuilder rope_sb;
		const char* separator = "";
		for (const auto& p : vec) {
			rope_sb << separator << vector_as_string(p.first);
			if (!simple) rope_sb << colorex(212);
			rope_sb << ":" << p.second;
			if (!simple) rope_sb << reset;
			separator = "|";
		}
		return rope_sb.str();
	}

	std::string vas(const vpair_vuc_i& vec, bool simple/* = false*/) {
		return vector_as_string(vec, simple);
	}

	std::string point_as_string(const cv::Point3i& pt) {
		rope::rope_stringbuilder rope_sb;
		rope_sb << "[" << pt.x << ";" << pt.y << ";" << pt.z << "]";
		return rope_sb.str();
	}

	std::string pas(const cv::Point3i& pt) {
		return point_as_string(pt);
	}

	const std::vector<pixel> COLOR_MAPPING = {
	    pixel(0, 0, 255),   pixel(0, 127, 255),  pixel(0, 255, 255),
	    pixel(0, 255, 127), pixel(0, 255, 0),    pixel(127, 255, 0),
	    pixel(255, 255, 0), pixel(255, 255, 255)};

	void paint_point(int x, int y, uint color, cv::Mat& img) {
		if (color < COLOR_MAPPING.size()) {
			img.at<pixel>(y, x) = COLOR_MAPPING[color];
		}
	}

	void paint_point(int i, int color, cv::Mat& img) {
		const int x = i % img.cols;
		const int y = i / img.cols;
		paint_point(x, y, color, img);
	}

	cv::Mat bgr_to_hsv(const cv::Mat& img, double scale /* = 1.0 */) {
		cv::Mat ret;
		if (scale < 0.999 || scale > 1.001) {
			cv::resize(img, ret, cv::Size(), scale, scale);
			cv::cvtColor(ret, ret, cv::COLOR_BGR2HSV);
		} else {
			cv::cvtColor(img, ret, cv::COLOR_BGR2HSV);
		}
		return ret;
	}

	matarr_3 bgr_to_hsv_3(const cv::Mat& img) {
		cv::Mat hsv = bgr_to_hsv(img, 1.0);
		matarr_3 hsv_split = utility::mat_split(hsv);
		return hsv_split;
	}

	matarr_3 bgr_to_hsv(const matarr_3& img) {
		cv::Mat hsv;
		cv::Mat rgb;
		cv::merge(img, rgb);
		// rgb.convertTo(hsv, COLOR_BGR2HSV);
		cv::cvtColor(rgb, hsv, cv::ColorConversionCodes::COLOR_BGR2HSV);
		matarr_3 channels;
		split(hsv, channels);
		return channels;
	}

	std::vector<matarr_3> bgr_ss_to_hsv(std::vector<vmat>& bgr_ss) {
		size_t levels = bgr_ss[0].size();
		std::vector<matarr_3> hsv_ss(levels);
		for (size_t i = 0; i < levels; ++i) {
			cv::Mat bgr = mat_merge(matarr_3{bgr_ss[0][i], bgr_ss[1][i], bgr_ss[2][i]});
			hsv_ss[i] = bgr_to_hsv_3(bgr);
		}
		return hsv_ss;
	}

	cv::Mat hsv_to_bgr(const cv::Mat& img, double scale /* = 1.0 */) {
		cv::Mat ret;
		if (scale < 0.999 || scale > 1.001) {
			cv::resize(img, ret, cv::Size(), scale, scale);
			cv::cvtColor(ret, ret, cv::COLOR_HSV2BGR);
		} else {
			cv::cvtColor(img, ret, cv::COLOR_HSV2BGR);
		}
		return ret;
	}

	matarr_3 mat_split(const cv::Mat& img) {
		matarr_3 ret;
		cv::split(img, ret);
		return ret;
	}

	cv::Mat mat_merge(const matarr_3& split) {
		cv::Mat ret(split[0].rows, split[0].cols, CV_8UC3);
		cv::merge(split, ret);
		return ret;
	}

	// CURRENTLY CAUSES CRASHES
	void remove_background(cv::Mat& image) {
		cv::Mat hsv = bgr_to_hsv(image);
		histogram hist = get_histogram(hsv);
		std::vector<histogram::hist_entry> hvals = hist.all_max();
		std::cout << hvals[0].x() << " " << hvals[1].x() << " " << hvals[2].x() << std::endl;

		hsv.forEach<pixel>([&](pixel& pix, const int*) {
			if (pix.x - 5 < static_cast<int>(hvals[0].x()) &&
			        pix.x + 5 > static_cast<int>(hvals[0].x())) {
				pix.x = 0;
				pix.y = 0;
				pix.z = 0;
			}
		});

		cv::cvtColor(hsv, image, cv::COLOR_HSV2BGR);
	}

	std::vector<unsigned int*> histogram::operator()(int i) {
		double bin_idx_div = 255 / h_1.size();
		if (channels() == 1) {
			return { &h_1[static_cast<int>(i / bin_idx_div)]};
		}
		return { &h_1[static_cast<int>(i / bin_idx_div)],
			        &h_2[static_cast<int>(i / bin_idx_div)],
			        &h_3[static_cast<int>(i / bin_idx_div)] };
	}

	std::vector<unsigned int*> histogram::operator()(int i, int j, int k) {
		double bin_idx_div = 255 / h_1.size();
		return { &h_1[static_cast<int>(i / bin_idx_div)],
			        &h_2[static_cast<int>(j / bin_idx_div)],
			        &h_3[static_cast<int>(k / bin_idx_div)] };
	}

	size_t histogram::channels() const {
		return h_3.empty() ? 1 : 3;
	}

	histogram::hist_entry histogram::max() const {
		hist_entry hval{0, 0};
		if (channels() == 3) {
			for (size_t i = 0; i < h_1.size(); ++i) {
				if (h_1[i] > hval.y_) {
					hval.y_ = h_1[i];
					hval.x_ = i;
				}
				if (h_2[i] > hval.y_) {
					hval.y_ = h_2[i];
					hval.x_ = i;
				}
				if (h_3[i] > hval.y_) {
					hval.y_ = h_3[i];
					hval.x_ = i;
				}
			}
		} else {
			for (size_t i = 0; i < h_1.size(); ++i) {
				if (h_1[i] > hval.y_) {
					hval.y_ = h_1[i];
					hval.x_ = i;
				}
			}
		}
		return hval;
	}

	std::vector<histogram::hist_entry> histogram::all_max() const {
		hist_entry xval{0, 0};
		hist_entry yval{0, 0};
		hist_entry zval{0, 0};
		if (channels() == 3) {
			for (size_t i = 0; i < h_1.size(); ++i) {
				if (h_1[i] > xval.y_) {
					xval.y_ = h_1[i];
					xval.x_ = i;
				}
				if (h_2[i] > yval.y_) {
					yval.y_ = h_2[i];
					yval.x_ = i;
				}
				if (h_3[i] > zval.y_) {
					zval.y_ = h_3[i];
					zval.x_ = i;
				}
			}
			return { xval, yval, zval };
		}
		for (size_t i = 0; i < h_1.size(); ++i) {
			if (h_1[i] > xval.y_) {
				xval.y_ = h_1[i];
				xval.x_ = i;
			}
		}
		return { xval };
	}

	histogram get_histogram(const cv::Mat& image, int bins /* = 255*/) {
		if (image.channels() == 1) {
			histogram hgram(bins);
			auto ip = image.ptr<uchar>();
			for (int i = 0; i < image.rows * image.cols; ++i) {
				std::vector<unsigned int*> v = hgram(*ip);
				++*v[0];
				++ip;
			}
			return hgram;
		}
		histogram hgram(bins, 3);
		auto ip = image.ptr<pixel>();
		for (int i = 0; i < image.rows * image.cols; ++i) {
			std::vector<unsigned int*> v = hgram(static_cast<int>(ip->x), static_cast<int>(ip->y), static_cast<int>(ip->z));
			++*v[0];
			++*v[1];
			++*v[2];
			++ip;
		}
		return hgram;
	}

	double compare_histograms(const cv::Mat& img_a, const cv::Mat& img_b) {
		if (img_a.empty() || img_b.empty()) {
			return 0;
		}
		// histogram ha = get_histogram(bgr_to_hsv(img_a));
		// show_im("hist", drawing::draw_histogram(ha));
		int bins = 255;
		int hist_size[] = { bins, bins, bins };
		// hue varies from 0 to 179, saturation from 0 to 255
		float vranges[] = { 0, 255 };
		const float* ranges[] = { vranges, vranges, vranges };
		// Use the 0-th and 1-st channels
		int channels[] = { 0, 1, 2 };
		cv::Mat hist_a;
		cv::Mat hist_b;
		cv::calcHist(&img_a, 1, channels, cv::Mat(), hist_a, 2, hist_size, ranges, true, false );
		cv::normalize( hist_a, hist_a, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );
		cv::calcHist(&img_b, 1, channels, cv::Mat(), hist_b, 2, hist_size, ranges, true, false );
		cv::normalize(hist_b, hist_b, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );
		return compareHist(hist_a, hist_b, 0);
	}

	double get_psnr(const cv::Mat& i1, const cv::Mat& i2) {
		cv::Mat s1;
		absdiff(i1, i2, s1);
		s1.convertTo(s1, CV_32F);
		s1 = s1.mul(s1);
		cv::Scalar s = sum(s1);
		double sse = s.val[0] + s.val[1] + s.val[2];
		if( sse <= 1e-10) {
			return 0;
		} else {
			double mse  = sse / (double)(i1.channels() * i1.total());
			double psnr = 10.0 * log10((255 * 255) / mse);
			return psnr;
		}
	}

	cv::Scalar get_mssim(const cv::Mat& i1, const cv::Mat& i2) {
		const double C1 = 6.5025, C2 = 58.5225;
		int d = CV_32F;
		cv::Mat I1, I2;
		i1.convertTo(I1, d);
		i2.convertTo(I2, d);
		cv::Mat I2_2   = I2.mul(I2);
		cv::Mat I1_2   = I1.mul(I1);
		cv::Mat I1_I2  = I1.mul(I2);
		cv::Mat mu1, mu2;
		GaussianBlur(I1, mu1, cv::Size(11, 11), 1.5);
		GaussianBlur(I2, mu2, cv::Size(11, 11), 1.5);
		cv::Mat mu1_2   =   mu1.mul(mu1);
		cv::Mat mu2_2   =   mu2.mul(mu2);
		cv::Mat mu1_mu2 =   mu1.mul(mu2);
		cv::Mat sigma1_2, sigma2_2, sigma12;
		GaussianBlur(I1_2, sigma1_2, cv::Size(11, 11), 1.5);
		sigma1_2 -= mu1_2;
		GaussianBlur(I2_2, sigma2_2, cv::Size(11, 11), 1.5);
		sigma2_2 -= mu2_2;
		GaussianBlur(I1_I2, sigma12, cv::Size(11, 11), 1.5);
		sigma12 -= mu1_mu2;
		cv::Mat t1, t2, t3;
		t1 = 2 * mu1_mu2 + C1;
		t2 = 2 * sigma12 + C2;
		t3 = t1.mul(t2);
		t1 = mu1_2 + mu2_2 + C1;
		t2 = sigma1_2 + sigma2_2 + C2;
		t1 = t1.mul(t2);
		cv::Mat ssim_map;
		divide(t3, t1, ssim_map);
		cv::Scalar mssim = mean(ssim_map);
		return mssim;
	}

	vpair_vuc_i copy_n_most_common_from_map(int n, const map_vuc_i& m) {
		vpair_vuc_i top_n(n);
		std::partial_sort_copy(m.begin(), m.end(), top_n.begin(), top_n.end(),
		                       [](std::pair<const std::vector<uchar>, int> const& l,
		                       std::pair<const std::vector<uchar>, int> const& r) {
			return l.second > r.second;
		});
		return top_n;
	}

	bool pair_compare(const std::pair<std::vector<uchar>, int>& l,
	                         const std::pair<std::vector<uchar>, int>& r) {
		return l.second > r.second;
	}

	vpair_vuc_i copy_most_common_from_map(int img_n, const map_vuc_i& m,
	                                       int color_count_threshold,
	                                       range<uchar> excluded_hue_range,
	                                       int at_least /*= 4*/) {
		size_t counter_size = m.size();
		vpair_vuc_i top_n(m.size());
		std::partial_sort_copy(m.begin(), m.end(), top_n.begin(), top_n.end(),
		                       pair_compare);

		if (top_n.size() <= 3) {
			return top_n;
		}

		vpair_vuc_i filtered_most_common;
		filtered_most_common.reserve(counter_size);
		int total = 0;
		int satisfying_amount = static_cast<int>(img_n * 0.7);
		for (size_t i = 0; i < top_n.size(); ++i) {
			const auto& pair = top_n[i];
			if (excluded_hue_range.any_in_range(pair.first)) {
				continue;
			}
			if (filtered_most_common.size() <= 1 ||
			        pair.second > color_count_threshold) {
				total += pair.second;
				filtered_most_common.push_back(pair);
			} else {
				return filtered_most_common;
			}
			if (total >= satisfying_amount || i >= top_n.size() ||
			        top_n[i + 1].second < at_least) {
				if (i + 1 < top_n.size() &&
				        (filtered_most_common.size() == 1 ||
				         top_n[i + 1].second > color_count_threshold)) {
					filtered_most_common.push_back(top_n[i + 1]);
				}
				return filtered_most_common;
			}
		}

		return filtered_most_common;
	}

	bool similar(int a, int b, double factor/* = 0.1*/) {
		return std::abs(a - b) <= factor * (a + b);
	}


	std::vector<std::string> split(const std::string& text, char sep) {
		std::vector<std::string> tokens;
		std::size_t start = 0;
		std::size_t end = 0;
		while ((end = text.find(sep, start)) != std::string::npos) {
			tokens.push_back(text.substr(start, end - start));
			start = end + 1;
		}
		tokens.push_back(text.substr(start));
		return tokens;
	}

	cv::Mat blend(const cv::Mat& a, const cv::Mat& b, double alpha) {
		double beta = 1.0 - alpha;
		cv::Mat res;
		cv::addWeighted(a, alpha, b, beta, 0.0, res);
		return res;
	}

	void resize_im(matarr_3& img, matarr_3& small, double resize) {
		for (int i = 0; i < 3; ++i) {
			cv::resize(img[i], small[i], cv::Size(), resize, resize, cv::INTER_AREA);
		}
	}

	void resize_im(matarr_3& bgr_a, matarr_3& bgr_b, matarr_3& a_small, matarr_3& b_small,
	               double resize) {
		for (int i = 0; i < 3; ++i) {
			cv::resize(bgr_a[i], a_small[i], cv::Size(), resize, resize,
			           cv::INTER_AREA);
			cv::resize(bgr_b[i], b_small[i], cv::Size(), resize, resize,
			           cv::INTER_AREA);
		}
	}

	bool num_in_list(int num, const std::vector<int>& list) {
		if (list.empty()) {
			return false;
		}
		uint i = 0;
		int n = list[i];
		while (n < num && i < list.size()) {
			++i;
			n = list[i];
		}
		return n == num;
	}

	bool point_in_ellipse(const cv::Point& center, const cv::Point& point, int h,
	                      int w, double angle) {
		double cosa = cos(angle);
		double sina = sin(angle);
		double ww = w * w;  // w / 2.0 * w / 2.0;
		double hh = h * h;  // h / 2.0 * h / 2.0;

		double cps = cosa * (point.x - center.x) + sina * (point.y - center.y);
		double smc = sina * (point.x - center.x) - cosa * (point.y - center.y);
		cps *= cps;
		smc *= smc;

		double e = (cps / ww) + (smc / hh);
		return e <= 1.0;
	}

	bool point_in_circle(const cv::Point& center, const cv::Point& point,
	                     int radius) {
		return sqrt(((center.x - point.x) * (center.x - point.x)) +
		            ((center.y - point.y) * (center.y - point.y))) < radius;
	}

	bool point_in_rectangle(const cv::Point& center, const cv::Point& point, int h,
	                        int w) {
		return (center.x > point.x - h / 2) && (center.x < point.x + h / 2) &&
		        (center.y > point.y - w / 2) && (center.y < point.y + w / 2);
	}

	bool general_point_in_parellelogram(const cv::Point2d& M, const cv::Point2d& A, const cv::Point2d& B, const cv::Point2d& C, const cv::Point2d& D) {
		(void)C;
		cv::Point2d p = cv::Point2d(M.x - A.x, M.y - A.y);
		cv::Vec2d ab = cv::Vec2d(B.x - A.x, B.y - A.y);
		cv::Vec2d ad = cv::Vec2d(D.x - A.x, D.y - A.y);
		cv::Vec2d ortho_ab = cv::Vec2d(-ab[1], ab[0]);
		cv::Vec2d ortho_ad = cv::Vec2d(-ad[1], ad[0]);
		cv::Vec2d orien_ab = (ortho_ab.dot(ad) >= 0 ? 1 : -1) * ortho_ab;
		cv::Vec2d orien_ad = (ab.dot(ortho_ad) >= 0 ? 1 : -1) * ortho_ad;

		double oab_p = orien_ab.dot(p);
		double oab_ad = orien_ab.dot(ad);
		double oad_p = orien_ad.dot(p);
		double ab_oad = ab.dot(orien_ad);
		return (0 < oab_p && oab_p < oab_ad) && (0 < oad_p && oad_p < ab_oad);
	}

	cv::Scalar hsv2bgr(float h, float s, float v) {
		// use values [0-360][0-1][0-1]
		float C = s * v;
		float X = C * (1 - abs(fmod(h / 60.0, 2) - 1));
		float m = v - C;
		float r;
		float g;
		float b;
		if (h >= 0 && h < 60) {
			r = C;
			g = X;
			b = 0;
		} else if (h >= 60 && h < 120) {
			r = X;
			g = C;
			b = 0;
		} else if (h >= 120 && h < 180) {
			r = 0;
			g = C;
			b = X;
		} else if (h >= 180 && h < 240) {
			r = 0;
			g = X;
			b = C;
		} else if (h >= 240 && h < 300) {
			r = X;
			g = 0;
			b = C;
		} else {
			r = C;
			g = 0;
			b = X;
		}
		int R = (r + m) * 255;
		int G = (g + m) * 255;
		int B = (b + m) * 255;
		return cv::Scalar(B, G, R);
	}

	cv::Scalar bgr2hsv(int B, int G, int R) {
		float r = R / 255.0;
		float g = G / 255.0;
		float b = B / 255.0;
		float cmax = max3(r, g, b);
		float cmin = min3(r, g, b);
		float delta = cmax - cmin;

		float h = 0;
		if (delta > 0.001) {
			if (cmax == r) {
				h = 60 * mod((g - b) / delta, 6);
			} else if (cmax == g) {
				h = 60 * (((b - r) / delta) + 2);
			} else {
				h = 60 * (((r - g) / delta) + 4);
			}
		}

		float s = delta < 0.001 ? 0 : delta / cmax;
		float v = cmax;

		int H = h / 2.0; // opencv halved hue
		int S = s * 255;
		int V = v * 255;
		return cv::Scalar(H, S, V);
	}

	double deltaE(const cv::Point3f& labA, const cv::Point3f& labB) {
	  float deltaL = labA.x - labB.x;
	  float deltaA = labA.y - labB.y;
	  float deltaB = labA.z - labB.z;
	  float c1 = std::sqrt(labA.y * labA.y + labA.z * labA.z);
	  float c2 = std::sqrt(labB.y * labB.y + labB.z * labB.z);
	  float deltaC = c1 - c2;
	  float deltaH = deltaA * deltaA + deltaB * deltaB - deltaC * deltaC;
	  deltaH = deltaH < 0 ? 0 : std::sqrt(deltaH);
	  float sc = 1.0 + 0.045 * c1;
	  float sh = 1.0 + 0.015 * c1;
	  float deltaLKlsl = deltaL / (1.0);
	  float deltaCkcsc = deltaC / (sc);
	  float deltaHkhsh = deltaH / (sh);
	  float i = deltaLKlsl * deltaLKlsl + deltaCkcsc * deltaCkcsc + deltaHkhsh * deltaHkhsh;
	  return i < 0 ? 0 : std::sqrt(i);
	}
}  // namespace utility

namespace quick_access_utility {
	// behold! the template wall!
	template<> cv::Mat give_mat<uchar> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_8UC1); }
	template<> cv::Mat give_mat<pixel> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_8UC3); }
	template<> cv::Mat give_mat<schar> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_8SC1); }
	template<> cv::Mat give_mat<ushort>(const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_16UC1); }
	template<> cv::Mat give_mat<short> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_16UC1); }
	template<> cv::Mat give_mat<int>   (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_32SC1); }
	template<> cv::Mat give_mat<float> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_32FC1); }
	template<> cv::Mat give_mat<double>(const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_64FC1); }

	template<> cv::Mat give_mat<uchar> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_8UC1); }
	template<> cv::Mat give_mat<pixel> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_8UC3); }
	template<> cv::Mat give_mat<schar> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_8SC1); }
	template<> cv::Mat give_mat<ushort>(int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_16UC1); }
	template<> cv::Mat give_mat<short> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_16UC1); }
	template<> cv::Mat give_mat<int>   (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_32SC1); }
	template<> cv::Mat give_mat<float> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_32FC1); }
	template<> cv::Mat give_mat<double>(int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_64FC1); }

	template<> cv::Mat give_mat<uchar, 3> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_8UC3); }
	template<> cv::Mat give_mat<schar, 3> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_8SC3); }
	template<> cv::Mat give_mat<ushort, 3>(const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_16UC3); }
	template<> cv::Mat give_mat<short, 3> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_16UC3); }
	template<> cv::Mat give_mat<int, 3>   (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_32SC3); }
	template<> cv::Mat give_mat<float, 3> (const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_32FC3); }
	template<> cv::Mat give_mat<double, 3>(const cv::Mat& original) { return cv::Mat::zeros(original.rows, original.cols, CV_64FC3); }

	template<> cv::Mat give_mat<uchar, 3> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_8UC3); }
	template<> cv::Mat give_mat<schar, 3> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_8SC3); }
	template<> cv::Mat give_mat<ushort, 3>(int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_16UC3); }
	template<> cv::Mat give_mat<short, 3> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_16UC3); }
	template<> cv::Mat give_mat<int, 3>   (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_32SC3); }
	template<> cv::Mat give_mat<float, 3> (int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_32FC3); }
	template<> cv::Mat give_mat<double, 3>(int rows, int cols) { return cv::Mat::zeros(rows, cols, CV_64FC3); }

	template<> cv::Mat give_mat<uchar> (const cv::Mat& original, uchar  value) { return cv::Mat(original.rows, original.cols, CV_8UC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<schar> (const cv::Mat& original, schar  value) { return cv::Mat(original.rows, original.cols, CV_8SC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<ushort>(const cv::Mat& original, ushort value) { return cv::Mat(original.rows, original.cols, CV_16UC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<short> (const cv::Mat& original, short  value) { return cv::Mat(original.rows, original.cols, CV_16UC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<int>   (const cv::Mat& original, int    value) { return cv::Mat(original.rows, original.cols, CV_32SC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<float> (const cv::Mat& original, float  value) { return cv::Mat(original.rows, original.cols, CV_32FC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<double>(const cv::Mat& original, double value) { return cv::Mat(original.rows, original.cols, CV_64FC1, cv::Scalar::all(value)); }

	template<> cv::Mat give_mat<uchar> (int rows, int cols, uchar  value) { return cv::Mat(rows, cols, CV_8UC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<schar> (int rows, int cols, schar  value) { return cv::Mat(rows, cols, CV_8SC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<ushort>(int rows, int cols, ushort value) { return cv::Mat(rows, cols, CV_16UC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<short> (int rows, int cols, short  value) { return cv::Mat(rows, cols, CV_16UC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<int>   (int rows, int cols, int    value) { return cv::Mat(rows, cols, CV_32SC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<float> (int rows, int cols, float  value) { return cv::Mat(rows, cols, CV_32FC1, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<double>(int rows, int cols, double value) { return cv::Mat(rows, cols, CV_64FC1, cv::Scalar::all(value)); }

	template<> cv::Mat give_mat<uchar, 3> (const cv::Mat& original, uchar  value) { return cv::Mat(original.rows, original.cols, CV_8UC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<schar, 3> (const cv::Mat& original, schar  value) { return cv::Mat(original.rows, original.cols, CV_8SC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<ushort, 3>(const cv::Mat& original, ushort value) { return cv::Mat(original.rows, original.cols, CV_16UC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<short, 3> (const cv::Mat& original, short  value) { return cv::Mat(original.rows, original.cols, CV_16UC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<int, 3>   (const cv::Mat& original, int    value) { return cv::Mat(original.rows, original.cols, CV_32SC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<float, 3> (const cv::Mat& original, float  value) { return cv::Mat(original.rows, original.cols, CV_32FC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<double, 3>(const cv::Mat& original, double value) { return cv::Mat(original.rows, original.cols, CV_64FC3, cv::Scalar::all(value)); }

	template<> cv::Mat give_mat<uchar, 3> (int rows, int cols, uchar  value) { return cv::Mat(rows, cols, CV_8UC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<schar, 3> (int rows, int cols, schar  value) { return cv::Mat(rows, cols, CV_8SC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<ushort, 3>(int rows, int cols, ushort value) { return cv::Mat(rows, cols, CV_16UC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<short, 3> (int rows, int cols, short  value) { return cv::Mat(rows, cols, CV_16UC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<int, 3>   (int rows, int cols, int    value) { return cv::Mat(rows, cols, CV_32SC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<float, 3> (int rows, int cols, float  value) { return cv::Mat(rows, cols, CV_32FC3, cv::Scalar::all(value)); }
	template<> cv::Mat give_mat<double, 3>(int rows, int cols, double value) { return cv::Mat(rows, cols, CV_64FC3, cv::Scalar::all(value)); }

}

namespace drawing {
	void draw_line_segment(cv::Mat& img, double x0, double y0, double x1, double y1,
	                       const pixel& color, int thickness /*= 1*/) {
		(void)thickness;
		if (!SHOW_IMAGES__) {
			return;
		}
		auto dx = static_cast<float>(abs(x1 - x0));
		auto dy = static_cast<float>(abs(y1 - y0));
		if (dx >= dy) {
			if (dx == 0) return;
			int ix0 = ceil(std::min(x0, x1));
			int ix1 = ceil(std::max(x0, x1));
			for (int x = ix0; x < ix1; x++) {
				int y = floor(y0 + (x + .5 - x0) * (y1 - y0) / (x1 - x0) + .5);
				if (x >= 0 && y >= 0 && x < img.cols && y < img.rows)
					img.at<pixel>(y, x) = color;
			}
		} else {
			int iy0 = ceil(std::min(y0, y1));
			int iy1 = ceil(std::max(y0, y1));
			for (int y = iy0; y < iy1; y++) {
				int x = floor(x0 + (y + .5 - y0) * (x1 - x0) / (y1 - y0) + .5);
				if (x >= 0 && y >= 0 && x < img.cols && y < img.rows)
					img.at<pixel>(y, x) = color;
			}
		}
	}

	void draw_arrow(cv::Mat& img, double x0, double y0, double x1, double y1,
	                const cv::Scalar& color, int thickness /* = 1*/) {
		if (!SHOW_IMAGES__ || (x0 == x1 && y0 == y1)) {
			return;
		}
		drawing::draw_line(img, x0, y0, x1, y1, color, thickness);
		if (x0 > x1) {
			double tmp = x0;
			x0 = x1;
			x1 = tmp;
			tmp = y0;
			y0 = y1;
			y1 = tmp;
		}
		cv::Vec2d direction(x1 - x0, y1 - y0);
		double magnitude = sqrt(direction[0] * direction[0] + direction[1] * direction[1]);
		direction /= magnitude;
		cv::Vec2d n(direction[1], -direction[0]);
		double mult_factor = magnitude * 0.1;
		cv::Point o = cv::Point(x1, y1) + (-mult_factor * direction);
		cv::Point end_down = o + mult_factor * n;
		cv::Point end_up = o + mult_factor * -n;
		drawing::draw_line(img, end_down.x, end_down.y, x1, y1, color, thickness);
		drawing::draw_line(img, end_up.x, end_up.y, x1, y1, color, thickness);
	}

	void draw_line(cv::Mat& img, double x0, double y0, double x1, double y1,
	                       const cv::Scalar& color, int thickness /*= 1*/) {
		if (!SHOW_IMAGES__) {
			return;
		}
		cv::line(img, cv::Point(x0, y0), cv::Point(x1, y1), color, thickness);
	}

	void draw_axis(cv::Mat& img, cv::Point p, cv::Point q, const cv::Scalar& color,
	               float scale /* = 0.2 */) {
		if (!SHOW_IMAGES__) {
			return;
		}
		double angle = atan2((double)p.y - q.y, (double)p.x - q.x);
		double hypotenuse =
		        sqrt((double)(p.y - q.y) * (p.y - q.y) + (p.x - q.x) * (p.x - q.x));
		q.x = (int)(p.x - scale * hypotenuse * cos(angle));
		q.y = (int)(p.y - scale * hypotenuse * sin(angle));
		line(img, p, q, color, 1, cv::LINE_AA);
		p.x = (int)(q.x + 9 * cos(angle + CV_PI / 4));
		p.y = (int)(q.y + 9 * sin(angle + CV_PI / 4));
		line(img, p, q, color, 1, cv::LINE_AA);
		p.x = (int)(q.x + 9 * cos(angle - CV_PI / 4));
		p.y = (int)(q.y + 9 * sin(angle - CV_PI / 4));
		line(img, p, q, color, 1, cv::LINE_AA);
	}

	void draw_square(cv::Mat& img, float x, float y, int radius,
	                 const cv::Scalar& color) {
		if (!SHOW_IMAGES__) {
			return;
		}
		int x0 = floor(x + .5);
		int y0 = floor(y + .5);
		for (int y = y0 - radius; y <= y0 + radius; y++) {
			for (int x = x0 - radius; x <= x0 + radius; x++) {
				if (x >= 0 && y >= 0 && x < img.cols && y < img.rows) {
					img.at<pixel>(y, x) =
					        pixel{static_cast<uchar>(color[0]),
					        static_cast<uchar>(color[1]),
					        static_cast<uchar>(color[2])};
			    }
		    }
	    }
    }

void draw_cross(cv::Mat& img, const cv::Point& p, int radius,
                const cv::Scalar& color, int thickness/* = 1*/) {
	if (SHOW_IMAGES__) {
		cv::line(img, p - cv::Point(radius, 0), p + cv::Point(radius, 0), color, thickness);
		cv::line(img, p - cv::Point(0, radius), p + cv::Point(0, radius), color, thickness);
	}
}

void mouse_info(cv::Mat& img, const mouse_s& mouse, const cv::Point& pos,
                bool onMouse /* = false*/) {
	pixel px = img.at<pixel>({mouse.x, mouse.y});
	drawing::draw_cross(img, mouse, 10,
	                    cv::Scalar(255 - px.x, 255 - px.y, 255 - px.z));
	rope::rope_stringbuilder rope_sb;
	rope_sb << "[" << mouse.x << ":" << mouse.y << "] = (" << static_cast<int>(px.x)
	        << ", " << static_cast<int>(px.y) << ", " << static_cast<int>(px.z)
	        << ") ";
	switch (mouse.event) {
	case 1:
		rope_sb << "l down";
		break;
	case 2:
		rope_sb << "r down";
		break;
	case 3:
		rope_sb << "m down";
		break;
	case 4:
		rope_sb << "l up";
		break;
	case 5:
		rope_sb << "r up";
		break;
	case 6:
		rope_sb << "m up";
		break;
	default:
		break;
	}

	if (onMouse || (pos.x == -1 && pos.y == -1)) {
		putText(img, rope_sb.str(), cv::Point(mouse.x + 7, mouse.y + 15),
		        cv::FONT_HERSHEY_COMPLEX, 0.4, cv::Scalar::all(0), 2, cv::LINE_AA);
		putText(img, rope_sb.str(), cv::Point(mouse.x + 7, mouse.y + 15),
		        cv::FONT_HERSHEY_COMPLEX, 0.4, cv::Scalar::all(255), 1,
		        cv::LINE_AA);
	} else {
		putText(img, rope_sb.str(), pos, cv::FONT_HERSHEY_COMPLEX, 0.4,
		        cv::Scalar::all(0), 2, cv::LINE_AA);
		putText(img, rope_sb.str(), pos, cv::FONT_HERSHEY_COMPLEX, 0.4,
		        cv::Scalar::all(255), 1, cv::LINE_AA);
	}
}

void draw_rectangle(cv::Mat& img, cv::Rect& rect, const cv::Scalar& color,
                    int thickness /* = 1*/, int overlap /* = 2*/) {
	if (DEBUG_OUTPUT__ && SHOW_IMAGES__ && DRAW_RECTANGLES__) {
		if (overlap > 0) {
			cv::Rect nr(std::min(img.cols - rect.width - 1, std::max(0, rect.x)),
			            std::min(img.rows - (rect.height + overlap) - 1,
			                     std::max(0, rect.y - overlap / 2)),
			            rect.width, rect.height + overlap);
			cv::rectangle(img, nr, color, thickness);
		} else {
			cv::rectangle(img, rect, color, thickness);
		}
	}
}

void draw_rectangle(cv::Mat& img, const cv::Point& pt1, const cv::Point& pt2,
                    const cv::Scalar& color, int thickness /* = 1*/,
                    int line_type /* = cv::LINE_8*/, int shift /*= 0*/) {
	if (SHOW_IMAGES__) {
		cv::rectangle(img, pt1, pt2, color, thickness, line_type, shift);
	}
}

void draw_rectangle(cv::Mat& img, const cv::Rect& r, const cv::Scalar& color,
                    int thickness /* = 1*/, int line_type /* = cv::LINE_8*/,
                    int shift /*= 0*/) {
	if (SHOW_IMAGES__) {
		cv::rectangle(img, r, color, thickness, line_type, shift);
	}
}

void draw_text(cv::Mat& img, const cv::String& text, const cv::Point& org,
               int fontface, double fontscale, const cv::Scalar& color,
               int thickness /*= 1*/, int line_type /* = cv::LINE_8*/,
               bool bottom_left_origin /* = false*/) {
	if (SHOW_IMAGES__) {
		cv::putText(img, text, org, fontface, fontscale, color, thickness,
		            line_type, bottom_left_origin);
	}
}

//static const std::array<uchar, 32> rand_array = {
//    0xfd, 0xe4, 0x65, 0xf4, 0x9b, 0xed, 0x79, 0xd3, 0x95, 0x2a, 0x6c, 0xec,
//    0x70, 0x62, 0xf5, 0xd4, 0xea, 0x57, 0x6e, 0xa8, 0xec, 0x6b, 0x1a, 0xea};

static int rand_index = 0;

cv::Mat plot(std::vector<plot_member<size_t>>& values) {
	int w = 400;
	int h = static_cast<int>(30 * values.size());
	cv::Mat image(h, w, CV_8UC3, cv::Scalar::all(35));
	int bar_start = 100;
	int i = 0;
	for (auto& pm : values) {
		if (pm.assigned_index() == -1) {
			pm.assigned_index(rand_index++);
		}
		draw_text(image, pm.name(), cv::Point(10, 18 + 30 * i),
		          cv::FONT_HERSHEY_SIMPLEX, 0.4, cv::Scalar::all(240), 1,
		          cv::LINE_AA);
		int pos = static_cast<int>(
		              bar_start +
		              (w - bar_start - 10) * (static_cast<double>(pm.value()) / pm.max()));
		draw_rectangle(image, cv::Point(bar_start, 4 + 30 * i),
		               cv::Point(pos, 26 + 30 * i), rand_colormap[pm.assigned_index()],
		        -1);
		++rand_index;
		int baseline = 0;
		cv::Size sz = cv::getTextSize(std::to_string(pm.value()),
		                              cv::FONT_HERSHEY_SIMPLEX, 0.4, 1, &baseline);
		draw_text(image, std::to_string(pm.value()),
		          cv::Point(pos - sz.width - 3, 18 + 30 * i),
		          cv::FONT_HERSHEY_SIMPLEX, 0.4, cv::Scalar::all(10), 2,
		          cv::LINE_AA);
		draw_text(image, std::to_string(pm.value()),
		          cv::Point(pos - sz.width - 3, 18 + 30 * i),
		          cv::FONT_HERSHEY_SIMPLEX, 0.4, cv::Scalar::all(240), 1,
		          cv::LINE_AA);
		++i;
	}
	show_im("plot", image);
	return image;
}

cv::Mat plot(cv::Mat& im, std::vector<plot_member<size_t>>& values) {
	cv::Mat image = cv::Mat::zeros(static_cast<int>(im.rows + 30 * values.size()),
	                               im.cols, CV_8UC3);
	cv::Mat new_part = plot(values);
	cv::vconcat(im, new_part, image);
	show_im("plot", image);
	return image;
}

cv::Mat draw_histogram(std::vector<cv::Mat>& hist, int hist_size, int plot_type) {
	int hist_w = 512;
	int hist_h = 400;
	int bin_w = cvRound(static_cast<double>(hist_w) / hist_size);
	std::vector<cv::Scalar> colors = {cv::Scalar(255, 0, 0),
	                                  cv::Scalar(0, 255, 0),
	                                  cv::Scalar(0, 0, 255)};
	cv::Mat hist_image(hist_h, hist_w, CV_8UC3, cv::Scalar::all(0));
	for (size_t i = 0; i < hist.size(); ++i) {
		cv::normalize(hist[i], hist[i], 0, hist_image.rows, cv::NORM_MINMAX, -1,
		              cv::Mat());
	}

	for (int i = 1; i < hist_size; i++) {
		switch (plot_type) {
		case 0:
			for (size_t j = 0; j < hist.size(); ++j) {
				cv::line(hist_image,
				         cv::Point(bin_w * (i - 1), hist_h - cvRound(hist[j].at<float>(i - 1))),
				         cv::Point(bin_w * (i), hist_h - cvRound(hist[j].at<float>(i))),
				         colors[j], 1, 8, 0);
			}
			break;
		case 1:
			std::vector<float> values = {hist[0].at<float>(i), hist[1].at<float>(i), hist[2].at<float>(i) };
			float b_v = values[0], g_v = values[1], r_v = values[2];
			std::vector<float> svalues = {b_v, g_v, r_v};
			std::sort(svalues.begin(), svalues.end());
			unsigned int b = b_v == svalues[2] ? 255 : 0;
			unsigned int g = g_v == svalues[2] ? 255 : 0;
			unsigned int r = r_v == svalues[2] ? 255 : 0;
			cv::line(hist_image,
			         cv::Point(bin_w * (i), hist_h),
			         cv::Point(bin_w * (i), hist_h - cvRound(svalues[2])),
			         cv::Scalar(b, g, r), 1, 8, 0);
			b |= b_v == svalues[1] ? 255 : 0;
			g |= g_v == svalues[1] ? 255 : 0;
			r |= r_v == svalues[1] ? 255 : 0;
			cv::line(hist_image,
			         cv::Point(bin_w * (i), hist_h),
			         cv::Point(bin_w * (i), hist_h - cvRound(svalues[1])),
			         cv::Scalar(b, g, r), 1, 8, 0);
			cv::line(hist_image,
			         cv::Point(bin_w * (i), hist_h),
			         cv::Point(bin_w * (i), hist_h - cvRound(svalues[0])),
			         cv::Scalar(255, 255, 255), 1, 8, 0);
			break;
		}
	}
	return hist_image;
}

cv::Mat draw_histogram(cv::Mat& hist, int hist_size) {
	int hist_w = 512;
	int hist_h = 400;
	int bin_w = cvRound((double)hist_w / hist_size);
	cv::Mat hist_image(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));
	cv::normalize(hist, hist, 0, hist_image.rows, cv::NORM_MINMAX, -1,
	              cv::Mat());
	for (int i = 1; i < hist_size; i++) {
	  cv::line(
	      hist_image,
	      cv::Point(bin_w * (i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
	      cv::Point(bin_w * (i), hist_h - cvRound(hist.at<float>(i))),
	      cv::Scalar(255, 0, 0), 1, 8, 0);
	  cv::line(
	      hist_image,
	      cv::Point(bin_w * (i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
	      cv::Point(bin_w * (i), hist_h - cvRound(hist.at<float>(i))),
	      cv::Scalar(0, 255, 0), 1, 8, 0);
	  cv::line(
	      hist_image,
	      cv::Point(bin_w * (i - 1), hist_h - cvRound(hist.at<float>(i - 1))),
	      cv::Point(bin_w * (i), hist_h - cvRound(hist.at<float>(i))),
	      cv::Scalar(0, 0, 255), 1, 8, 0);
	}

	return hist_image;
}

void additive_circle(cv::Mat& io, const cv::Point& center, int radius, float addition, bool acting_agent/* = false*/) {
	(void)acting_agent;
	int nx = std::max(0, center.x - radius);
	int ny = std::max(0, center.y - radius);
	cv::Rect roi(nx, ny, std::min(2 * radius + 1, io.cols - 1 - center.x), std::min(2 * radius + 1, io.rows - 1 - center.y));
	cv::Mat wololo = io(roi); // ayoyoyo wololo
	int r_square = radius * radius;
	for (int row = 0; row < wololo.rows; ++row) {
		auto pt = wololo.ptr<uchar>(row);
		for (int col = 0; col < wololo.cols; ++col) {
			//auto current = *pt;
			int dx = radius - col;
			int dy = radius - row;
			if ((dx * dx + dy * dy) <= r_square) {
				*pt += addition;
				//*pt = *pt == 0 ? 2 : std::min(0xFF, static_cast<int>(*pt * addition + 0.5));
			}
			++pt;
		}
	}
	wololo.copyTo(io(roi));
}
}  // namespace drawing


#ifdef __linux__
static unsigned long long last_total_user, last_total_user_low, last_total_sys, last_total_idle;
static clock_t last_CPU, last_sys_CPU, last_user_CPU;
static int num_processors;
static bool init_total, init_current;

double get_cpu_used_total() {
	if (!init_total) {
		FILE* file = fopen("/proc/stat", "r");
		fscanf(file, "cpu %llu %llu %llu %llu", &last_total_user, &last_total_user_low,
		       &last_total_sys, &last_total_idle);
		fclose(file);
		init_total = true;
	}
	double percent;
	FILE* file;
	unsigned long long totalUser;
	unsigned long long totalUserLow;
	unsigned long long totalSys;
	unsigned long long totalIdle;
	unsigned long long total;

	file = fopen("/proc/stat", "r");
	fscanf(file, "cpu %llu %llu %llu %llu", &totalUser, &totalUserLow,
	       &totalSys, &totalIdle);
	fclose(file);

	if (totalUser < last_total_user || totalUserLow < last_total_user_low ||
	        totalSys < last_total_sys || totalIdle < last_total_idle){
		//Overflow detection. Just skip this value.
		percent = -1.0;
	}
	else{
		total = (totalUser - last_total_user) + (totalUserLow - last_total_user_low) +
		        (totalSys - last_total_sys);
		percent = static_cast<double>(total);
		total += (totalIdle - last_total_idle);
		percent /= static_cast<double>(total);
		percent *= 100;
	}

	last_total_user = totalUser;
	last_total_user_low = totalUserLow;
	last_total_sys = totalSys;
	last_total_idle = totalIdle;

	return percent;
}

double get_cpu_used_by_current() {
	if (!init_current) {
		FILE* filef;
		struct tms timeSample{};
		char line[128];

		last_CPU = times(&timeSample);
		last_sys_CPU = timeSample.tms_stime;
		last_user_CPU = timeSample.tms_utime;

		filef = fopen("/proc/cpuinfo", "r");
		num_processors = 0;
		while(fgets(line, 128, filef) != nullptr){
			if (strncmp(line, "processor", 9) == 0) num_processors++;
		}
		fclose(filef);
		init_current = true;
	}
	struct tms timeSample{};
	clock_t now;
	double percent;

	now = times(&timeSample);
	if (now <= last_CPU || timeSample.tms_stime < last_sys_CPU ||
	        timeSample.tms_utime < last_user_CPU){
		//Overflow detection. Just skip this value.
		percent = -1.0;
	}
	else{
		percent = static_cast<double>((timeSample.tms_stime - last_sys_CPU) +
		                              (timeSample.tms_utime - last_user_CPU));
		percent /= static_cast<double>(now - last_CPU);
		percent /= num_processors;
		percent *= 100;
	}
	last_CPU = now;
	last_sys_CPU = timeSample.tms_stime;
	last_user_CPU = timeSample.tms_utime;

	return percent;
}
#endif

#ifdef _WIN32
// this is not supported on windows
double get_cpu_used_total() {
	return -1.0;
}

double get_cpu_used_by_current() {
	return -1.0;
}
#endif


// DEFINITIONS OF VARIOUS CLASS METHODS

std::size_t point_hasher::operator()(const std::vector<pixel>& k) const {
	using std::hash;
	using std::size_t;

	return ((hash<uchar>()(k[0].x) ^ (hash<uchar>()(k[0].y) << 1)) >> 1) ^
	        (hash<uchar>()(k[0].z) << 1);
}

void a_ofn::name(const std::filesystem::path& value) {
	name_ = value;
}
const std::filesystem::path& a_ofn::path() const {
	return name_;
}

std::filesystem::path& a_ofn::path() {
	return name_;
}

#ifdef __linux__
bool open_file_name::open_file() {
	char* filename = osdialog_file(OSDIALOG_OPEN, "../data/", nullptr, nullptr);
	if (filename) {
		name(std::filesystem::path(filename));
		free(filename);
		return true;
	}
	return false;
}
#endif

#ifdef _WIN32
open_file_name::open_file_name(int index) {
	ZeroMemory(&ofn_, sizeof(ofn_));
	ofn_.lStructSize = sizeof(ofn_);
	ofn_.hwndOwner = hwnd_;
	ofn_.lpstrFile = sz_file_;

	ofn_.lpstrFile[0] = '\0';
	ofn_.nMaxFile = sizeof(sz_file_);
	ofn_.lpstrFilter =
	        "Video/Image"
	        "\0"
	        "*.AVI;*.MP4;*.MOV;*.PNG;*.BMP;*.JPEG;*.JPG;*.TIFF;*.TIF"
	        "\0"  // index 1
	        "Video (.avi, .mp4, .mov)"
	        "\0"
	        "*.AVI;*.MP4;*.MOV"
	        "\0"  // index 2
	        "Image (.png, .bmp, .jpeg, .jpg, .tiff, .tif)"
	        "\0"
	        "*.PNG;*.BMP;*.JPEG;*.JPG;*.TIFF;*.TIF"
	        "\0";  // index 3

	ofn_.nFilterIndex = index;
	ofn_.lpstrFileTitle = nullptr;
	ofn_.nMaxFileTitle = 0;
	ofn_.lpstrInitialDir = nullptr;
	ofn_.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
}

bool open_file_name::open_file() override {
	if (!GetOpenFileNameA(&ofn_)) {
		return false;
	}
	name_ = ofn_.lpstrFile;
	return true;
}

int open_file_name::get_filter_flag() const { return ofn_.nFilterIndex; }
#endif

namespace video_control {
	void skip_to(cv::VideoCapture& capture, int to_frame) {
		capture.set(cv::CAP_PROP_POS_FRAMES, to_frame);
	}
}

void show_im(const std::string& name, const cv::Mat& img,
                    const std::string& stdoutput/* = ""*/) {
	if (SHOW_IMAGES__ && !img.empty()) {
		cv::startWindowThread(); // cv::imshow() stops updating the window when called
		                         // from a non-main thread after some time, this prevents that
		cv::namedWindow(name);
		//}
		if (img.empty()) {
			std::cerr << "attempted to show empty image\n" << std::flush;
		}
		cv::imshow(name, img);
		cv::waitKey(1);
		if (!stdoutput.empty()) {
			std::cout << stdoutput << "\n" << std::flush;
		}
	}
}

int key_wait(int delay/* = 0*/) {
	if (SHOW_IMAGES__) {
		auto k = cv::waitKey(delay);
		return k;
	}
	return 0;
}

int dhash(cv::Mat& im, int hash_size /* = 8*/) {
	cv::Mat resz;
	cv::cvtColor(im, resz, cv::COLOR_BGR2GRAY);
	// im.convertTo()
	//	cv::Mat lur = cv::Mat::zeros(im.rows, im.cols, CV_8UC1);
	//	cv::applyColorMap(resz, lur, cv::COLORMAP_HSV);
	//	cv::show_im("dhash", lur);
	//	key_wait(1);
	cv::resize(resz, resz, cv::Size(hash_size + 1, hash_size));
	cv::Mat res(hash_size, hash_size, CV_8UC1);
	for (int row = 0; row < hash_size; ++row) {
		auto* p = resz.ptr<uchar>(row);
		auto* r = res.ptr<uchar>(row);
		for (int col = 0; col < hash_size; ++col) {
			if (col == 0) {
				++p;
			}
			*r = *(p - 1) < *p;
			++p;
			++r;
		}
	}
	return cv::sum(res)[0];
}

namespace image_modification {
	// TODO remove magic numbers here, have them as parameters
	void squish_value_channel(bool compare_on_saturation, const cv::Mat& hsv,
	                          int value_thresh_low,
	                          int value_thresh_low_onsat, int value_thresh_high,
	                          int gray_threshold, int precision,
	                          bool use_out_of_range_vals, uchar out_of_range_black, uchar out_of_range_white) {
		cv::Mat img;
		hsv.copyTo(img);
		img.forEach<cv::Point3_<uint8_t>>(
		            [&, value_thresh_low, value_thresh_low_onsat, value_thresh_high,
		            use_out_of_range_vals,
		            gray_threshold](cv::Point3_<uint8_t>& pix, const int* position) {
			(void)position;
			pix.x = decrease_precision(pix.x, precision, precision);
			bool black = false;
			if (compare_on_saturation) {
				if (pix.z < value_thresh_low_onsat && pix.y < gray_threshold) {
					pix.z = 1;
					pix.y = 254;
					pix.x = use_out_of_range_vals ? out_of_range_black : 0;
					black = true;
				}
			} else {
				if (pix.z < value_thresh_low && pix.y < gray_threshold) {
					pix.z = 1;
					pix.y = 254;
					pix.x = use_out_of_range_vals ? out_of_range_black : 0;
					black = true;
				}
			}
			if (!black) {
				if (pix.z > value_thresh_high && pix.y < gray_threshold) {
					pix.z = 254;
					pix.y = 1;
					pix.x = use_out_of_range_vals ? out_of_range_white : 0;
				}
			}
		});
		img.copyTo(hsv);
	}

	void squish_low_value_single_im(const cv::Mat& img, uchar value_thresh,
	                                bool use_out_of_range_vals, uchar out_of_range_black) {
		img.forEach<cv::Point3_<uint8_t>>(
		            [&](cv::Point3_<uint8_t>& pix,
		            const int* position) {
			(void)position;
			if (pix.z < value_thresh && pix.y < 60) {
				pix.z = 0;
				pix.y = 0;
				pix.x = use_out_of_range_vals ? out_of_range_black : 0;
			}
		});
	}

	void squish_low_value_split_im(matarr_3& mats, uchar value_thresh,
	                               int gray_thresh_sat, int gray_thresh_val,
	                               bool use_out_of_range_vals, uchar out_of_range_black) {
    #pragma omp parallel for
		for (int row = 0; row < mats[0].rows; ++row) {
			auto* hpt = mats[0].ptr<uchar>(row);
			auto* spt = mats[1].ptr<uchar>(row);
			auto* vpt = mats[2].ptr<uchar>(row);
			for (int col = 0; col < mats[0].cols; ++col) {
				if (*vpt < value_thresh || (*spt < gray_thresh_sat &&
				                            *vpt < gray_thresh_val)) {
					*vpt = 0;
					*spt = 0;
					*hpt = use_out_of_range_vals ? out_of_range_black : 0;
				}
				++hpt;
				++vpt;
				++spt;
			}
		}
	}

	void squish_low_value_many_im(std::vector<matarr_3>& mats, uchar value_thresh,
	                              int gray_thresh_sat, int gray_thresh_val,
	                              bool use_out_of_range_vals, uchar out_of_range_black) {
    #pragma omp parallel for
		for (size_t i = 0; i < mats.size(); ++i) {
			squish_low_value_split_im(mats[i], value_thresh, gray_thresh_sat,
			                          gray_thresh_val, use_out_of_range_vals, out_of_range_black);
		}
	}

	void squish_high_value_single_im(const cv::Mat& img, uchar value_thresh,
	                                 bool use_out_of_range_vals, uchar out_of_range_white) {
		img.forEach<cv::Point3_<uint8_t>>(
		            [&](cv::Point3_<uint8_t>& pix,
		            const int* position) {
			(void)position;
			if (pix.z > value_thresh && pix.y < 60) {
				pix.z = 255;
				pix.y = 0;
				pix.x = use_out_of_range_vals ? out_of_range_white : 0;
			}
		});
	}

	void squish_high_value_split_im(matarr_3& mats, uchar value_thresh,
	                                int gray_thresh_sat, int gray_thresh_val,
	                                bool use_out_of_range_vals, uchar out_of_range_white) {
		/*	img.forEach<pixel>(
									[value_thresh](pixel& pix, const int*
	position) { (void)position; if (pix.z > value_thresh && pix.y < 60) { pix.z =
	255; pix.y = 255; pix.x = 230;
	}
	});*/
    #pragma omp parallel for
		for (int row = 0; row < mats[0].rows; ++row) {
			auto* hpt = mats[0].ptr<uchar>(row);
			auto* spt = mats[1].ptr<uchar>(row);
			auto* vpt = mats[2].ptr<uchar>(row);
			for (int col = 0; col < mats[0].cols; ++col) {
				if (*vpt > value_thresh || (*spt < gray_thresh_sat &&
				                            *vpt > 255 - gray_thresh_val)) {
					*vpt = 255;
					*spt = 0;
					*hpt = use_out_of_range_vals ? out_of_range_white : 0;
				}
				++hpt;
				++vpt;
				++spt;
			}
		}
	}

	void squish_high_value_many_im(std::vector<matarr_3>& mats, uchar value_thresh,
	                               int gray_thresh_sat, int gray_thresh_val,
	                               bool use_out_of_range_vals, uchar out_of_range_white) {
    #pragma omp parallel for
		for (size_t i = 0; i < mats.size(); ++i) {
			squish_high_value_split_im(mats[i], value_thresh, gray_thresh_sat,
			                           gray_thresh_val, use_out_of_range_vals, out_of_range_white);
		}
	}

	void sharpen(cv::Mat& img) {
		cv::Mat kernel =
		        (cv::Mat_<float>(3, 3) << -1, -1, -1, -1, 9, -1, -1, -1, -1);
		cv::filter2D(img, img, -1, kernel);
	}

	void color_correction(const cv::Mat& hsv, uchar low_value_thresh, uchar high_value_thresh, bool sharpen, bool use_out_of_range_vals, uchar out_of_range_black, uchar out_of_range_white) {
		cv::Mat cc;
		hsv.copyTo(cc);
		if (sharpen) {
			image_modification::sharpen(cc);
		}
		squish_low_value_single_im(cc, low_value_thresh, use_out_of_range_vals, out_of_range_black);
		squish_high_value_single_im(cc, high_value_thresh, use_out_of_range_vals, out_of_range_white);

		shift_hue_cv_mat(cc, use_out_of_range_vals, out_of_range_black);
		cc.copyTo(hsv);
	}

	void shift_hue_cv_mat(const cv::Mat& img, bool use_out_of_range_vals, uchar out_of_range_black) {
		img.forEach<cv::Point3_<uint8_t>>(
		            [&, use_out_of_range_vals](cv::Point3_<uint8_t>& pix,
		            const int* position) {
			(void)position;
			if (pix.y > 100 && pix.z > 125) {
				pix.x = decrease_precision(pix.x, 0);
			}
			if (pix.y < 25) {
				pix.y = 0;
				if (pix.z < 120) {
					pix.x = use_out_of_range_vals ? out_of_range_black : 0;
					pix.z = 0;
				}
			}
		});
	}

	void shift_hue_split(matarr_3& mats, bool use_out_of_range_vals, uchar out_of_range_black) {
		cv::Mat merged;
		merge(mats.data(), 3, merged);
		merged.forEach<cv::Point3_<uint8_t>>(
		            [&, use_out_of_range_vals](cv::Point3_<uint8_t>& pix,
		            const int* position) {
			(void)position;
			uint8_t h = pix.x;
			uint8_t s = pix.y;
			uint8_t v = pix.z;
			if ((h > 160 && h <= 180) || h < 8) {
				pix.x = 0;
			}
			if (h <= 30 && h > 20 && s > 100 && v > 80) {
				pix.x = 25;
			}
			if (h <= 120 && h > 100 && s > 100 && v > 70) {
				pix.x = 110;
			}
			if (s < 25) {
				pix.y = 0;
				if (v < 120) {
					pix.x = use_out_of_range_vals ? out_of_range_black : 0;
					pix.z = 0;
				}
			}
		}
		);
		split(merged, mats);
		return;

    #pragma omp parallel for
		for (int row = 0; row < mats[0].rows; ++row) {
			auto* hpt = mats[0].ptr<uchar>(row);
			auto* spt = mats[1].ptr<uchar>(row);
			auto* vpt = mats[2].ptr<uchar>(row);
			uchar hue = *hpt;
			uchar sat = *spt;
			uchar val = *vpt;
			for (int col = 0; col < mats[0].cols; ++col) {
				if ((hue > 160 && hue <= 180) || hue < 8) {
					*hpt = 0;
					// if (*vpt > 75 && *spt >= GRAY_THRESHOLD) {
					//    *hpt = 180;
					//}
				}
				if (hue <= 30 && hue > 20 && sat > 100 && val > 80) {
					*hpt = 25;
				}
				if (hue <= 120 && hue > 100 && sat > 100 && val > 70) {
					*hpt = 110;
				}
				if (sat < 25) {
					*spt = 0;
					if (val < 120) {
						*hpt = use_out_of_range_vals ? out_of_range_black : 0;
						*vpt = 0;
					}
				}
				++hpt;
				++vpt;
				++spt;
			}
		}
	}

	void shift_hue_vsplit(std::vector<matarr_3>& mats, bool use_out_of_range_vals, uchar out_of_range_black) {
    #pragma omp parallel for
		for (size_t i = 0; i < mats.size(); ++i) {
			shift_hue_split(mats[i], use_out_of_range_vals, out_of_range_black);
		}
	}
}

namespace custom_cv {
	using namespace cv;
	const int draw_shift_bits = 4;
	const int draw_multiplier = 1 << draw_shift_bits;

	static void _draw_keypoint(InputOutputArray img, const KeyPoint& p, const Scalar& color, DrawMatchesFlags flags) {
		CV_Assert(!img.empty());
		Point center(cvRound(p.pt.x * custom_cv::draw_multiplier), cvRound(p.pt.y * custom_cv::draw_multiplier));

		if (!!(flags & DrawMatchesFlags::DRAW_RICH_KEYPOINTS)) {
			int radius = cvRound(p.size/2 * custom_cv::draw_multiplier); // KeyPoint::size is a diameter

			// draw the circles around keypoints with the keypoints size
			circle(img, center, radius, color, 1, LINE_AA, custom_cv::draw_shift_bits);

			// draw orientation of the keypoint, if it is applicable
			if (p.angle != -1) {
				float srcAngleRad = p.angle * (float)CV_PI/180.f;
				Point orient(cvRound(cos(srcAngleRad)*radius),
				             cvRound(sin(srcAngleRad)*radius));
				line(img, center, center + orient, color, 1, LINE_AA, custom_cv::draw_shift_bits);
			}
		} else {
			// draw center with R=3
			int radius = 3 * custom_cv::draw_multiplier;
			circle(img, center, radius, color, 1, LINE_AA, custom_cv::draw_shift_bits);
		}
	}

	static void _prepare_image(InputArray src, const Mat& dst) {
		CV_CheckType(src.type(), src.type() == CV_8UC1 || src.type() == CV_8UC3 || src.type() == CV_8UC4, "Unsupported source image");
		CV_CheckType(dst.type(), dst.type() == CV_8UC3 || dst.type() == CV_8UC4, "Unsupported destination image");
		const int src_cn = src.channels();
		const int dst_cn = dst.channels();

		if (src_cn == dst_cn)
			src.copyTo(dst);
		else if (src_cn == 1)
			cvtColor(src, dst, dst_cn == 3 ? COLOR_GRAY2BGR : COLOR_GRAY2BGRA);
		else if (src_cn == 3 && dst_cn == 4)
			cvtColor(src, dst, COLOR_BGR2BGRA);
		else if (src_cn == 4 && dst_cn == 3)
			cvtColor(src, dst, COLOR_BGRA2BGR);
		else
			CV_Error(Error::StsInternal, "");
	}

	static void _prepare_img_and_draw_keypoints(InputArray img1, const std::vector<KeyPoint>& keypoints1,
	                                            InputArray img2, const std::vector<KeyPoint>& keypoints2,
	                                            InputOutputArray _outImg, Mat& outImg1, Mat& outImg2,
	                                            const Scalar& singlePointColor, DrawMatchesFlags flags) {
		Mat outImg;
		Size img1size = img1.size(), img2size = img2.size();
		Size size(MAX(img1size.width, img2size.width), img1size.height + img2size.height);
		if( !!(flags & DrawMatchesFlags::DRAW_OVER_OUTIMG) ) {
			outImg = _outImg.getMat();
			if( size.width > outImg.cols || size.height > outImg.rows )
				CV_Error( Error::StsBadSize, "outImg has size less than need to draw img1 and img2 together" );
			outImg1 = outImg( Rect(0, 0, img1size.width, img1size.height) );
			outImg2 = outImg( Rect(0, img1size.height, img2size.width, img2size.height) );
		} else {
			const int cn1 = img1.channels(), cn2 = img2.channels();
			const int out_cn = std::max(3, std::max(cn1, cn2));
			_outImg.create(size, CV_MAKETYPE(img1.depth(), out_cn));
			outImg = _outImg.getMat();
			outImg = Scalar::all(0);
			outImg1 = outImg( Rect(0, 0, img1size.width, img1size.height) );
			outImg2 = outImg( Rect(0, img1size.height, img2size.width, img2size.height) );

			_prepare_image(img1, outImg1);
			_prepare_image(img2, outImg2);
		}

		// draw keypoints
		if( !(flags & DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS) ) {
			Mat _outImg1 = outImg( Rect(0, 0, img1size.width, img1size.height) );
			drawKeypoints( _outImg1, keypoints1, _outImg1, singlePointColor, flags | DrawMatchesFlags::DRAW_OVER_OUTIMG );

			Mat _outImg2 = outImg( Rect(0, img1size.height, img2size.width, img2size.height) );
			drawKeypoints( _outImg2, keypoints2, _outImg2, singlePointColor, flags | DrawMatchesFlags::DRAW_OVER_OUTIMG );
		}
	}

	static void _draw_match(InputOutputArray outImg, InputOutputArray outImg1, InputOutputArray outImg2 ,
	                       const KeyPoint& kp1, const KeyPoint& kp2, const Scalar& matchColor, DrawMatchesFlags flags,
	                       const int matchesThickness) {
		RNG& rng = theRNG();
		bool isRandMatchColor = matchColor == Scalar::all(-1);
		Scalar color = isRandMatchColor ? Scalar( rng(256), rng(256), rng(256), 255 ) : matchColor;

		_draw_keypoint( outImg1, kp1, color, flags );
		_draw_keypoint( outImg2, kp2, color, flags );

		Point2f pt1 = kp1.pt,
		        pt2 = kp2.pt,
		        //dpt2 = Point2f( std::min(pt2.x+outImg1.size().width, float(outImg.size().width-1)), pt2.y );
		        dpt2 = Point2f( pt2.x, std::min(pt2.y+outImg1.size().height, float(outImg.size().height-1)) );

		line( outImg,
		      Point(cvRound(pt1.x*custom_cv::draw_multiplier), cvRound(pt1.y*custom_cv::draw_multiplier)),
		      Point(cvRound(dpt2.x*custom_cv::draw_multiplier), cvRound(dpt2.y*custom_cv::draw_multiplier)),
		      color, matchesThickness, LINE_AA, custom_cv::draw_shift_bits );
	}


	static void draw_matches( InputArray img1, const std::vector<KeyPoint>& keypoints1,
	                  InputArray img2, const std::vector<KeyPoint>& keypoints2,
	                  const std::vector<DMatch>& matches1to2, InputOutputArray outImg,
	                  const int matchesThickness, const Scalar& matchColor,
	                  const Scalar& singlePointColor, const std::vector<char>& matchesMask,
	                  DrawMatchesFlags flags )
	{
		if( !matchesMask.empty() && matchesMask.size() != matches1to2.size() )
			CV_Error( Error::StsBadSize, "matchesMask must have the same size as matches1to2" );

		Mat outImg1, outImg2;
		custom_cv::_prepare_img_and_draw_keypoints( img1, keypoints1, img2, keypoints2,
		                             outImg, outImg1, outImg2, singlePointColor, flags );

		// draw matches
		for( size_t m = 0; m < matches1to2.size(); m++ )
		{
			if( matchesMask.empty() || matchesMask[m] )
			{
				int i1 = matches1to2[m].queryIdx;
				int i2 = matches1to2[m].trainIdx;
				CV_Assert(i1 >= 0 && i1 < static_cast<int>(keypoints1.size()));
				CV_Assert(i2 >= 0 && i2 < static_cast<int>(keypoints2.size()));

				const KeyPoint &kp1 = keypoints1[i1], &kp2 = keypoints2[i2];
				custom_cv::_draw_match( outImg, outImg1, outImg2, kp1, kp2, matchColor, flags, matchesThickness );
			}
		}
	}

	void draw_matches(InputArray img1, const std::vector<KeyPoint>& keypoints1,
	                        InputArray img2, const std::vector<KeyPoint>& keypoints2,
	                        const std::vector<DMatch>& matches1to2, InputOutputArray outImg,
	                        const Scalar& matchColor, const Scalar& singlePointColor,
	                        const std::vector<char>& matchesMask, DrawMatchesFlags flags ) {
		custom_cv::draw_matches(img1, keypoints1,
		                       img2, keypoints2,
		                       matches1to2, outImg,
		                       1, matchColor,
		                       singlePointColor, matchesMask,
		                       flags);
	}
} // namespace custom_cv
