TEMPLATE = app
TARGET = perimeter_detector
CONFIG += c++17
CONFIG -= app_bundle
CONFIG -= qt

# project build directories
#DESTDIR     = $$system(pwd)
#OBJECTS_DIR = $$DESTDIR/obj
# C++ flags

QMAKE_CXXFLAGS += -std=c++17 -fopenmp=libomp -std=c++17 -Wno-unknown-pragmas

QMAKE_CXXFLAGS_RELEASE += -Ofast
QMAKE_CXXFLAGS_DEBUG += -g

QMAKE_CC = /usr/bin/clang-8
QMAKE_CXX = /usr/bin/clang++-8

SOURCES += \
    external/clipper/clipper.cpp \
    external/lsd.cpp \
    external/osdialog/osdialog.c \
    external/osdialog/osdialog_gtk2.c \
    external/osdialog/osdialog_gtk3.c \
    external/xlsreader/XlsReader.cpp \
    feature_analyzer.cpp \
    main.cpp \
    scalespace_analyzer.cpp \
    tools/advertisement.cpp \
    tools/formatting.cpp \
    tools/gaussian_iir.cpp \
    tools/gui.cpp \
    tools/rope_stringbuilder.cpp \
    tools/terminal_color_output.cpp \
    tools/xls_loader.cpp \
    utility.cpp

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += gtk+-3.0
unix: PKGCONFIG += opencv4

QMAKE_LFLAGS += -fopenmp
LIBS += -fopenmp -lxlsreader -lstdc++fs -lopencv_calib3d -lX11

INCLUDEPATH += /usr/local/lib/ \
    openxlsx/library/ \
    external/ \
    external/clipper/ \
    external/cv-plot/CvPlot/inc \
    tools/

HEADERS += \
    config.h \
    external/clipper/clipper.hpp \
    external/cvui.h \
    external/lsd.h \
    external/motion_deblur.h \
    external/osdialog/osdialog.h \
    external/xlsreader/XlsReader.h \
    feature_analyzer.h \
    scalespace_analyzer.h \
    tools/ad_sequencer.h \
    tools/advertisement.h \
    tools/formatting.h \
    tools/gaussian_iir.h \
    tools/gui.h \
    tools/paths.h \
    tools/rope_stringbuilder.h \
    tools/terminal_color_output.h \
    tools/video_player.h \
    tools//xls_loader.h \
    utility.h \
