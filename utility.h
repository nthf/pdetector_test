﻿#pragma once

#include "clipper.hpp"
#include "config.h"
#include "gaussian_iir.h"
#include "paths.h"
#include "rope_stringbuilder.h"

#include <numeric>
#include <opencv2/opencv.hpp>
#include <thread>

#define CVPLOT_HEADER_ONLY

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#include <CvPlot/cvplot.h>
#pragma GCC diagnostic pop
#pragma GCC diagnostic pop

/*
 * This header provides basic utility operations
 * some functions may be unused, outdated
 */

/*
 * cv::Mat types (used when accessing the data with cv::Mat::at)
 *
 * CV_8U  -- uint8_t,  uchar
 * CV_8S  -- int8_t,   schar
 * CV_16U -- uint16_t, ushort
 * CV_16S -- int16_t,  short
 * CV_32S -- int32_t,  int
 * CV_32F -- float
 * CV_64F -- double
*/

/// @brief pi rounded to 5 decimal places
constexpr double PI_5 = 3.14159;

using pixel = cv::Point3_<uint8_t>;

/// @brief random colors to pick from if needed
static const std::vector<cv::Scalar> rand_colormap = {
    { 0xff, 0xb3, 0xf9 }, { 0xff, 0xb0, 0x3f }, { 0xd3, 0xd9, 0x19 },
    { 0x6a, 0xd8, 0xad }, { 0x51, 0x36, 0x31 }, { 0x95, 0xac, 0x94 },
    { 0x7d, 0x38, 0xa3 }, { 0xd2, 0xfa, 0x61 }, { 0xb4, 0x84, 0x80 },
    { 0x8d, 0x02, 0x10 }, { 0x30, 0x27, 0xa5 }, { 0x1d, 0xba, 0xbf },
    { 0x8d, 0xf7, 0x4a }, { 0x8b, 0xdf, 0x99 }, { 0x6b, 0xc3, 0x80 },
    { 0xc5, 0x87, 0x36 }, { 0xe2, 0x21, 0x92 }, { 0x16, 0x85, 0xec },
    { 0x1c, 0x4b, 0x14 }, { 0x89, 0xad, 0x79 }, { 0x84, 0x88, 0x03 },
    { 0x81, 0x4b, 0x6e }, { 0xfa, 0x92, 0xec }, { 0xfd, 0x17, 0xe6 }
};

/**
 * @brief functor for hashing a vector of points of 3 values
 * @see cv::Point3_
 */
class point_hasher {
public:
	/**
	 * @brief hashing operator
	 * @param k vector of points of 3 values
	 * @return  hash value of the first point
	 * @todo not incorrect but could look into taking other points into account
	 */
	std::size_t operator()(const std::vector<pixel>& k) const;
};

/**
 * @brief provides a templated range structure
 * @tparam T numeric type
 */
template <typename T>
class range {
private:
	/// @brief begin_ start of range
	T begin_;
	/// @brief end_ end of range
	T end_;
	/// @brief step_ step between each value of range
	T step_;

public:
	/**
	 * @brief iterator class for the range
	 */
	class iterator {
		/// @brief value of iterator
		T i_;
		/// @brief istep_ step between each value of range
		const T istep_;
		friend class range;
	public:
		/**
		 * @brief dereference to get value held at iterator
		 * @return value at iterator
		 */
		T operator*() const { return i_; }
		/**
		 * @brief prefix increment, moves value by step towards end
		 * @return this iterator
		 */
		const iterator& operator++() {
			i_ += istep_;
			return *this;
		}
		/**
		 * @brief postfix increment, copies this iterator and moves value by step towards end
		 * @return copy of this iterator
		 */
		iterator operator++(int) {
			iterator copy(*this);
			i_ += istep_;
			return copy;
		}

		/**
		 * @brief equality comparison between two iterators
		 * @param other iterator to compare this to
		 * @return true if iterators are the same
		 */
		bool operator==(const iterator& other) const { return !operator!=(other); }
		/**
		 * @brief inequality comparison between two iterators
		 * @param other iterator to compare this to
		 * @return true if iterators are different
		 */
		bool operator!=(const iterator& other) const {
			return i_ < 0 ? i_ > other.i_ : i_ < other.i_;
		}

		/// @brief conversion to type T by giving iterator's value
		operator T() const { return i_; }

		/// @brief conversion to type const T by giving iterator's value
		operator T() { return i_; }

		/**
		 * @brief overload for output to stream
		 * @param stream stream to output to
		 * @param it iterator to output to stream
		 * @return stream
		 */
		friend std::ostream& operator<<(std::ostream& stream, const iterator& it) {
			if (std::is_same<T, char>::value || std::is_same<T, unsigned char>::value) {
				stream << static_cast<int>(it.i_);
			} else {
				stream << it.i_;
			}
			return stream;
		}
	protected:
		/**
		 * @brief constructor, sets the first value and the step to get to following values
		 * @param start initial value
		 * @param step  step between each value of range
		 */
		iterator(T start, T step) : i_(start), istep_(step) {}
	};

	/**
	 * @brief gives the first value of range
	 * @return first value of range
	 */
	[[nodiscard]] iterator begin() const { return iterator(begin_, step_); }

	/**
	 * @brief gives the last value of range
	 * @return last value of range
	 */
	[[nodiscard]] iterator end() const { return iterator(end_, step_); }
	/**
	 * @brief constructor of range
	 * @param begin start value of range
	 * @param end   end value of range
	 * @param step  step between each value between begin(including) and end(excluding)
	 */
	range(T begin, T end, T step = 1) : begin_(begin), end_(end), step_(step) {
		if ((step_ == 0) || (!(begin_ >= 0 && end_ > 0 && step_ > 0) &&
		                     !(begin_ <= 0 && end_ < 0 && step_ < 0))) {
			throw std::invalid_argument("invalid range initialization");
		}
	}

	/**
	 * @brief constructor overload with only the end specified, range starts at 0
	 * @param end end value of range
	 */
	range(T end) : range(0, end) {}

	/**
	 * @brief checks whether a number is within this range
	 * @param n number to check
	 * @return  true if the beginning of the range is lesser or equal to n and end is greater than n
	 */
	bool in_range(T n) { return begin_ <= n && n < end_; }

	bool static test(T n, T l, T h) { return l <= n && n < h; }

	/**
	 * @brief checks whether any number in a given vector is within this range
	 * @param v vector to check
	 * @return  true if any number within vector v is within given range
	 */
	bool any_in_range(const std::vector<T>& v) {
		for (const auto& i : v) {
			if (in_range(i)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @brief conversion to std::vector
	 */
	operator std::vector<T>() {
		std::vector<T> ret;
		for (T i = begin_; i < end_; i += step_) {
			ret.push_back(i);
		}
		return ret;
	}
};

template<typename T>
class clamper {
public:
	clamper() = delete;
	inline static int directional_clamp(T min, T max, T value, int& direction) {
		if (value < min) {
			direction = -1;
			return min;
		}
		if (value >= max) {
			direction = 1;
			return max - 1;
		}
		return value;
	}
};

using map_vuc_i = std::map<std::vector<uchar>, int>;
using vpair_vuc_i = std::vector<std::pair<std::vector<uchar>, int>>;
using vmat = std::vector<cv::Mat>;
using matarr_3 = std::array<cv::Mat, 3>;

struct mouse_s;

template <typename T = size_t>
class range;

/**
 * @brief modulo operation
 * @param a dividend
 * @param b divisor
 * @return  positive remainder of a divided by b
 */
int mod(int a, int b);

template<typename T>
T min_1(T a) {
	return std::max(static_cast<T>(1), a);
}

/**
 * @brief abstract class for file opening
 */
class a_ofn {
private:
	/// @brief path to chosen file
	std::filesystem::path name_;
protected:
	/**
	 * @brief name setter
	 * @param value new value for name
	 */
	void name(const std::filesystem::path& value);
public:
	/**
	 * @brief const file path getter
	 * @return path to chosen file
	 */
	[[nodiscard]] virtual const std::filesystem::path& path() const;

	/**
	 * @brief file path getter
	 * @return path to chosen file
	 */
	[[nodiscard]] virtual std::filesystem::path& path();
	/**
	 * @brief method for selecting files in the file system
	 * @return true on successful file navigation
	 */
	virtual bool open_file() = 0;
};

#ifdef __linux__
/**
 * @brief class to handle opening files on linux
 */
class open_file_name : public a_ofn {
public:
	open_file_name() = default;

	/**
	 * @brief opens a dialog to choose file
	 * @return true if file choice was successful
	 */
	bool open_file() override;
};
#endif

#ifdef _WIN32
/**
 * @brief container for opening a file through windows common dialog box
 */
class open_file_name : public i_ofn {
	OPENFILENAME ofn_;          // common dialog box structure
	char sz_file_[260] = "\0";  // buffer for file name
	HWND hwnd_ = nullptr;       // owner window
	HANDLE hf_ = nullptr;       // file handle
public:
	open_file_name(int index);
	bool open_file() override;
	int get_filter_flag() const;
};
#endif

enum area_shape {
	AREA_CIRCLE,
	AREA_RECTANGLE,
	AREA_ELLIPSE
};

enum class morph_ops {
	CLOSE,
	OPEN,
	CLOSE_OPEN,
	OPEN_CLOSE,
	ERODE,
	DILATE
};

/**
 * @brief contains various functions that perform useful operations that can be used in multiple
 *        contexts such as mathematical operations and common operations on matrices
 */
namespace utility {
	static constexpr double DEG_MUL = 180 / M_PI;
	static constexpr double RAD_MUL = M_PI / 180;
	/**
	 * @brief converts radians to degrees
	 * @param angle angle in radians
	 * @return      angle in degrees
	 */
	double rad_to_deg(double angle);

	/**
	 * @brief converts degrees to radians
	 * @param angle angle in degrees
	 * @return      angle in radians
	 */
	double deg_to_rad(double angle);


	double vector_angle(double x, double y);
	double vector_magnitude(double x, double y);

	/**
	 * @brief calculates the distance between point A and B
	 * @param A point A
	 * @param B point B
	 * @return distance between point A and B rounded down
	 */
	int distance(const cv::Point& A, const cv::Point& B);

	template<typename T>
	cv::Point_<T> sum_pts(const std::initializer_list<cv::Point_<T>>& pt_list) {
		cv::Point_<T> ret;
		for (const auto& p : pt_list) {
			ret.x += p.x;
			ret.y += p.y;
		}
		return ret;
	}

	template<typename T, typename U>
	T sum_pts(const U& pt_list) {
		T ret;
		for (const auto& p : pt_list) {
			ret.x += p.x;
			ret.y += p.y;
		}
		return ret;
	}

	/// distance from point p to line
	double point_to_line_distance(const cv::Point& p, const cv::Vec4i& line);
	/// distance from point p to line segment
	double point_to_ls_distance(const cv::Point& p, const cv::Vec4i& line);

	/**
	 * @brief gives shortest distance between two rectangles
	 * @param r0 first rectangle
	 * @param r1 second rectangle
	 * @return   shortest distance between the edges of two rectangles
	 */
	double rect_distance(const cv::Rect& r0, const cv::Rect& r1);

	bool lines_intersect(const cv::Point& p1, const cv::Point& q1, const cv::Point& p2, const cv::Point& q2);

	template<typename T>
	cv::Vec2d normalize(cv::Vec<T, 2> v) {
		double m = sqrt(v(0) * v(0) + v(1) * v(1));
		cv::Vec2d rv(v(0) / m, v(1) / m);
		return rv;
	}

	template<typename T>
	double crossm(const cv::Vec<T, 2>& v1, const cv::Vec<T, 2>& v2) {
		double z = v1(0) * v2(1) - v1(1) * v2(0);
		return z * z;
	}

	/**
	 * @brief sum
	 * @tparam T primitive type of point values
	 * @param pt point with 3 dimensions
	 * @return   sum of point elements
	 */
	template<typename T>
	inline double sum(const cv::Point3_<T>& pt) {
		return pt.x + pt.y + pt.z;
	}

	/**
	 * @brief determines variance in a vector of values
	 * @tparam T  primitive type of values in given vector
	 * @param vec vector to calculate variance of
	 * @return    variance of vector vec
	 */
	template<typename T, typename U>
	T variance(const std::vector<U> &vec) {
		size_t sz = vec.size();
		if (sz == 1) return 0.0;
		T mean = std::accumulate(vec.begin(), vec.end(), 0.0) / sz;
		auto variance_func = [&mean, &sz](T accumulator, const U& val) {
			return accumulator + ((val - mean) * (val - mean) / (sz - 1));
		};

		return std::accumulate(vec.begin(), vec.end(), 0.0, variance_func);
	}

	/**
	 * @brief min3 gets the lowest value out of 3 given
	 * @tparam T type of values
	 * @param a  first value
	 * @param b  second value
	 * @param c  third value
	 * @return   lowest value
	 */
	template<typename T>
	T min3(const T& a, const T& b, const T& c) {
		return (a < b) ? (a < c ? a : c) : ((b < c) ? b : c);
	}

	/**
	 * @brief max3 gets the highest value out of 3 given
	 * @tparam T type of values
	 * @param a  first value
	 * @param b  second value
	 * @param c  third value
	 * @return   highest value
	 */
	template<typename T>
	T max3(const T& a, const T& b, const T& c) {
		return (a > b) ? (a > c ? a : c) : ((b > c) ? b : c);
	}

	/**
	 * @brief dispersion calculates the dispersion and variance for given set of points
	 * @tparam T       type of values
	 * @param pts      points to get dispersion of
	 * @param variance variable to save variance to
	 * @return         dispersion of x, y and x+y coordinates as a point
	 */
	template<typename pts_t, typename var_t>
	cv::Point3i dispersion(const std::vector<cv::Point_<pts_t>>& pts, cv::Point3_<var_t>& variance) {
		if (pts.empty()) {
			variance = { 0, 0, 0 };
			return { -1, -1, -1 };
		}
		std::vector<pts_t> vy;
		std::vector<pts_t> vx;
		std::vector<pts_t> vz;
		std::transform(pts.begin(), pts.end(), std::back_inserter(vy), [](const cv::Point_<pts_t>& pt) { return pt.y; });
		std::transform(pts.begin(), pts.end(), std::back_inserter(vx), [](const cv::Point_<pts_t>& pt) { return pt.x; });
		std::transform(pts.begin(), pts.end(), std::back_inserter(vz), [](const cv::Point_<pts_t>& pt) { return pt.x + pt.y; });
		var_t var_x = utility::variance<pts_t>(vx);
		var_t var_y = utility::variance<pts_t>(vy);
		var_t var_z = utility::variance<pts_t>(vz);
		variance = { var_x, var_y, var_z };
		size_t n = pts.size();
		//double f = 1.0 / static_cast<double>(n);
		int avg_x = 0;
		int avg_y = 0;
		for (const auto& p : pts) {
			avg_x += p.x;
			avg_y += p.y;
		}
		avg_x /= n;
		avg_y /= n;
		cv::Point central_p(avg_x, avg_y);
		double sumx = 0.0;
		double sumy = 0.0;
		double sum = 0.0;
		for (const auto& p : pts) {
			sum += sqrt((p.x - central_p.x) * (p.x - central_p.x) + (p.y - central_p.y) * (p.y - central_p.y));
			sumx += sqrt((p.x - central_p.x) * (p.x - central_p.x));
			sumy += sqrt((p.y - central_p.y) * (p.y - central_p.y));
		}
		return { static_cast<int>(sumx / n), static_cast<int>(sumy / n), static_cast<int>(sum / n)};
	}

	/**
	 * @brief clumps points together into a smaller area towards the centroid
	 * @param pts       point to clump together
	 * @param dx        factor to clump by in the x direction
	 * @param dy        factor to clump by in the y direction
	 * @param w         width of image where points originate from
	 * @param from      beginning of an interval of x coordinates
	 * @param to        end of an interval of x coordinates
	 * @param points    visualization of points, "?", "_", "<"
	 * @param points_d  visualization of clumped points
	 */
	std::vector<cv::Point> densify(std::vector<cv::Point>& pts, double dx, double dy, int w, double from, double to);

	/**
	 * @brief prints a progress bar into standard output
	 * @param barwidth width of progress bar in chars
	 * @param count    number of iterations needed to reach 100%
	 * @param position current position of the bar end
	 * @param progress percentage of how much the bar is completed
	 * @param color    flag for colorful terminal output
	 */
	void progress_bar(int barwidth, int count, int* position, float* progress);

	/**
	 * @brief concise file opening function
	 * @param path    chosen path is saved here
	 * @return        status code: 0 - success, 1 - dialog closed, 2 - chosen path empty/invalid
	 */
	int open_file(std::string& path);
	/**
	 * @brief adds a border to an image
	 * @param img     input image
	 * @param out     output image
	 * @param padding amount of padding to add to each side of the img image
	 */
	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding);
	/**
	 * @brief adds a border to an image
	 * @param img       input image
	 * @param out       output image
	 * @param padding0  padding added to west
	 * @param padding1  padding added to north
	 * @param padding2  padding added to east
	 * @param padding3  padding added to south
	 */
	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding0,
	               const int padding1, const int padding2, const int padding3);
	/**
	 * @brief adds a border to an image
	 * @param img     input image
	 * @param out     output image
	 * @param padding amount of padding to add to each side of the img image
	 * @param color   specifies the color that the new border should have
	 */
	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding,
	               const cv::Scalar& color);
	void pad_image(const cv::Mat& img, cv::Mat& out, const int padding0,
	               const int padding1, const int padding2, const int padding3,
	               const cv::Scalar& color);
	/**
	 * @brief removes a border from an image
	 * @param img     input image
	 * @param out     output image
	 * @param padding amount of padding to remove from each side of the img image
	 */
	void depad_image(const cv::Mat& img, cv::Mat& out, int padding, const cv::Mat& mask = cv::Mat());

	inline void overlay_image_3c(const cv::Mat& img1, const cv::Mat& img2, cv::Mat& dest) {
		img1.copyTo(dest);
		for (int row = 0; row < img2.rows; ++row) {
			auto* src = img2.ptr<pixel>(row);
			auto* dst = dest.ptr<pixel>(row);
			for (int col = 0; col < img2.cols; ++col) {
				if (src->x > 0 || src->y > 0 || src->z > 0) {
					dst->x = src->x;
					dst->y = src->y;
					dst->z = src->z;
				}
				++src;
				++dst;
			}
		}
	}

	template<typename T>
	inline void overlay_image_1c(const cv::Mat& img1, const cv::Mat& img2, cv::Mat& dest, int threshold) {
		img1.copyTo(dest);
		for (int row = 0; row < img2.rows; ++row) {
			auto* src = img2.ptr<T>(row);
			auto* dst = dest.ptr<T>(row);
			for (int col = 0; col < img2.cols; ++col) {
				if (*src > threshold) {
					*dst = *src;
				}
				++src;
				++dst;
			}
		}
	}

	int count_in_area(const cv::Mat& img, const cv::Point& point, area_shape shape, int dx, int dy = 0);

	/**
	 * @brief checks whether img contains specified hue
	 * @param img   image to check
	 * @param hue   hue to check for
	 * @param ratio how much of the image is of specified hue
	 * @return true if img contains hue
	 */
	bool contains_hue(const cv::Mat& img, uchar hue, double* ratio = nullptr);

	/**
	 * @brief rotates the image at a given angle without cutting away parts of the original image,
	 *        filling in empty areas with zero value pixels
	 * @param input  input image
	 * @param output output image
	 * @param angle  angle of rotation in degrees (positive = counter-clockwise)
	 */
	void rotate(const cv::Mat& input, cv::Mat& output, double angle);

	/**
	 * @brief finds the smallest axis-aligned bounding box around non-zero contents of image and
	 *        cuts away parts of the image that are outside the box
	 * @param input  input image
	 * @param output output image
	 */
	void trim(const cv::Mat& input, cv::Mat& output);

	/**
	 * @brief rotates the image and cuts away large areas of zero pixels
	 * @see rotate(const cv::Mat& input, cv::Mat& output, double angle)
	 * @see trim(const cv::Mat& input, cv::Mat& output)
	 * @param input input image
	 * @param angle angle to rotate at
	 * @return output image
	 */
	cv::Mat rotate_and_trim(const cv::Mat& input, double angle);

	/**
	 * @brief checks if image is blurry according to the variance of its laplacian
	 * @param input input image
	 * @param threshold
	 * @return true if variance is below threshold
	 */
	bool img_is_blurry(const cv::Mat& input, int threshold);

	/**
	 * @brief all_true  check if all values in container are true
	 * @tparam T        container class with defined begin() and end() containing booleans
	 * @param container container to check
	 * @return          true if all values are true
	 */
	template<typename collection, typename value_t = typename collection::value_type>
	bool all_same(const collection& container) {
		if (container.size() <= 1) {
			return true;
		}
		const value_t* previous = &container.front();
		for (size_t i = 1; i < container.size(); ++i) {
			if (container[i] != *previous) {
				return false;
			}
			previous = &container[i];
		}
		return true;
	}

	/**
	 * @brief all_true  check if all values in container are true
	 * @tparam T        container class with defined begin() and end() containing booleans
	 * @param container container to check
	 * @param max_difference the maximum allowed difference between values for them to be considered same
	 * @return          true if all values are true
	 */
	template<typename collection, typename value_t = typename collection::value_type>
	bool all_same(const collection& container, value_t max_difference) {
		if (container.size() <= 1) {
			return true;
		}
		value_t previous = container.front();
		for (size_t i = 1; i < container.size(); ++i) {
			if (std::abs(container[i] - previous) >= max_difference) {
				return false;
			}
			previous = container[i];
		}
		return true;
	}

	template<typename collection, typename value_t = typename collection::value_type>
	std::vector<int> get_difference_vector(const collection& container) {
		std::vector<int> diff_v;
		for (size_t i = 1; i < container.size(); ++i) {
			diff_v.push_back(container[i] - container[i - 1]);
		}
		return diff_v;
	}

	/**
	 * @brief arranges images from an array into a single image
	 * @param vec_mat       images to arrange
	 * @param window_height height of new combined image
	 * @param n_rows        number of rows images should be arranged into
	 * @return              a composite image made of multiple images
	 */
	template <typename T>
	cv::Mat images_to_grid(std::vector<T>& vec_mat, const int window_height,
	                       uint n_rows) {
		const size_t n = vec_mat.size();
		n_rows = int(n_rows > n ? n : n_rows);
		const int edge_thickness = 10;
		const int images_per_row = int(ceil(double(n) / n_rows));
		const int resize_height = int(
		                              floor(2.0 *
		                                    ((floor(double(window_height - edge_thickness) / n_rows)) / 2.0)) -
		                              edge_thickness);
		int max_row_length = 0;

		std::vector<int> resize_width;
		for (uint i = 0; i < n; ++i) {
			int this_row_len = 0;
			for (int k = 0; k < images_per_row; k++) {
				const double aspect_ratio = double(vec_mat[i].cols) / vec_mat[i].rows;
				int temp = int(ceil(resize_height * aspect_ratio));
				resize_width.push_back(temp);
				this_row_len += temp;
				if (i == n) {
					break;
				}
			}
			if ((this_row_len + edge_thickness * (images_per_row + 1)) >
			        max_row_length) {
				max_row_length = this_row_len + edge_thickness * (images_per_row + 1);
			}
		}
		const int window_width = max_row_length;
		cv::Mat grid_image(window_height, window_width, CV_8UC3,
		                   cv::Scalar(255, 255, 255));

		for (uint k = 0, i = 0; i < n_rows; i++) {
			const int y =
			        static_cast<int>(i * resize_height + (i + 1) * edge_thickness);
			int x_end = edge_thickness;
			for (int j = 0; j < images_per_row && k < n; k++, j++) {
				const int x = x_end;
				cv::Rect roi(x, y, resize_width[k], resize_height);
				const cv::Size s = grid_image(roi).size();
				cv::Mat target_roi(s, CV_8UC3);
				if (vec_mat[k].channels() != grid_image.channels()) {
					if (vec_mat[k].channels() == 1) {
						cv::cvtColor(vec_mat[k], target_roi, cv::COLOR_GRAY2BGR);
					}
				} else {
					vec_mat[k].copyTo(target_roi);
				}
				cv::resize(target_roi, target_roi, s);
				if (target_roi.type() != grid_image.type()) {
					target_roi.convertTo(target_roi, grid_image.type());
				}
				target_roi.copyTo(grid_image(roi));
				x_end += resize_width[k] + edge_thickness;
			}
		}
		return grid_image;
	}

	/**
	 * @brief masks all matrices in the given container using mask
	 * @param mask          mask to apply to all matrices of array
	 * @param array         container of matrices of 3 channels
	 * @param default_color determines the color of pixels that were masked away
	 */
	void mask_mat_array(const cv::Mat& mask, std::vector<cv::Mat>& array, const cv::Scalar& default_color = cv::Scalar(0, 0, 0));

	/**
	 * @brief masks all matrices in the given container using mask
	 * @param mask          mask to apply to all matrices of array
	 * @param array         container of matrices, with 3 channels split into their separate matrices
	 * @param default_color determines the color of pixels that were masked away
	 */
	void mask_mat_array(const cv::Mat& mask, std::vector<matarr_3>& array, const cv::Scalar& default_color = cv::Scalar(0, 0, 0));

	/**
	 * @brief applies a binary mask to an image
	 * @param mask          mask to apply
	 * @param img           input image
	 * @param output        output image
	 * @param default_color determines the color of pixels that were masked away
	 */
	void apply_binary_mask(const cv::Mat& mask, const cv::Mat& img,
	                       cv::Mat& output, const cv::Scalar& default_color = cv::Scalar(0, 0, 0));

	void alpha_mask(const cv::Mat& mask, const cv::Mat& img, cv::Mat& output);

	/**
	 * @brief turns a vector of unsigned chars into a string
	 * @param vec     vector of unsigned chars
	 * @return string made from input vector
	 */
	std::string vector_as_string(const std::vector<uchar>& vec);
	/**
	 * @brief vas shorter name for vector_as_string
	 * @see vector_as_string
	 * @param vec vector of unsigned chars
	 * @return    string made from input vector
	 */
	std::string vas(const std::vector<uchar>& vec);

	/**
	 * @brief turns a vector of vector_counts into a string
	 * @param vec    vector counts
	 * @param simple set to false for color escape sequences
	 * @return       string made from input vector
	 */
	std::string vector_as_string(const vpair_vuc_i& vec, bool simple = false);

	/**
	 * @brief vas shorter name for vector_as_string
	 * @see vector_as_string
	 * @param vec    vector counts
	 * @param simple set to false for color escape sequences
	 * @return       string made from input vector
	 */
	std::string vas(const vpair_vuc_i& vec, bool simple = false);

	/**
	 * @brief turns a point of integers into a string
	 * @param pt      point of integers
	 * @return string made from input point
	 */
	std::string point_as_string(const cv::Point3i& pt);
	/**
	 * @brief shorter name for point_as_string
	 * @see point_as_string
	 * @param pt      point of integers
	 * @return string made from input point
	 */
	std::string pas(const cv::Point3i& pt);

	template<typename T, typename iterator, typename func>
	iterator erase_if(std::vector<T>& vec, const func& op) {
		return vec.erase(std::remove_if(vec.begin(), vec.end(), op), vec.end());
	}

	template<typename M, typename V>
	void map_values(const M& m, V& v) {
		for (typename M::const_iterator it = m.begin(); it != m.end(); ++it) {
			v.push_back(it->second);
		}
	}

	template<typename M, typename V>
	void map_keys(const M& m, V& v) {
		for (typename M::const_iterator it = m.begin(); it != m.end(); ++it) {
			v.push_back(it->first);
		}
	}

	/**
	 * @brief paints a point with specified color at specified coordinates
	 * @param x     x coordinate
	 * @param y     y coordinate
	 * @param color color to paint with
	 * @param img   image to paint in
	 */
	void paint_point(int x, int y, uint color, cv::Mat& img);

	/**
	 * @brief paints a point with specified color at specified index
	 * @param i      index in image
	 * @param color  color to paint with
	 * @param img    image to paint in
	 * @param img    image to paint in
	 */
	void paint_point(int i, int color, cv::Mat& img);

	/**
	 * @brief converts BGR image format to HSV
	 * @param img   image to convert
	 * @param scale optional resize factor
	 * @return      HSV image
	 */
	cv::Mat bgr_to_hsv(const cv::Mat& img, double scale = 1.0);

	/**
	 * @brief converts the img image from BGR to HSV and splits the new
	 *        HSV matrix into separate channels
	 * @param img image to convert and split
	 * @return    split HSV image channels
	 */
	matarr_3 bgr_to_hsv_3(const cv::Mat& img);

	/**
	 * @brief converts the img split channels from BGR to HSV
	 * @param img split RGB channels of image
	 * @return    split HSV image channels
	 */
	matarr_3 bgr_to_hsv(const matarr_3& img);
	/**
	 * @brief converts the img vector of images from BGR to HSV and
	 *        splits the new HSV matrices into separate channels
	 * @param bgr_ss images to convert and split
	 * @return       vector of split HSV image channels
	 */
	std::vector<matarr_3> bgr_ss_to_hsv(std::vector<vmat>& bgr_ss);

	/**
	 * @brief converts HSV image format to BGR
	 * @param img   image to convert
	 * @param scale optional resize factor
	 * @return      BGR image
	 */
	cv::Mat hsv_to_bgr(const cv::Mat& img, double scale = 1.0);

	/**
	 * @brief splits an image into separate channels
	 * @param img image to split
	 * @return    array of image channels
	 */
	matarr_3 mat_split(const cv::Mat& img);

	/**
	 * @brief merges split image channels into one image
	 * @param split split image channels
	 * @return      merged image
	 */
	cv::Mat mat_merge(const matarr_3& split);

	/**
	 * @brief remove_background TODO
	 * @param image
	 */
	void remove_background(cv::Mat& image);

	/**
	 * @brief class representing the histogram of 1 or 3 channeled images
	 */
	class histogram {
		/// @brief histogram of channel 1
		std::vector<unsigned int> h_1;
		/// @brief histogram of channel 2
		std::vector<unsigned int> h_2;
		/// @brief histogram of channel 3
		std::vector<unsigned int> h_3;

	public:
		/**
		 * @brief constructor for histogram
		 * @param n_channels specification of the number of channels to use
		 */
		histogram(int bins = 255, int n_channels = 1) {
			if (n_channels != 1 && n_channels != 3) {
				throw std::runtime_error("invalid histogram channel number");
			}
			h_1.resize(bins);
			if (n_channels == 3) {
				h_2.resize(bins);
				h_3.resize(bins);
			}
		}

		/**
		 * @brief to be used for index element access
		 * @param i index
		 * @return  value of histogram at index i
		 */
		std::vector<unsigned int*> operator()(int i);

		/**
		 * @brief to be used for index element access
		 * @param i index for channel 1
		 * @param j index for channel 2
		 * @param k index for channel 3
		 * @return  values of histogram at indices i, j, k
		 */
		std::vector<unsigned int*> operator()(int i, int j, int k);

		/**
		 * @brief channels returns the number of channels that this histogram works with
		 * @return number of channels in histogram
		 */
		[[nodiscard]] size_t channels() const;

		/**
		 * @brief an entry within histogram
		 */
		class hist_entry {
			friend histogram;
			/**
			 * @brief x_ location on x axis
			 */
			unsigned int x_;
			/**
			 * @brief y_ location on y axis
			 */
			unsigned int y_;
		public:
			/**
			 * @brief x accessor
			 * @return coordinate x
			 */
			[[nodiscard]] unsigned int x() const {
				return x_;
			}

			/**
			 * @brief y accessor
			 * @return coordinate y
			 */
			[[nodiscard]] unsigned int y() const {
				return y_;
			}

			/**
			 * @brief constructor creates a new histogram entry
			 * @param x location
			 * @param y value
			 */
			hist_entry(unsigned int x, unsigned int y) : x_(x), y_(y) {}
		};

		/**
		 * @brief max returns the maximum value found among all channels
		 * @return value where the histogram maximum is found and the maximum itself from among all channels
		 */
		[[nodiscard]] hist_entry max() const;


		/**
		 * @brief max returns the maximum value found among all channels
		 * @return value where the histogram maximum is found and the maximum itself from among all channels
		 */
		[[nodiscard]] std::vector<hist_entry> all_max() const;
	};

	/**
	 * @brief creates a histogram for the given image
	 * @param image image to make histogram for
	 * @return      histogram of image
	 */
	histogram get_histogram(const cv::Mat& image, int bins = 255);

	/**
	 * @brief takes two images and compares their histograms
	 * @param img_a first image
	 * @param img_b second image
	 * @return      quantification of histogram similarity
	 */
	double compare_histograms(const cv::Mat& img_a, const cv::Mat& img_b);

	double get_psnr(const cv::Mat& i1, const cv::Mat& i2);
	cv::Scalar get_mssim(const cv::Mat& i1, const cv::Mat& i2);

	bool pair_compare(const std::pair<std::vector<uchar>, int>& l,
	                         const std::pair<std::vector<uchar>, int>& r);

	/**
	 * @brief find the n most common values from a map
	 * @param n number of values to find
	 * @param m map to search
	 * @return  vector of key-value pairs of size n
	 */
	template <typename K, typename V>
	std::vector<std::pair<K, V>> copy_n_most_common_from_map(
	        int n, const std::map<K, V>& m) {
		std::vector<std::pair<K, V>> top_n(n);
		std::partial_sort_copy(
		            m.begin(), m.end(), top_n.begin(), top_n.end(),
		            [](const std::pair<K, V>& l, const std::pair<K, V>& r) {
			return l.second > r.second;
		});
		return top_n;
	}

	/**
	 * @brief the n most common values from a map of
	 *        vector counts
	 * @param n number of values to find
	 * @param m map to search
	 * @return  vector of key-value pairs of size n
	 */
	vpair_vuc_i copy_n_most_common_from_map(int n, const map_vuc_i& m);
	/**
	 * @brief find the n most common values from a map of
	 *        vector counts
	 * @param img_n                 image pixel count
	 * @param m                     map to search
	 * @param color_count_threshold minimum threshold for a vector to be considered
	 * @param use_oo_values         flag for using out of range values
	 * @param oor_white             out of range white value
	 * @param oor_black             out of range black value
	 * @param min_n_of_samples      minimum threshold for taking further values from input map
	 * @return                      vector of key-value pairs of size n
	 */
	vpair_vuc_i copy_most_common_from_map(int img_n, const map_vuc_i& m,
	                                       int color_count_threshold,
	                                       range<uchar> excluded_hue_range,
	                                       int at_least = 4);

	template<typename container, typename comp_f>
	container copy_most_common_from_cont(int img_n, const container& m,
	                                     int color_count_threshold,
	                                     range<uchar> excluded_hue_range,
	                                     comp_f cmp,
	                                     bool use_oor_values,
	                                     int oor_white,
	                                     int oor_black,
	                                     int min_n_of_samples /*= 4*/) {
		size_t counter_size = m.size();
		container top_n(m.size());
		//int total_count = 0;
		std::partial_sort_copy(m.begin(), m.end(), top_n.begin(), top_n.end(), cmp);

		if (top_n.size() <= 3) {
			return top_n;
		}

		container filtered_most_common;
		filtered_most_common.reserve(counter_size);
		int filtered_total = 0;

		bool got_white = false;
		bool got_black = false;
		bool filter_full = false;

		int satisfying_amount = static_cast<int>(img_n * 0.85);
		for (size_t i = 0; i < top_n.size(); ++i) {
			const auto& val = top_n[i];
			if (!filter_full) {
				if ((*(excluded_hue_range.begin()) >= 0) &&
				        excluded_hue_range.any_in_range(val.get_values())) {
					continue;
				}
				if (filtered_most_common.size() <= 1 ||
				        val.get_count() > color_count_threshold) {
					filtered_total += val.get_count();
					filtered_most_common.push_back(val);
					if (use_oor_values) {
						got_white = got_white || top_n[i + 1].front() == oor_white;
						got_black = got_black || top_n[i + 1].front() == oor_black;
					}
				} else {
					filter_full = true;
					continue;
				}
				if (filtered_total >= satisfying_amount || i >= top_n.size() ||
				        top_n[i + 1].get_count() < min_n_of_samples) {
					if (i + 1 < top_n.size() &&
					        (filtered_most_common.size() == 1 ||
					         top_n[i + 1].get_count() > color_count_threshold)) {
						filtered_most_common.push_back(top_n[i + 1]);
						if (use_oor_values) {
							got_white = got_white || top_n[i + 1].front() == oor_white;
							got_black = got_black || top_n[i + 1].front() == oor_black;
						}
					}
					filter_full = true;
					continue;
				}
			} else {
				if (!use_oor_values || (got_white && got_black)) {
					break;
				}
				if (!got_white && val.front() == oor_white && val.get_count() > color_count_threshold * 0.75) {
					filtered_most_common.push_back(val);
					got_white = true;
				}
				if (!got_black && val.front() == oor_black && val.get_count() > color_count_threshold * 0.75) {
					filtered_most_common.push_back(val);
					got_black = true;
				}
			}
		}

		return filtered_most_common;
	}

	template<typename key_t, typename value_t>
	std::pair<key_t, value_t> get_max(const std::map<key_t, value_t>& x) {
		return *std::max_element(x.begin(), x.end(),
		                         [](const std::pair<key_t, value_t>& p1,
		                            const std::pair<key_t, value_t>& p2) {
			return p1.second < p2.second;
		});
	}

	template<typename key_t, typename value_t>
	std::pair<key_t, value_t> get_min(const std::map<key_t, value_t>& x) {
		return *std::min_element(x.begin(), x.end(),
		                         [](const std::pair<key_t, value_t>& p1,
		                            const std::pair<key_t, value_t>& p2) {
			return p1.second < p2.second;
		});
	}

	/**
	 * @brief similar compare two values to determine their similarity
	 * @param a value a
	 * @param b value b
	 * @param factor lower factor requires lower difference between the two input
	 * values
	 * @return true if values are similar considering given factor
	 */
	bool similar(int a, int b, double factor = 0.1);

	/**
	 * @brief check if given values are all similar to eachother
	 * @tparam T                numeric type
	 * @param values            values to check
	 * @param similarity_factor how similar the values should be
	 * @param def               default value to handle empty and one value
	 * @return                  true if values are similar to eachother
	 */
	template <typename T>
	bool is_near_constant(std::vector<T>& values, double similarity_factor = 0.1,
	                      bool def = true) {
		if (values.empty() || values.size() == 1) {
			return def;
		}
		for (size_t i = 1; i < values.size(); ++i) {
			if (!similar(values[i - 1], values[i], similarity_factor)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @brief removes cache padding from a vector (padded to prevent cache line fighting)
	 * @tparam V       numeric type
	 * @param original padded vector
	 * @param padding  padding between elements of vector
	 * @return         depadded vector
	 */
	template <typename V>
	std::vector<V> depad_vector(std::vector<V>& original, int padding) {
		size_t original_sz = original.size();
		std::vector<V> res(original.size() / padding);
		size_t i = 0;
		size_t j = 0;
		for (; j < original_sz; ++i, j += static_cast<uint>(padding)) {
			res[i] = original[j];
		}
		return res;
	}

	/**
	 * @brief conditional removal from a container
	 * @tparam container iterable container type
	 * @tparam predicate function that returns bool
	 * @param items      container of items
	 * @param pred       predicate, items that satisfy it are removed from the container
	 */
	template <typename container, typename predicate>
	void erase_if(container& items, const predicate& pred) {
		for (auto it = items.begin(); it != items.end();) {
			if (pred(*it)) {
				it = items.erase(it);
			} else {
				++it;
			}
		}
	}

	/**
	 * @brief splits a string at specified separator
	 * @param text string to split
	 * @param sep  separator to separate by
	 * @return     separated string sequences save specified separators
	 */
	std::vector<std::string> split(const std::string& text, char sep);

	/**
	 * @brief blend two images together
	 * @param a     image a
	 * @param b     image b
	 * @param alpha weight of image a, for image b the weight
	 *              is calculated as (1 - alpha)
	 * @return      blended image
	 */
	cv::Mat blend(const cv::Mat& a, const cv::Mat& b, double alpha);

	/**
	 * @brief resizes an image along its x and y axis
	 * @param img image to resize
	 * @param small result is saved here
	 * @param resize scaling factor
	 */
	void resize_im(matarr_3& img, matarr_3& small, double resize);

	/**
	 * @brief resizes two images along its x and y axis,
	 *        only here for convenience
	 * @param bgr_a   image a to resize
	 * @param bgr_b   image b to resize
	 * @param a_small result a is saved here
	 * @param b_small result b is saved here
	 * @param resize  scaling factor
	 */
	void resize_im(matarr_3& bgr_a, matarr_3& bgr_b, matarr_3& a_small, matarr_3& b_small,
	               double resize);

	/**
	 * @brief check if a number is in a list
	 * @param num  number to check if it is in a list
	 * @param list list to check if it contains a number
	 * @return     true if a number is in a list, false if a number is not in a list
	 */
	bool num_in_list(int num, const std::vector<int>& list);

	/**
	 * @brief check if point is in ellipse
	 * @param center center of ellipse
	 * @param point  point to test
	 * @param h      height of ellipse
	 * @param w      width of ellipse
	 * @param angle  angle of ellipse rotation
	 * @return       true if point is in ellipse
	 */
	bool point_in_ellipse(const cv::Point& center, const cv::Point& point, int h, int w, double angle);

	/**
	 * @brief check if point is in circle
	 * @param center center of circle
	 * @param point  point to test
	 * @param radius radius of circle
	 * @return       true if point is in circle
	 */
	bool point_in_circle(const cv::Point& center, const cv::Point& point, int radius);

	/**
	 * @brief check if point is in rectangle
	 * @param center center of rectangle
	 * @param point  point to test
	 * @param h      height of rectangle
	 * @param w      width of rectangle
	 * @return       true if point is in rectangle
	 */
	bool point_in_rectangle(const cv::Point& center, const cv::Point& point, int h, int w);

	bool general_point_in_parellelogram(const cv::Point2d& M, const cv::Point2d& A, const cv::Point2d& B, const cv::Point2d& C, const cv::Point2d& D);

	/**
	 * @brief converts given HSV values to RGB
	 * @param h hue
	 * @param s saturation
	 * @param v value
	 * @return  cv::Scalar holding BGR values
	 */
	cv::Scalar hsv2bgr(float h, float s, float v);
	cv::Scalar bgr2hsv(int B, int G, int R);

	double deltaE(const cv::Point3f& lab_a, const cv::Point3f& lab_b);

	inline bool similar_color_pixels(const cv::Point3f& a, const cv::Point3f& b) {
		return deltaE(a, b) < 50;
	}

	template<typename T, typename func>
	T triangulation(const std::vector<cv::Point>& polygon, const cv::Mat& img, const func& op) {
		cv::Rect r(0, 0, img.cols, img.rows);
		cv::Subdiv2D subdiv(r);
		for (const cv::Point& p : polygon) {
			subdiv.insert(p);
		}
		std::vector<cv::Vec6f> triangle_list;
		subdiv.getTriangleList(triangle_list);
		T ret;
		std::vector<cv::Point> pt(3);
		for (size_t i = 0; i < triangle_list.size(); i++) {
			cv::Vec6f t = triangle_list[i];
			pt[0] = cv::Point(cvRound(t[0]), cvRound(t[1]));
			pt[1] = cv::Point(cvRound(t[2]), cvRound(t[3]));
			pt[2] = cv::Point(cvRound(t[4]), cvRound(t[5]));
			op(pt, ret);
		}
		return ret;
	}
}  // namespace utility

namespace quick_access_utility {
	inline cv::Mat give_mat(const cv::Mat& original) {
		return cv::Mat::zeros(original.rows, original.cols, original.type());
	}

	template<typename T> cv::Mat give_mat(const cv::Mat& original);

	template<typename T> cv::Mat give_mat(int rows, int cols);

	template<typename T, uchar n> cv::Mat give_mat(const cv::Mat& original);

	template<typename T, uchar n> cv::Mat give_mat(int rows, int cols);

	template<typename T> cv::Mat give_mat(const cv::Mat& original, T value);

	template<typename T> cv::Mat give_mat(int rows, int cols, T value);

	template<typename T, uchar n> cv::Mat give_mat(const cv::Mat& original, T value);

	template<typename T, uchar n> cv::Mat give_mat(int rows, int cols, T value);
}

/**
 * @brief contains functions for drawing shapes into an OpenCV matrix
 */
namespace drawing {
	/**
	 * @brief draws a line segment from point 0 to point 1
	 * @param img   image to draw in
	 * @param x0    x of point 0
	 * @param y0    y of point 0
	 * @param x1    x of point 1
	 * @param y1    y of point 1
	 * @param color color to draw with
	 */
	void draw_line_segment(cv::Mat& img, double x0, double y0, double x1, double y1,
	                       const pixel& color, int thickness = 1);

	void draw_arrow(cv::Mat& img, double x0, double y0, double x1, double y1,
	                const cv::Scalar& color, int thickness = 1);

	/**
	 * @brief draws a line segment from point 0 to point 1
	 * @param img   image to draw in
	 * @param x0    x of point 0
	 * @param y0    y of point 0
	 * @param x1    x of point 1
	 * @param y1    y of point 1
	 * @param color color to draw with
	 */
	void draw_line(cv::Mat& img, double x0, double y0, double x1, double y1, const cv::Scalar& color, int thickness = 1);

	/**
	 * @brief draw a coordinate axis (call twice, once for each axis,
	 *        changing parameter q)
	 * @param img   image to draw in
	 * @param p     central point
	 * @param q     point along x or y axis
	 * @param color color to draw with
	 * @param scale to multiply lengths with
	 */
	void draw_axis(cv::Mat& img, cv::Point p, cv::Point q, const cv::Scalar& color,
	               float scale = 0.2);
	/**
	 * @brief draw_square draw a square area in an image at specified coordinates
	 *        with a specified color
	 * @param img    image to draw in
	 * @param x      x coordinate
	 * @param y      y coordinate
	 * @param radius half of square side
	 * @param color  color to draw with
	 */
	void draw_square(cv::Mat& img, float x, float y, int radius,
	                 const cv::Scalar& color);

	/**
	 * @brief draw a cross in an image at specified
	 *        coordinates with specified color
	 * @param img    image to draw in
	 * @param x      x coordinate
	 * @param y      y coordinate
	 * @param radius half of cross height/width
	 * @param color  color to draw with
	 */
	void draw_cross(cv::Mat& img, const cv::Point& p, int radius,
	                const cv::Scalar& color, int thickness = 1);

	/**
	 * @brief get info about where in the image is the mouse pointer
	 * @param img   image base for the position of the mouse
	 * @param mouse structure to hold data about position of mouse pointer
	 */
	void mouse_info(cv::Mat& img, const mouse_s& mouse, const cv::Point& pos,
	                bool onMouse = false);

	/**
	 * @brief draw text into the specified image
	 * @param img                image to draw in
	 * @param text               text to draw
	 * @param org                point of origin
	 * @param fontface           font face to use
	 * @param fontscale          font scale
	 * @param color              text color
	 * @param thickness          text thickness
	 * @param line_type          type of line to use (anti-aliasing)
	 * @param bottom_left_origin flag for origin in bottom left corner of image
	 */
	void draw_text(cv::Mat& img, const cv::String& text, const cv::Point& org, int fontface, double fontscale, const cv::Scalar& color, int thickness = 1, int line_type = cv::LINE_8, bool bottom_left_origin = false);

	/**
	 * @brief draw a rectangle in an image at specified coordinates
	 *        given by a rectangle of generosity
	 * @param img       image to draw in
	 * @param rect      rectangle model to draw into the image
	 * @param color     color to draw with
	 * @param thickness thickness of drawn rectangle
	 * @param overlap   extra bulk is added to the rectangles to make them overlap
	 */
	void draw_rectangle(cv::Mat& img, cv::Rect& rect, const cv::Scalar& color,
	                    int thickness = 1, int overlap = 2);

	/**
	 * @brief draw a rectangle in an image at specified coordinates
	 * @param img       image to draw in
	 * @param pt1       start point
	 * @param pt2       end point
	 * @param color     rectangle color
	 * @param thickness border thickness, negative values for filled in rectangle
	 * @param line_type type of line to use (anti-aliasing)
	 * @param shift     number of fractional bits in the point coordinates
	 */
	void draw_rectangle(cv::Mat& img, const cv::Point& pt1, const cv::Point& pt2, const cv::Scalar& color, int thickness = -1, int line_type = cv::LINE_8, int shift = 0);

	/**
	 * @brief draw a rectangle in an image
	 * @param img       image to draw in
	 * @param r         rectangle to draw
	 * @param color     rectangle color
	 * @param thickness border thickness, negative values for filled in rectangle
	 * @param line_type type of line to use (anti-aliasing)
	 * @param shift     number of fractional bits in the point coordinates
	 */
	void draw_rectangle(cv::Mat& img, const cv::Rect& r, const cv::Scalar& color,
	                    int thickness = 1, int line_type = cv::LINE_8, int shift = 0);

	/**
	 * @brief used for graphical visualization of various data
	 * @tparam T type of data to visualize (numeric)
	 */
	template<typename T>
	struct plot_member {
	private:
		/// @brief maximum value that can be reached
		T max_;
		/// @brief value that this plot member holds
		T value_;
		/// @brief name for the data
		std::string name_;
		/// @brief index for being given a color
		int assigned_index_ = -1;
	public:
		/**
		 * @brief assigned index getter
		 * @return assigned index
		 */
		[[nodiscard]] inline int assigned_index() const { return assigned_index_; }
		/**
		 * @brief assigned_index setter
		 * @param value new value for assigned index
		 */
		inline void assigned_index(int value) { assigned_index_ = value; }
		/**
		 * @brief max value getter
		 * @return max value
		 */
		[[nodiscard]] inline T max() const { return max_; }
		/**
		 * @brief held value getter
		 * @return held value
		 */
		[[nodiscard]] inline T value() const { return value_; }
		/**
		 * @brief name getter
		 * @return name
		 */
		[[nodiscard]] inline const std::string& name() const { return name_; }
		/**
		 * @brief comparison between two plot members by their name
		 * @param a plot member a
		 * @param b plot member b
		 * @return  true if name of a is < than name of b
		 */
		friend bool operator<(const plot_member& a, const plot_member& b) {
			return a.name_ < b.name_;
		}
		/**
		 * @brief comparison between this and another plot member by their name
		 * @param other an instance of plot member
		 * @return true if name of this is < than name of other
		 */
		bool operator<(const plot_member& other) const {
			return name_ < other.name_;
		}

		/**
		 * @brief constructor
		 * @param name name of plot member
		 * @param value value to hold
		 * @param max maximum value
		 */
		plot_member(std::string name, T value, T max) : max_(max), value_(value), name_(std::move(name)) {}
	};

	/**
	 * @brief draws all plot members given in the container to visualize their magnitude
	 * @param values container of plot members, each with their own value
	 * @return       image containing the visualization of values
	 */
	cv::Mat plot(std::vector<plot_member<size_t>>& values);
	/**
	 * @brief appends more plot members to an image im
	 * @param im     image to append a new image to
	 * @param values new values to visualize
	 * @return       image creates from the original im, vertically concatenated
	 *               with the visualization of values
	 */
	cv::Mat plot(cv::Mat& im, std::vector<plot_member<size_t>>& values);

	/**
	 * @brief draws a histogram into a matrix that can be displayed using cv::show_im()
	 * @param hist histogram
	 * @return     plot of histogram
	 */
	inline cv::Mat draw_histogram(utility::histogram& hist) {
		cv::Mat output = cv::Mat::zeros(300, 530, CV_8UC3);
		auto max = static_cast<double>(hist.max().y());
		if (hist.channels() == 3) {
			for (int i = 0; i < 255; ++i) {
				auto v = hist(i);
				auto fv0 = static_cast<unsigned int>(*v[0] / max * 280);
				auto fv1 = static_cast<unsigned int>(*v[1] / max * 280);
				auto fv2 = static_cast<unsigned int>(*v[2] / max * 280);
				std::vector<unsigned int> y_vec = { fv0, fv1, fv2 };

				std::sort(y_vec.begin(), y_vec.end(), [](int a, int b) { return a > b; });

				unsigned int r = fv2 == y_vec[0] ? 255 : 0;
				unsigned int g = fv1 == y_vec[0] ? 255 : 0;
				unsigned int b = fv0 == y_vec[0] ? 255 : 0;
				draw_rectangle(output, cv::Point(10 + i * 2, 290 - y_vec[0]), cv::Point(11 + i * 2, 290), cv::Scalar(b, g, r));
				r |= fv2 == y_vec[1] ? 255 : 0;
				g |= fv1 == y_vec[1] ? 255 : 0;
				b |= fv0 == y_vec[1] ? 255 : 0;
				draw_rectangle(output, cv::Point(10 + i * 2, 290 - y_vec[1]), cv::Point(11 + i * 2, 290), cv::Scalar(b, g, r));
				draw_rectangle(output, cv::Point(10 + i * 2, 290 - y_vec[2]), cv::Point(11 + i * 2, 290), cv::Scalar(255, 255, 255));
			}
		} else {
			for (int i = 0; i < 255; ++i) {
				auto v = hist(i);
				int y = static_cast<int>(*v[0] / max * 280);
				draw_rectangle(output, cv::Point(10 + i * 2, 290 - y), cv::Point(11 + i * 2, 290), cv::Scalar(255, 255, 255));
			}
		}
		return output;
	}

	cv::Mat draw_histogram(std::vector<cv::Mat>& hist, int hist_size, int plot_type);
	cv::Mat draw_histogram(cv::Mat& hist, int hist_size);

	void additive_circle(cv::Mat& io, const cv::Point& center, int radius, float addition, bool acting_agent = false);
}  // namespace drawing

namespace video_control {
	/**
	 * @brief skip_to sets position in provided capture to specified frame
	 * @param capture  capture to set position in
	 * @param to_frame frame to set position to
	 */
	void skip_to(cv::VideoCapture& capture, int to_frame);
}


/**
 * @brief debug function for concise showing of images only if image
 *        showing is desired globally, with optional output
 * @param name      name of window
 * @param img       image to show
 * @param stdoutput optional output, ends with std::"
"
 */
void show_im(const std::string& name, const cv::Mat& img,
                    const std::string& stdoutput = "");

/**
 * @brief cv::waitKey automatic handling of cv::waitKey() with lack of images
 * @param delay    time in ms to wait for (0 = infinity)
 * @return         code of key pressed, 0 if none
 */
int key_wait(int delay = 0);

/**
 * @brief lowers the precision of color hue by performing integer
 *        division and multiplication by the same number
 * @param c         color hue
 * @param precision divisor
 * @return          color hue divisible by given divisor
 */
inline uchar decrease_precision(uchar c, uchar precision = 5, uchar added = 30) {
	c = c > 180 ? c / std::max(static_cast<uchar>(1), precision)
	            : static_cast<uchar>((((c + added) % 180) / std::max(static_cast<uchar>(1), precision)));
	return c % 180;
}

struct nullable_int { // inspired by C#, mostly just int with attached bool value
private:
	int value_{0};
	bool null_{false};
public:
	nullable_int(int num) : value_(num), null_(false) {}
	nullable_int(std::nullptr_t) : value_(0), null_(true) {}

	int* val() {
		return null_ ? nullptr : &value_;
	}

	operator int() {
		if (null_) {
			throw std::invalid_argument("nullable int is null");
		}
		return value_;
	}

	nullable_int& operator=(int new_value) {
		if (null_) {
			null_ = false;
		}
		value_ = new_value;
		return *this;
	}

	nullable_int& operator=(std::nullptr_t) {
		if (!null_) {
			null_ = true;
		}
		value_ = 0;
		return *this;
	}

	bool is_null() {
		return null_;
	}
};

/**
 * @brief wrapper for OpenCV's cv::Rect to add support for 'direction' as used in rectangle search
 */
struct rect : public cv::Rect {
private:
	/**
	 * @brief internal OpenCV rectangle
	 */
	cv::Rect r_;
	/**
	 * @brief specifies which direction the rectangle search went (up, down)
	 */
	int dir_;
public:
	/**
	 * @brief const rectangle getter
	 * @return rectangle
	 */
	[[nodiscard]] inline const cv::Rect& r() const { return r_; }
	/**
	 * @brief rectangle getter
	 * @return rectangle
	 */
	[[nodiscard]] inline cv::Rect& r() { return r_; }
	/**
	 * @brief rectangle setter
	 * @param value new value for rectangle
	 */
	inline void r(const cv::Rect& value) { r_ = value; }
	/**
	 * @brief direction getter
	 * @return direction
	 */
	[[nodiscard]] inline int dir() { return dir_; }
	/**
	 * @brief direction setter
	 * @param value new value for direction
	 */
	inline void dir(int value) { dir_ = value; }

	/**
	 * @brief constructor for non-binary rectangle search
	 * @param r rectangle to initialize with
	 */
	rect(cv::Rect& r) : r_(r), dir_(0) {}
	/**
	 * @brief constructor for non-binary rectangle search but no pre-existing
	 * opencv rectangle
	 * @param x new rectangle x
	 * @param y new rectangle y
	 * @param w new rectangle width
	 * @param h new rectangle height
	 */
	rect(int x, int y, int w, int h) : rect(x, y, w, h, 0) {}
	/**
	 * @brief constructor for binary rectangle search and no pre-existing
	 * opencv rectangle
	 * @param x new rectangle x
	 * @param y new rectangle y
	 * @param w new rectangle width
	 * @param h new rectangle height
	 * @param d new rectangle direction
	 */
	rect(int x, int y, int w, int h, int d) : r_(x, y, w, h), dir_(d) {}

	[[nodiscard]] inline cv::Point_<int> br() const { return r_.br(); }
	[[nodiscard]] inline cv::Point_<int> tl() const { return r_.tl(); }
	[[nodiscard]] inline int area() const { return r_.area(); }
};

/**
 * @brief functor for comparing two rect
 * @see rect
 */
struct rcomp {
	/**
	 * @brief overload for comparing rectangles with directions
	 * @param left  lefthand rectangle
	 * @param right righthand rectangle
	 * @return      true if left is more left than right (x coordinate)
	 */
	bool operator()(const rect& left, const rect& right) const {
		if (left.r().x == right.r().x) {
			return left.r().y < right.r().y;
		}
		return left.r().x < right.r().x;
	}
};

/**
 * @brief functor for comparing two cv::Point
 * @see cv::Point
 */
struct pcomp_x {
	bool operator()(const cv::Point& left, const cv::Point& right) const {
		return left.x == right.x ? left.y >= right.y : left.x >= right.x;
	}
};

struct pcomp_y {
	bool operator()(const cv::Point& left, const cv::Point& right) const {
		return left.y == right.y ? left.x < right.x : left.y < right.y;
	}
};

/**
 * @brief functor for comparing two cv::Point inverted
 * @see cv::Point
 */
struct pcomp_inv {
	bool operator()(const cv::Point& left, const cv::Point& right) const {
		return left.x == right.x ? left.y < right.y : left.x < right.x;
	}
};

using point_set = std::set<cv::Point, pcomp_x>;

/**
 * @brief structure to hold information about keypoints
 */
struct keypoint {
private:
	cv::Point2f pt_;
	size_t size_;
	double angle_;
	cv::Rect area_;
public:
	/**
	 * @brief const x getter
	 * @return x coordinate of point
	 */
	[[nodiscard]] inline int x() const {
		return pt_.x;
	}

	inline keypoint()
	    : pt_(cv::Point(0, 0)), size_(0), angle_(0), area_(cv::Rect()) {}

	/**
	 * @brief x getter
	 * @return x coordinate of point
	 */
	inline int x() {
		return pt_.x;
	}

	/**
	 * @brief const y getter
	 * @return y coordinate of point
	 */
	[[nodiscard]] inline int y() const {
		return pt_.y;
	}

	/**
	 * @brief y getter
	 * @return y coordinate of point
	 */
	inline int y() {
		return pt_.y;
	}

	/**
	 * @brief const point getter
	 * @return the point structure that defines the location of this keypoint
	 */
	[[nodiscard]] inline const cv::Point2f& pt() const {
		return pt_;
	}

	/**
	 * @brief point getter
	 * @return the point structure that defines the location of this keypoint
	 */
	inline cv::Point2f& pt() {
		return pt_;
	}

	/**
	 * @brief const area getter
	 * @return returns a rectangular area with this keypoint at its center
	 */
	[[nodiscard]] inline const cv::Rect& area() const {
		return area_;
	}

	/**
	 * @brief area getter
	 * @return returns a rectangular area with this keypoint at its center
	 */
	inline cv::Rect& area() {
		return area_;
	}

	/**
	 * @brief returns the size of the keypoint
	 * @return size of keypoint
	 */
	[[nodiscard]] inline size_t size() const {
		return size_;
	}

	/**
	 * @brief returns the angle of the keypoint
	 * @return angle of keypoint
	 */
	[[nodiscard]] inline double angle() const {
		return angle_;
	}

	/**
	 * @brief < comparison with other keypoints
	 * @param other another keypoint
	 * @return true if this keypoint is more to the left and top than the other one
	 */
	inline bool operator<(const keypoint& other) {
		return pt_.x == other.pt_.x ? pt_.y >= other.pt_.y : pt_.x >= other.pt_.x;
	}

	/**
	 * @brief < comparison with other keypoints
	 * @param other another keypoint
	 * @return true if this keypoint is more to the left and top than the other one
	 */
	inline bool operator<(const keypoint& other) const {
		return pt_.x == other.pt_.x ? pt_.y >= other.pt_.y : pt_.x >= other.pt_.x;
	}

	/**
	 * @brief automatic conversion to const cv::Point
	 */
	inline operator cv::Point() const {
		return pt_;
	}

	/**
	 * @brief automatic conversion to cv::Point
	 */
	inline operator cv::Point() {
		return pt_;
	}

	/**
	 * @brief defining a + operation for points that makes sense i'm sure
	 * @param p another point
	 * @return point X, where OAXp is a parallelogram, O being the origin of the coordinate system,
	 * A is this point
	 */
	inline cv::Point2f operator+(const cv::Point2f& p) const {
		return cv::Point2f(pt_.x + p.x, pt_.y + p.y);
	}

	/**
	 * @brief vector difference
	 * @param p other vector
	 * @return vector difference of this and p
	 */
	inline cv::Vec2f operator-(const cv::Point& p) const {
		return cv::Vec2f(pt_.x - p.x, pt_.y - p.y);
	}

	/**
	 * @brief shifting point by vector
	 * @param p directional vector
	 * @return this point shifted by p
	 */
	inline cv::Point2f operator+(const cv::Vec2f& p) {
		pt_.x += p[0];
		pt_.y += p[1];
		return pt_;
	}

	/**
	 * @brief shifting point by vector
	 * @param p directional vector
	 * @return this point shifted by p
	 */
	inline cv::Point2f operator+(const cv::Vec2f& p) const {
		return cv::Point2f(pt_.x + p[0], pt_.y + p[1]);
	}

	/**
	 * @brief shifting point by negative of vector
	 * @param p directional vector
	 * @return this point shifted by negative of p
	 */
	inline cv::Point2f operator-(const cv::Vec2f& p) {
		pt_.x -= p[0];
		pt_.y -= p[1];
		return cv::Point2f(pt_.x, pt_.y);
	}

	/**
	 * @brief shifting point by negative of vector
	 * @param p directional vector
	 * @return this point shifted by negative of p
	 */
	inline cv::Point2f operator-(const cv::Vec2f& p) const {
		return cv::Point2f(pt_.x - p[0], pt_.y - p[1]);
	}

	/**
	 * @brief constructor for creating a keypoint
	 * @param pt    underlying point, defines location
	 * @param sz    size of keypoint
	 * @param angle angle of keypoint
	 * @param area  rectangular area which has this keypoint at its center
	 */
	inline keypoint(const cv::Point& pt, size_t sz, double angle, const cv::Rect& area)
	    : pt_(pt), size_(sz), angle_(angle), area_(area) {}
	/**
	 * @brief constructor for creating a keypoint
	 * @param x     x coordinate of this point
	 * @param y     y coordinate of this point
	 * @param sz    size of keypoint
	 * @param angle angle of keypoint
	 * @param area  rectangular area which has this keypoint at its center
	 */
	inline keypoint(int x, int y, size_t sz, double angle, const cv::Rect& area)
	    : keypoint(cv::Point(x, y), sz, angle, area) {}
};

/**
 * @brief shifting point by negative of vector
 * @param p directional vector
 * @return this point shifted by negative of p
 */
inline cv::Vec2f operator-(const cv::Point& p, const keypoint& kp) {
	return cv::Vec2f(p.x - kp.x(), p.y - kp.y());
}

/**
 * @brief shifting point by vector
 * @param p directional vector
 * @return this point shifted by p
 */
inline cv::Point2f operator+(const cv::Point& p, const cv::Vec2f& v) {
	return cv::Point2f(p.x + v[0], p.y + v[1]);
}

using keypoint_set = std::set<keypoint>;

/**
 * @brief class for bounding boxes
 */
class bounding_box_t {
	/**
	 * @brief box_ the box itself
	 */
	cv::Rect2d box_;
	/**
	 * @brief centroid_ centroid of the box
	 */
	cv::Point centroid_;

	/**
	 * @brief valid_ validity flag, used to check on trackers
	 */
	bool valid_ = false;
public:
	/**
	 * @brief automatic conversion to cv::Rect2d
	 */
	inline operator cv::Rect2d() {
		return box_;
	}

	/**
	 * @brief gets the box
	 * @return box
	 */
	inline cv::Rect2d& r() {
		return box_;
	}

	/**
	 * @brief sets the validity flag to false
	 */
	inline void invalidate() {
		valid_ = false;
	}

	/**
	 * @brief gets the validity status of the bounding box
	 * @return true if bounding box is valid
	 */
	[[nodiscard]] inline bool valid() const {
		return valid_;
	}

	/**
	 * @brief constructor, creates a box and sets its validity
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param w width
	 * @param h height
	 * @param centroid centroid of the box
	 */
	inline bounding_box_t(int x, int y, int w, int h, const cv::Point& centroid)
	    : box_(x, y, w, h), centroid_(centroid) {
		valid_ = x >= 0 && y >= 0 && w > 1 && h > 1;
	}

	/**
	 * @brief default constructor, boxes created this way are invalid
	 */
	inline bounding_box_t() : box_(0, 0, 0, 0), centroid_(0, 0) {}

	/**
	 * @brief encompasses a set of points with a bounding box
	 * @param points points to include in the box
	 * @return box, where all given points are within
	 */
	static bounding_box_t create(const std::vector<cv::Point>& points) {
		int x0 = INT_MAX;
		int y0 = INT_MAX;
		int x1 = 0;
		int y1 = 0;
		int avg_x = 0;
		int avg_y = 0;
		for (const auto& p : points) {
			if (p.x < x0) {
				x0 = p.x;
			}
			if (p.y < y0) {
				y0 = p.y;
			}
			if (p.x > x1) {
				x1 = p.x;
			}
			if (p.y > y1) {
				y1 = p.y;
			}
			avg_x += p.x;
			avg_y += p.y;
		}
		avg_x /= min_1(points.size());
		avg_y /= min_1(points.size());
		return { x0, y0, x1 - x0, y1 - y0, { avg_x, avg_y }};
	}

	/**
	 * @brief encompasses a set of points with a bounding box
	 * @param points points to include in the box
	 * @return box, where all given points are within
	 */
	static bounding_box_t create(const std::set<keypoint>& points) {
		int x0 = INT_MAX;
		int y0 = INT_MAX;
		int x1 = 0;
		int y1 = 0;
		int avg_x = 0;
		int avg_y = 0;
		for (const auto& p : points) {
			if (p.x() < x0) {
				x0 = p.x();
			}
			if (p.y() < y0) {
				y0 = p.y();
			}
			if (p.x() > x1) {
				x1 = p.x();
			}
			if (p.y() > y1) {
				y1 = p.y();
			}
			avg_x += p.x();
			avg_y += p.y();
		}
		avg_x /= min_1(points.size());
		avg_y /= min_1(points.size());
		return { x0, y0, x1 - x0, y1 - y0, { avg_x, avg_y }};
	}

	/**
	 * @brief checks the validity of given boxes
	 * @param boxes boxes to check
	 * @return true if all boxes are valid
	 */
	static bool all_invalid(const std::vector<bounding_box_t>& boxes) {
		for (const auto& box : boxes) {
			if (box.valid()) {
				return false;
			}
		}
		return true;
	}
};

/**
 * @brief check if string starts with a different string
 * @param string_a main string to check in
 * @param string_b substring to check for
 * @return         true if string_b is found at the beginning of string_a
 */
inline bool starts_with(const std::string& string_a,
                        const std::string& string_b) {
	return string_a.compare(0, string_b.length(), string_b) == 0;
}

/**
 * @brief turns all characters in a string to lowercase
 * @param str string to modify
 * @return    string with all characters in lowercase
 */
inline std::string to_lower(const std::string& str) {
	rope::rope_stringbuilder rope_sb;
	for (const char c : str) {
		rope_sb << static_cast<uchar>(std::tolower(static_cast<uchar>(c)));
	}
	return rope_sb.str();
}

/**
 * @brief check if image contains no values other than zero
 * @param img image to check
 * @return    true if image only contains zero
 */
inline bool zero_image(cv::Mat& img) { return cv::countNonZero(img) > 0; }

/**
 * @brief starts a timer
 * @return time of start
 */
inline std::chrono::time_point<std::chrono::high_resolution_clock>
timer_start() {
	return std::chrono::high_resolution_clock::now();
}

/**
 * @brief ends a timer
 * @param start time of timer start
 * @param msg optional message to send to output
 */
inline void timer_end(
        const std::chrono::time_point<std::chrono::high_resolution_clock>& start,
        std::ofstream* stream = nullptr, long long* time = nullptr, const std::string& msg = "") {
	const auto end = std::chrono::high_resolution_clock::now();
	const std::chrono::duration<double> elapsed_seconds = end - start;
	const long long ms =
	        std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_seconds)
	        .count();
	if (time) {
		*time = ms;
	}
	if (stream) {
		(*stream) << msg << "~ " << ms << "ms\n" << std::flush;
	}
}

/**
 * @brief gets the current total cpu usage stat
 * @return total cpu usage
 */
double get_cpu_used_total();

/**
 * @brief gets the current cpu usage for this process stat
 * @return cpu usage of this process
 */
double get_cpu_used_by_current();

/**
 * @brief function timer, starts a timer, calls a function, then ends
 *        the timer and outputs the time it took to run
 * @tparam ret    return type of given function
 * @tparam F      function
 * @tparam args   function arguments
 * @param f       function to run
 * @param fn_name function name
 * @param ms      pointer to export the time taken
 * @param prnt    set to true to have the time printed
 * @param arg     arguments to function
 * @return        return from the function
 */
template <typename ret, typename F, typename... args>
ret f_timer(F f, const std::string& fn_name, long long* ms = nullptr,
            bool prnt = true, args... arg) {
	const auto start = std::chrono::high_resolution_clock::now();
	const ret res = f(arg...);
	const auto end = std::chrono::high_resolution_clock::now();
	const std::chrono::duration<double> elapsed_seconds = end - start;
	if (ms == nullptr) {
		const long long time =
		        std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_seconds)
		        .count();
		if (prnt) {
			std::cout << fn_name << " time " << time << "ms\n" << std::flush;
		}
	} else {
		*ms = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed_seconds)
		      .count();

		if (prnt) {
			std::cout << fn_name << " time " << ms << "ms\n" << std::flush;
		}
	}
	return res;
}

/**
 * @brief class to represent results of various methods on a frame
 */
class result {
	bool good_{};
	long long time_{};
	double cpu_use_{};
	rope::rope_stringbuilder extra_str_{};

public:
	bool& good() { return good_; }
	long long& time() { return time_; }
	double& cpu_use() { return cpu_use_; }

	result(bool good, long long time, double cpu_use)
	    : good_(good), time_(time), cpu_use_(cpu_use) {}

	result() {}

	/**
	 * @brief performs a median filter pass on the given collection of results, removing small mistakes
	 * @param vec    collection of results to filter
	 * @param win_sz size of filter - use odd values!
	 */
	static void median_filter(std::vector<result>& vec, size_t win_sz) {
		if ((win_sz & 1) != 1) {
			throw std::string("trying to use median filter of even size on boolean values. bold move!");
		}
		std::vector<bool> window(win_sz);
		for (size_t i = win_sz - 1; i < vec.size(); ++i) {
			for (size_t k = 0; k < win_sz; ++k) {
				window[k] = vec[i - (win_sz - 1) + k].good_;
			}
			std::sort(window.begin(), window.end());
			bool res = window[win_sz / 2];
			vec[i - (win_sz / 2)].good_ = res;
		}
	}

	/**
	 * @brief performs a median filter pass on the given collection of results, removing small mistakes
	 * @param vec    collection of results to filter
	 * @param win_sz size of filter - use odd values!
	 */
	template<typename T>
	static void median_filter(T& collection, size_t win_sz) {
		if ((win_sz & 1) != 1) {
			throw std::string("trying to use median filter of even size on boolean values. bold move!");
		}
		std::vector<bool> window(win_sz);
		for (size_t i = win_sz - 1; i < collection.size(); ++i) {
			for (size_t k = 0; k < win_sz; ++k) {
				window[k] = collection[i - (win_sz - 1) + k];
			}
			std::sort(window.begin(), window.end());
			bool res = window[win_sz / 2];
			collection[i - (win_sz / 2)] = res;
		}
	}

	template<typename T>
	static bool past_n_majority(T& collection, size_t n, unsigned int max_n, bool strict) {
		size_t t_count = 0;
		double w_count = 0;
		auto rit = collection.rbegin();
		size_t o_n = n;
		n = std::min(n, static_cast<size_t>(min_1(max_n / 2 + (max_n & 0b1))));
		for (size_t i = 0; i < n; ++i) {
			if (*rit) {
				++t_count;
				w_count += (n - i) / static_cast<double>(n);
			}
			++rit;
		}
		// look at the amount of trues? like it shouldn't be low overall
		// on histogram change don't worry too much until n is max_n??
		if (n >= 2 && t_count == 1) {
			collection.back() = false;
			return collection.back();
		}
		collection.back() = ((strict && max_n <= 1) ? t_count > o_n / 2 : t_count > 0) && w_count > 0.5 * n;
		return collection.back();
	}

	operator bool() const {
		return good_;
	}

	rope::rope_stringbuilder& extra_str() { return extra_str_; }
};

/**
 * @brief point of x, y, z coordinates with added weight
 */
struct WPoint3 {
private:
	/**
	 * @brief x_ x coordinate
	 */
	int x_ = 0;
	/**
	 * @brief y_ y coordinate
	 */
	int y_ = 0;
	/**
	 * @brief z_ z coordinate
	 */
	int z_ = 0;
	/**
	 * @brief w_ weight
	 */
	double w_ = 0;

public:
	/**
	 * @brief x getter
	 * @return x
	 */
	[[nodiscard]] inline int x() const { return x_; }
	/**
	 * @brief x setter
	 * @param value new value for x
	 */
	inline void x(int value) { x_ = value; }
	/**
	 * @brief y getter
	 * @return y
	 */
	[[nodiscard]] inline int y() const { return y_; }
	/**
	 * @brief y setter
	 * @param value new value for y
	 */
	inline void y(int value) { y_ = value; }
	/**
	 * @brief z getter
	 * @return z
	 */
	[[nodiscard]] inline int z() const { return z_; }
	/**
	 * @brief z setter
	 * @param value new value for
	 */
	inline void z(int value) { z_ = value; }
	/**
	 * @brief weight getter
	 * @return weight
	 */
	[[nodiscard]] inline double w() const { return w_; }
	/**
	 * @brief weight setter
	 * @param value new value for weight
	 */
	inline void w(double value) { w_ = value; }

	/**
	 * @brief constructor
	 * @param x x coordinate
	 * @param y y coordinate
	 * @param z z coordinate
	 * @param w weight
	 */
	WPoint3(int x, int y, int z, double w) : x_(x), y_(y), z_(z), w_(w) {}
	WPoint3() = default;
};

/**
 * @brief comparison between two points of two values
 * @param a point a
 * @param b point b
 * @return true if point a closer to the origin
 */
inline bool operator<(const cv::Point_<int>& a, const cv::Point_<int>& b) {
	return (a.x * a.x + a.y * a.y) < (b.x * b.x + b.y * b.y);
}

/**
 * @brief struct to hold information about mouse pointer
 */
struct mouse_s : public cv::Point {
	/**
	 * @brief what happened while mouse pointer was on image
	 */
	int event{};
};

template<typename T>
class simple_mat {
	size_t rows_{};
	size_t cols_{};
	std::vector<std::vector<T>> matrix_;
public:
	simple_mat(int width, int height)
	    : rows_(height), cols_(width),
	      matrix_(std::vector<std::vector<T>>(height, std::vector<T>(width))) {}

	simple_mat() : simple_mat(0, 0) {}

	T& operator()(int x, int y) {
		return matrix_[y][x];
	}

	std::vector<T>& row(int y) {
		return matrix_[y];
	}

	size_t rows() {
		return rows_;
	}

	size_t cols() {
		return cols_;
	}
};



struct simple_parallelogram {
	cv::Point2d A_;
	cv::Point2d B_;
	cv::Point2d C_;
	cv::Point2d D_;

	double h_{};
	double w_{};

	bool valid_ = true;
	simple_parallelogram(const cv::Point& A, const cv::Point& B, const cv::Point& C, const cv::Point& D, double h)
	    : A_(A), B_(B), C_(C), D_(D), h_(h) {
		cv::Vec2d AB(B.x - A.x, B.y - A.y);
		cv::Vec2d AC(C.x - A.x, C.y - A.y);
		w_ = sqrt(AB(0) * AB(0) + AB(1) * AB(1));
	}

	simple_parallelogram(const cv::Point& A, const cv::Point& B, const cv::Point& C, const cv::Point& D)
	    : A_(A), B_(B), C_(C), D_(D) {}

	bool test_point_inside(const cv::Point& point) const {
		return utility::general_point_in_parellelogram(point, A_, B_, C_, D_);
	}

	simple_parallelogram() : A_(-1, -1), B_(-1, -1), C_(-1, -1), D_(-1, -1), valid_(false) {}

	inline bool valid() const { return valid_; }

	[[nodiscard]] inline double area() const { return h_ * w_; }
	[[nodiscard]] inline double get_h() const { return h_; }
};

template<typename T = int>
struct line_segment_ {
private:
	cv::Point_<T> p_{-1, -1};
	cv::Point_<T> q_{-1, -1};
	cv::Vec<T, 2> vector_{0, 0};
	double magnitude_{-1};
	double angle_{0};
	bool valid_{false};

	cv::Point_<double> middle_{0, 0};

	void check_valid_() {
		valid_ = p_.x >= 0 && p_.y >= 0 && q_.x >= 0 && q_.y >= 0 && !(p_.x == q_.x && p_.y == q_.y);
	}

	void calculate_middle_() {
		middle_.x = p_.x + vector_(0) / 2.0;
		middle_.y = p_.y + vector_(1) / 2.0;
	}

	bool swap_order_() {
		if (p_.x < q_.x || (p_.x == q_.x && p_.y < q_.y)) {
			return false;
		}
		cv::Point_<T> tmp;
		tmp = p_;
		p_ = q_;
		q_ = tmp;
		return true;
	}

	void init_params_() {
		vector_ = q_ - p_;
		if (swap_order_()) {
			vector_ *= -1;
		}
		magnitude_ = utility::vector_magnitude(vector_(0), vector_(1));
		angle_ = utility::vector_angle(vector_(0), vector_(1));
		if (angle_ >= 270) {
			angle_ -= 360;
		}
		check_valid_();
		calculate_middle_();
	}
public:
	line_segment_(T p1, T p2, T q1, T q2) : p_(p1, p2), q_(q1, q2) {
		init_params_();
	}

	line_segment_(const cv::Vec<T, 4>& v) : line_segment_(v(0), v(1), v(2), v(3)) {}

	line_segment_(const cv::Point_<T>& a, const cv::Point_<T>& b) : line_segment_(a.x, a.y, b.x, b.y) {}

	line_segment_() {};

	line_segment_(T p1, T p2, T q1, T q2, double scalar) : p_(p1, p2), q_(q1, q2) {
		init_params_();
		vector_ *= scalar;
		q_ = p_ + vector_;
	}

	line_segment_(const line_segment_& other) : p_(other.p_), q_(other.q_),
	                                            vector_(other.vector_),
	                                            magnitude_(other.magnitude_),
	                                            angle_(other.angle_),
	                                            valid_(other.valid_),
	                                            middle_(other.middle_) {}

	line_segment_& operator=(const line_segment_& other) {
		p_ = other.p_;
		q_ = other.q_;
		vector_ = other.vector_;
		magnitude_ = other.magnitude_;
		angle_ = other.angle_;
		middle_ = other.middle_;
		check_valid_();
		return *this;
	}

	~line_segment_() {}

	[[nodiscard]] const cv::Point_<T>& p() const {
		return p_;
	}

	[[nodiscard]] const cv::Point_<T>& q() const {
		return q_;
	}

	void set_p(const cv::Point_<T>& new_p) {
		p_ = new_p;
		init_params_();
	}

	void set_q(const cv::Point_<T>& new_q) {
		q_ = new_q;
		init_params_();
	}

	void set_pq(const cv::Point_<T>& new_p, const cv::Point_<T>& new_q) {
		p_ = new_p;
		q_ = new_q;
		init_params_();
	}

	line_segment_<T>& operator/=(double scalar) {
		this->p_ /= scalar;
		this->q_ /= scalar;
		init_params_();
		return *this;
	}

	line_segment_<T> operator/(double scalar) {
		return *this /= scalar;
	}

	[[nodiscard]] const cv::Vec<T, 2>& v() const {
		return vector_;
	}

	[[nodiscard]] double magnitude() const {
		return magnitude_;
	}

	[[nodiscard]] double angle() const {
		return angle_;
	}

	[[nodiscard]] bool is_valid() const {
		return valid_;
	}

	[[nodiscard]] cv::Vec<T, 4> to_cv_vec4() const {
		return cv::Vec<T, 4>(p_.x, p_.y, q_.x, q_.y);
	}

	[[nodiscard]] cv::Vec<T, 2> to_cv_vec2() const {
		return cv::Vec<T, 2>(q_.x - p_.x, q_.y - p_.y);
	}

	[[nodiscard]] cv::Point_<double> get_middle() const {
		return middle_;
	}

	[[nodiscard]] cv::Vec2d get_normal() const {
		return cv::Vec2d(v()(1) / magnitude(), -v()(0) / magnitude());
	}

	template<typename U>
	[[nodiscard]] bool intersects(const line_segment_<U>& other) const {
		return utility::lines_intersect(this->p(), this->q(), other.p(), other.q());
	}

	[[nodiscard]] line_segment_<T> average_with(const line_segment_<T>& other) const {
		return line_segment_<T>((this->p().x + other.p().x) / 2.0,
		                        (this->p().y + other.p().y) / 2.0,
		                        (this->q().x + other.q().x) / 2.0,
		                        (this->q().y + other.q().y) / 2.0);
	}

	void scale(double scalar) {
		scalar = abs(scalar);
		double change = (1 - scalar) / 2.0;
		p_ = p_ + (vector_ * change);
		q_ = q_ + (vector_ * -change);
	}

	void shrink_expand(T amount) {
		cv::Vec2d v(vector_(0) / magnitude_, vector_(1) / magnitude_);
		v *= amount;
		p_ = p_ + -v;
		q_ = q_ +  v;
	}

	double shortest_distance_to_point(const cv::Point& point) {
		cv::Vec2d ab(q_.x - p_.x, q_.y - p_.y);
		cv::Vec2d bp(point.x - q_.x, point.y - q_.y);
		cv::Vec2d ap(point.x - p_.x, point.y - p_.y);

		double ab_bp = (ab(0) * bp(0) + ab(1) * bp(1));
		double ab_ap = (ab(0) * ap(0) + ab(1) * ap(1));

		if (ab_bp > 0) {
			double y = point.y - q_.y;
			double x = point.x - q_.x;
			return sqrt(x * x + y * y);
		} else if (ab_ap < 0) {
			double y = point.y - p_.y;
			double x = point.x - p_.x;
			return sqrt(x * x + y * y);
		} else {
			return abs(ab(0) * ap(1) - ab(1) * ap(0)) / sqrt(ab(0) * ab(0) + ab(1) * ab(1));
		}
	}
};

typedef line_segment_<int> line_segment;

struct line_sequence {
private:
	std::vector<line_segment> lines_{};
	double total_magnitude_{0};
	inline double get_line_magnitude_(const line_segment& line) {
		int dx = line.q().x - line.p().x;
		int dy = line.q().y - line.p().y;
		return sqrt(dx * dx + dy * dy);
	}
	line_segment longest_{};
	double largest_magnitude_;
	uint count_;

	inline void append_(const line_segment& line) {
		lines_.push_back(line);
		double magnitude = get_line_magnitude_(line);
		total_magnitude_ += magnitude;
		if (magnitude > largest_magnitude_) {
			largest_magnitude_ = magnitude;
			longest_ = lines_.back();
		}
		++count_;
	}

public:
	enum class append_mode {
		APPEND,
		APPEND_AND_CONNECT,
		CONNECT_MIDDLES,
		CLEAR_AND_CONNECT_MIDDLES
	};

	line_sequence(const std::initializer_list<line_segment>& segments) : lines_(segments) {
		for (const auto& s : segments) {
			double magnitude = get_line_magnitude_(s);
			if (magnitude > largest_magnitude_) {
				largest_magnitude_ = magnitude;
				longest_ = s;
			}
		}
		count_ = segments.size();
	}
	[[nodiscard]] inline bool empty() const {
		return lines_.empty();
	}
	[[nodiscard]] inline size_t size() const {
		return lines_.size();
	}
	[[nodiscard]] inline uint count() const {
		return count_;
	}
	[[nodiscard]] inline double length() const {
		return total_magnitude_;
	}

	inline void append_line(const line_segment& line, const cv::Point& tail_tip, append_mode mode) {
		switch(mode) {
		case append_mode::APPEND_AND_CONNECT:
			append_(line_segment(tail_tip.x, tail_tip.y, line.p().x, line.p().y));
		case append_mode::APPEND:
			append_(line);
			break;
		case append_mode::CLEAR_AND_CONNECT_MIDDLES:
			lines_.clear();
		case append_mode::CONNECT_MIDDLES:
			append_(line_segment(tail_tip.x, tail_tip.y, line.get_middle().x, line.get_middle().y));
			break;
		}
	}

	[[nodiscard]] inline const line_segment& back() const {
		return lines_.back();
	}
	inline void clear() {
		lines_.clear();
		total_magnitude_ = 0;
	}
	line_segment& operator[](int idx) {
		return lines_[idx];
	}
	const line_segment& operator[](int idx) const {
		return lines_[idx];
	}
	[[nodiscard]] inline std::vector<line_segment>::iterator begin() {
		return lines_.begin();
	}
	[[nodiscard]] inline const std::vector<line_segment>::const_iterator begin() const {
		return lines_.begin();
	}
	[[nodiscard]] inline std::vector<line_segment>::iterator end() {
		return lines_.end();
	}
	[[nodiscard]] inline const std::vector<line_segment>::const_iterator end() const {
		return lines_.end();
	}
	[[nodiscard]] inline double get_largest_magnitude() const {
		return largest_magnitude_;
	}
	[[nodiscard]] inline const line_segment& get_longest() const {
		return longest_;
	}
};

class polygon {
	ClipperLib::Path pts_;
public:
	polygon(const std::initializer_list<cv::Point>& pts) {
		for (const auto p : pts) {
			pts_.emplace_back(p.x, p.y);
		}
	}

	polygon(const ClipperLib::Path& pts)
	    : pts_(pts) {}

	std::vector<polygon> bool_op(const polygon& other, ClipperLib::ClipType op) {
		ClipperLib::Paths solution;

		ClipperLib::Clipper c;
		c.AddPath(pts_, ClipperLib::ptSubject, true);
		c.AddPath(other.pts_, ClipperLib::ptClip, true);
		c.Execute(op, solution, ClipperLib::pftNonZero, ClipperLib::pftNonZero);

		std::vector<polygon> ret;
		for (const auto& s : solution) {
			ret.emplace_back(s);
		}
		return ret;
	}

	bool intersects(const polygon& other) {
		return !bool_op(other, ClipperLib::ctIntersection).empty();
	}

	const std::vector<cv::Point> points() const {
		std::vector<cv::Point> ret;
		std::transform(pts_.begin(), pts_.end(), std::back_inserter(ret), [](const ClipperLib::IntPoint& clp) {return cv::Point(clp.X, clp.Y); });
		return ret;
	}

	cv::Point operator[](int i) const {
		return cv::Point(pts_[i].X, pts_[i].Y);
	}

	template<typename T, typename U>
	static bool point_in_polygon(const cv::Point_<U>& point, const T& polygon) {
	  int i, j, nvert = polygon.size();
	  bool c = false;
	  for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		  if(((polygon[i].y >= point.y ) != (polygon[j].y >= point.y) ) &&
		          (point.x <= (polygon[j].x - polygon[i].x) * (point.y - polygon[i].y) / (polygon[j].y - polygon[i].y) + polygon[i].x)) {
			  c = !c;
		  }
	  }
	  return c;
	}
};

template<typename T>
class linked_list {
	template<typename U>
	class node { // double-linked node
		friend class linked_list<U>;
		node<U>* prev_ = nullptr;
		std::unique_ptr<node<U>> next_;
		T data_;
	public:
		node<U>* next() { return next_.get(); }
		node<U>* prev() { return prev_; }

		const U& data() const {
			return data_;
		}

		node(const U& data) : data_(data) {}
	};

private:
	using ptr = std::unique_ptr<node<T>>;

	ptr first_;
	node<T>*  last_ = nullptr;

public:
	void clear() {
		first_ = nullptr;
		last_  = nullptr;
	}

	node<T>* push_back(ptr&& item) {
		if (!first_) {
			first_ = std::move(item);
			last_  = first_.get();
		} else {
			last_->next_ = std::move(item);
			last_->next_->prev_ = last_;
			last_ = last_->next_.get();
		}
		return last_;
	}

	template<typename U>
	node<T>* push_back(const U& item) {
		return push_back(std::make_unique<node<T>>(node<T>(item)));
	}

	void concat(linked_list<T>& other) {
		if (!other.first_ ) return;
		if (last_ ) {
			last_->next_ = std::move( other.first_ );
			last_->next_->prev_ = last_;
			last_ = other.last_;
		} else {
			first_ = std::move( other.first_ );
			last_  = other.last_;
		}
	}

	node<T>* erase(node<T>* item) {
		node<T>* ret = item->next() ? item->next() : item->prev() ? item->prev() : nullptr;
		if (item->next_) {
			item->next_->prev_ = item->prev_;
		} else {
			last_ = item->prev_;
		}
		ptr owner;
		if (item->prev_) {
			owner = std::move(item->prev_->next_);
			item->prev_->next_ = std::move(item->next_);
			item->prev_ = nullptr;
		} else {
			owner = std::move(first_);
			first_ = std::move(item->next_);
		}
		return ret;
	}

	[[nodiscard]] node<T>* first() { return first_.get();}
	[[nodiscard]] const node<T>* first() const { return first_.get(); }

	[[nodiscard]] node<T>* last() { return last_; }
	[[nodiscard]] const node<T>* last() const { return last_; }
};

/**
 * @brief function to get a hash code of cv::Mat
 * @param im        image to hash
 * @param hash_size size of hash
 * @return          image's hash code
 */
int dhash(cv::Mat& im, int hash_size = 8);

namespace image_modification {
	// TODO remove magic numbers here, have them as parameters
	void squish_value_channel(bool compare_on_saturation, const cv::Mat& hsv,
	                          int value_thresh_low,
	                          int value_thresh_low_onsat, int value_thresh_high,
	                          int gray_threshold, int precision,
	                          bool use_out_of_range_vals, uchar out_of_range_black, uchar out_of_range_white);

	void squish_low_value_single_im(const cv::Mat& img, uchar value_thresh,
	                                bool use_out_of_range_vals, uchar out_of_range_black);
	void squish_low_value_split_im(matarr_3& mats, uchar value_thresh,
	                               int gray_thresh_sat, int gray_thresh_val,
	                               bool use_out_of_range_vals, uchar out_of_range_black);
	void squish_low_value_many_im(std::vector<matarr_3>& mats, uchar value_thresh,
	                              int gray_thresh_sat, int gray_thresh_val,
	                              bool use_out_of_range_vals, uchar out_of_range_black);

	void squish_high_value_single_im(const cv::Mat& img, uchar value_thresh,
	                                 bool use_out_of_range_vals, uchar out_of_range_white);
	void squish_high_value_split_im(matarr_3& mats, uchar value_thresh,
	                                int gray_thresh_sat, int gray_thresh_val,
	                                bool use_out_of_range_vals, uchar out_of_range_white);
	void squish_high_value_many_im(std::vector<matarr_3>& mats, uchar value_thresh,
	                               int gray_thresh_sat, int gray_thresh_val,
	                               bool use_out_of_range_vals, uchar out_of_range_white);

	void sharpen(cv::Mat& img);
	void color_correction(const cv::Mat& hsv, uchar low_value_thresh, uchar high_value_thresh, bool sharpen, bool use_out_of_range_vals, uchar out_of_range_black, uchar out_of_range_white);

	void shift_hue_cv_mat(const cv::Mat& img, bool use_out_of_range_vals, uchar out_of_range_black);
	void shift_hue_split(matarr_3& mats, bool use_out_of_range_vals, uchar out_of_range_black);
	void shift_hue_vsplit(std::vector<matarr_3>& mats, bool use_out_of_range_vals, uchar out_of_range_black);
}

class plot {
public:
	enum color {
		W = 0b000000,
		B = 0b000001,
		G = 0b000010,
		C = 0b000011,
		R = 0b000100,
		M = 0b000101,
		Y = 0b000110,
		K = 0b001111,
	};

	enum style {
		CIRCLE = 0b010000,
		POINT  = 0b100000,
		LINE   = 0b110000,
		NONE   = 0b000000
	};

	class line_style {
		std::string spec_;
	public:
		line_style(const std::string& line_spec) : spec_(line_spec) {}
		line_style(const char* line_spec) : spec_(line_spec) {}
		line_style(int line_spec) {
			int style = line_spec & 0b110000;
			int color = line_spec & 0b001111;
			std::string spec = "";
			switch (style) {
			case CIRCLE:
				spec.append("o");
				break;
			case POINT:
				spec.append(".");
				break;
			case LINE:
				spec.append("-");
			default:
				break;
			}
			switch (color) {
			case R:
				spec.append("r");
				break;
			case G:
				spec.append("g");
				break;
			case B:
				spec.append("b");
				break;
			case C:
				spec.append("c");
				break;
			case M:
				spec.append("m");
				break;
			case Y:
				spec.append("y");
				break;
			case K:
				spec.append("k");
				break;
			case W:
				spec.append("w");
				break;
			}
			spec_ = spec;
		}

		const std::string& get_spec() const {
			return spec_;
		}
	};

private:
	CvPlot::Axes plot_;
public:
	inline void show(const std::string& name) {
		CvPlot::show(name, plot_);
	}
	inline cv::Mat render(int rows, int cols) {
		return plot_.render(rows, cols);
	}
	template<typename U>
	inline void add_plot(const U& data, const line_style& line_style) {
		plot_.create<CvPlot::Series>(data, line_style.get_spec());
	}
	template<typename T>
	inline void add_plot(const T& x, const T& data, const line_style& line_style) {
		plot_.create<CvPlot::Series>(x, data, line_style.get_spec());
	}
	template<typename T, typename U>
	inline void add_plot(const T& x, const U& data, const line_style& line_style) {
		plot_.create<CvPlot::Series>(x, data, line_style.get_spec());
	}
	template<typename T>
	inline void add_plot(const T& x, const std::vector<T>& data, const line_style& line_style) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_style.get_spec());
		}
	}
	template<typename T, typename U>
	inline void add_plot(const T& x, const std::vector<U>& data, const line_style& line_style) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_style.get_spec());
		}
	}
	template<typename T>
	inline void add_plot(const T& x, const std::vector<T>& data, const std::vector<line_style>& line_styles) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_styles[i % data.size()].get_spec());
		}
	}
	template<typename T, typename U>
	inline void add_plot(const T& x, const std::vector<U>& data, const std::vector<line_style>& line_styles) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_styles[i % data.size()].get_spec());
		}
	}

	template<typename T>
	plot(const T& data, line_style line_style) : plot_(CvPlot::makePlotAxes()) {
		plot_.create<CvPlot::Series>(data, line_style.get_spec());
	}
	template<typename T>
	plot(const T& x, const T& data, line_style line_style) : plot_(CvPlot::makePlotAxes()) {
		plot_.create<CvPlot::Series>(x, data, line_style.get_spec());
	}
	template<typename T, typename U>
	plot(const T& x, const U& data, line_style line_style) : plot_(CvPlot::makePlotAxes()) {
		plot_.create<CvPlot::Series>(x, data, line_style.get_spec());
	}
	template<typename T>
	plot(const T& x, const std::vector<T>& data, line_style line_style) : plot_(CvPlot::makePlotAxes()) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_style.get_spec());
		}
	}
	template<typename T, typename U>
	plot(const T& x, const std::vector<U>& data, line_style line_style) : plot_(CvPlot::makePlotAxes()) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_style.get_spec());
		}
	}
	template<typename T>
	plot(const T& x, const std::vector<T>& data, const std::vector<line_style>& line_styles) : plot_(CvPlot::makePlotAxes()) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_styles[i % line_styles.size()].get_spec());
		}
	}
	template<typename T, typename U>
	plot(const T& x, const std::vector<U>& data, const std::vector<line_style>& line_styles) : plot_(CvPlot::makePlotAxes()) {
		for (size_t i = 0; i < data.size(); ++i) {
			plot_.create<CvPlot::Series>(x, data[i], line_styles[i % line_styles.size()].get_spec());
		}
	}

	template<typename T>
	plot(const T& data) : plot(data, B) {}
	template<typename T>
	plot(const T& x, const T& data) : plot(x, data, B) {}
	template<typename T, typename U>
	plot(const T& x, const U& data) : plot(x, data, B) {}
	template<typename T>
	plot(const T& x, const std::vector<T>& data) : plot(x, data, {B}) {}
	template<typename T, typename U>
	plot(const T& x, const std::vector<U>& data) : plot(x, data, {B}) {}
};

namespace custom_cv {
	// currently only to provide ability to create a vertically concatenated output image from drawing matches
	void draw_matches(cv::InputArray img1, const std::vector<cv::KeyPoint>& keypoints1,
	                  cv::InputArray img2, const std::vector<cv::KeyPoint>& keypoints2,
	                  const std::vector<cv::DMatch>& matches1to2, cv::InputOutputArray outImg,
	                  const cv::Scalar& matchColor, const cv::Scalar& singlePointColor,
	                  const std::vector<char>& matchesMask, cv::DrawMatchesFlags flags);
} // namespace custom_cv
