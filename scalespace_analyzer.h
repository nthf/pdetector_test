#pragma once

#include "ad_sequencer.h"
#include "terminal_color_output.h"
#include "utility.h"

#include <opencv2/core.hpp>
#include <stack>
#include <thread>


enum detector_status {
	S_UNSET = -1, // S_UNRISE
	S_SUCCESS,
	S_FILE_NOT_OPEN,
	S_FILE_NOT_FOUND,
	S_IMAGE_OPEN_UNSUCCESSFUL,
	S_VIDEO_OPEN_UNSUCCESSFUL,
	S_GOT_IMAGE_WHEN_EXPECTED_VIDEO
};

class line_ss_value {
	std::vector<uchar> values_{};
	uchar val_{};
	int count_{};
	bool bg_{};
public:
	line_ss_value(std::vector<uchar>&& values, uchar val, int count, bool bg)
	    : values_(std::move(values)), val_(val), count_(count), bg_(bg) {}
	line_ss_value(const std::vector<uchar>& values, uchar val, int count, bool bg, size_t new_size = 1)
	    : values_(values), val_(val), count_(count), bg_(bg) {
		if (bg) {
			values_.resize(new_size, values.front());
		}
	}
	line_ss_value(const line_ss_value& other)
	    : values_(other.values_), count_(other.count_), bg_(other.bg_) {}
	line_ss_value() {}

	inline line_ss_value& operator=(const line_ss_value& other) {
		if (this != &other) {
			values_.clear();
			values_.resize(other.values_.size());
			std::copy(other.values_.begin(), other.values_.end(), values_.begin());
			count_ = other.count_;
			bg_ = other.bg_;
		}
		return *this;
	}

	inline operator const std::vector<uchar>() const {
		return get_values();
	}

	inline operator int() const {
		return get_count();
	}

	[[nodiscard]] inline uchar get_val() const { return val_; }
	[[nodiscard]] inline int get_count() const { return count_; }
	inline void set_count(int count) { count_ = count; }
	[[nodiscard]] inline bool is_bg() const { return bg_; }
	inline void set_bg(bool bg) { bg_ = bg; }
	[[nodiscard]] inline const std::vector<uchar>& get_values() const { return values_; }
	inline void clear() { values_.clear(); count_ = 0; bg_ = false; }
	inline void push_back(uchar v) { values_.push_back(v); ++count_; }
	[[nodiscard]] inline uchar front() const { return values_.front(); }
	[[nodiscard]] inline bool empty()  const { return values_.empty(); }
	inline line_ss_value& operator++() { ++count_; return *this; }
	inline line_ss_value operator++(int) { line_ss_value tmp(*this); operator++(); return tmp; }

	[[nodiscard]] inline size_t size() const { return values_.size(); }

	uchar operator[](std::size_t idx)       { return values_[idx]; }
	uchar operator[](std::size_t idx) const { return values_[idx]; }


	static std::string vector_as_string(const std::vector<line_ss_value>& vec, bool simple) {
		rope::rope_stringbuilder rope_sb;
		const char* separator = "";
		for (const auto& p : vec) {
			rope_sb << separator << utility::vector_as_string(p.values_);
			if (!simple) rope_sb << colorex(212);
			rope_sb << ":" << p.count_;
			if (!simple) rope_sb << reset;
			separator = "|";
		}
		return rope_sb.str();
	}

};

/**
 * @brief handles the logic of the scalespace detection part of the algorithm
 */
class scalespace_analyzer {
	/// @brief factor to resize the img images with in both directions
	double const resize_{0.5};

	double const rect_accept_ratio_{0.025}; // 0.025 is okay
	/// @brief number of levels of scalespace to generate when halving sigma
	int const ss_generation_levels_{3};
	/**
	 * @brief number of vector samples that must be
	 *        reached before the vector is considered (when calculating vector_counts)
	 */
	int const at_least_vector_samples_{static_cast<int>(80 * resize_)};
	int const color_samples_until_exclusion_{6};
	/// @brief maximum number of colors that can be found before continuing
	int const color_count_threshold_{10};
	/// @brief tolerance when comparing hues
	int const hue_tolerance_{8};
	/// @brief tolerance when comparing saturations
	int const sat_tolerance_{40};
	/**
	 * @brief threshold above which colors are considered
	 *        white should their saturation be below gray_threshold_
	 */
	int const high_value_threshold_{180};
	/// @brief saturation threshold below which the color is deemed gray
	int const gray_threshold_sat_{85};
	int const gray_threshold_val_{50};
	/**
	 * @brief factor to perform integer division and
	 *        multiplication with to reduce the number of unique numbers
	 */
	int const value_round_down_{1};
	/**
	 * @brief factor to divide image size by to get the
	 *        initial size of a rectangle used in rectangle search
	 */
	int const rectangle_size_factor_{7};
	/// @brief number of gaps allowed in a successful rectangle search
	int const max_gaps_allowed_{3};
	/// @brief multiplier to search-rectangle height to shift search-area vertical borders
	double const r_search_padding_f_{1.5};

	/// @brief width of structuring element used in morphological opening
	static int const morph_open_se_width_{10};
	/// @brief minimum number of matches required for a positive detection result
	uint const match_count_minimum_{static_cast<uint>(400 * resize_ * resize_)};
	/**
	 * @brief flag to determine whether resize_ should be used
	 *        (true) or if the resizing factor should be calculated (false)
	 */
	bool const constant_resize_{true};

	/**
	 * @brief debug constant, marks last frame reached when
	 *        generating ground truth
	 */
	static int const GT_GEN_FRAME_LAST;

	/**
	 * @brief flag for whether color correction should be
	 *        done before scalespace generation
	 */
	static bool const CORRECT_COLORS_PRE_SSGEN;

	/**
	 * @brief flag for whether to encode black and
	 *        white colors as values in the hue channel beyond OpenCV's range (0-179)
	 *        where white is 230 and black is 210
	 */
	static bool const USE_OUT_OF_RANGE_HUE_VALUES;
	static int const OUT_OF_RANGE_WHITE;
	static int const OUT_OF_RANGE_BLACK;
	static char const OUT_OF_RANGE_RED;

	/// @brief hue that should be skipped
	static uchar const RESERVED_UNUSED_HUE;

	/// @brief path where ground truth should be written if it is being generated
	const std::string gt_path_ = paths::ground_truth_path;
	/// @brief output stream for ground truth
	std::ofstream output_gt_;

	/// @brief path to file that results shall be written to
	const std::string output_file_ = paths::result_path;
	/// @brief output stream for results
	std::ofstream output_;
	/// @brief collection where results are held before being logged
	std::vector<std::pair<bool, long long>> results_;

	/**
	 * @brief hue that should not be considered for matches,
	 *        use to remove field matching
	 */
	int16_t excluded_hue_ = -1;

	/// @brief flag for whether the advertisement panel has a centerboard
	bool has_center_ = true;
	/// @brief convenient access to the centerboard of the advertisement panel
	centerboard centerboard_;

	/// @brief argc from command line argument
	int argc_{};
	/// @brief argv from command line argument
	char** argv_{};

	/// @brief widths of image a and image b
	int width_[2]{};
	/// @brief heights of image a and image b
	int height_[2]{};
	/// @brief framerates of image a and image b
	int fps_[2]{};
	/// @brief fourcc codes of image a and image b
	int fourcc_[2]{};
	/// @brief frame counts of image a and image b
	int frame_count_[2]{};

	/// @brief mask for frame a to decrease data volume
	cv::Mat a_ref_;
	/// @brief current image a
	cv::Mat frame_a_;
	/// @brief current image b
	cv::Mat frame_b_;
	/// @brief resized image a
	cv::Mat ressz_a_;
	/// @brief resize image b
	cv::Mat ressz_b_;

	/// @brief image a and image b in a single array
	std::array<cv::Mat*, 2> frames_{};

	/**
	 * @brief depending on saturation of image b this holds saturation
	 *        or hue channels of image a and image b
	 */
	std::array<cv::Mat*, 2> im_primary_{};
	/**
	 * @brief if im_primary_ contains hue channels, this contains
	 *        saturation channels and vice-versa
	 */
	std::array<cv::Mat*, 2> im_secondary_{};

	std::array<cv::Mat*, 2> im_tertiary_{};

	/// @brief current processed frame's number
	int frame_num_{0};

	/**
	 * @brief flag for whether scalespace comparison is done using the saturation
	 *        channel or not (in which case hue channel is used instead)
	 */
	bool sat_comp_ = false;

	/// @brief flag to use when debugging
	bool stepping_mode_{false};
	/**
	 * @brief list of frames where the program should automatically
	 *        pause while in stepping mode
	 */
	std::vector<int> step_list_{1};

	/// @brief short memory for past results to remove insanity
	std::deque<int> result_history_;

	std::vector<double> scale_weights{};

	/**
	 * @brief get the next frame from cap and save it to frame, save frame
	 *        to frames_ at index
	 * @param cap   video capture to advance
	 * @param frame next frame gets saved into this
	 * @param index index for storing in frames_, 0 for image a, 1 for image b
	 */
	inline void advance(cv::VideoCapture& cap, cv::Mat& frame, int index) {
		cap >> frame;
		frames_.at(index) = &frame;
	}

	/**
	 * @brief generates scalespace for img
	 * @param img        img image
	 * @param scalespace result is saved into this
	 */
	void parallel_gen_scalespace_exp(cv::Mat& img, vmat& vmat);
	/**
	 * @brief generates scalespace for img
	 * @param img        img image
	 * @param scalespace result is saved into this
	 */
	void parallel_gen_scalespace_halving(cv::Mat& img,
	                                            vmat& vmat);

	/**
	 * @brief calls the correct scalespace generation methods
	 *        for its inputs
	 * @param frame_a      image a, has parallel_gen_scalespace_exp called on it
	 * @param frame_b      image b, has parallel_gen_scalespace_halving called on it
	 * @param scalespace_a result for image a is saved into this
	 * @param scalespace_b result for image b is saved into this
	 */
	void generate_scalespace(cv::Mat& frame_a, cv::Mat& frame_b,
	                         vmat& scalespace_a, vmat& scalespace_b);
	/**
	 * @brief calls a scalespace generation method depending
	 *        on chosen mode
	 * @param img        img image
	 * @param scalespace result for img image is saved into this
	 * @param mode       determines which scalespace generation
						 method is called ('a' - exp, 'b' - halving)
	 */
	void generate_scalespace(cv::Mat& img, vmat& vmat,
	                                char mode);

	/**
	 * @brief compares color hues
	 * @param a hue a
	 * @param b hue b
	 * @return      true if the absolute difference of hue a and hue b is less or equal
	 *              than HUE_TOLERANCE
	 * @see         HUE_TOLERANCE
	 */
	inline bool hue_compare(int a, int b) {
		if (excluded_hue_ >= 0 && (std::abs(a - excluded_hue_) < hue_tolerance_ || std::abs(b - excluded_hue_) < hue_tolerance_)) {
			return false;
		}
		if (a < 10 && b < 10) {
			return true;
		}
		if (std::max(a, b) > 180) {  // values above 180 are used for special colors
			// - black, white, red
			return a == b;
			//return abs(a - b) <= hue_tolerance_;
		}
		return std::min(abs(a - b), 180 - abs(a - b)) <= hue_tolerance_;
	}

	/**
	 * @brief compares color hues
	 * @param a         hue a
	 * @param b         hue b
	 * @param tolerance tolerance
	 * @return      true if the absolute difference of hue a and hue b is less or equal
	 *              than tolerance
	 */
	inline bool hue_compare_wt(int a, int b, int tolerance) {
		if (excluded_hue_ >= 0 && (std::abs(a - excluded_hue_) < tolerance ||
		                           std::abs(b - excluded_hue_) < tolerance)) {
			return false;
		}
		if (a < tolerance && b < tolerance) {
			return true;
		}
		if (std::max(a, b) > 180) {  // values above 180 are used for special colors
			// - black, white, red
			return a == b;
			//return abs(a - b) <= hue_tolerance_;
		}
		return std::min(abs(a - b), 180 - abs(a - b)) <= tolerance;
	}

	inline bool hue_compare_relative_progress(int a, int b, int tolerance, nullable_int& prev_a, nullable_int& prev_b) {
		if (excluded_hue_ >= 0 && (std::abs(a - excluded_hue_) < hue_tolerance_ || std::abs(b - excluded_hue_) < hue_tolerance_)) {
			return false;
		}
		if (prev_a.is_null()) {
			prev_a = a;
			prev_b = b;
			return true;
		}
		//if (a == OUT_OF_RANGE_WHITE && a == b) {
		//	return true;
		//}
		int da = a - prev_a;
		int db = b - prev_b;

		prev_a = a;
		prev_b = b;
		//if (da * db < 0) {
		int diff = std::abs(da - db);
		return !((da * db < 0) && (diff >= tolerance)) || diff > 165;
		//	return false;
		//}
		//return true;
		//return std::abs(da - db) < tolerance;
	}

	/**
	 * @brief compares color es with custom tolerance
	 * @param a         hue a
	 * @param b         hue b
	 * @param tolerance tolerance to use when comparing hues
	 * @return          true if the absolute difference of hue a and hue b is less or equal
	 *                  than tolerance if both hues are below 180. Above 180 true is only returned
	 *                  if the hues are equal
	 */
	inline bool hue_compare_w_tolerance(int a, int b, int tolerance) {
		if (excluded_hue_ >= 0 && (std::abs(a - excluded_hue_) < hue_tolerance_ || std::abs(b - excluded_hue_) < hue_tolerance_)) {
			return false;
		}
		if (std::max(a, b) < 10 || std::max(a, b) > 180) {
			return a == b;
		}
		return std::min(abs(a - b), 180 - abs(a - b)) <= tolerance;
	}

	/**
	 * @brief compares color saturations
	 * @param a saturation a
	 * @param b saturation b
	 * @return  true if the absolute difference of saturation a and saturation b is
	 *          less or equal than SAT_TOLERANCE
	 * @see     SAT_TOLERANCE
	 */
	inline bool saturation_compare(int a, int b) {
		return std::min(abs(a - b), 255 - abs(a - b)) <= sat_tolerance_;
	}

	/**
	 * @brief the weight of the values based on their
	 *        variation, used when preparing image for rectangle search and chosen scale
	 *        was large enough
	 * @param values values to be evaluated
	 * @return       weight that decides how much given values should contribute
	 */
	double get_weight(const std::vector<uchar>& values);

	/**
	 * @brief places x along an increasing convex curve
	 * @param x weight
	 * @return  square of x
	 */
	inline double get_weight_curve(double x) {
		// return 3.797379 + (-3.77527755)/(1 + pow((x / 1.485998), 2.656611));
		return x * x;
	}

	/**
	 * @brief searches img vectors for areas of similarity,
	 *        saves the index where a similarity was found into offset
	 * @param values_a             img vector a
	 * @param values_b             img vector b
	 * @param offset               result of search is saved here
	 * @param comparing_saturation determines whether hue_color_match or
	 *                             sat_color_match is used
	 * @return                     true if any similarity was found
	 */
	bool seek_similarities(const std::vector<uchar>& values_a,
	                       const std::vector<uchar>& values_b, int* offset,
	                       bool comparing_saturation);
	/**
	 * @brief searches img vectors for areas of similarity,
	 *        saves the index where a similarity was found into offset also gives finds
	 *        at different scales different weights
	 * @param values_a             img vector a
	 * @param values_b             img vector b
	 * @param x
	 * @param y
	 * @param scale_sequence       scales are saved into this in the order that they are
	 *                             found together with the weights obtained from img vector a
	 * @param scale_count          counter for the number of matches for each scale
	 * @param weight_count         counter for the weights
	 * @param comparing_saturation determines whether hue_color_match or
	 *                             sat_color_match is used
	 * @param bg                   flag for when comparison is done using a background color
	 * @return                     true if any similarity was found
	 */
	bool seek_similarities(const std::vector<uchar>& values_a,
	                       const line_ss_value& values_b, int x, int y,
	                       std::vector<std::pair<uint, uint>>& scale_sequence,
	                       std::map<int, int>& scale_count,
	                       std::map<double, double>& weight_count,
	                       bool comparing_saturation, bool differential, bool bg);

	/**
	 * @brief saves the contents of matches into points
	 * @param points  collection of weighted 3D points,
	 *                their structure is: (x, y, scale, weight)
	 * @param matches collection of scales and their weights
	 * @param x       x coordinate
	 * @param y       y coordinate
	 */
	void populate_point_vector(std::vector<WPoint3>& points,
	                           std::vector<std::pair<uint, uint>>& matches,
	                           int x, int y);

	/**
	 * @brief iterates through a line of values which are
	 *        equally far from the borders of the image in scalespace and creates an
	 *        array which holds the values from each level of the scalespace
	 * @param base_img      base image
	 * @param ss scalespace to look through
	 * @param offset        distance from the image border
	 * @param length length of the line
	 * @param unique_colors unique colors are counted and the
	 *                      result of that is stored here
	 * @param background    store detected background color value here
	 * @return              map with vector counts
	 */
	std::vector<line_ss_value> get_values_on_line(bool round_down, const cv::Mat& base_img, const vmat& ss,
	        int offset, int length, std::map<uchar, int>& unique_colors, int* background);

	/**
	 * @brief performs binary morphology on img img, first
	 *        closing, then opening
	 * @param img      binary image
	 * @param count    number of non-zero pixels is stored here after processing
	 * @param se_width width of structure element, a line is used
	 */
	void rect_se_morphology(cv::Mat& img, uint* count, morph_ops op,
	                        int se_width, int se_height);

	/**
	 * @brief performs binary morphology on img img, first
	 *        closing, then opening
	 * @param img    binary image
	 * @param count  number of non-zero pixels is stored here after processing
	 */
	inline void line_se_morphology(cv::Mat& img, uint* count, morph_ops op, int width = morph_open_se_width_) {
		rect_se_morphology(img, count, op, width, 1);
	}

	/**
	 * @brief draws points stored in a collection into a binary image
	 * @param match_points points to draw
	 * @param match_count  number of found matches
	 * @param scale        dominant scale for the image obtained earlier
	 * @param h            height of the image
	 * @param w            width of the image
	 * @param mask         binary image that masks the result
	 * @return grayscale   or binary image of points from match_points, depending on
	 *                     whether the weights of the points were used
	 */
	cv::Mat points_to_image(const std::vector<WPoint3>& match_points,
	                        uint match_count, int scale, int h, int w,
	                        cv::Mat& mask);

	/**
	 * @brief checks if a clump of interest in an image contains
	 *        any non-zero pixels
	 * @param img     image to check
	 * @param r       clump of interest
	 * @param overlap shifts the border to allow overlap without
	 *                breaking the math of rectangle search
	 * @return        ratio of nonzero pixels to total
	 */
	double nonzero_image(const cv::Mat& img, const rect& r, int overlap = 2);
	/**
	 * @brief performs a check whether it would be possible to continue
	 *        in rectangle search should there be a gap allowed
	 * @param img image to check
	 * @param rec clump of interest
	 * @param out image used to save a visualization of the result
	 * @return    pair of: true if gap would allow further search
	 *            and the rectangle where gap jump ended at
	 */
	std::pair<bool, cv::Rect> gap_check(cv::Mat& img, cv::Rect& rec,
	                                    cv::Mat& out);
	/**
	 * @brief checks if only one significant line was found
	 *        in rectangle search
	 * @param lengths  lengths of attempted searches
	 * @param expected length that is expected to be reached by only one line
	 * @return         true if only one length is greater or equal than expected
	 */
	bool one_significant_line(std::vector<int>& lengths, int expected);
	/**
	 * @brief determines the locations and sizes of new
	 *        rectangles for rectangle search and adds them to a queue
	 * @param s        stack of rectangles to search
	 * @param searched set of rectangles already searched
	 * @param img      image that is being searched
	 * @param out      image used for visualization of rectangle search
	 * @param binary   flag to determine if rectangle search is binary or not
	 * @param r        last searched clump of interest
	 * @param w        width of rectangles
	 * @param h        height of rectangles
	 */
	void add_new_rectangles(std::stack<rect>& s, std::set<rect, rcomp>& searched,
	                        const cv::Mat& img, cv::Mat& out, bool binary,
	                        rect& r, int w, int h);
	/**
	 * @brief check if the number of matches is a sensible amount
	 *        for the given scale
	 * @param match_count counter of matches
	 * @param color_count counter of colors
	 * @param scale       scale that determines the limit of matches
	 * @param img         image that is being processed
	 * @return            true if match_count is satisfactory
	 */
	bool check_max_count(uint match_count, uint color_count, uint scale,
	                     const cv::Mat& img);
	/**
	 * @brief initializes the rectangle search
	 * @param h            height of rectangles
	 * @param w            width of rectangles
	 * @param gaps_allowed number of gaps allowed in the search
	 * @param binary       flag determining if search is binary or not
	 * @param max_slope    maximum slope length allowed in a binary rectangle search
	 * @param match_count  number of matches
	 * @param scale        dominant scale for the image obtained earlier
	 * @param max_scale    maximum scale that can be reached
	 * @param img          image that is being processed
	 */
	void init_rectangle_search(int* h, int* w, int* gaps_allowed, bool* binary,
	                           int* max_slope, uint match_count, int scale,
	                           int max_scale, const cv::Mat& img);

	/**
	 * @brief checks if the combination of the two rectangles covers anything significant where they meet
	 * @param img       image to check within
	 * @param curr_r    current rectangle
	 * @param next_r    next rectangle in stack
	 * @param min_ratio minimal ratio of fill that combined rectangle needs
	 * @return true if combined rectangle fill ratio is above min_ratio
	 */
	bool test_combined_between_rect(const cv::Mat& img, rect& curr_r, rect& next_r, double min_ratio);

	/**
	 * @brief handles the control of generating new rectangles
	 *        after one was accepted
	 * @param j         current iteration
	 * @param length    length of the current rectangle sequence
	 * @param binary    flag determining if search is binary or not
	 * @param w         width of rectangles
	 * @param h         height of rectangles
	 * @param slope     current slope
	 * @param out       image used for visualization of rectangle search
	 * @param r         accepted rectangle
	 * @param s         stack of rectangles to search
	 * @param searched  set of rectangles already searched
	 * @param img       image that is being processed
	 * @param lengths   lengths of all rectangle sequences
	 * @param max_slope maximum slope
	 * @param max_up    maximum distance rectangles can travel up
	 * @param max_dn    maximum distance rectangles can travel down
	 * @param fill      ratio of non-zero pixels in rectangle
	 * @param min_ratio minimum accepted fill ratio
	 * @return          true if it is possible to continue the search from r or
	 *                  if the search successfully ends at r
	 */
	bool accept_rectangle(int* j, int* length, const bool* binary, const int* w,
	                      const int* h, int* slope, cv::Mat& out, rect& r,
	                      std::stack<rect>& s, std::set<rect, rcomp>& searched,
	                      const cv::Mat& img, std::vector<int>& lengths,
	                      int max_slope, int* max_up, int* max_dn,
	                      double* fill = nullptr, double min_ratio = 0.0);

	bool good_verticality(cv::Mat& img, int w, int h);

	/**
	 * @brief looks for a structure made of rectangles
	 * @param img         img image to search
	 * @param match_count number of matches
	 * @param binary      flag determining if search is binary or not
	 * @return            true if search was successful
	 */
	bool rectangle_structure_search(cv::Mat& img, uint match_count,
	                                bool binary = true);
	/**
	 * @brief looks for a structure made of rectangles
	 * @param img       img image to search
	 * @param number    of matches
	 * @param number    of colors
	 * @param scale     dominant scale for the image obtained earlier
	 * @param max_scale maximum scale that can be reached
	 * @return          true if search was successful
	 */
	bool rectangle_structure_search(cv::Mat& img, uint match_count,
	                                uint color_count, int scale, int max_scale);

	/**
	 * @brief initializes the process of scalespace
	 *        similarity matching
	 * @param n             number of pixels in img image
	 * @param match_count   number of matches is saved here
	 * @param bg_color      value of background color
	 * @param vector_counts counts of vectors from scalespace line are saved here
	 * @param match_points  points where match was found
	 * @param base_image_main_primary img image a
	 * @param base_image_alt_primary img image b
	 * @param ss_to_detect scalespace to detect
	 * @return true if scalespace matching may commence
	 */
	bool init_scalespace_matching(int* n, uint* match_count, int* bg_color,
	                              std::vector<line_ss_value>& line_values,
	                              std::vector<WPoint3>& match_points,
	                              const cv::Mat& base_image_main_primary,
	                              const cv::Mat& base_image_alt_primary,
	                              const vmat& ss_to_detect);
	/**
	 * @brief initializes the process of scalespace
	 *        similarity matching
	 * @param n                       number of pixels in img image
	 * @param match_count             number of matches is saved here
	 * @param vector_counts           counts of vectors from scalespace line are saved here
	 * @param match_points            points where match was found
	 * @param base_image_main_primary img image a
	 * @return                        true if scalespace matching may commence
	 */
	bool init_scalespace_matching(bool sat_comp, int* n, uint* match_count,
	                              vpair_vuc_i& vpair_vuc_i,
	                              std::vector<WPoint3>& match_points,
	                              const cv::Mat& base_image_main_primary);
	/**
	 * @brief compares scalespaces to find similarities
	 * @param im_base_a             one channel from img image a
	 * @param im_base_b             one channel from img image b
	 * @param sc_im_base_a          one secondary channel from img image a
	 * @param sc_im_base_b          one secondary channel from img image b
	 * @param ss_to_search          scalespace that is to be searched
	 * @param ss_to_detect          scalespace that is to be detected
	 * @param compare_on_saturation flag to determine whether hue or saturation is
	 *                              the primary channel
	 * @param most_common_hue       most common hue of image b
	 * @return                      true if scalespaces are similar
	 */
	bool compare_scalespaces(const cv::Mat& im_base_a, const cv::Mat& im_base_b,
	                         const cv::Mat& sc_im_base_a,
	                         const cv::Mat& sc_im_base_b,
	                         const vmat& ss_to_search,
	                         const vmat& ss_to_detect,
	                         bool compare_on_saturation = false,
	                         int most_common_hue = -1);
	/**
	 * @brief compares scalespaces to find similarities
	 * @param im_base_a             one channel from img image a
	 * @param sc_im_base_a          one secondary channel from img image a
	 * @param ss_to_search          scalespace that is to be searched
	 * @param compare_on_saturation flag to determine whether hue or saturation is
	 *                              the primary channel
	 * @param most_common_hue       most common hue of image b
	 * @return                      true if scalespaces are similar
	 */
	bool compare_scalespaces(const cv::Mat& im_base_a,
	                         const cv::Mat& sc_im_base_a,
	                         const vmat& ss_to_search,
	                         bool compare_on_saturation = false,
	                         int most_common_hue = -1);

	/**
	 * @brief determines if the image is primarily low in saturation
	 * @param img             image to check
	 * @param rat             ratio of high to low saturation pixels of the image
	 * @param most_common_hue most common hue from the image
	 * @param msg             flag to determine if a message should be output
	 * @return                true if image is primarily low in saturation
	 */
	bool gray_image(const matarr_3& img, double* rat, int* most_common_hue,
	                bool msg = true);

	/**
	 * @brief determines if the image is primarily low in saturation
	 * @param img             image to check (rgb)
	 * @param rat             ratio of high to low saturation pixels of the image
	 * @param most_common_hue most common hue from the image
	 * @param msg             flag to determine if a message should be output
	 * @return                true if image is primarily low in saturation
	 */
	bool gray_image(const cv::Mat& img, double* rat, int* most_common_hue,
	                bool msg = true);

	/**
	 * @brief prepares data for processing
	 *        depending on the saturation of img image b
	 * @param compare_on_saturation flag to determine whether saturation channel
	 *                              should be primary
	 * @param im_a primary          primary channel image a
	 * @param im_b primary          primary channel image b
	 * @param sc_im_a secondary     secondary channel image a
	 * @param sc_im_b secondary     secondary channel image b
	 * @param hsv_ss_a              scalespace with hsv channels a
	 * @param hsv_ss_b              scalespace with hsv channels b
	 * @return                      index of primary channel
	 */
	int prepare_channels_for_processing(bool compare_on_saturation, cv::Mat& im_a,
	                                    cv::Mat& im_b, cv::Mat& sc_im_a,
	                                    cv::Mat& sc_im_b,
	                                    std::vector<matarr_3>& hsv_ss_a,
	                                    std::vector<matarr_3>& hsv_ss_b);
	/**
	 * @brief prepares data for processing
	 * @param compare_on_saturation flag to determine whether saturation channel
	 *                              should be primary
	 * @param im_a primary          primary image
	 * @param sc_im secondary       secondary image
	 * @param hsv_ss                scalespace with hsv channels
	 * @param prim                  pointer to primary channel
	 * @param sec                   pointer to secondary channel
	 * @return                      chosen primary channel
	 */
	static int prepare_channels_for_processing(bool compare_on_saturation,
	                                           cv::Mat& im_a, cv::Mat& sc_im,
	                                           std::vector<matarr_3>& hsv_ss,
	                                           cv::Mat* prim, cv::Mat* sec);

	/**
	 * @brief remove channels that are unwanted from
	 *        the scalespace
	 * @param ss      scalespace to remove channels from
	 * @param dst     scalespace of a single channel
	 * @param channel channel to extract
	 */
	static void get_one_channel_scalespace(std::vector<matarr_3>& ss,
	                                       vmat& dst, int channel);

	/**
	 * @brief create a scalespace for each channel of img images
	 * @param bgr_ss_a scalespace a is saved here
	 * @param bgr_ss_b scalespace b is saved here
	 * @param a_small  img BGR split image a
	 * @param b_small  img BGR split image b
	 */
	void populate_scalespace(std::vector<vmat>& bgr_ss_a,
	                         std::vector<vmat>& bgr_ss_b, matarr_3& a_small,
	                         matarr_3& b_small);

	/**
	 * @brief create a scalespace for each channel of img image
	 * @param bgr_ss scalespace is saved here
	 * @param small  img BGR split image
	 * @param mode   determines which scalespace generation method
	 *               should be used ('a' - exp, 'b' - halving)
	 */
	void populate_scalespace(std::vector<vmat>& bgr_ss, matarr_3& small,
	                         char mode);

	/**
	 * @brief checks if the last result is a sensible one
	 * @param res last result
	 */
	void sane_result(bool* res);

public:
	/**
	 * @brief performs the scalespace detection algorithm on two inputs
	 * @param frame_a input one
	 * @param frame_b input two
	 * @param a_ref   reference to original input a image
	 * @return        result of scalespace detection
	 */
	bool run_on_input(const cv::Mat& frame_a, const cv::Mat& frame_b, cv::Mat& a_ref);
	/**
	 * @brief sets the number of the frame that the algorithm is at
	 * @param fn new frame number
	 */
	inline void set_frame_n(int fn) {
		frame_num_ = fn;
	}
private:
	/**
	 * @brief attempts to open images and passes
	 *        them forward into processing
	 * @param path_a path to image a
	 * @param path_b path to image b
	 * @return       true if opening and processing was successful
	 */
	bool open_image(
	        const std::string& path_a,
	        const std::string& path_b);  // attempts to open an image to work with
	/**
	 * @brief attempts to open videos and passes each frame forward
	 *        into processing
	 * @param path_a path to video a
	 * @param path_b path to video b
	 * @return       true if opening and processing was successful
	 */
	bool open_video(
	        const std::string& path_a,
	        const std::string& path_b);  // attempts to open a video to work with
	/**
	 * @brief attempts to open a video and passes each frame forward
	 *        into processing
	 * @param path_a path to video
	 * @return       true if opening and processing was successful
	 */
	bool open_video(const std::string& path_a);

	/**
	 * @brief gets various metadata about provided video capture
	 * @param cap   video capture
	 * @param index index to specify which video this is (0 - 'a', 1 - 'b')
	 */
	void get_video_info(cv::VideoCapture& cap, int index);

	/**
	 * @brief pauses the processing of video inputs, for controls
	 *        refer to definition
	 */
	void pause();

	/**
	 * @brief check if video capture is okay
	 * @param cap     video capture to check
	 * @param path    path to video
	 * @param message message output in case of error
	 * @return        true if video capture is okay
	 */
	bool check_videocapture(cv::VideoCapture& cap, const std::string& path,
	                        std::string& message);
	/**
	 * @brief check if image is okay
	 * @param img     image to check
	 * @param path    path to image
	 * @param message message output in case of error
	 * @return        true if image is okay
	 */
	bool check_image(const cv::Mat& img, const std::string& path,
	                 std::string& message);

	/**
	 * @brief runs the detector
	 * @return true if detection was successful
	 */
	bool run_detector_scalespace_hs();
	/**
	 * @brief runs the detector in automatic img b
	 *        feed mode
	 * @return true if detection was successful
	 */
	bool run_detector_scalespace_hs_auto();
	/**
	 * @brief runs the detector in manual img a
	 *        and b feed mode
	 * @return true if detection was successful
	 */
	bool run_detector_scalespace_hs_manual();
	/**
	 * @brief handles the control around running the detector
	 *        including timing
	 * @param message set to true if messages should be output
	 */
	bool run_detector(bool message = true);

	/**
	 * @brief send messages to an output stream
	 * @param stream   receiving output stream
	 * @param "
"ine  set to true if messages should end with std::"
"
	 * @param messages vector of messages to send to output
	 */
	inline void std_out(std::ostream& stream, const bool endline,
	                    const std::vector<std::string>& messages) {
		if (DEBUG_OUTPUT__) {
			for (const auto& m : messages) {
				stream << m;
			}
			if (endline) {
				stream << rope::REN;
			}
		}
	}

	/**
	 * @brief send message to an output stream
	 * @param stream  receiving output stream
	 * @param "
"ine set to true if message should end with std::"
"
	 * @param message message to send to output
	 */
	template <typename T>
	void std_out(std::ostream& stream, const bool endline, const T& message) {
		if (DEBUG_OUTPUT__) {
			stream << message;
			if (endline) {
				stream << rope::REN;
			}
		}
	}

	/**
	 * @brief log the results
	 */
	void log();

	/**
	 * @brief check if histogram of weights has similar peaks
	 * @param m histogram of peaks
	 * @return  true if peaks of m are similar
	 */
	bool weights_have_similar_peaks(std::map<double, double>& m);
	/**
	 * @brief handles the control of processes
	 *        after scalespace search
	 * @param im_base_a        img image
	 * @param out              image to save results for visualization
	 * @param scale_count      counter of occurences of each scale
	 * @param weight_count     counter of occurences of each weight
	 * @param match_points     points where scalespace match was found
	 * @param match_count      number of matches
	 * @param bg_count         number of matches of bg
	 * @param color_count      number of colors
	 * @param hue_finds_in_sat number of matches based on hue in otherwise
	 *                         saturation-focused search
	 * @param n                number of image pixels
	 * @param ss_to_search_sz  size of searched scalespcae
	 * @param ss_to_detect_sz  size of detected scalespace
	 * @param bg_mask          flag if only background color was used
	 * @return                 result of rectangle search
	 */
	bool post_scalespace_search_processing(const cv::Mat& im_base_a, cv::Mat& out,
	                                       std::map<int, int>& scale_count,
	                                       std::map<double, double>& weight_count,
	                                       std::vector<WPoint3>& match_points,
	                                       uint match_count, uint bg_count, uint color_count,
	                                       cv::Mat& hue_finds_in_sat, int n,
	                                       int ss_to_search_sz,
	                                       int ss_to_detect_sz, bool only_bg);

	/**
	 * @brief iterates over the bank and prepares
	 *        each advertisement
	 */
	void prepare_advertisements_in_bank();
	/**
	 * @brief parses a line according to a format used to save
	 *        information about advertisement frames
	 * @param line line to be parsed
	 * @param ad   advertisement that the line belongs to
	 */
	void parseline(const std::string& line, advertisement* ad);

	/**
	 * @brief performs calculations for the given advertisement
	 *        that prepare it for scalespace comparison with a frame from the broadcast
	 * @param sat_comp        flag for whether the saturation channel is being used
	 * @param hsv_ss          scalespace with hsv channels
	 * @param ad              advertisement that is being processed
	 * @param most_common_hue most common hue in the current
	 *                        frame of the advertisement
	 * @param stream          stream to optionally output results to
	 * @param prim            pointer to primary channel
	 * @param sec             pointer to secondary channel
	 */
	template<typename output>
	void process_scalespace(bool sat_comp, std::vector<matarr_3>& hsv_ss,
	                               advertisement* ad, int most_common_hue,
	                               output* stream, cv::Mat* prim,
	                               cv::Mat* sec, int16_t excluded_hue) {
		cv::Mat im_a;
		cv::Mat sc_im;

		int channel = prepare_channels_for_processing(sat_comp, im_a, sc_im, hsv_ss, prim, sec);

		vmat ss_a;
		get_one_channel_scalespace(hsv_ss, ss_a, channel);

		// prepare vector clusters

		const int offset = (im_a.rows) / 2;
		const int inside_line_length = (im_a.cols) - 2 * offset;
		std::map<uchar, int> unique_colors;
		const std::vector<line_ss_value> ss_vector_counter =
		        get_values_on_line(false, ad->image(), ss_a, offset, inside_line_length, unique_colors, nullptr);

		ad->unique_colors().push_back(unique_colors.size());

		auto v_counts = utility::copy_most_common_from_cont(
		                             inside_line_length, ss_vector_counter, color_count_threshold_,
		                             range<uchar>(excluded_hue < 0 ? -1 : std::max(0, excluded_hue - hue_tolerance_ - 1),
		                                          std::min(255 / value_round_down_, excluded_hue + hue_tolerance_ + 2)),
		                             [](const line_ss_value& a, const line_ss_value& b){ return a.get_count() > b.get_count(); },
		                             USE_OUT_OF_RANGE_HUE_VALUES, OUT_OF_RANGE_WHITE, OUT_OF_RANGE_BLACK, at_least_vector_samples_);
		// vector_counts v_counts = utility::get_most_common_from_map(
		//                                   inside_line_length, ss_vector_counter, COLOR_COUNT_THRESHOLD,
		//                                   AT_LEAST_VECTOR_SAMPLES);

		std::vector<line_ss_value> filtered;
		for (auto& v : v_counts) {
			if (v.get_count() < 10 && filtered.size() > 4) {
				break;
			}
			filtered.push_back(v);
		}

		if (stream) {
			*stream << sat_comp << "~" << most_common_hue << "/" << unique_colors.size()
			        << "/" << line_ss_value::vector_as_string(filtered, false) << "\n";
		}

		//ad->vector_counts().push_back(filtered);
	}

	/**
	 * @brief checks whether the specified file exists and its
	 * @param path   path to file
	 * @param file   file stream
	 * @param func   predicate function to check with
	 * @param params parameters for checking function
	 * @return       true if file exists and checking function was successful
	 */
	template <typename predicate, typename... args>
	bool check_file_exist(std::filesystem::path& path, std::fstream* file,
	                      predicate func, args... params) {
		bool read = true;
		if (std::filesystem::exists(path)) {
			file->open(path, std::ios::in);
			read = func(file, params...);
			if (!read) {
				std::cout << lmagenta << "------ old file values do not match, " << std::flush;
				file->close();
			}
		} else {
			std::cout << "------ file not found, " << std::flush;
			read = false;
		}
		return read;
	}

	/**
	 * @brief checks if the algorithm output is the same for the
	 *        first line of given file
	 * @param file                     file stream
	 * @param ad                       advertisement that is being checked
	 * @param correct_colors_pre_ssgen flag for whether colors should be corrected
	 *                                 before generating scalespaces
	 * @param center                   flag for whether the advertisement is centerboard
	 * @param prim                     pointer to primary channel
	 * @param sec                      pointer to secondary channel
	 * @return                         true if first line of file matches algorithm output
	 */
	static bool check_first_line(std::fstream* file, scalespace_analyzer* ssd, advertisement* ad,
	                             bool correct_colors_pre_ssgen, bool center,
	                             cv::Mat* prim, cv::Mat* sec, int excluded_hue);

	/**
	 * @brief prepares a single advertisement frame
	 * @param ad                       advertisement that is being prepared
	 * @param correct_colors_pre_ssgen flag for whether colors should be corrected
	 *                                 before generating scalespaces
	 * @param prim                     pointer to primary channel
	 * @param sec                      pointer to secondary channel
	 * @param stream                   stream for optional output
	 * @param center                   flag for whether the advertisement is centerboard
	 * @param specified_sat_comp       determines whether saturation channel is primary
	 *                                 no matter the advertisement content
	 */
	template<typename output>
	static void prepare_ad_image_one(advertisement* ad, scalespace_analyzer* ssd,
	                                 bool correct_colors_pre_ssgen, cv::Mat* prim,
	                                 cv::Mat* sec, int excluded_hue,
	                                 output* output_stream = nullptr,
	                                 bool center = false,
	                                 bool specified_sat_comp = false) {
		matarr_3 bgr;

		cv::Mat img;
		ad->image().copyTo(img);

		double low_saturation_ratio = -1.0;
		int most_common_hue = -1;
		bool sat_comp;

		if (correct_colors_pre_ssgen) {
			cv::Mat hsv = utility::bgr_to_hsv(img, 1.0);
			matarr_3 hsvsplit = utility::mat_split(hsv);
			sat_comp = ssd->gray_image(hsvsplit, &low_saturation_ratio, &most_common_hue, false);

			if (center) {
				sat_comp = specified_sat_comp;
			}

			sat_comp = false;
			image_modification::squish_value_channel(sat_comp, hsv, 70, 55, 220, ssd->gray_threshold_sat_, 1, USE_OUT_OF_RANGE_HUE_VALUES, OUT_OF_RANGE_BLACK, OUT_OF_RANGE_WHITE);
			// utility::color_correction(sat_comp, hsv, false);
			img = utility::hsv_to_bgr(hsv);
		}

		split(img, bgr);
		matarr_3 small;

		std::vector<vmat> bgr_ss(3);

		utility::resize_im(bgr, small, ssd->resize_);

		cv::Mat small_m;

		merge(small.data(), 3, small_m);
		// show_im("smallm", small_m);
		// key_wait(0);

		ssd->populate_scalespace(bgr_ss, small, 'b');

		std::vector<matarr_3> hsv_ss = utility::bgr_ss_to_hsv(bgr_ss);

		if (!correct_colors_pre_ssgen) {
			sat_comp = ssd->gray_image(hsv_ss[0], &low_saturation_ratio, &most_common_hue, false);
			cc_for_auto_b_feed(sat_comp, hsv_ss, ssd->gray_threshold_sat_, ssd->gray_threshold_val_, false, false);
		}

		ad->sat().push_back(sat_comp);
		ad->most_common_hues().push_back(most_common_hue);

		if (!center) {
			ssd->process_scalespace(sat_comp, hsv_ss, ad, most_common_hue, output_stream, prim, sec, excluded_hue);
		} else {
			ssd->process_scalespace(sat_comp, hsv_ss, ad, most_common_hue, output_stream, prim, sec, excluded_hue);
		}
	}

	/**
	 * @brief NOT_IMPLEMENTED prepares a transition frame
	 *        that was created from two different advertisements
	 * @see   advertisement_bank::pairings_
	 * @param a      frame from advertisement a
	 * @param b      frame from advertisement b
	 * @param alpha  weight of advertisement a, for advertisement b
	 *               the weight is (1 - alpha)
	 * @param stream stream for optional output
	 */
	void prepare_ad_image_blend(cv::Mat& a, cv::Mat& b, double alpha,
	                            std::fstream* stream = nullptr);
	/**
	 * @brief prepares advertisement that is a still image
	 * @param video                    flag for whether the ad is a video
	 * @param file_good                flag to determine if a new file has to be created or an
	 *                                 existing one can be used
	 * @param path                     path to file
	 * @param ad                       advertisement to prepare
	 * @param center                   flag for whether the advertisement is centerboard
	 * @param correct_colors_pre_ssgen flag for whether colors should be corrected
	 *                                 before generating scalespaces
	 */
	void prepare_ad(bool video, bool file_good, std::filesystem::path& path,
	                std::fstream* file, advertisement* ad, bool center,
	                bool correct_colors_pre_ssgen);

	/**
	 * @brief saves current detector state to path
	 */
	void save_state(std::filesystem::path& path);

	/**
	 * @brief content controller to handle changing of advertisements as they
	 *        appear in content list
	 */
	std::unique_ptr<content_controller> cc_ = nullptr;

	/**
	 * @brief a value to divide max_for_scale by that gives nice results
	 */
	static constexpr double MIN_DIVISOR = 5.3;

	/**
	 * @brief gets the maximum number of match counts for a given scale
	 * @param im_width          width of image
	 * @param scale             given scale
	 * @param color_count       number of found colors
	 * @param samples_condition number of needed samples
	 * @return                  number of matches that can be found at a
	 *                          given scale before image gets disqualified
	 */
	inline uint max_for_scale(uint im_width, uint scale, const uint color_count = 1,
	                          const int samples_condition = 2) {
		return static_cast<uint>(
		            im_width * std::pow(2, scale) * 15.85 *
		            (static_cast<int>(color_count) >= samples_condition
		             ? static_cast<double>(color_count) / samples_condition + 0.5
		             : 1));
	}

	/**
	 * @brief gets the minimum number of match counts for a given scale
	 * @param im_width width of image
	 * @param scale    given scale
	 * @return         number of matches that have to be found at a given
	 *                 scale so the image does not get disqualified
	 */
	inline uint min_for_scale(uint im_width, uint scale) {
		return max_for_scale(im_width, scale) / MIN_DIVISOR;
	}

	/**
	 * @brief gets the minimum number of match counts for a given scale
	 * @param max maximum number of match counts for some scale
	 * @return    number of matches that have to be found at a given
	 *            scale so the image does not get disqualified
	 */
	inline uint min_for_scale(uint max) { return max / MIN_DIVISOR; }

	void fix_scale(int* scale, int total, int sz, int desired, int count_at_scale, int count_at_desired);

	int get_weighted_max(int im_width, std::map<int, int>& m,
	                     int* count_at_scale);
public:
	/**
	 * @brief checks if file is likely to be an image
	 * @param file openfilename that had a file chosen
	 * @return     true if file has a common image extension
	 *             (jpeg, jpg, tiff, tif, png, bmp)
	 */
	static bool is_image(open_file_name& file) {
		const std::string& path = file.path();

		std::string extension = path.substr(path.length() - 4);

		return type_from_extension(extension) == paths::file_type::IMAGE;
	}

	/**
	 * @brief assuming path_a and path_b only differ in one
	 *        character on the same spot, automatically find and replace the character to
	 *        get a valid path_b
	 * @param path_a  img path
	 * @return        path_b which varies from path_a only in having 'B'
	 *                instead of 'A' in the same position
	 */
	static std::string get_pathb_automatic(const std::string& path_a) {
		const std::string search_str = "capA";
		std::string path_b(path_a);
		const size_t found = path_a.find(search_str);
		if (found != std::string::npos) {
			path_b[found + 3] = 'B';
		}
		return path_b;
	}

	/**
	 * @brief assuming path_b and path_a only differ in one
	 *        character on the same spot, automatically find and replace the character to
	 *        get a valid path_a
	 * @param path_b  img path
	 * @return        path_a which varies from path_b only in having 'A' instead of 'B'
	 *                in the same position
	 */
	static std::string get_patha_automatic(const std::string& path_b) {
		const std::string search_str = "capB";
		std::string path_a(path_b);
		const size_t found = path_b.find(search_str);
		if (found != std::string::npos) {
			path_a[found + 3] = 'A';
		}
		return path_a;
	}

	scalespace_analyzer(int resize) : resize_(resize) {}

	scalespace_analyzer() = default;
	/**
	 * @brief begins the detector and its algorithm
	 * @param argc argument count from the command line
	 * @param argv arguments from the command line
	 * @return     status code, see implementation for details
	 */
	detector_status detector_start(int argc, char** argv);

	/**
	 * @brief adds a content controller to the detector,
	 *        this determines whether img b is fed automatically
	 * @param cc content controller to add to the detector
	 */
	inline void add_content_controller(std::unique_ptr<content_controller> cc) {
		cc_ = std::move(cc);
		prepare_advertisements_in_bank();
	}

	/**
	 * @brief sets hue for exclusion; this hue will not be considered as
	 *        matchable between the two inputs
	 * @param exclusion hue to exclude
	 */
	inline void set_exclusion(int exclusion) {
		excluded_hue_ = exclusion / value_round_down_;
	}

	/**
	 * @brief sets the content for the centerboard
	 * @param path_center path to centerboard content
	 * @return            status code
	 */
	detector_status set_centerboard(const std::filesystem::path& path_center);

	static void cc_images_a_b(bool compare_on_saturation,
	                          std::vector<matarr_3>& hsv_ss_a,
	                          std::vector<matarr_3>& hsv_ss_b,
	                          int gray_threshold_sat,
	                          int gray_threshold_val,
	                          bool use_out_of_range_values);
	static void cc_for_auto_b_feed(bool compare_on_saturation,
	                               std::vector<matarr_3>& hsv_ss,
	                               int gray_threshold_sat, int gray_threshold_val,
	                               bool use_out_of_range_values,
	                               bool display);
};
